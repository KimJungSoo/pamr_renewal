#pragma once

#include "util\CriticalSection.h"
#include "util\semaphore.h"
#include <map>
#include <thread>
#include <queue>

#define INFO_PROGRAM_START  0
#define INFO_PROGRAM_TERMINATED 1

#define INFO_MODULE_CREATE_SUCCEED	1000
#define INFO_MODULE_DELETE_SUCCEED	1001
#define INFO_MODULE_DELETE_FAILED	1002
#define INFO_CONNECT_SUCCEED		1003
#define INFO_RESET_SUCCEED			1004
#define INFO_DISCONNECT_SUCCEED		1005
// Path Plan information 1600~1699
#define INFO_PATH_PLAN_LOAD_NODEPOINT_SUCCEED	1600
#define INFO_PATH_PLAN_LOAD_NODEPOINT_FAILED	1601

#define WARN_INVALID_THREAD_PERIOD		2000
#define WARN_INVALID_DATA_CHECK_COUNT	2001
#define WARN_INVALID_COMIZOA_ID			2002
#define WARN_RESTRICTED_STATE			2003
#define WARN_INVALID_CONNECTION_TIMEOUT	2004
#define WARN_INVALID_DATA_TIMEOUT		2005
#define WARN_INVALID_PORTNO				2006
#define WARN_INVALID_BAUDRATE			2007
#define WARN_INVALID_ANGLE_RESOLUTION	2008
#define WARN_INVALID_MOTION_SPEED_SETTING	2009
#define WARN_PRECISEMOVE_INVALID_TARGET_LANDMARK_INDEX	2010

//*************** ERROR CODE ***************//
// Laser Scanner Error 4000~4099
#define ERROR_LASER_CONNECT_FAILED 4000
#define ERROR_LASER_DISCONNECT_FAILED 4001
#define ERROR_LASER_DATA_CHECK_FAILED 4002
#define ERROR_LASER_DATA_DELAYED 4003
#define ERROR_LASER_THREAD_DELAYED 4004

// Gyro Sensor Error 4100~4199
#define ERROR_GYRO_CONNECT_FAILED 4100
#define ERROR_GYRO_DISCONNECT_FAILED 4101
#define ERROR_GYRO_DATA_CHECK_FAILED 4102
#define ERROR_GYRO_DATA_DELAYED 4103
#define ERROR_GYRO_THREAD_DELAYED 4104

// Guide Sensor Error 4200~4299
#define ERROR_GUIDE_CONNECT_FAILED 4200
#define ERROR_GUIDE_DISCONNECT_FAILED 4201
#define ERROR_GUIDE_DATA_CHECK_FAILED 4202
#define ERROR_GUIDE_DATA_DELAYED 4203
#define ERROR_GUIDE_THREAD_DELAYED 4204


// Comizoa IO Sensor Error 4300~4399
#define ERROR_COMIZOAIO_CONNECT_FAILED 4300
#define ERROR_COMIZOAIO_DISCONNECT_FAILED 4301
#define ERROR_COMIZOAIO_DATA_CHECK_FAILED 4302
#define ERROR_COMIZOAIO_DATA_DELAYED 4303
#define ERROR_COMIZOAIO_THREAD_DELAYED 4304
#define ERROR_COMIZOAIO_DAEMON_IS_NOT_RUNNING	4305
#define ERROR_COMIZOAIO_DLL_LOAD_FAILED			4306
#define ERROR_COMIZOAIO_LOADING_DEVICE_FAILED	4307
#define ERROR_COMIZOAIO_IO_PIN_SETTING_FAILED	4308
#define ERROR_COMIZOAIO_NOT_MATCHED_PIN_NUMBER	4309
#define ERROR_COMIZOAIO_SEARCH_DEVICE_FAILED	4310

// Motion Card Error 4400~4499
#define ERROR_MOTION_CARD_CONNECT_FAILED 4400
#define ERROR_MOTION_CARD_DISCONNECT_FAILED 4401
#define ERROR_MOTION_CARD_DATA_CHECK_FAILED 4402
#define ERROR_MOTION_CARD_DATA_DELAYED 4403
#define ERROR_MOTION_CARD_THREAD_DELAYED 4404
#define ERROR_MOTION_CARD_INITIALIZATION_FAILED	4405
#define ERROR_MOTION_CARD_DLL_LOAD_FAILED		4406
#define ERROR_MOTION_CARD_LOADING_DEVICE_FAILED	4407
#define ERROR_MOTION_CARD_MOTION_STOP_FAILED	4408
#define ERROR_MOTION_CARD_MOTION_EMG_STOP_FAILED 4409
#define ERROR_MOTION_CARD_SERVO_ON_FAILED		4410
#define ERROR_MOTION_CARD_SERVO_OFF_FAILED		4411
#define ERROR_MOTION_CARD_SET_POSITION_FAILED	4412
#define ERROR_MOTION_CARD_RESET_POSITION_FAILED	4413
#define ERROR_MOTION_CARD_LIFT_HOMING_FAILED	4414
#define ERROR_MOTION_CARD_LIFT_UPDOWN_FAILED	4415

// Motion Controller Error 4500~4599
#define ERROR_MOTIONCTR_INITIALIZE_FAILED 4500

// Path Plan Error 4600~4699
#define ERROR_NO_NODE_FILE			4600
#define ERROR_NODE_NUMBER_RANGE		4601
#define ERROR_NO_PATH_FILE			4602
#define ERROR_PATH_NODE_COUNT		4603
#define ERROR_CAN_NOT_FIND_NODE		4604
#define ERROR_PATH_NODE_IS_EMPTY	4605

// File read failed 4700~4799
#define ERROR_EVENT_MANAGER_INIFILE_READ_FAILED 4700
#define ERROR_NO_LANDMARK_FILE	4701

namespace eventManager {
	enum eMessageType {
		MSG_ERROR = 1,
		MSG_WARN,
		MSG_INFO,
		MSG_REPORT,
	};
	class CEventNode {
	public:
		CEventNode(eMessageType m, int e, bool bFileSave, bool bSendACS) {
			m_MsgType = m;
			m_nEventCode = e;
			m_bFileSave = bFileSave;
			m_bSendACS = bSendACS;
		}
		CEventNode(eMessageType m, int e, std::string s) {
			m_MsgType = m;
			m_nEventCode = e;
			m_strErrorMSG = s;
		}
		eMessageType m_MsgType;
		int m_nEventCode;
		bool m_bFileSave;
		bool m_bSendACS;
		std::string m_strErrorMSG;
	};

	class CEventManager
	{
	private:
		PINIReadWriter* PINIReaderWriter;
		CriticalSection m_cs;
		cSemaphore m_sema;
		std::queue<CEventNode*> m_tasks;
		std::thread m_thread;
		bool m_isThreadLoop;
		std::vector<std::vector<CEventNode>> m_ArrayList;
		std::map<int, std::string> m_EventCodeTable;

		void pushList(CEventNode*);
		int findNode(CEventNode*);
		CEventNode* PopTask();
		void Terminate();
		static int ThreadFunction(CEventManager *wqSemaphore);
		char* CStringToChar(CString&, char*, int);
		static void  WriteLog(int, char*, ...);

	public:
		CEventManager();
		virtual ~CEventManager();
		int PushTask(eMessageType, int, bool, bool);
		std::vector<std::vector<CEventNode>> getErrorArrayList();
		std::map<int, std::string> getEventCodeTable();
		void clearError();
		void clearError(std::string sensorName);
	};
}