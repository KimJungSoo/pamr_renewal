#include "stdafx.h"
#include "PAMR.h"
#include "StartPositionChange.h"
#include "PAMRDlg.h"
//#include "Operation.h"


IMPLEMENT_DYNAMIC(CStartPositionChange, CDialogEx)

CStartPositionChange::CStartPositionChange(CWnd* pParent /*=NULL*/)
	: CDialogEx(CStartPositionChange::IDD, pParent)
{

}

CStartPositionChange::~CStartPositionChange()
{
////////////////////////////////////////////////////////////////////////////////////////////////////
	// Progress Ctrl
	if(NULL != m_MyProgressCtrl)
	{
		m_MyProgressCtrl->Stop();

		m_MyProgressCtrl->DestroyWindow();
		delete m_MyProgressCtrl;
		m_MyProgressCtrl = NULL;

		SetFocus();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////
}

void CStartPositionChange::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_1, m_btStartPosition1);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_2, m_btStartPosition2);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_3, m_btStartPosition3);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_4, m_btStartPosition4);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_5, m_btStartPosition5);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_6, m_btStartPosition6);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_7, m_btStartPosition7);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_8, m_btStartPosition8);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_9, m_btStartPosition9);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_10, m_btStartPosition10);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_11, m_btStartPosition11);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_12, m_btStartPosition12);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_13, m_btStartPosition13);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_14, m_btStartPosition14);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_15, m_btStartPosition15);
	DDX_Control(pDX, IDC_BUTTON_START_POSITION_16, m_btStartPosition16);
}


BEGIN_MESSAGE_MAP(CStartPositionChange, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_1, &CStartPositionChange::OnBnClickedButtonStartPosition1)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_2, &CStartPositionChange::OnBnClickedButtonStartPosition2)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_3, &CStartPositionChange::OnBnClickedButtonStartPosition3)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_4, &CStartPositionChange::OnBnClickedButtonStartPosition4)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_5, &CStartPositionChange::OnBnClickedButtonStartPosition5)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_6, &CStartPositionChange::OnBnClickedButtonStartPosition6)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_7, &CStartPositionChange::OnBnClickedButtonStartPosition7)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_8, &CStartPositionChange::OnBnClickedButtonStartPosition8)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_9, &CStartPositionChange::OnBnClickedButtonStartPosition9)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_10, &CStartPositionChange::OnBnClickedButtonStartPosition10)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_11, &CStartPositionChange::OnBnClickedButtonStartPosition11)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_12, &CStartPositionChange::OnBnClickedButtonStartPosition12)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_13, &CStartPositionChange::OnBnClickedButtonStartPosition13)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_14, &CStartPositionChange::OnBnClickedButtonStartPosition14)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_15, &CStartPositionChange::OnBnClickedButtonStartPosition15)
	ON_BN_CLICKED(IDC_BUTTON_START_POSITION_16, &CStartPositionChange::OnBnClickedButtonStartPosition16)
	ON_BN_CLICKED(IDCANCEL, &CStartPositionChange::OnBnClickedCancel)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CStartPositionChange 메시지 처리기입니다.

BOOL CStartPositionChange::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Progress Ctrl Setting
	m_MyProgressCtrl = new CMyProgressCtrl;
	m_MyProgressCtrl->Create(CMyProgressCtrl::IDD, this);

	CString strDialogTitle;
	strDialogTitle.Format(_T("StartPositionChange 초기화 중입니다."));
	m_MyProgressCtrl->Set(strDialogTitle, 0, 100, 1, true);
	m_MyProgressCtrl->m_strProgressStatus.Format(_T("StartPositionChange 초기화 중입니다."));
	////////////////////////////////////////////////////////////////////////////////////////////////////
	m_MyProgressCtrl->SetProgressPos(10);		// %
	m_MyProgressCtrl->SetProgressStatus(_T("StartPositionChange 초기화 중입니다."));
	////////////////////////////////////////////////////////////////////////////////////////////////////

	InitStartPositionChange();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CStartPositionChange::OnBnClickedButtonStartPosition1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[0];
	m_nSelectPositionDirection = m_nButton_PositionDirection[0];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition2()
{
	// TODO: Add your control notification handler code here
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[1];
	m_nSelectPositionDirection = m_nButton_PositionDirection[1];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition3()
{
	// TODO: Add your control notification handler code here
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[2];
	m_nSelectPositionDirection = m_nButton_PositionDirection[2];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition4()
{
	// TODO: Add your control notification handler code here
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[3];
	m_nSelectPositionDirection = m_nButton_PositionDirection[3];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition5()
{
	// TODO: Add your control notification handler code here
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[4];
	m_nSelectPositionDirection = m_nButton_PositionDirection[4];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition6()
{
	// TODO: Add your control notification handler code here
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[5];
	m_nSelectPositionDirection = m_nButton_PositionDirection[5];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition7()
{
	// TODO: Add your control notification handler code here
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[6];
	m_nSelectPositionDirection = m_nButton_PositionDirection[6];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition8()
{
	// TODO: Add your control notification handler code here
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[7];
	m_nSelectPositionDirection = m_nButton_PositionDirection[7];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition9()
{
	// TODO: Add your control notification handler code here
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[8];
	m_nSelectPositionDirection = m_nButton_PositionDirection[8];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition10()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[9];
	m_nSelectPositionDirection = m_nButton_PositionDirection[9];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition11()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[10];
	m_nSelectPositionDirection = m_nButton_PositionDirection[10];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition12()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[11];
	m_nSelectPositionDirection = m_nButton_PositionDirection[11];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition13()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[12];
	m_nSelectPositionDirection = m_nButton_PositionDirection[12];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition14()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[13];
	m_nSelectPositionDirection = m_nButton_PositionDirection[13];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition15()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[14];
	m_nSelectPositionDirection = m_nButton_PositionDirection[14];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedButtonStartPosition16()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectStartPositionNodeNo = m_nButton_PositionNodeNo[15];
	m_nSelectPositionDirection = m_nButton_PositionDirection[15];

	char szMsg[1000] = {0};
	CString strMsg;
	strMsg.Format(_T("Start Position Changed : [%d]Position"), m_nSelectStartPositionNodeNo);
	CStringToChar(strMsg, szMsg, 1000);
	WriteLog(0x10, "0x10\t%s\n", szMsg);

	OnBnClickedCancel();
}


void CStartPositionChange::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

////////////////////////////////////////////////////////////////////////////////////////////////////
	// Progress Ctrl Stop
	if(NULL != m_MyProgressCtrl)
	{
		m_MyProgressCtrl->Stop();

		m_MyProgressCtrl->DestroyWindow();
		delete m_MyProgressCtrl;
		m_MyProgressCtrl = NULL;

		SetFocus();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef MODAL_DIALOG_START_POSITION_CHANGE
	m_pOperation->m_bStartPositionChangeViewed = false;
#endif

	CDialogEx::OnCancel();
}


void CStartPositionChange::OnClose()
{
	// TODO: Add your message handler code here and/or call default

////////////////////////////////////////////////////////////////////////////////////////////////////
	// Progress Ctrl Stop
	if(NULL != m_MyProgressCtrl)
	{
		m_MyProgressCtrl->Stop();

		m_MyProgressCtrl->DestroyWindow();
		delete m_MyProgressCtrl;
		m_MyProgressCtrl = NULL;

		SetFocus();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef MODAL_DIALOG_START_POSITION_CHANGE
	m_pOperation->m_bStartPositionChangeViewed = false;
#endif

	CDialogEx::OnClose();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CStartPositionChange::InitStartPositionChange()
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Progress Ctrl Start
	m_MyProgressCtrl->ShowWindow(SW_SHOW);
	m_MyProgressCtrl->Start();
	////////////////////////////////////////////////////////////////////////////////////////////////////
	m_MyProgressCtrl->SetProgressPos(50);		// %
	m_MyProgressCtrl->SetProgressStatus(_T("설정된 시작 위치 값을 불러오는 중입니다."));
	////////////////////////////////////////////////////////////////////////////////////////////////////

	Initialize_Button_Position();

	Wait(100);

	m_nButtonCount_Position = 0;
	m_nSelectStartPositionNodeNo = 0;	// 0 : 선택안함, 1 ~ N : 선택한 NodeNo

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Progress Ctrl Stop
	if(NULL != m_MyProgressCtrl)
	{
		m_MyProgressCtrl->Stop();

		m_MyProgressCtrl->DestroyWindow();
		delete m_MyProgressCtrl;
		m_MyProgressCtrl = NULL;

		SetFocus();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////

	return TRUE;
}


void CStartPositionChange::InitDialog()
{
	// => Dialog 초기화 해야 할 내용을 넣음

	return;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void CStartPositionChange::Initialize_Button_Position()
{
	ifstream fin;
	char inputString[100];
	char* type;
	char* token;
	int i;

	CString pathFileName = _T("");
	pathFileName.Format(_T("./data/ButtonPosition.txt"));

	fin.open(pathFileName);

	int nNo = 0;
	CString strName = _T("");
	CString strDescription = _T("");
	CString strNote = _T("");
	int nNodeNo = 0;
	int nDirection = 0;		// 0 : 순방향, 1 : 역방향

	fin.getline(inputString, 100);
	m_nButtonCount_Position = atoi(inputString);

	if(m_nButtonCount_Position > MAX_BUTTON_COUNT_START_POSITION)
		m_nButtonCount_Position = MAX_BUTTON_COUNT_START_POSITION;

	//pjs//TRACE("Button Count - Position : %d", m_nButtonCount_Position);

	int nCount;
	for(nCount = 0; nCount < m_nButtonCount_Position; nCount++)
	{
		fin.getline(inputString, 100);

		token = strtok_s(inputString, ",", &type);
		nNo = atoi(token);

		token = strtok_s(NULL, ",", &type);
		strName = CharToCString(token);

		token = strtok_s(NULL, ",", &type);
		strDescription = CharToCString(token);

		token = strtok_s(NULL, ",", &type);
		strNote = CharToCString(token);

		token = strtok_s(NULL, ",", &type);
		nNodeNo = atoi(token);

		token = strtok_s(NULL, ",", &type);
		nDirection = atoi(token);

		m_ButtonText_Position[nCount].nNo = nNo;
		m_ButtonText_Position[nCount].strName = strName;
		m_ButtonText_Position[nCount].strDescription = strDescription;
		m_ButtonText_Position[nCount].strNote = strNote;

		m_nButton_PositionNodeNo[nCount] = nNodeNo;
		m_nButton_PositionDirection[nCount] = nDirection;		// 0 : 순방향, 1 : 역방향
	}
	fin.close();

	if(nCount != m_nButtonCount_Position)
	{
		//pjs//TRACE("Not valid Button Count - Position : %d, %d\n", m_nButtonCount_Position, nCount);
	}
	else
	{
		//pjs//TRACE("Button - Position Text Read Sucess\n\n%d\n", m_nButtonCount_Position);
		for(i = 0; i < m_nButtonCount_Position; i++)
		{
			//pjs//TRACE("%d, %s, %s, %s\n", m_ButtonText_Position[i].nNo, m_ButtonText_Position[i].strName, m_ButtonText_Position[i].strDescription, m_ButtonText_Position[i].strNote);
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	if(m_nButtonCount_Position >= 1)
	{
		m_btStartPosition1.ShowWindow(true);

		int nIndex = 0;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition1.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition1.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 2)
	{
		m_btStartPosition2.ShowWindow(true);

		int nIndex = 1;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition2.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition2.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 3)
	{
		m_btStartPosition3.ShowWindow(true);

		int nIndex = 2;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition3.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition3.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 4)
	{
		m_btStartPosition4.ShowWindow(true);

		int nIndex = 3;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition4.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition4.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 5)
	{
		m_btStartPosition5.ShowWindow(true);

		int nIndex = 4;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition5.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition5.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 6)
	{
		m_btStartPosition6.ShowWindow(true);

		int nIndex = 5;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition6.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition6.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 7)
	{
		m_btStartPosition7.ShowWindow(true);

		int nIndex = 6;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition7.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition7.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 8)
	{
		m_btStartPosition8.ShowWindow(true);

		int nIndex = 7;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition8.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition8.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 9)
	{
		m_btStartPosition9.ShowWindow(true);

		int nIndex = 8;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition9.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition9.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 10)
	{
		m_btStartPosition10.ShowWindow(true);

		int nIndex = 9;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition10.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition10.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 11)
	{
		m_btStartPosition11.ShowWindow(true);

		int nIndex = 10;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition11.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition11.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 12)
	{
		m_btStartPosition12.ShowWindow(true);

		int nIndex = 11;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition12.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition12.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 13)
	{
		m_btStartPosition13.ShowWindow(true);

		int nIndex = 12;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition13.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition13.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 14)
	{
		m_btStartPosition14.ShowWindow(true);

		int nIndex = 13;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition14.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition14.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 15)
	{
		m_btStartPosition15.ShowWindow(true);

		int nIndex = 14;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition15.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition15.ShowWindow(false);
	}

	if(m_nButtonCount_Position >= 16)
	{
		m_btStartPosition15.ShowWindow(true);

		int nIndex = 15;
		CString strTemp = m_ButtonText_Position[nIndex].strName + _T("\n(") + m_ButtonText_Position[nIndex].strDescription + _T(")\n") + m_ButtonText_Position[nIndex].strNote;
		m_btStartPosition16.SetWindowTextW(strTemp);
	}
	else
	{
		m_btStartPosition16.ShowWindow(false);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////

