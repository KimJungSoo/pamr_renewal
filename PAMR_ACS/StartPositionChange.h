#pragma once

#define MAX_BUTTON_COUNT_START_POSITION		16

////////////////////////////////////////////////////////////////////////////////////////////////////
#include "MyProgressCtrl.h"
////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct _tag_ButtonText_
{
	int nNo;
	CString strName;
	CString strDescription;
	CString strNote;
} ButtonText_;

class COperation;

class CStartPositionChange : public CDialogEx
{
	DECLARE_DYNAMIC(CStartPositionChange)

public:
	CStartPositionChange(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CStartPositionChange();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_START_POSITION_CHANGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonStartPosition1();
	afx_msg void OnBnClickedButtonStartPosition2();
	afx_msg void OnBnClickedButtonStartPosition3();
	afx_msg void OnBnClickedButtonStartPosition4();
	afx_msg void OnBnClickedButtonStartPosition5();
	afx_msg void OnBnClickedButtonStartPosition6();
	afx_msg void OnBnClickedButtonStartPosition7();
	afx_msg void OnBnClickedButtonStartPosition8();
	afx_msg void OnBnClickedButtonStartPosition9();
	afx_msg void OnBnClickedButtonStartPosition10();
	afx_msg void OnBnClickedButtonStartPosition11();
	afx_msg void OnBnClickedButtonStartPosition12();
	afx_msg void OnBnClickedButtonStartPosition13();
	afx_msg void OnBnClickedButtonStartPosition14();
	afx_msg void OnBnClickedButtonStartPosition15();
	afx_msg void OnBnClickedButtonStartPosition16();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnClose();

////////////////////////////////////////////////////////////////////////////////////////////////////

	CMyProgressCtrl *m_MyProgressCtrl;

////////////////////////////////////////////////////////////////////////////////////////////////////
		
	CButton m_btStartPosition1;
	CButton m_btStartPosition2;
	CButton m_btStartPosition3;
	CButton m_btStartPosition4;
	CButton m_btStartPosition5;
	CButton m_btStartPosition6;
	CButton m_btStartPosition7;
	CButton m_btStartPosition8;
	CButton m_btStartPosition9;
	CButton m_btStartPosition10;
	CButton m_btStartPosition11;
	CButton m_btStartPosition12;
	CButton m_btStartPosition13;
	CButton m_btStartPosition14;
	CButton m_btStartPosition15;
	CButton m_btStartPosition16;

////////////////////////////////////////////////////////////////////////////////////////////////////

	COperation *m_pOperation;

	BOOL InitStartPositionChange();
	void InitDialog();

////////////////////////////////////////////////////////////////////////////////////////////////////

	void CStartPositionChange::Initialize_Button_Position();
	int m_nButtonCount_Position;
	ButtonText_ m_ButtonText_Position[MAX_BUTTON_COUNT_START_POSITION];
	int m_nButton_PositionNodeNo[MAX_BUTTON_COUNT_START_POSITION];
	int m_nButton_PositionDirection[MAX_BUTTON_COUNT_START_POSITION];			// 0 : 순방향, 1 : 역방향

	int m_nSelectStartPositionNodeNo;	// 0 : 선택안함, 1 ~ N : 선택한 NodeNo
	int m_nSelectPositionDirection;		// 0 : 순방향, 1 : 역방향

////////////////////////////////////////////////////////////////////////////////////////////////////

};
