#include "stdafx.h"
#include "RobotParameter.h"

#define INI_FILE_PATH	"C:\\potenit\\ini\\PotenitAMR.ini"
//#define INI_FILE_PATH	"PotenitAMR.ini"
#define INI_FILE_PATH_WSTR  _T(INI_FILE_PATH)

RobotParameter::RobotParameter()
{
	initialize();
}

RobotParameter::~RobotParameter()
{
	
	delete PINIReaderWriter;
}

bool RobotParameter::initialize()
{	
	string strInIFileName = INI_FILE_PATH;


	PINIReaderWriter = DEBUG_NEW PINIReadWriter(strInIFileName);	//설정파일 읽기

	if(PINIReaderWriter->ParseError() < 0)			//파일을 읽지 못한 경우 프로그램을 종료
	{
		//cout << "Can't load "<<strInIFileName<<endl;;
		TRACE("Can't load: %s\n",strInIFileName);
		//_getch();

		return false;
	}

	// Get [SERVER]
	m_strServerIP = PINIReaderWriter->getStringValue("SERVER", "IP", "no");
	m_nServerPort = PINIReaderWriter->getIntValue("SERVER", "PORT", 0);

	// Get [MAP]
	m_strMapNameNPath = PINIReaderWriter->getStringValue("MAP", "PATH&NAME", "no");
	m_nMapSizeXm = PINIReaderWriter->getIntValue("MAP", "MAP_SIZE_X", 0); // [m]
	m_nMapSizeYm = PINIReaderWriter->getIntValue("MAP", "MAP_SIZE_Y", 0); // [m]
	m_dMApOffset_X = PINIReaderWriter->getIntValue("MAP", "X_OFFSET", 0); // [m]
	m_dMApOffset_Y = PINIReaderWriter->getIntValue("MAP", "Y_OFFSET", 0); // [m]
	m_dHeight = PINIReaderWriter->getDoubleValue("MAP", "HEIGHT", 0.3);
	m_dSDThres = PINIReaderWriter->getDoubleValue("MAP", "STANDARD_DAVIATION_TH", 7.0);

	// Get [PATH]
	m_strNodePointNameNPath = PINIReaderWriter->getStringValue("PATH", "NODEPOINT_PATH&NAME", "no");
	m_strPathDataNameNPath = PINIReaderWriter->getStringValue("PATH", "PATHDATA_PATH&NAME", "no");
	m_strStationPointNameNPath = PINIReaderWriter->getStringValue("PATH", "STATIONPOINT_PATH&NAME", "no");
	m_strChargerPointNameNPath = PINIReaderWriter->getStringValue("PATH", "CHARGERPOINT_PATH&NAME", "no");
	m_strWorkPlacePointNameNPath = PINIReaderWriter->getStringValue("PATH", "WORKPLACEPOINT_PATH&NAME", "no");
	m_strLastRobotPoseNameNPath = PINIReaderWriter->getStringValue("PATH", "LASTROBOTPOSE_PATH&NAME", "no");			//pjs 추가 - 마지막 위치 저장 및 사용
	m_strInitRobotPoseNameNPath = PINIReaderWriter->getStringValue("PATH", "INITROBOTPOSE_PATH&NAME", "no");			//pjs 추가 - 로봇 위치 초기화 설정

	// Get [ROBOT]
	m_nRobotID = PINIReaderWriter->getIntValue("ROBOT", "ROBOT_ID", 0);
	m_strAMR_IP = PINIReaderWriter->getStringValue("ROBOT", "ROBOT_IP", "no");
	m_nAMR_Port = PINIReaderWriter->getIntValue("ROBOT", "ROBOT_PORT", 0);
	m_strAMR_WLAN_Name = PINIReaderWriter->getStringValue("ROBOT", "ROBOT_WLAN_NAME", "no");

	m_dInitialPositionX = PINIReaderWriter->getDoubleValue("ROBOT", "INITIAL_POSITION_X", 0.);
	m_dInitialPositionY = PINIReaderWriter->getDoubleValue("ROBOT", "INITIAL_POSITION_Y", 0.);
	m_dInitialPositionTh = PINIReaderWriter->getDoubleValue("ROBOT", "INITIAL_POSITION_TH", 0.);

	m_dRadiusofRobot = PINIReaderWriter->getDoubleValue("ROBOT", "ROBOT_RADIUS", 0.);
	m_dWheelBaseofRobot = PINIReaderWriter->getDoubleValue("ROBOT", "ROBOT_WHEEL_BASE", 0.);
	m_dGearRatio = PINIReaderWriter->getDoubleValue("ROBOT", "GEAR_RATIO", 0);
	m_nEncoderResolution = PINIReaderWriter->getIntValue("ROBOT", "ENCODER_RESOLUTION", 0);
	m_dWheelRadius = PINIReaderWriter->getDoubleValue("ROBOT", "WHEEL_RADIUS", 0.);

	m_dWeightAngleVelocity = PINIReaderWriter->getDoubleValue("ROBOT", "WEIGHT_ANGLE_VELOCITY", 1.0);
	m_dMaxRobotVelocity = PINIReaderWriter->getDoubleValue("ROBOT", "MAX_VELOCITY");
	m_dMinRobotVelocity = PINIReaderWriter->getDoubleValue("ROBOT", "MIN_VELOCITY");
	m_dMaxRobotAngleVelocity = PINIReaderWriter->getDoubleValue("ROBOT", "MAX_ANGLE_VELOCITY");
	m_dMaxRobotAutonomusVelocity = PINIReaderWriter->getDoubleValue("ROBOT", "MAX_VELOCITY_AUTONOMUS");			//pjs 추가 - 자율 주행 최대 속도를 설정, 단 MAX_VELOCITY을 넘을 수 없음
	m_dMaxRobotGuideVelocity = PINIReaderWriter->getDoubleValue("ROBOT", "MAX_VELOCITY_GUIDE");
	m_dMaxRobotGuideAngleVelocity = PINIReaderWriter->getDoubleValue("ROBOT", "MAX_ANGLE_VELOCITY_GUIDE", 650.0);
	m_dMaxRobotGuideVelocitySlow = PINIReaderWriter->getDoubleValue("ROBOT", "MAX_VELOCITY_GUIDE_SLOW");
	m_dMaxRobotGuideAngleVelocitySlow = PINIReaderWriter->getDoubleValue("ROBOT", "MAX_ANGLE_VELOCITY_GUIDE_SLOW");
	m_dRobotRotationAngleSpeed = PINIReaderWriter->getDoubleValue("ROBOT", "ROTATION_ANGLE_SPEED");
	m_nRobotStraightMoveSpeed = PINIReaderWriter->getIntValue("ROBOT", "STRAIGHT_MOVE_SPEED", 0);

	m_initRobotPosForMap.setX(PINIReaderWriter->getDoubleValue("ROBOT", "INIT_XPOSE_MAP", 0.0));
	m_initRobotPosForMap.setY(PINIReaderWriter->getDoubleValue("ROBOT", "INIT_YPOSE_MAP", 0.0));
	m_initRobotPosForMap.setThetaDeg(PINIReaderWriter->getDoubleValue("ROBOT", "INIT_THETADEG_MAP", 0.0));
	m_nUsingLastPose = PINIReaderWriter->getIntValue("ROBOT", "USING_LAST_POSE", 0);
	m_initRobotPosForNav.setX(PINIReaderWriter->getDoubleValue("ROBOT", "INIT_XPOSE_NAV", 0.0));
	m_initRobotPosForNav.setY(PINIReaderWriter->getDoubleValue("ROBOT", "INIT_YPOSE_NAV", 0.0));
	m_initRobotPosForNav.setThetaDeg(PINIReaderWriter->getDoubleValue("ROBOT", "INIT_THETADEG_NAV", 0.0));
	//m_strLastRobotPoseNameNPath = PINIReaderWriter->getStringValue("ROBOT", "LASTROBOTPOSE_PATH&NAME", "no");		//pjs 추가 - 마지막 위치 저장 및 사용
	m_strGlobalLocalization = PINIReaderWriter->getStringValue("ROBOT", "GLOBAL_LOCALIZATION", "no");
	m_nObstacleDetectionTime = PINIReaderWriter->getIntValue("ROBOT", "OBSTACLEDETECTION_TIME", 3);
	m_nLocalization = PINIReaderWriter->getIntValue("ROBOT", "LOCALIZATION", 0);

	m_nRunTime = PINIReaderWriter->getIntValue("ROBOT", "RUN_TIME", 0);
	m_nLoadingMode = PINIReaderWriter->getIntValue("ROBOT", "LOADING_MODE", 0);
	m_nFirstMuteOn = PINIReaderWriter->getIntValue("ROBOT", "FIRST_MUTE_ON", 0);

	//pjs 추가 - Battery Min, Max, ChargeRequired
	m_dBatteryVoltageMax = PINIReaderWriter->getDoubleValue("ROBOT", "BATTERY_VOLTAGE_MAX", 54.0);
	m_dBatteryVoltageMin = PINIReaderWriter->getDoubleValue("ROBOT", "BATTERY_VOLTAGE_MIN", 48.0);
	m_dChargeRequiredPersent = PINIReaderWriter->getDoubleValue("ROBOT", "CHARGE_REQUIRED_PERCENT", 10.0);
	//m_nUseAutoChargingStation = PINIReaderWriter->getIntValue("ROBOT", "USE_AUTO_CHARGING_STATION", 0);
	//m_dAutoChargingCompleteVoltage = PINIReaderWriter->getDoubleValue("ROBOT", "AUTO_CHARGING_COMPLETE_VOLTAGE", 26.50);
	//m_dChargingAlarmCompleteVoltage = PINIReaderWriter->getDoubleValue("ROBOT", "CHARGING_ALARM_COMPLETE_VOLTAGE", 24.50);

	m_nUseDevicePowerSeparationMode = PINIReaderWriter->getIntValue("ROBOT", "USE_DEVICE_POWER_SEPARATION_MODE", 0);
	m_nUseLowStandbyPowerMode = PINIReaderWriter->getIntValue("ROBOT", "USE_LOW_STANDBY_POWER_MODE", 0);

	m_nUseBreakMode = PINIReaderWriter->getIntValue("ROBOT", "USE_BREAK_MODE", 0);

	m_nUseReset_EmergencyStopMode = PINIReaderWriter->getIntValue("ROBOT", "USE_RESET_EMERGENCY_STOP_MODE", 1);		// 1 ; Reset 기능 / 0 : Pause 기능 => Default : Reset 기능

	m_dLift_Up_Limit = PINIReaderWriter->getDoubleValue("ROBOT", "LIFT_UP_LIMIT", 0.0);		
	m_dLift_Down_Limit = PINIReaderWriter->getDoubleValue("ROBOT", "LIFT_DOWN_LIMIT", 0.0);	
	m_nLiftMotorType = PINIReaderWriter->getIntValue("ROBOT", "LIFT_MOTOR_TYPE", 0);


	// Get [SENSOR]
	m_nGyroComPort = PINIReaderWriter->getIntValue("SENSOR", "GYRO_COM_PORT", 0);

	m_nGuideSensorCount = PINIReaderWriter->getIntValue("SENSOR", "GUIDE_SENSOR_COUNT", 0);
	m_nGuideComPort1 = PINIReaderWriter->getIntValue("SENSOR", "GUIDE_COM_PORT1", 0);
	m_nGuideComPort2 = PINIReaderWriter->getIntValue("SENSOR", "GUIDE_COM_PORT2", 0);
	//pjs 2019.03.08 GuideCan Port 설정
	m_nGuideCanPort = PINIReaderWriter->getIntValue("SENSOR", "GUIDE_CAN_PORT", 3);

	m_nGuideStopMarkSensorFront = PINIReaderWriter->getIntValue("SENSOR", "USE_GUIDE_STOP_MARK_SENSOR_FRONT", 0);
	m_nGuideStopMarkSensorRear = PINIReaderWriter->getIntValue("SENSOR", "USE_GUIDE_STOP_MARK_SENSOR_REAR", 0);
	m_nUseMatchDualGuideStopMarkSensor = PINIReaderWriter->getIntValue("SENSOR", "USE_MATCH_DUAL_GUIDE_STOP_MARK_SENSOR", 0);

	m_nMagneticGuideSensorXOffset = PINIReaderWriter->getIntValue("SENSOR", "MAGNETIC_GUIDE_SENSOR_XOFFSET", 0);
	m_nMagneticGuideSensorWidth = PINIReaderWriter->getIntValue("SENSOR", "MAGNETIC_GUIDE_SENSOR_WIDTH", 0);

	m_dMagneticGuideSensorMin = PINIReaderWriter->getDoubleValue("SENSOR", "MAGNETIC_GUIDE_SENSOR_MIN");
	m_dMagneticGuideSensorMax = PINIReaderWriter->getDoubleValue("SENSOR", "MAGNETIC_GUIDE_SENSOR_MAX");
	m_dMagneticGuideFindAngle = PINIReaderWriter->getDoubleValue("SENSOR", "MAGNETIC_GUIDE_FIND_ANGLE");

	m_nBatteryChecker = PINIReaderWriter->getIntValue("SENSOR", "BATTERY_CHECKER_COM_PORT", 0);

	m_nLaserScannerType = PINIReaderWriter->getIntValue("SENSOR", "LASER_SCANNER_TYPE", 0);
	m_nLaserScannerCount = PINIReaderWriter->getIntValue("SENSOR", "LASER_SCANNER_COUNT", 0);
	m_nLaserScannerHead = PINIReaderWriter->getIntValue("SENSOR", "LASER_SCANNER_HEAD", 0);

	m_strSICKLMSLaserIP_1 = PINIReaderWriter->getStringValue("SENSOR", "LASER_CONNECTION_IP_1", "no");
	m_nLaserTCPPort1 = PINIReaderWriter->getIntValue("SENSOR", "LASER_CONNECTION_PORT_1", 0);
	m_strSICKLMSLaserIP_2 = PINIReaderWriter->getStringValue("SENSOR", "LASER_CONNECTION_IP_2", "no");
	m_nLaserTCPPort2 = PINIReaderWriter->getIntValue("SENSOR", "LASER_CONNECTION_PORT_2", 0);
	
	m_nLaserMaxDist = PINIReaderWriter->getIntValue("SENSOR", "LASER_MAX_DISTANCE", 0);
	m_nLaserMinDist = PINIReaderWriter->getIntValue("SENSOR", "LASER_MIN_DISTANCE", 0); 


	m_nLaserHeight = PINIReaderWriter->getIntValue("SENSOR", "LASER_HEIGHT", 0);
	m_nLaserXOffset = PINIReaderWriter->getIntValue("SENSOR", "LASER_XOFFSET", 0);
	m_nLaserYOffset = PINIReaderWriter->getIntValue("SENSOR", "LASER_YOFFSET", 0); 

	m_nLaserHeight1 = PINIReaderWriter->getIntValue("SENSOR", "LASER1_HEIGHT", 0);
	m_nLaserXOffset1 = PINIReaderWriter->getIntValue("SENSOR", "LASER1_XOFFSET", 0);
	m_nLaserYOffset1 = PINIReaderWriter->getIntValue("SENSOR", "LASER1_YOFFSET", 0);

	m_nLaserHeight2 = PINIReaderWriter->getIntValue("SENSOR", "LASER2_HEIGHT", 0);
	m_nLaserXOffset2 = PINIReaderWriter->getIntValue("SENSOR", "LASER2_XOFFSET", 0);
	m_nLaserYOffset2 = PINIReaderWriter->getIntValue("SENSOR", "LASER2_YOFFSET", 0);


	m_nLaser_Installation_Angle_Offset1 = PINIReaderWriter->getIntValue("SENSOR", "LASER1_INSTALLATION_ANGLE_OFFSET", 0);
	m_nLaser_Start_Invalid_Count1 = PINIReaderWriter->getIntValue("SENSOR", "LASER1_START_INVALID_COUNT", 0);
	m_nLaser_End_Invalid_Count1 = PINIReaderWriter->getIntValue("SENSOR", "LASER1_END_INVALID_COUNT", 0);

	m_nLaser_Installation_Angle_Offset2 = PINIReaderWriter->getIntValue("SENSOR", "LASER2_INSTALLATION_ANGLE_OFFSET", 0);
	m_nLaser_Start_Invalid_Count2 = PINIReaderWriter->getIntValue("SENSOR", "LASER2_START_INVALID_COUNT", 0);
	m_nLaser_End_Invalid_Count2 = PINIReaderWriter->getIntValue("SENSOR", "LASER2_END_INVALID_COUNT", 0);

	m_dLaser_Angle_Step1 = PINIReaderWriter->getDoubleValue("SENSOR", "LASER1_ANGLE_STEP", 0.5);
	m_dLaser_Angle_Step2 = PINIReaderWriter->getDoubleValue("SENSOR", "LASER2_ANGLE_STEP", 0.5);

		m_initRobotPosForMap.setX(PINIReaderWriter->getDoubleValue("ROBOT", "INIT_XPOSE_MAP", 0.0));

	// Get [KANAYAMA_MOTION_CONTROL]	
	m_nDistToTarget = PINIReaderWriter->getIntValue("KANAYAMA_MOTION_CONTROL", "DISTANCE_TO_TARGET", 0);
	m_nDesiredVel = PINIReaderWriter->getIntValue("KANAYAMA_MOTION_CONTROL", "DESIRED_VELOCITY", 0); 
	m_nGoalArea = PINIReaderWriter->getIntValue("KANAYAMA_MOTION_CONTROL", "GOAL_AREA", 0);
	m_dKX = PINIReaderWriter->getDoubleValue("KANAYAMA_MOTION_CONTROL", "X_GAIN");
	m_dKY = PINIReaderWriter->getDoubleValue("KANAYAMA_MOTION_CONTROL", "Y_GAIN");
	m_dKT = PINIReaderWriter->getDoubleValue("KANAYAMA_MOTION_CONTROL", "T_GAIN");
	m_nDirectionofRotation = PINIReaderWriter->getIntValue("KANAYAMA_MOTION_CONTROL", "DIRECTION_OF_ROTATION", 0);	

	// Get [PARTICLE_FILTER]
	m_nMaxParticleNum = PINIReaderWriter->getIntValue("PARTICLE_FILTER", "MAX_SAMPLE_NUM", 3000);
	m_nMinParticleNum = PINIReaderWriter->getIntValue("PARTICLE_FILTER", "MIN_SAMPLE_NUM", 500); 
	m_dDevationforTrans = PINIReaderWriter->getDoubleValue("PARTICLE_FILTER", "DEVATION_TRANS");
	m_dDevationforRotate = PINIReaderWriter->getDoubleValue("PARTICLE_FILTER", "DEVATION_ROTATE");
	m_dDevationforTransRotate = PINIReaderWriter->getDoubleValue("PARTICLE_FILTER", "DEVATION_TRANSROTATE");

	m_Accum_Dist_Global = PINIReaderWriter->getDoubleValue("PARTICLE_FILTER", "ACCUM_DIST_GLOBAL", 300.0);
	m_Accum_Angle_Global = PINIReaderWriter->getDoubleValue("PARTICLE_FILTER", "ACCUM_ANGLE_GLOBAL", 10.0);
	m_Accum_Dist_Local = PINIReaderWriter->getDoubleValue("PARTICLE_FILTER", "ACCUM_DIST_LOCAL", 100.0);
	m_Accum_Angle_Local = PINIReaderWriter->getDoubleValue("PARTICLE_FILTER", "ACCUM_ANGLE_LOCAL", 3.0);

	// Get [MOTOR_DRIVER]
	m_nRobotMotorComport = PINIReaderWriter->getIntValue("MOTOR_DRIVER", "MOTOR_COMPORT", 0);
	m_nRoboCubeDriver = PINIReaderWriter->getIntValue("MOTOR_DRIVER", "ROBOCUBEDRIVER", 0);
	m_nMotorDirection = PINIReaderWriter->getIntValue("MOTOR_DRIVER", "DIRECTION", 0);

	// Get [DIGITAL_IO]
	m_nIo = PINIReaderWriter->getIntValue("DIGITAL_IO", "IO", 0);

	// Get [PHIDGETS]
	m_nQuantity = PINIReaderWriter->getIntValue("PHIDGETS", "QUANTITY", 0);
	m_PhidgetsSerialNumber1 = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1", 0);
	m_PhidgetsSerialNumber2 = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2", 0);

	// Phidget 정/역상 정보를 ini에서 불러옴(0 : Forward, 1: Reverse)

	m_nPhidget1InCh[0] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH00_IN", 0);
	m_nPhidget1InCh[1] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH01_IN", 0);
	m_nPhidget1InCh[2] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH02_IN", 0);
	m_nPhidget1InCh[3] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH03_IN", 0);
	m_nPhidget1InCh[4] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH04_IN", 0);
	m_nPhidget1InCh[5] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH05_IN", 0);
	m_nPhidget1InCh[6] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH06_IN", 0);
	m_nPhidget1InCh[7] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH07_IN", 0);
	m_nPhidget1InCh[8] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH08_IN", 0);
	m_nPhidget1InCh[9] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH09_IN", 0);
	m_nPhidget1InCh[10] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH10_IN", 0);
	m_nPhidget1InCh[11] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH11_IN", 0);
	m_nPhidget1InCh[12] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH12_IN", 0);
	m_nPhidget1InCh[13] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH13_IN", 0);
	m_nPhidget1InCh[14] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH14_IN", 0);
	m_nPhidget1InCh[15] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH15_IN", 0);

	m_nPhidget1OutCh[0] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH00_OUT", 0);
	m_nPhidget1OutCh[1] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH01_OUT", 0);
	m_nPhidget1OutCh[2] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH02_OUT", 0);
	m_nPhidget1OutCh[3] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH03_OUT", 0);
	m_nPhidget1OutCh[4] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH04_OUT", 0);
	m_nPhidget1OutCh[5] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH05_OUT", 0);
	m_nPhidget1OutCh[6] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH06_OUT", 0);
	m_nPhidget1OutCh[7] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH07_OUT", 0);
	m_nPhidget1OutCh[8] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH08_OUT", 0);
	m_nPhidget1OutCh[9] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH09_OUT", 0);
	m_nPhidget1OutCh[10] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH10_OUT", 0);
	m_nPhidget1OutCh[11] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH11_OUT", 0);
	m_nPhidget1OutCh[12] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH12_OUT", 0);
	m_nPhidget1OutCh[13] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH13_OUT", 0);
	m_nPhidget1OutCh[14] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH14_OUT", 0);
	m_nPhidget1OutCh[15] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS1_CH15_OUT", 0);

	m_nPhidget2InCh[0] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH00_IN", 0);
	m_nPhidget2InCh[1] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH01_IN", 0);
	m_nPhidget2InCh[2] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH02_IN", 0);
	m_nPhidget2InCh[3] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH03_IN", 0);
	m_nPhidget2InCh[4] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH04_IN", 0);
	m_nPhidget2InCh[5] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH05_IN", 0);
	m_nPhidget2InCh[6] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH06_IN", 0);
	m_nPhidget2InCh[7] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH07_IN", 0);
	m_nPhidget2InCh[8] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH08_IN", 0);
	m_nPhidget2InCh[9] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH09_IN", 0);
	m_nPhidget2InCh[10] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH10_IN", 0);
	m_nPhidget2InCh[11] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH11_IN", 0);
	m_nPhidget2InCh[12] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH12_IN", 0);
	m_nPhidget2InCh[13] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH13_IN", 0);
	m_nPhidget2InCh[14] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH14_IN", 0);
	m_nPhidget2InCh[15] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH15_IN", 0);

	m_nPhidget2OutCh[0] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH00_OUT", 0);
	m_nPhidget2OutCh[1] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH01_OUT", 0);
	m_nPhidget2OutCh[2] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH02_OUT", 0);
	m_nPhidget2OutCh[3] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH03_OUT", 0);
	m_nPhidget2OutCh[4] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH04_OUT", 0);
	m_nPhidget2OutCh[5] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH05_OUT", 0);
	m_nPhidget2OutCh[6] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH06_OUT", 0);
	m_nPhidget2OutCh[7] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH07_OUT", 0);
	m_nPhidget2OutCh[8] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH08_OUT", 0);
	m_nPhidget2OutCh[9] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH09_OUT", 0);
	m_nPhidget2OutCh[10] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH10_OUT", 0);
	m_nPhidget2OutCh[11] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH11_OUT", 0);
	m_nPhidget2OutCh[12] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH12_OUT", 0);
	m_nPhidget2OutCh[13] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH13_OUT", 0);
	m_nPhidget2OutCh[14] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH14_OUT", 0);
	m_nPhidget2OutCh[15] = PINIReaderWriter->getIntValue("PHIDGETS", "PHIDGETS2_CH15_OUT", 0);

	// Get [OBJECT_FOLLOW]
	m_nFindUseReflector = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "FIND_USE_REFLECTOR", 0);
	m_nFindCountInvalidValueReflector = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "FIND_COUNT_INVALID_VALUE_REFLECTOR", 0);
	m_nFindCountInvalidValueGap = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "FIND_COUNT_INVALID_VALUE_GAP", 0);
	m_dFindLimitPointGap = PINIReaderWriter->getDoubleValue("OBJECT_FOLLOW", "FIND_LIMIT_POINT_GAP");

	m_nFilteringLimitObjectWidthMax = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "FILTERING_LIMIT_OBJECT_WIDTH_MAX", 0);
	m_nFilteringLimitObjectWidthMin = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "FILTERING_LIMIT_OBJECT_WIDTH_MIN", 0);
	m_nFilteringTraceRange = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "FILTERING_TRACE_RANGE", 0);
	m_nFilteringLimitTraceTime = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "FILTERING_LIMIT_TRACE_TIME", 0);

	m_dTargetingWeightValueDistance = PINIReaderWriter->getDoubleValue("OBJECT_FOLLOW", "TARGETING_WEIGHT_VALUE_DISTANCE");
	m_dTargetingWeightValueThDeg = PINIReaderWriter->getDoubleValue("OBJECT_FOLLOW", "TARGETING_WEIGHT_VALUE_TH_DEG");
	m_nTargetingTergeting = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "TARGETING_TARGETING", 0);
	m_nTargetingLimitObjectDistanceInitial = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "TARGETING_LIMIT_OBJECT_DISTANCE_INITIAL", 0);
	m_nTargetingLimitTraceFailCount = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "TARGETING_LIMIT_TRACE_FAIL_COUNT", 0);
	m_nTargetTraceRange = PINIReaderWriter->getIntValue("OBJECT_FOLLOW", "TARGETING_TRACE_RANGE", 0);

	m_dMoveObjectFlollowMaxV = PINIReaderWriter->getDoubleValue("OBJECT_FOLLOW", "MOVE_OBJECT_FLOLLOW_MAX_V");
	m_dMoveFollowGap = PINIReaderWriter->getDoubleValue("OBJECT_FOLLOW", "MOVE_FOLLOW_GAP");
	m_dMoveMaxScanDistance = PINIReaderWriter->getDoubleValue("OBJECT_FOLLOW", "MOVE_MAX_SCAN_DISTANCE");
	m_dMoveRotateRange = PINIReaderWriter->getDoubleValue("OBJECT_FOLLOW", "MOVE_ONLY_ROTATE_RANGE");

	// Get [TUNNEL_MOVE]
	m_dTunnelFindScanLimiteDistance = PINIReaderWriter->getDoubleValue("TUNNEL_MOVE", "FIND_SCAN_LIMITE_DISTANCE");
	m_nTunnelFindLaserScanningObjectValidCount = PINIReaderWriter->getIntValue("TUNNEL_MOVE", "FIND_LASER_SCANNING_OBJECT_VALID_COUNT", 0);
	m_nTunnelFindCountInvalidValueGap = PINIReaderWriter->getIntValue("TUNNEL_MOVE", "FIND_COUNT_INVALID_VALUE_GAP", 0);
	m_dTunnelFindLimitPointGap = PINIReaderWriter->getDoubleValue("TUNNEL_MOVE", "FIND_LIMIT_POINT_GAP");

	m_nTunnelFilteringWheelToHusingGap = PINIReaderWriter->getIntValue("TUNNEL_MOVE", "FILTERING_WHEEL_TO_HOUSING_GAP", 0);
	m_nTunnelFilteringCartSafetyGap = PINIReaderWriter->getIntValue("TUNNEL_MOVE", "FILTERING_CART_SAFETY_GAP", 0);
	m_nTunnelFilteringObjectToSafetyGap = PINIReaderWriter->getIntValue("TUNNEL_MOVE", "FILTERING_OBJECT_TO_SAFETY_GAP", 0);

	m_dTunnelTergetingScanLimiteDistance = PINIReaderWriter->getDoubleValue("TUNNEL_MOVE", "TARGETING_SCAN_LIMITE_DISTANCE");
	m_nTunnelTergetingScanLimiteDegree = PINIReaderWriter->getIntValue("TUNNEL_MOVE", "TARGETING_SCAN_LIMITE_DEGREE", 0);
	m_dTunnelTergetingWeightValueDistance = PINIReaderWriter->getDoubleValue("TUNNEL_MOVE", "TARGETING_WEIGHT_VALUE_DISTANCE");
	m_dTunnelTergetingWeightValueThDeg = PINIReaderWriter->getDoubleValue("TUNNEL_MOVE", "TARGETING_WEIGHT_VALUE_TH_DEG");
	m_dTunnelTergetingWeightValueDirection = PINIReaderWriter->getDoubleValue("TUNNEL_MOVE", "TARGETING_WEIGHT_VALUE_DIRECTION");
	m_nTunnelTergetingLimitTraceFailCount = PINIReaderWriter->getIntValue("TUNNEL_MOVE", "TARGETING_LIMIT_TRACE_FAIL_COUNT", 0);

	// Get [PRECISION_POSITION_MOVE]
	m_dPrecisionPositionFindScanLimiteDistance = PINIReaderWriter->getDoubleValue("PRECISION_POSITION_MOVE", "FIND_SCAN_LIMITE_DISTANCE");
	m_nPrecisionPositionFindLaserScanningObjectValidCount = PINIReaderWriter->getIntValue("PRECISION_POSITION_MOVE", "FIND_LASER_SCANNING_OBJECT_VALID_COUNT", 0);
	m_nPrecisionPositionFindCountInvalidValueGap = PINIReaderWriter->getIntValue("PRECISION_POSITION_MOVE", "FIND_COUNT_INVALID_VALUE_GAP", 0);
	m_dPrecisionPositionFindLimitPointGap = PINIReaderWriter->getDoubleValue("PRECISION_POSITION_MOVE", "FIND_LIMIT_POINT_GAP");

	m_nPrecisionPositionFilteringWheelToHusingGap = PINIReaderWriter->getIntValue("PRECISION_POSITION_MOVE", "FILTERING_WHEEL_TO_HOUSING_GAP", 0);
	m_nPrecisionPositionFilteringCartSafetyGap = PINIReaderWriter->getIntValue("PRECISION_POSITION_MOVE", "FILTERING_CART_SAFETY_GAP", 0);
	m_nPrecisionPositionFilteringObjectToSafetyGap = PINIReaderWriter->getIntValue("PRECISION_POSITION_MOVE", "FILTERING_OBJECT_TO_SAFETY_GAP", 0);

	m_dPrecisionPositionTergetingScanLimiteDistance = PINIReaderWriter->getDoubleValue("PRECISION_POSITION_MOVE", "TARGETING_SCAN_LIMITE_DISTANCE");
	m_nPrecisionPositionTergetingScanLimiteDegree = PINIReaderWriter->getIntValue("PRECISION_POSITION_MOVE", "TARGETING_SCAN_LIMITE_DEGREE", 0);
	m_dPrecisionPositionTergetingWeightValueDistance = PINIReaderWriter->getDoubleValue("PRECISION_POSITION_MOVE", "TARGETING_WEIGHT_VALUE_DISTANCE");
	m_dPrecisionPositionTergetingWeightValueThDeg = PINIReaderWriter->getDoubleValue("PRECISION_POSITION_MOVE", "TARGETING_WEIGHT_VALUE_TH_DEG");
	m_dPrecisionPositionTergetingWeightValueDirection = PINIReaderWriter->getDoubleValue("PRECISION_POSITION_MOVE", "TARGETING_WEIGHT_VALUE_DIRECTION");
	m_nPrecisionPositionTergetingLimitTraceFailCount = PINIReaderWriter->getIntValue("PRECISION_POSITION_MOVE", "TARGETING_LIMIT_TRACE_FAIL_COUNT", 0);

	// Get [GUI_DISP]
	LRF1_DISP = PINIReaderWriter->getIntValue("GUI_DISP", "LRF1_DISP", 1);
	LRF2_DISP = PINIReaderWriter->getIntValue("GUI_DISP", "LRF2_DISP", 1);
	NODE_DISP = PINIReaderWriter->getIntValue("GUI_DISP", "NODE_DISP", 1);
	STATION_DISP = PINIReaderWriter->getIntValue("GUI_DISP", "STATION_DISP", 1);

	PATHDATA_DISP = PINIReaderWriter->getIntValue("GUI_DISP", "PATHDATA_DISP", 1);
	PARTICLE_DISP = PINIReaderWriter->getIntValue("GUI_DISP", "PARTICLE_DISP", 1);
	ROBOT_DISP = PINIReaderWriter->getIntValue("GUI_DISP", "ROBOT_DISP", 1);

	// [OBSTACLE_RANGE]					//pjs 추가 - ObstacleDetect ini 설정
	m_nObstacleRangeDisp = PINIReaderWriter->getIntValue("OBSTACLE_RANGE", "OBSTACLE_RANGE_DISP", 0);
	m_dObstacleStopDistanceMin = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_STOP_DISTANCE_MIN", 300.0);
	m_dObstacleStopDistanceMax = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_STOP_DISTANCE_MAX", 700.0);
	m_dObstacleWarnDistanceMax = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_WARN_DISTANCE_MAX", 1700.0);
	m_dObstacleStopWidthBasic = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_STOP_WIDTH_BASIC", 50.0);
	m_dObstacleStopWidthLoaded = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_STOP_WIDTH_LOADED", 150.0);


	//m_dObstacle_Stop_Distance_Min = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_STOP_DISTANCE_MIN", 700.0);
	//m_dObstacle_Stop_Distance_Max = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_STOP_DISTANCE_MAX", 800.0);
	//m_dObstacle_Stop_Width_Min = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_STOP_WIDTH_MIN", 700.0);
	//m_dObstacle_Stop_Width_Max = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_STOP_WIDTH_MAX", 800.0);

	//m_dObstacle_Warn_Distance_Min = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_WARN_DISTANCE_MIN", 800.0);
	//m_dObstacle_Warn_Distance_Max = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_WARN_DISTANCE_MAX", 2000.0);
	//m_dObstacle_Warn_Width_Min = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_WARN_WIDTH_MIN", 800.0);
	//m_dObstacle_Warn_Width_Max = PINIReaderWriter->getDoubleValue("OBSTACLE_RANGE", "OBSTACLE_WARN_WIDTH_MAX", 900.0);




	return true;
}

void RobotParameter::saveParameter()
{
	string strInIFileName = INI_FILE_PATH; 	//기본지도 이름은 kuns.ini파일에 명시되어 있다. 

	CString strTemp;

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Save [SERVER]
	strTemp = m_strServerIP.c_str();
	WritePrivateProfileString(_T("SERVER"), _T("IP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nServerPort);
	WritePrivateProfileString(_T("SERVER"), _T("PORT"), strTemp, INI_FILE_PATH_WSTR);

	// Save [MAP]
	strTemp = m_strMapNameNPath.c_str();
	WritePrivateProfileString(_T("MAP"), _T("PATH&NAME"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nMapSizeXm);
	WritePrivateProfileString(_T("MAP"), _T("MAP_SIZE_X"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nMapSizeYm);
	WritePrivateProfileString(_T("MAP"), _T("MAP_SIZE_Y"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_dMApOffset_X);
	WritePrivateProfileString(_T("MAP"), _T("X_OFFSET"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_dMApOffset_Y);
	WritePrivateProfileString(_T("MAP"), _T("Y_OFFSET"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.2f", m_dHeight);
	WritePrivateProfileString(_T("MAP"), _T("HEIGHT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.2f", m_dSDThres);
	WritePrivateProfileString(_T("MAP"), _T("STANDARD_DAVIATION_TH"), strTemp, INI_FILE_PATH_WSTR);

	// Save [PATH]
	strTemp = m_strNodePointNameNPath.c_str();
	WritePrivateProfileString(_T("PATH"), _T("NODEPOINT_PATH&NAME"), strTemp, INI_FILE_PATH_WSTR);
	strTemp = m_strPathDataNameNPath.c_str();
	WritePrivateProfileString(_T("PATH"), _T("PATHDATA_PATH&NAME"), strTemp, INI_FILE_PATH_WSTR);
	strTemp = m_strStationPointNameNPath.c_str();
	WritePrivateProfileString(_T("PATH"), _T("STATIONPOINT_PATH&NAME"), strTemp, INI_FILE_PATH_WSTR);
	strTemp = m_strChargerPointNameNPath.c_str();
	WritePrivateProfileString(_T("PATH"), _T("CHARGERPOINT_PATH&NAME"), strTemp, INI_FILE_PATH_WSTR);
	strTemp = m_strWorkPlacePointNameNPath.c_str();
	WritePrivateProfileString(_T("PATH"), _T("WORKPLACEPOINT_PATH&NAME"), strTemp, INI_FILE_PATH_WSTR);
	//pjs 추가 - 마지막 위치 저장 및 사용
	strTemp = m_strLastRobotPoseNameNPath.c_str();
	WritePrivateProfileString(_T("PATH"), _T("LASTROBOTPOSE_PATH&NAME"), strTemp, INI_FILE_PATH_WSTR);
	//pjs 추가 - 로봇 위치 초기화 설정
	strTemp = m_strInitRobotPoseNameNPath.c_str();
	WritePrivateProfileString(_T("PATH"), _T("INITROBOTPOSE_PATH&NAME"), strTemp, INI_FILE_PATH_WSTR);


	// Save [ROBOT]
	strTemp.Format(L"%d", m_nRobotID);
	WritePrivateProfileString(_T("ROBOT"), _T("ROBOT_ID"), strTemp, INI_FILE_PATH_WSTR);
	strTemp = m_strAMR_IP.c_str();
	WritePrivateProfileString(_T("ROBOT"), _T("ROBOT_IP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nAMR_Port);
	WritePrivateProfileString(_T("ROBOT"), _T("ROBOT_PORT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp = m_strAMR_WLAN_Name.c_str();
	WritePrivateProfileString(_T("ROBOT"), _T("ROBOT_WLAN_NAME"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%3.3f", m_dInitialPositionX);
	WritePrivateProfileString(_T("ROBOT"), _T("INITIAL_POSITION_X"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%3.3f", m_dInitialPositionY);
	WritePrivateProfileString(_T("ROBOT"), _T("INITIAL_POSITION_Y"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%3.3f", m_dInitialPositionTh);
	WritePrivateProfileString(_T("ROBOT"), _T("INITIAL_POSITION_TH"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%f", m_dRadiusofRobot);
	WritePrivateProfileString(_T("ROBOT"), _T("ROBOT_RADIUS"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%f", m_dWheelBaseofRobot);
	WritePrivateProfileString(_T("ROBOT"), _T("ROBOT_WHEEL_BASE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%3.2f", m_dGearRatio);
	WritePrivateProfileString(_T("ROBOT"), _T("GEAR_RATIO"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nEncoderResolution);
	WritePrivateProfileString(_T("ROBOT"), _T("ENCODER_RESOLUTION"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%f", m_dWheelRadius);
	WritePrivateProfileString(_T("ROBOT"), _T("WHEEL_RADIUS"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%4.2f", m_dWeightAngleVelocity);
	WritePrivateProfileString(_T("ROBOT"), _T("WEIGHT_ANGLE_VELOCITY"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dMaxRobotVelocity);
	WritePrivateProfileString(_T("ROBOT"), _T("MAX_VELOCITY"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dMinRobotVelocity);
	WritePrivateProfileString(_T("ROBOT"), _T("MIN_VELOCITY"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dMaxRobotAngleVelocity);
	WritePrivateProfileString(_T("ROBOT"), _T("MAX_ANGLE_VELOCITY"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dMaxRobotAutonomusVelocity);					//pjs 추가 - 자율 주행 최대 속도를 설정, 단 MAX_VELOCITY을 넘을 수 없음
	WritePrivateProfileString(_T("ROBOT"), _T("MAX_VELOCITY_AUTONOMUS"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dMaxRobotGuideVelocity);
	WritePrivateProfileString(_T("ROBOT"), _T("MAX_VELOCITY_GUIDE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dMaxRobotGuideAngleVelocity);
	WritePrivateProfileString(_T("ROBOT"), _T("MAX_ANGLE_VELOCITY_GUIDE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dMaxRobotGuideVelocitySlow);
	WritePrivateProfileString(_T("ROBOT"), _T("MAX_VELOCITY_GUIDE_SLOW"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dMaxRobotGuideAngleVelocitySlow);
	WritePrivateProfileString(_T("ROBOT"), _T("MAX_ANGLE_VELOCITY_GUIDE_SLOW"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dRobotRotationAngleSpeed);
	WritePrivateProfileString(_T("ROBOT"), _T("ROTATION_ANGLE_SPEED"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nRobotStraightMoveSpeed);
	WritePrivateProfileString(_T("ROBOT"), _T("STRAIGHT_MOVE_SPEED"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%f", m_initRobotPosForMap.getX());
	WritePrivateProfileString(_T("ROBOT"), _T("INIT_XPOSE_MAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.2f", m_initRobotPosForMap.getY());
	WritePrivateProfileString(_T("ROBOT"), _T("INIT_YPOSE_MAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.2f", m_initRobotPosForMap.getThetaDeg());
	WritePrivateProfileString(_T("ROBOT"), _T("INIT_THETADEG_MAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nUsingLastPose);
	WritePrivateProfileString(_T("ROBOT"), _T("USING_LAST_POSE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%f", m_initRobotPosForNav.getX());
	WritePrivateProfileString(_T("ROBOT"), _T("INIT_XPOSE_NAV"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.2f", m_initRobotPosForNav.getY());
	WritePrivateProfileString(_T("ROBOT"), _T("INIT_YPOSE_NAV"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.2f", m_initRobotPosForNav.getThetaDeg());
	WritePrivateProfileString(_T("ROBOT"), _T("INIT_THETADEG_NAV"), strTemp, INI_FILE_PATH_WSTR);
	//pjs 추가 - 마지막 위치 저장 및 사용
	//strTemp = m_strLastRobotPoseNameNPath.c_str();
	//WritePrivateProfileString(_T("ROBOT"), _T("LASTROBOTPOSE_PATH&NAME"), strTemp, INI_FILE_PATH_WSTR);
	strTemp = m_strGlobalLocalization.c_str();
	WritePrivateProfileString(_T("ROBOT"), _T("GLOBAL_LOCALIZATION"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nObstacleDetectionTime);
	WritePrivateProfileString(_T("ROBOT"), _T("OBSTACLEDETECTION_TIME"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nLocalization);
	WritePrivateProfileString(_T("ROBOT"), _T("LOCALIZATION"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nRunTime);
	WritePrivateProfileString(_T("ROBOT"), _T("RUN_TIME"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d\\t\\t// 0 : Nomal, 1 : Lift, 2 : Tow, 3 : Conveyor Belt", m_nLoadingMode);
	WritePrivateProfileString(_T("ROBOT"), _T("LOADING_MODE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nFirstMuteOn);
	WritePrivateProfileString(_T("ROBOT"), _T("FIRST_MUTE_ON"), strTemp, INI_FILE_PATH_WSTR);

	//pjs 추가 - Battery Min, Max, ChargeRequired
	strTemp.Format(L"%4.2f", m_dBatteryVoltageMax);
	WritePrivateProfileString(_T("ROBOT"), _T("BATTERY_VOLTAGE_MAX"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dBatteryVoltageMin);
	WritePrivateProfileString(_T("ROBOT"), _T("BATTERY_VOLTAGE_MIN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dChargeRequiredPersent);
	WritePrivateProfileString(_T("ROBOT"), _T("CHARGE_REQUIRED_PERCENT"), strTemp, INI_FILE_PATH_WSTR);
	//strTemp.Format(L"%d", m_nUseAutoChargingStation);
	//WritePrivateProfileString(_T("ROBOT"), _T("USE_AUTO_CHARGING_STATION"), strTemp, INI_FILE_PATH_WSTR);
	//strTemp.Format(L"%4.2f", m_dAutoChargingCompleteVoltage);
	//WritePrivateProfileString(_T("ROBOT"), _T("AUTO_CHARGING_COMPLETE_VOLTAGE"), strTemp, INI_FILE_PATH_WSTR);
	//strTemp.Format(L"%4.2f", m_dChargingAlarmCompleteVoltage);
	//WritePrivateProfileString(_T("ROBOT"), _T("CHARGING_ALARM_COMPLETE_VOLTAGE"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nUseDevicePowerSeparationMode);
	WritePrivateProfileString(_T("ROBOT"), _T("USE_DEVICE_POWER_SEPARATION_MODE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nUseLowStandbyPowerMode);
	WritePrivateProfileString(_T("ROBOT"), _T("USE_LOW_STANDBY_POWER_MODE"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nUseBreakMode);
	WritePrivateProfileString(_T("ROBOT"), _T("USE_BREAK_MODE"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nUseReset_EmergencyStopMode);
	WritePrivateProfileString(_T("ROBOT"), _T("USE_RESET_EMERGENCY_STOP_MODE"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_dLift_Up_Limit);
	WritePrivateProfileString(_T("ROBOT"), _T("LIFT_UP_LIMIT"), strTemp, INI_FILE_PATH_WSTR);


	strTemp.Format(L"%d", m_dLift_Down_Limit);
	WritePrivateProfileString(_T("ROBOT"), _T("LIFT_DOWN_LIMIT"), strTemp, INI_FILE_PATH_WSTR);

	//pjs 2019.03.18 Lift Motor 개수에 따라서 다르게 적용 //0 : Lift Motor Count 2, 1 : Lift Motor Count 1
	strTemp.Format(L"%d", m_nLiftMotorType);
	WritePrivateProfileString(_T("ROBOT"), _T("LIFT_MOTOR_TYPE"), strTemp, INI_FILE_PATH_WSTR);

	// Save [SENSOR]
	strTemp.Format(L"%d", m_nGyroComPort);
	WritePrivateProfileString(_T("SENSOR"), _T("GYRO_COM_PORT"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nGuideSensorCount);
	WritePrivateProfileString(_T("SENSOR"), _T("GUIDE_SENSOR_COUNT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nGuideComPort1);
	WritePrivateProfileString(_T("SENSOR"), _T("GUIDE_COM_PORT1"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nGuideComPort2);
	WritePrivateProfileString(_T("SENSOR"), _T("GUIDE_COM_PORT2"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nGuideStopMarkSensorFront);
	WritePrivateProfileString(_T("SENSOR"), _T("USE_GUIDE_STOP_MARK_SENSOR_FRONT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nGuideStopMarkSensorRear);
	WritePrivateProfileString(_T("SENSOR"), _T("USE_GUIDE_STOP_MARK_SENSOR_REAR"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nUseMatchDualGuideStopMarkSensor);
	WritePrivateProfileString(_T("SENSOR"), _T("USE_MATCH_DUAL_GUIDE_STOP_MARK_SENSOR"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nMagneticGuideSensorXOffset);
	WritePrivateProfileString(_T("SENSOR"), _T("MAGNETIC_GUIDE_SENSOR_XOFFSET"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nMagneticGuideSensorWidth);
	WritePrivateProfileString(_T("SENSOR"), _T("MAGNETIC_GUIDE_SENSOR_WIDTH"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%4.2f", m_dMagneticGuideSensorMin);
	WritePrivateProfileString(_T("SENSOR"), _T("MAGNETIC_GUIDE_SENSOR_MIN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dMagneticGuideSensorMax);
	WritePrivateProfileString(_T("SENSOR"), _T("MAGNETIC_GUIDE_SENSOR_MAX"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_dMagneticGuideFindAngle);
	WritePrivateProfileString(_T("SENSOR"), _T("MAGNETIC_GUIDE_FIND_ANGLE"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nBatteryChecker);
	WritePrivateProfileString(_T("SENSOR"), _T("BATTERY_CHECKER_COM_PORT"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nLaserScannerType);
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_SCANNER_TYPE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nLaserScannerCount);
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_SCANNER_COUNT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d\\t\\t// 0 = HEAD_TOP , 1 = HEAD_BOTTOM", m_nLaserScannerHead);
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_SCANNER_HEAD"), strTemp, INI_FILE_PATH_WSTR);

	strTemp = m_strSICKLMSLaserIP_1.c_str();
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_CONNECTION_IP_1"), strTemp, INI_FILE_PATH_WSTR);	
	strTemp.Format(L"%d", m_nLaserTCPPort1);
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_CONNECTION_PORT_1"), strTemp, INI_FILE_PATH_WSTR);
	strTemp = m_strSICKLMSLaserIP_2.c_str();
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_CONNECTION_IP_2"), strTemp, INI_FILE_PATH_WSTR);	
	strTemp.Format(L"%d", m_nLaserTCPPort2);
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_CONNECTION_PORT_2"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nLaserMaxDist);
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_MAX_DISTANCE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nLaserMinDist);
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_MIN_DISTANCE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nLaserHeight);
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_HEIGHT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nLaserXOffset);
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_XOFFSET"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nLaserYOffset);
	WritePrivateProfileString(_T("SENSOR"), _T("LASER_YOFFSET"), strTemp, INI_FILE_PATH_WSTR);

	// Save [KANAYAMA_MOTION_CONTROL]
	strTemp.Format(L"%d", m_nDistToTarget);
	WritePrivateProfileString(_T("KANAYAMA_MOTION_CONTROL"), _T("DISTANCE_TO_TARGET"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nDesiredVel);
	WritePrivateProfileString(_T("KANAYAMA_MOTION_CONTROL"), _T("DESIRED_VELOCITY"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nGoalArea);
	WritePrivateProfileString(_T("KANAYAMA_MOTION_CONTROL"), _T("GOAL_AREA"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.2f", m_dKX);
	WritePrivateProfileString(_T("KANAYAMA_MOTION_CONTROL"), _T("X_GAIN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.2f", m_dKY);
	WritePrivateProfileString(_T("KANAYAMA_MOTION_CONTROL"), _T("Y_GAIN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.2f", m_dKT);
	WritePrivateProfileString(_T("KANAYAMA_MOTION_CONTROL"), _T("T_GAIN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nDirectionofRotation);
	WritePrivateProfileString(_T("KANAYAMA_MOTION_CONTROL"), _T("DIRECTION_OF_ROTATION"), strTemp, INI_FILE_PATH_WSTR);

	// Save [PARTICLE_FILTER]
	strTemp.Format(L"%d", m_nMaxParticleNum);
	WritePrivateProfileString(_T("PARTICLE_FILTER"), _T("MAX_SAMPLE_NUM"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nMinParticleNum);
	WritePrivateProfileString(_T("PARTICLE_FILTER"), _T("MIN_SAMPLE_NUM"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.4f", m_dDevationforTrans);
	WritePrivateProfileString(_T("PARTICLE_FILTER"), _T("DEVATION_TRANS"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.4f", m_dDevationforRotate);
	WritePrivateProfileString(_T("PARTICLE_FILTER"), _T("DEVATION_ROTATE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%0.4f", m_dDevationforTransRotate);
	WritePrivateProfileString(_T("PARTICLE_FILTER"), _T("DEVATION_TRANSROTATE"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%4.2f", m_Accum_Dist_Global);
	WritePrivateProfileString(_T("PARTICLE_FILTER"), _T("ACCUM_DIST_GLOBAL"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_Accum_Angle_Global);
	WritePrivateProfileString(_T("PARTICLE_FILTER"), _T("ACCUM_ANGLE_GLOBAL"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_Accum_Dist_Local);
	WritePrivateProfileString(_T("PARTICLE_FILTER"), _T("ACCUM_DIST_LOCAL"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%4.2f", m_Accum_Angle_Local);
	WritePrivateProfileString(_T("PARTICLE_FILTER"), _T("ACCUM_ANGLE_LOCAL"), strTemp, INI_FILE_PATH_WSTR);

	// Save [MOTOR_DRIVER]
	strTemp.Format(L"%d", m_nRobotMotorComport);
	WritePrivateProfileString(_T("MOTOR_DRIVER"), _T("MOTOR_COMPORT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nRoboCubeDriver);
	WritePrivateProfileString(_T("MOTOR_DRIVER"), _T("ROBOCUBEDRIVER"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nMotorDirection);
	WritePrivateProfileString(_T("MOTOR_DRIVER"), _T("DIRECTION"), strTemp, INI_FILE_PATH_WSTR);

	// Save [DIGITAL_IO]
	strTemp.Format(L"%d", m_nIo);
	WritePrivateProfileString(_T("DIGITAL_IO"), _T("IO"), strTemp, INI_FILE_PATH_WSTR);

	// Save [PHIDGETS]
	strTemp.Format(L"%d", m_nQuantity);
	WritePrivateProfileString(_T("PHIDGETS"), _T("QUANTITY"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_PhidgetsSerialNumber1);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_PhidgetsSerialNumber2);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d			// 0 : Normal, 1: Reverse", m_nPhidget1InCh[0]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH00_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[1]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH01_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[2]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH02_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[3]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH03_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[4]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH04_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[5]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH05_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[6]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH06_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[7]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH07_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[8]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH08_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[9]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH09_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[10]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH10_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[11]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH11_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[12]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH12_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[13]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH13_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[14]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH14_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1InCh[15]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH15_IN"), strTemp, INI_FILE_PATH_WSTR);
	
	strTemp.Format(L"%d", m_nPhidget1OutCh[0]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH00_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[1]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH01_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[2]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH02_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[3]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH03_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[4]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH04_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[5]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH05_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[6]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH06_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[7]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH07_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[8]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH08_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[9]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH09_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[10]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH10_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[11]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH11_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[12]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH12_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[13]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH13_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[14]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH14_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget1OutCh[15]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS1_CH15_OUT"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nPhidget2InCh[0]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH00_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[1]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH01_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[2]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH02_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[3]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH03_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[4]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH04_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[5]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH05_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[6]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH06_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[7]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH07_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[8]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH08_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[9]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH09_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[10]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH10_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[11]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH11_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[12]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH12_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[13]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH13_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[14]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH14_IN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2InCh[15]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH15_IN"), strTemp, INI_FILE_PATH_WSTR);
	
	strTemp.Format(L"%d", m_nPhidget2OutCh[0]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH00_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[1]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH01_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[2]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH02_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[3]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH03_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[4]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH04_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[5]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH05_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[6]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH06_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[7]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH07_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[8]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH08_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[9]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH09_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[10]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH10_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[11]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH11_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[12]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH12_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[13]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH13_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[14]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH14_OUT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPhidget2OutCh[15]);
	WritePrivateProfileString(_T("PHIDGETS"), _T("PHIDGETS2_CH15_OUT"), strTemp, INI_FILE_PATH_WSTR);

	// Save [OBJECT_FOLLOW]
	strTemp.Format(L"%d", m_nFindUseReflector);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("FIND_USE_REFLECTOR"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nFindCountInvalidValueReflector);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("FIND_COUNT_INVALID_VALUE_REFLECTOR"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nFindCountInvalidValueGap);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("FIND_COUNT_INVALID_VALUE_GAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%3.2f", m_dFindLimitPointGap);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("FIND_LIMIT_POINT_GAP"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nFilteringLimitObjectWidthMax);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("FILTERING_LIMIT_OBJECT_WIDTH_MAX"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nFilteringLimitObjectWidthMin);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("FILTERING_LIMIT_OBJECT_WIDTH_MIN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nFilteringTraceRange);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("FILTERING_TRACE_RANGE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nFilteringLimitTraceTime);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("FILTERING_LIMIT_TRACE_TIME"), strTemp, INI_FILE_PATH_WSTR);
	
	strTemp.Format(L"%3.2f", m_dTargetingWeightValueDistance);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("TARGETING_WEIGHT_VALUE_DISTANCE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%3.2f", m_dTargetingWeightValueThDeg);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("TARGETING_WEIGHT_VALUE_TH_DEG"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nTargetingTergeting);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("TARGETING_TARGETING"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nTargetingLimitObjectDistanceInitial);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("TARGETING_LIMIT_OBJECT_DISTANCE_INITIAL"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nTargetingLimitTraceFailCount);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("TARGETING_LIMIT_TRACE_FAIL_COUNT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nTargetTraceRange);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("TARGETING_TRACE_RANGE"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%5.1f", m_dMoveObjectFlollowMaxV);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("MOVE_OBJECT_FLOLLOW_MAX_V"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dMoveFollowGap);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("MOVE_FOLLOW_GAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dMoveMaxScanDistance);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("MOVE_MAX_SCAN_DISTANCE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dMoveRotateRange);
	WritePrivateProfileString(_T("OBJECT_FOLLOW"), _T("MOVE_ONLY_ROTATE_RANGE"), strTemp, INI_FILE_PATH_WSTR);

	// Save [TUNNEL_MOVE]
	strTemp.Format(L"%5.1f", m_dTunnelFindScanLimiteDistance);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("FIND_SCAN_LIMITE_DISTANCE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nTunnelFindLaserScanningObjectValidCount);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("FIND_LASER_SCANNING_OBJECT_VALID_COUNT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nTunnelFindCountInvalidValueGap);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("FIND_COUNT_INVALID_VALUE_GAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dTunnelFindLimitPointGap);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("FIND_LIMIT_POINT_GAP"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nTunnelFilteringWheelToHusingGap);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("FILTERING_WHEEL_TO_HOUSING_GAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nTunnelFilteringCartSafetyGap);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("FILTERING_CART_SAFETY_GAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nTunnelFilteringObjectToSafetyGap);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("FILTERING_OBJECT_TO_SAFETY_GAP"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%5.1f", m_dTunnelTergetingScanLimiteDistance);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("TARGETING_SCAN_LIMITE_DISTANCE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nTunnelTergetingScanLimiteDegree);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("TARGETING_SCAN_LIMITE_DEGREE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dTunnelTergetingWeightValueDistance);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("TARGETING_WEIGHT_VALUE_DISTANCE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dTunnelTergetingWeightValueThDeg);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("TARGETING_WEIGHT_VALUE_TH_DEG"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dTunnelTergetingWeightValueDirection);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("TARGETING_WEIGHT_VALUE_DIRECTION"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nTunnelTergetingLimitTraceFailCount);
	WritePrivateProfileString(_T("TUNNEL_MOVE"), _T("TARGETING_LIMIT_TRACE_FAIL_COUNT"), strTemp, INI_FILE_PATH_WSTR);

	// Save [PRECISION_POSITION_MOVE]
	strTemp.Format(L"%5.1f", m_dPrecisionPositionFindScanLimiteDistance);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("FIND_SCAN_LIMITE_DISTANCE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPrecisionPositionFindLaserScanningObjectValidCount);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("FIND_LASER_SCANNING_OBJECT_VALID_COUNT"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPrecisionPositionFindCountInvalidValueGap);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("FIND_COUNT_INVALID_VALUE_GAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dPrecisionPositionFindLimitPointGap);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("FIND_LIMIT_POINT_GAP"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%d", m_nPrecisionPositionFilteringWheelToHusingGap);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("FILTERING_WHEEL_TO_HOUSING_GAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPrecisionPositionFilteringCartSafetyGap);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("FILTERING_CART_SAFETY_GAP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPrecisionPositionFilteringObjectToSafetyGap);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("FILTERING_OBJECT_TO_SAFETY_GAP"), strTemp, INI_FILE_PATH_WSTR);

	strTemp.Format(L"%5.1f", m_dPrecisionPositionTergetingScanLimiteDistance);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("TARGETING_SCAN_LIMITE_DISTANCE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPrecisionPositionTergetingScanLimiteDegree);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("TARGETING_SCAN_LIMITE_DEGREE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dPrecisionPositionTergetingWeightValueDistance);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("TARGETING_WEIGHT_VALUE_DISTANCE"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dPrecisionPositionTergetingWeightValueThDeg);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("TARGETING_WEIGHT_VALUE_TH_DEG"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%5.1f", m_dPrecisionPositionTergetingWeightValueDirection);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("TARGETING_WEIGHT_VALUE_DIRECTION"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", m_nPrecisionPositionTergetingLimitTraceFailCount);
	WritePrivateProfileString(_T("PRECISION_POSITION_MOVE"), _T("TARGETING_LIMIT_TRACE_FAIL_COUNT"), strTemp, INI_FILE_PATH_WSTR);

	// Save [GUI_DISP]
	strTemp.Format(L"%d", LRF1_DISP);
	WritePrivateProfileString(_T("GUI_DISP"), _T("LRF1_DISP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", LRF2_DISP);
	WritePrivateProfileString(_T("GUI_DISP"), _T("LRF2_DISP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", NODE_DISP);
	WritePrivateProfileString(_T("GUI_DISP"), _T("NODE_DISP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", STATION_DISP);
	WritePrivateProfileString(_T("GUI_DISP"), _T("STATION_DISP"), strTemp, INI_FILE_PATH_WSTR);


	strTemp.Format(L"%d", PATHDATA_DISP);
	WritePrivateProfileString(_T("GUI_DISP"), _T("PATHDATA_DISP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", PARTICLE_DISP);
	WritePrivateProfileString(_T("GUI_DISP"), _T("PARTICLE_DISP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%d", ROBOT_DISP);
	WritePrivateProfileString(_T("GUI_DISP"), _T("ROBOT_DISP"), strTemp, INI_FILE_PATH_WSTR);


	// [OBSTACLE_RANGE]						//pjs 추가 - ObstacleDetect ini 설정

	strTemp.Format(L"%d", m_nObstacleRangeDisp);
	WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_RANGE_DISP"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%.2f", m_dObstacleStopDistanceMin);
	WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_STOP_DISTANCE_MIN"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%.2f", m_dObstacleStopDistanceMax);
	WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_STOP_DISTANCE_MAX"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%.2f", m_dObstacleWarnDistanceMax);
	WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_WARN_DISTANCE_MAX"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%.2f", m_dObstacleStopWidthBasic);
	WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_STOP_WIDTH_BASIC"), strTemp, INI_FILE_PATH_WSTR);
	strTemp.Format(L"%.2f", m_dObstacleStopWidthLoaded);
	WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_STOP_WIDTH_LOADED"), strTemp, INI_FILE_PATH_WSTR);

	//strTemp.Format(L"%d", m_dObstacle_Stop_Distance_Min);
	//WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_STOP_DISTANCE_MIN"), strTemp, INI_FILE_PATH_WSTR);
	//strTemp.Format(L"%d", m_dObstacle_Stop_Distance_Max);
	//WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_STOP_DISTANCE_MAX"), strTemp, INI_FILE_PATH_WSTR);
	//strTemp.Format(L"%d", m_dObstacle_Stop_Width_Min);
	//WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_STOP_WIDTH_MIN"), strTemp, INI_FILE_PATH_WSTR);
	//strTemp.Format(L"%d", m_dObstacle_Stop_Width_Max);
	//WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_STOP_WIDTH_MAX"), strTemp, INI_FILE_PATH_WSTR);

	//strTemp.Format(L"%d", m_dObstacle_Warn_Distance_Min);
	//WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_WARN_DISTANCE_MIN"), strTemp, INI_FILE_PATH_WSTR);
	//strTemp.Format(L"%d", m_dObstacle_Warn_Distance_Max);
	//WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_WARN_DISTANCE_MAX"), strTemp, INI_FILE_PATH_WSTR);
	//strTemp.Format(L"%d", m_dObstacle_Warn_Width_Min);
	//WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_WARN_WIDTH_MIN"), strTemp, INI_FILE_PATH_WSTR);
	//strTemp.Format(L"%d", m_dObstacle_Warn_Width_Max);
	//WritePrivateProfileString(_T("OBSTACLE_RANGE"), _T("OBSTACLE_WARN_WIDTH_MAX"), strTemp, INI_FILE_PATH_WSTR);

}

////////////////////////////////////////////////////////////////////////////////////////////////////

// Set [SERVER]
void RobotParameter::setServerIP(string strServerIP)
{
	m_strServerIP = strServerIP;
}
void RobotParameter::setServerPort(int nServerPort)
{
	m_nServerPort = nServerPort;
}

// Set [MAP]
void RobotParameter::setMapNameNPath(string sMapName)
{
	m_strMapNameNPath = sMapName;
}
void RobotParameter::setMapSize(int nSizeXm, int nSizeYm)
{
	//작성될 지도의 크기를 설정한다. 
	m_nMapSizeXm = nSizeXm; //지도 x,y 크기 단위: m로 설정
	m_nMapSizeYm = nSizeYm; //지도 x,y 크기 단위: m로 설정
}
void RobotParameter::setMapXOffset(int dXoffset)
{
	m_dMApOffset_X = dXoffset;
}
void RobotParameter::setMapYOffset(int dYoffset)
{
	m_dMApOffset_Y = dYoffset;
}
void RobotParameter::setHeight(double dHeight)
{
	m_dHeight = dHeight; // m로 설정
}
void RobotParameter::setSDValue(double dSDValue)
{
	m_dSDThres = dSDValue;
}

// Set [PATH]
void RobotParameter::setNodePointNameNPath(string sMapName)
{		
	m_strNodePointNameNPath = sMapName;
}
void RobotParameter::setPathDataNameNPath(string sMapName)
{
	m_strPathDataNameNPath = sMapName;
}
void RobotParameter::setStationPointNameNPath(string sMapName)
{
	m_strStationPointNameNPath = sMapName;
}
void RobotParameter::setChargerPointNameNPath(string sMapName)
{
	m_strChargerPointNameNPath = sMapName;
}
void RobotParameter::setWorkPlacePointNameNPath(string sMapName)
{
	m_strWorkPlacePointNameNPath = sMapName;
}
void RobotParameter::setLastRobotPoseNameNPath(string strLastRobotPoseNameNPath)		//pjs 추가 - 마지막 위치 저장 및 사용
{
	m_strLastRobotPoseNameNPath = strLastRobotPoseNameNPath;
}
void RobotParameter::setInitRobotPoseNameNPath(string strInitRobotPoseNameNPath)		//pjs 추가 - 로봇 위치 초기화 설정
{
	m_strInitRobotPoseNameNPath = strInitRobotPoseNameNPath;
}

// Set [ROBOT]
void RobotParameter::setRobotID(int nID)
{
	//로봇의 ID를 설정해주는 함수
	m_nRobotID = nID;
}
void RobotParameter::setAMR_IP(string strAMR_IP)
{
	m_strAMR_IP = strAMR_IP;
}
void RobotParameter::setAMR_Port(int nAMR_Port)
{
	m_nAMR_Port = nAMR_Port;
}
void RobotParameter::setAMR_WLAN_Name(string strAMR_WLAN_Name)
{
	m_strAMR_WLAN_Name = strAMR_WLAN_Name;
}

void RobotParameter::setInitialPositionX(double dInitialPositionX)
{
	m_dInitialPositionX = dInitialPositionX;
}
void RobotParameter::setInitialPositionY(double dInitialPositionY)
{
	m_dInitialPositionY = dInitialPositionY;
}
void RobotParameter::setInitialPositionTh(double dInitialPositionTh)
{
	m_dInitialPositionTh = dInitialPositionTh;
}

void RobotParameter::setRobotRadius(double dRadius)
{
	m_dRadiusofRobot = dRadius;
}
void RobotParameter::setWheelBaseofRobot(double dWheelBaseofRobot)
{
	m_dWheelBaseofRobot = dWheelBaseofRobot;
}
void RobotParameter::setGearRatio(double dGearRatio)
{
	m_dGearRatio = dGearRatio;
}
void RobotParameter::setEncoderResolution(int nEncoderResolution)
{
	m_nEncoderResolution = nEncoderResolution;
}
void RobotParameter::setWheelRadius(double dWheelRadius)
{
	m_dWheelRadius = dWheelRadius;
}

void RobotParameter::setWeightAngleVelocity(double dWeightAngleVelocity)
{
	//로봇의 각속도 가중치를 입력
	m_dWeightAngleVelocity = dWeightAngleVelocity;
}
void RobotParameter::setMaxRobotVelocity(double dMaxVelocity)
{
	//로봇의 최대속도를 입력
	m_dMaxRobotVelocity = dMaxVelocity;
}
void RobotParameter::setMinRobotVelocity(double dMinVelocity)
{
	//로봇의 최소속도를 입력
	m_dMinRobotVelocity = dMinVelocity;
}
void RobotParameter::setMaxRobotAngleVelocity(double MaxRobotAngleVelocity)
{
	m_dMaxRobotAngleVelocity = MaxRobotAngleVelocity;
}
void RobotParameter::setMaxRobotAutonomusVelocity(double MaxRobotAutonomusVelocity)
{
	m_dMaxRobotAutonomusVelocity = MaxRobotAutonomusVelocity;
}
void RobotParameter::setMaxRobotGuideVelocity(double dMaxRobotGuideVelocity)
{
	m_dMaxRobotGuideVelocity = dMaxRobotGuideVelocity;
}
void RobotParameter::setMaxRobotGuideAngleVelocity(double dMaxRobotGuideAngleVelocity)
{
	m_dMaxRobotGuideAngleVelocity = dMaxRobotGuideAngleVelocity;
}
void RobotParameter::setMaxRobotGuideVelocitySlow(double dMaxRobotGuideVelocitySlow)
{
	m_dMaxRobotGuideVelocitySlow = dMaxRobotGuideVelocitySlow;
}
void RobotParameter::setMaxRobotGuideAngleVelocitySlow(double dMaxRobotGuideAngleVelocitySlow)
{
	m_dMaxRobotGuideAngleVelocitySlow = dMaxRobotGuideAngleVelocitySlow;
}
void RobotParameter::setRobotRotationAngleSpeed(double dRobotRotationAngleSpeed)
{
	m_dRobotRotationAngleSpeed = dRobotRotationAngleSpeed;
}
void RobotParameter::setRobotStraightMoveSpeed(int nRobotStraightMoveSpeed)
{
	m_nRobotStraightMoveSpeed = nRobotStraightMoveSpeed;
}

void RobotParameter::setInitRobotPoseForMap(RobotPose initRobotPos)
{
	m_initRobotPosForMap = initRobotPos;
}
void RobotParameter::setUsingLastPose(int nUsingLastPose)
{
	m_nUsingLastPose = nUsingLastPose;
}
void RobotParameter::setInitRobotPoseForNav(RobotPose initRobotPos)
{
	m_initRobotPosForNav = initRobotPos;
}
//void RobotParameter::setLastRobotPoseNameNPath(string strLastRobotPoseNameNPath)		//pjs 추가 - 마지막 위치 저장 및 사용
//{
//	m_strLastRobotPoseNameNPath = strLastRobotPoseNameNPath;
//}
void RobotParameter::setGlobalLocalization(string sGlobalLocalization)
{
	m_strGlobalLocalization = sGlobalLocalization;
}
void RobotParameter::setObstacleDetectionTime(int nObstacleDetectionTime)
{
	m_nObstacleDetectionTime = nObstacleDetectionTime;
}
void RobotParameter::setLocalization(int nLocalization)
{
	m_nLocalization = nLocalization;
}
void RobotParameter::setAMRRunigtime(int nRuningTime)
{
	m_nRunTime = nRuningTime;
}
void RobotParameter::setLoadingMode(int nLoadingMode)
{
	m_nLoadingMode = nLoadingMode;
}
void RobotParameter::setFirstMuteOn(int nFirstMuteOn)
{
	m_nFirstMuteOn = nFirstMuteOn;
}

//pjs 추가 - Battery Min, Max, ChargeRequired
void RobotParameter::setBatteryVoltageMax(double dBatteryVoltageMax)
{
	m_dBatteryVoltageMax = dBatteryVoltageMax;
}
void RobotParameter::setBatteryVoltageMin(double dBatteryVoltageMin)
{
	m_dBatteryVoltageMin = dBatteryVoltageMin;
}
void RobotParameter::setChargeRequiredPersent(double dChargeRequiredPersent)
{
	m_dChargeRequiredPersent = dChargeRequiredPersent;
}
//void RobotParameter::setUseAutoChargingStation(int nUseAutoChargingStation)
//{
//	m_nUseAutoChargingStation = nUseAutoChargingStation;
//}
//void RobotParameter::setAutoChargingCompleteVoltage(double dAutoChargingCompleteVoltage)
//{
//	m_dAutoChargingCompleteVoltage = dAutoChargingCompleteVoltage;
//}
//void RobotParameter::setChargingAlarmCompleteVoltage(double dChargingAlarmCompleteVoltage)
//{
//	m_dChargingAlarmCompleteVoltage = dChargingAlarmCompleteVoltage;
//}
void RobotParameter::setUseDevicePowerSeparationMode(int nUseDevicePowerSeparationMode)
{
	m_nUseDevicePowerSeparationMode = nUseDevicePowerSeparationMode;
}
void RobotParameter::setUseLowStandbyPowerMode(int nUseLowStandbyPowerMode)
{
	m_nUseLowStandbyPowerMode = nUseLowStandbyPowerMode;
}

void RobotParameter::setUseBreakMode(int nUseBreakMode)
{
	m_nUseBreakMode = nUseBreakMode;
}

void RobotParameter::setUseReset_EmergencyStopMode(int nUseReset_EmergencyStopMode)
{
	m_nUseReset_EmergencyStopMode = nUseReset_EmergencyStopMode;
}


// Set [SENSOR]
void RobotParameter::setGyroComPort(int nGyroComPort)
{
	m_nGyroComPort = nGyroComPort;
}

void RobotParameter::setGuideSensorCount(int nGuideSensorCount)
{
	m_nGuideSensorCount = nGuideSensorCount;
}
void RobotParameter::setGuideComPort1(int nGuideComPort)
{
	m_nGuideComPort1 = nGuideComPort;
}
void RobotParameter::setGuideComPort2(int nGuideComPort)
{
	m_nGuideComPort2 = nGuideComPort;
}

void RobotParameter::setGuideStopMarkSensorFront(int nGuideStopMarkSensorFront)
{
	m_nGuideStopMarkSensorFront = nGuideStopMarkSensorFront;
}
void RobotParameter::setGuideStopMarkSensorRear(int nGuideStopMarkSensorRear)
{
	m_nGuideStopMarkSensorRear = nGuideStopMarkSensorRear;
}
void RobotParameter::setUseMatchDualGuideStopMarkSensor(int nUseMatchDualGuideStopMarkSensor)
{
	m_nUseMatchDualGuideStopMarkSensor = nUseMatchDualGuideStopMarkSensor;
}

void RobotParameter::setMagneticGuideSensorXOffset(int nMagneticGuideSensorXOffset)
{
	m_nMagneticGuideSensorXOffset = nMagneticGuideSensorXOffset;
}
void RobotParameter::setMagneticGuideSensorWidth(int nMagneticGuideSensorWidth)
{
	m_nMagneticGuideSensorWidth = nMagneticGuideSensorWidth;
}

void RobotParameter::setMagneticGuideSensorMin(double dMagneticGuideSensorMin)
{
	m_dMagneticGuideSensorMin = dMagneticGuideSensorMin;
}
void RobotParameter::setMagneticGuideSensorMax(double dMagneticGuideSensorMax)
{
	m_dMagneticGuideSensorMax = dMagneticGuideSensorMax;
}
void RobotParameter::setMagneticGuideFindAngle(double dMagneticGuideFindAngle)
{
	m_dMagneticGuideFindAngle = dMagneticGuideFindAngle;
}

void RobotParameter::setBatteryChecker(int nBatterChecker)
{
	m_nBatteryChecker = nBatterChecker;
}

void RobotParameter::setLaserScannerType(int nLaserScannerType )
{
	m_nLaserScannerType = nLaserScannerType;
}
void RobotParameter::setLaserScannerCount(int nLaserScannerCount)
{
	m_nLaserScannerCount = nLaserScannerCount;
}
void RobotParameter::setLaserScannerHead(int nLaserScannerHead)
{
	m_nLaserScannerHead = nLaserScannerHead;
}

void RobotParameter::setSICKLMSLaserComport1(string strLaserIP)
{
	m_strSICKLMSLaserIP_1 = strLaserIP;
}
void RobotParameter::setLaserTCPPort1(int nLaserTCPPort)
{
	m_nLaserTCPPort1 = nLaserTCPPort;
}
void RobotParameter::setSICKLMSLaserComport2(string strLaserIP)
{
	m_strSICKLMSLaserIP_2 = strLaserIP;
}
void RobotParameter::setLaserTCPPort2(int nLaserTCPPort)
{
	m_nLaserTCPPort2 = nLaserTCPPort;
}

void RobotParameter::setLaserMaxDist(int nMaxDist)
{
	//상단 레이저의 최대 탐지거리를 설정하는 함수
	m_nLaserMaxDist = nMaxDist;
}
void RobotParameter::setLaserMinDist(int nMinDist)
{
	//상단 레이저의 최소 탐지거리를 설정하는 함수
	m_nLaserMinDist = nMinDist;
}


void RobotParameter::setLaserHeight(int nHeight)
{
	//틸트 축으로 부터 상단 레이저까지의 높이값을 설정하는 함수
	m_nLaserHeight = nHeight;
}
void RobotParameter::setLaserXOffset(int nXOffset)
{
	//틸트 축으로 부터 상단 레이저까지의 x offset을 설정하는 함수
	m_nLaserXOffset = nXOffset;
}
void RobotParameter::setLaserYOffset(int nYOffset)
{
	//틸트 축으로 부터 상단 레이저까지의 y offset을 설정하는 함수
	m_nLaserYOffset = nYOffset;
}

void RobotParameter::setLaserHeight1(int nHeight)
{
	//틸트 축으로 부터 상단 레이저까지의 높이값을 설정하는 함수
	m_nLaserHeight1 = nHeight;
}
void RobotParameter::setLaserXOffset1(int nXOffset)
{
	//틸트 축으로 부터 상단 레이저까지의 x offset을 설정하는 함수
	m_nLaserXOffset1 = nXOffset;
}
void RobotParameter::setLaserYOffset1(int nYOffset)
{
	//틸트 축으로 부터 상단 레이저까지의 y offset을 설정하는 함수
	m_nLaserYOffset1 = nYOffset;
}


void RobotParameter::setLaserHeight2(int nHeight)
{
	//틸트 축으로 부터 상단 레이저까지의 높이값을 설정하는 함수
	m_nLaserHeight2 = nHeight;
}
void RobotParameter::setLaserXOffset2(int nXOffset)
{
	//틸트 축으로 부터 상단 레이저까지의 x offset을 설정하는 함수
	m_nLaserXOffset2 = nXOffset;
}
void RobotParameter::setLaserYOffset2(int nYOffset)
{
	//틸트 축으로 부터 상단 레이저까지의 y offset을 설정하는 함수
	m_nLaserYOffset2 = nYOffset;
}


// Set [KANAYAMA_MOTION_CONTROL]
void RobotParameter::setTargetDistance(int nDistToTarget)
{
	m_nDistToTarget = nDistToTarget;
}
void RobotParameter::setDesiedVel(int nDesiredVel)
{
	m_nDesiredVel = nDesiredVel;
}
void RobotParameter::setGoalArea(int nGoalArea)
{
	m_nGoalArea = nGoalArea;
}
void RobotParameter::setdKX(double dKX)
{
	m_dKX = dKX;
}
void RobotParameter::setdKY(double dKY)
{
	m_dKY = dKY;
}
void RobotParameter::setdKT(double dKT)
{
	m_dKT = dKT;
}
void RobotParameter::setDirectionofRotation(int nDirectionofRotation)
{
	m_nDirectionofRotation = nDirectionofRotation;
}

// Set [PARTICLE_FILTER]
void RobotParameter::setMaxParticleNum(int nMaxNum)
{
	//최대 particle 갯수를 설정하는 함수
	m_nMaxParticleNum = nMaxNum;
}
void RobotParameter::setMinParticleNUm(int nMinNUm)
{
	//최소 particle 갯수를 설정하는 함수
	m_nMinParticleNum = nMinNUm;
}
void RobotParameter::setDeviationforTrans(double dDevationforTrans)
{
	m_dDevationforTrans = dDevationforTrans;
}
void RobotParameter::setDeviationforRotae(double dDevationforRotate)
{
	m_dDevationforRotate = dDevationforRotate;
}
void RobotParameter::setDeviationforTransRotae(double dDevationforTransRotate)
{
	m_dDevationforTransRotate = dDevationforTransRotate;
}

// Set [MOTOR_DRIVER]
void RobotParameter::setRobotMotorComport(int nRobotMotorComport)
{
	m_nRobotMotorComport = nRobotMotorComport;
}
void RobotParameter::setRoboCubeMotordriver(int nRoboCubeDriver)
{
	m_nRoboCubeDriver = nRoboCubeDriver;
}
void RobotParameter::setMotorDrection(int nMotorDirection)
{
	m_nMotorDirection = nMotorDirection;
}

// Set [DIGITAL_IO]
void RobotParameter::setIODevice(int nIo)
{
	m_nIo = nIo;
}

// Set [PHIDGETS]
void RobotParameter::setPhidgetsDeviceQuantity(int nQuantity)
{
	m_nQuantity = nQuantity;
}
void RobotParameter::setDeviceSerialNumber1(int PhidgetsSerialNumber1)
{
	m_PhidgetsSerialNumber1 = PhidgetsSerialNumber1;
}
void RobotParameter::setDeviceSerialNumber2(int PhidgetsSerialNumber2)
{
	m_PhidgetsSerialNumber2 = PhidgetsSerialNumber2;
}

void RobotParameter::setPhidget1InCh(int nChNum, int nChData)
{
	m_nPhidget1InCh[nChNum] = nChData;
}
void RobotParameter::setPhidget1OutCh(int nChNum, int nChData)
{
	m_nPhidget1OutCh[nChNum] = nChData;
}
void RobotParameter::setPhidget2InCh(int nChNum, int nChData)
{
	m_nPhidget2InCh[nChNum] = nChData;
}
void RobotParameter::setPhidget2OutCh(int nChNum, int nChData)
{
	m_nPhidget2OutCh[nChNum] = nChData;
}

// Set [OBJECT_FOLLOW]
void RobotParameter::setFindUseReflector(int nFindUseReflector)
{
	m_nFindUseReflector = nFindUseReflector;
}
void RobotParameter::setFindCountInvalidValueReflector(int nFindCountInvalidValueReflector)
{
	m_nFindCountInvalidValueReflector = nFindCountInvalidValueReflector;
}
void RobotParameter::setFindCountInvalidValueGap(int nFindCountInvalidValueGap)
{
	m_nFindCountInvalidValueGap = nFindCountInvalidValueGap;
}
void RobotParameter::setFindLimitPointGap(double dFindLimitPointGap)
{
	m_dFindLimitPointGap = dFindLimitPointGap;
}

void RobotParameter::setFilteringLimitObjectWidthMax(int nFilteringLimitObjectWidthMax)
{
	m_nFilteringLimitObjectWidthMax = nFilteringLimitObjectWidthMax;
}
void RobotParameter::setFilteringLimitObjectWidthMin(int nFilteringLimitObjectWidthMin)
{
	m_nFilteringLimitObjectWidthMin = nFilteringLimitObjectWidthMin;
}
void RobotParameter::setFilteringTraceRange(int nFilteringTraceRange)
{
	m_nFilteringTraceRange = nFilteringTraceRange;
}
void RobotParameter::setFilteringLimitTraceTime(int nFilteringLimitTraceTime)
{
	m_nFilteringLimitTraceTime = nFilteringLimitTraceTime;
}

void RobotParameter::setTargetingWeightValueDistance(double dTargetingWeightValueDistance)
{
	m_dTargetingWeightValueDistance = dTargetingWeightValueDistance;
}
void RobotParameter::setTargetingWeightValueThDeg(double dTargetingWeightValueThDeg)
{
	m_dTargetingWeightValueThDeg = dTargetingWeightValueThDeg;
}
void RobotParameter::setTargetingTergeting(int nTargetingTergeting)
{
	m_nTargetingTergeting = nTargetingTergeting;
}
void RobotParameter::setTargetingLimitObjectDistanceInitial(int nTargetingLimitObjectDistanceInitial)
{
	m_nTargetingLimitObjectDistanceInitial = nTargetingLimitObjectDistanceInitial;
}
void RobotParameter::setTargetingLimitTraceFailCount(int nTargetingLimitTraceFailCount)
{
	m_nTargetingLimitTraceFailCount = nTargetingLimitTraceFailCount;
}
void RobotParameter::setTargetTraceRange(int nTargetTraceRange)
{
	m_nTargetTraceRange = nTargetTraceRange;
}

void RobotParameter::setMoveObjectFlollowMaxV(double dMoveObjectFlollowMaxV)
{
	m_dMoveObjectFlollowMaxV = dMoveObjectFlollowMaxV;
}
void RobotParameter::setMoveFollowGap(double dMoveFollowGap)
{
	m_dMoveFollowGap = dMoveFollowGap;
}
void RobotParameter::setMoveMaxScanDistance(double dMoveMaxScanDistance)
{
	m_dMoveMaxScanDistance = dMoveMaxScanDistance;
}
void RobotParameter::setMoveRotateRange(double dMoveRotateRange)
{
	m_dMoveRotateRange = dMoveRotateRange;
}

// Set [TUNNEL_MOVE]
void RobotParameter::setTunnelFindScanLimiteDistance(double dTunnelFindScanLimiteDistance)
{
	m_dTunnelFindScanLimiteDistance = dTunnelFindScanLimiteDistance;
}
void RobotParameter::setTunnelFindLaserScanningObjectValidCount(int nTunnelFindLaserScanningObjectValidCount)
{
	m_nTunnelFindLaserScanningObjectValidCount = nTunnelFindLaserScanningObjectValidCount;
}
void RobotParameter::setTunnelFindCountInvalidValueGap(int nTunnelFindCountInvalidValueGap)
{
	m_nTunnelFindCountInvalidValueGap = nTunnelFindCountInvalidValueGap;
}
void RobotParameter::setTunnelFindLimitPointGap(double dTunnelFindLimitPointGap)
{
	m_dTunnelFindLimitPointGap = dTunnelFindLimitPointGap;
}

void RobotParameter::setTunnelFilteringWheelToHusingGap(int nTunnelFilteringWheelToHusingGap)
{
	m_nTunnelFilteringWheelToHusingGap = nTunnelFilteringWheelToHusingGap;
}
void RobotParameter::setTunnelFilteringCartSafetyGap(int nTunnelFilteringCartSafetyGap)
{
	m_nTunnelFilteringCartSafetyGap = nTunnelFilteringCartSafetyGap;
}
void RobotParameter::setTunnelFilteringObjectToSafetyGap(int nTunnelFilteringObjectToSafetyGap)
{
	m_nTunnelFilteringObjectToSafetyGap = nTunnelFilteringObjectToSafetyGap;
}

void RobotParameter::setTunnelTergetingScanLimiteDistance(double dTunnelTergetingScanLimiteDistance)
{
	m_dTunnelTergetingScanLimiteDistance = dTunnelTergetingScanLimiteDistance;
}
void RobotParameter::setTunnelTergetingScanLimiteDegree(int nTunnelTergetingScanLimiteDegree)
{
	m_nTunnelTergetingScanLimiteDegree = nTunnelTergetingScanLimiteDegree;
}
void RobotParameter::setTunnelTergetingWeightValueDistance(double dTunnelTergetingWeightValueDistance)
{
	m_dTunnelTergetingWeightValueDistance = dTunnelTergetingWeightValueDistance;
}
void RobotParameter::setTunnelTergetingWeightValueThDeg(double dTunnelTergetingWeightValueThDeg)
{
	m_dTunnelTergetingWeightValueThDeg = dTunnelTergetingWeightValueThDeg;
}
void RobotParameter::setTunnelTergetingWeightValueDirection(double dTunnelTergetingWeightValueDirection)
{
	m_dTunnelTergetingWeightValueDirection = dTunnelTergetingWeightValueDirection;
}
void RobotParameter::setTunnelTergetingLimitTraceFailCount(int nTunnelTergetingLimitTraceFailCount)
{
	m_nTunnelTergetingLimitTraceFailCount = nTunnelTergetingLimitTraceFailCount;
}

// Set [PRECISION_POSITION_MOVE]
void RobotParameter::setPrecisionPositionFindScanLimiteDistance(double dPrecisionPositionFindScanLimiteDistance)
{
	m_dPrecisionPositionFindScanLimiteDistance = dPrecisionPositionFindScanLimiteDistance;
}
void RobotParameter::setPrecisionPositionFindLaserScanningObjectValidCount(int nPrecisionPositionFindLaserScanningObjectValidCount)
{
	m_nPrecisionPositionFindLaserScanningObjectValidCount = nPrecisionPositionFindLaserScanningObjectValidCount;
}
void RobotParameter::setPrecisionPositionFindCountInvalidValueGap(int nPrecisionPositionFindCountInvalidValueGap)
{
	m_nPrecisionPositionFindCountInvalidValueGap = nPrecisionPositionFindCountInvalidValueGap;
}
void RobotParameter::setPrecisionPositionFindLimitPointGap(double dPrecisionPositionFindLimitPointGap)
{
	m_dPrecisionPositionFindLimitPointGap = dPrecisionPositionFindLimitPointGap;
}

void RobotParameter::setPrecisionPositionFilteringWheelToHusingGap(int nPrecisionPositionFilteringWheelToHusingGap)
{
	m_nPrecisionPositionFilteringWheelToHusingGap = nPrecisionPositionFilteringWheelToHusingGap;
}
void RobotParameter::setPrecisionPositionFilteringCartSafetyGap(int nPrecisionPositionFilteringCartSafetyGap)
{
	m_nPrecisionPositionFilteringCartSafetyGap = nPrecisionPositionFilteringCartSafetyGap;
}
void RobotParameter::setPrecisionPositionFilteringObjectToSafetyGap(int nPrecisionPositionFilteringObjectToSafetyGap)
{
	m_nPrecisionPositionFilteringObjectToSafetyGap = nPrecisionPositionFilteringObjectToSafetyGap;
}

void RobotParameter::setPrecisionPositionTergetingScanLimiteDistance(double dPrecisionPositionTergetingScanLimiteDistance)
{
	m_dPrecisionPositionTergetingScanLimiteDistance = dPrecisionPositionTergetingScanLimiteDistance;
}
void RobotParameter::setPrecisionPositionTergetingScanLimiteDegree(int nPrecisionPositionTergetingScanLimiteDegree)
{
	m_nPrecisionPositionTergetingScanLimiteDegree = nPrecisionPositionTergetingScanLimiteDegree;
}
void RobotParameter::setPrecisionPositionTergetingWeightValueDistance(double dPrecisionPositionTergetingWeightValueDistance)
{
	m_dPrecisionPositionTergetingWeightValueDistance = dPrecisionPositionTergetingWeightValueDistance;
}
void RobotParameter::setPrecisionPositionTergetingWeightValueThDeg(double dPrecisionPositionTergetingWeightValueThDeg)
{
	m_dPrecisionPositionTergetingWeightValueThDeg = dPrecisionPositionTergetingWeightValueThDeg;
}
void RobotParameter::setPrecisionPositionTergetingWeightValueDirection(double dPrecisionPositionTergetingWeightValueDirection)
{
	m_dPrecisionPositionTergetingWeightValueDirection = dPrecisionPositionTergetingWeightValueDirection;
}
void RobotParameter::setPrecisionPositionTergetingLimitTraceFailCount(int nPrecisionPositionTergetingLimitTraceFailCount)
{
	m_nPrecisionPositionTergetingLimitTraceFailCount = nPrecisionPositionTergetingLimitTraceFailCount;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

// Get [SERVER]
string RobotParameter::getServerIP()
{
	return m_strServerIP;
}
int RobotParameter::getServerPort()
{
	return m_nServerPort;
}

// Get [MAP]
string RobotParameter::getMapNameNPath()
{
	return m_strMapNameNPath;
}
int RobotParameter::getMapSizeXm()
{
	//지도 x,y 크기를 넘겨줌
	return m_nMapSizeXm;
}
int RobotParameter::getMapSizeYm()
{
	//지도 x,y 크기를 넘겨줌
	return m_nMapSizeYm;
}
int RobotParameter::getXoffset()
{
	return m_dMApOffset_X;
}
int RobotParameter::getYoffset()
{
	return m_dMApOffset_Y;
}
double RobotParameter::getHeight()
{
	return m_dHeight;
}
double RobotParameter::getSDValue()
{
	return m_dSDThres;
}

// Get [PATH]
string RobotParameter::getNodePointNameNPath()
{
	return m_strNodePointNameNPath;
}
string RobotParameter::getPathDataNameNPath()
{
	return m_strPathDataNameNPath;
}

string RobotParameter::getStationPointNameNPath()
{
	return m_strStationPointNameNPath;
}

string RobotParameter::getChargerPointNameNPath()
{
	return m_strChargerPointNameNPath;
}

string RobotParameter::getWorkPlacePointNameNPath()
{
	return m_strWorkPlacePointNameNPath;
}
string RobotParameter::getLastRobotPoseNameNPath()				//pjs 추가 - 마지막 위치 저장 및 사용
{
	return m_strLastRobotPoseNameNPath;
}
string RobotParameter::getInitRobotPoseNameNPath()				//pjs 추가 - 로봇 위치 초기화 설정
{
	return m_strInitRobotPoseNameNPath;
}
// Get [ROBOT]
int RobotParameter::getRobotID()
{
	// RobotID를 넘겨줌
	return m_nRobotID;
}
string RobotParameter::getAMR_IP()
{
	return m_strAMR_IP;
}
int RobotParameter::getAMR_Port()
{
	return m_nAMR_Port;
}
string RobotParameter::getAMR_WLAN_Name()
{
	return m_strAMR_WLAN_Name;
}

double RobotParameter::getInitialPositionX()
{
	return m_dInitialPositionX;
}
double RobotParameter::getInitialPositionY()
{
	return m_dInitialPositionY;
}
double RobotParameter::getInitialPositionTh()
{
	return m_dInitialPositionTh;
}

double RobotParameter::getRobotRadius()
{
	// 로봇의 반지름을 넘겨줌
	return m_dRadiusofRobot;
}
double RobotParameter::getWheelBaseofRobot()
{
	return m_dWheelBaseofRobot;
}
double RobotParameter::getGearRatio()
{
	return m_dGearRatio;
}
int RobotParameter::getEncoderResolution()
{
	return m_nEncoderResolution;
}
double RobotParameter::getWheelRadius()
{
	return m_dWheelRadius;
}

double RobotParameter::getWeightAngleVelocity()
{
	return m_dWeightAngleVelocity;
}
double RobotParameter::getMaxRobotVelocity()
{
	//로봇의 최대 속도를 넘겨줌
	return m_dMaxRobotVelocity;
}
double RobotParameter::getMinRobotVelocity()
{
	//로봇의 최소 속도를 넘겨줌
	return m_dMinRobotVelocity;
}
double RobotParameter::getMaxRobotAngleVelocity()
{
	return m_dMaxRobotAngleVelocity;
}
double RobotParameter::getMaxRobotAutonomusVelocity()		//pjs 추가 - 자율 주행 최대 속도를 설정, 단 MAX_VELOCITY을 넘을 수 없음
{
	return m_dMaxRobotAutonomusVelocity;
}
double RobotParameter::getMaxRobotGuideVelocity()
{
	return m_dMaxRobotGuideVelocity;
}
double RobotParameter::getMaxRobotGuideAngleVelocity()
{
	return m_dMaxRobotGuideAngleVelocity;
}
double RobotParameter::getMaxRobotGuideVelocitySlow()
{
	return m_dMaxRobotGuideVelocitySlow;
}
double RobotParameter::getMaxRobotGuideAngleVelocitySlow()
{
	return m_dMaxRobotGuideAngleVelocitySlow;
}
double RobotParameter::getRobotRotationAngleSpeed()
{
	return m_dRobotRotationAngleSpeed;
}
int RobotParameter::getRobotStraightMoveSpeed()
{
	return m_nRobotStraightMoveSpeed;
}

RobotPose RobotParameter::getInitRobotPoseForMap()
{
	return m_initRobotPosForMap;
}
int RobotParameter::getUsingLastPose()
{
	return m_nUsingLastPose;
}
RobotPose RobotParameter::getInitRobotPoseForNav()
{
	return m_initRobotPosForNav;
}
//string RobotParameter::getLastRobotPoseNameNPath()				//pjs 추가 - 마지막 위치 저장 및 사용
//{
//	return m_strLastRobotPoseNameNPath;
//}
string RobotParameter::getGlobalLocalization()
{
	return m_strGlobalLocalization;
}
int RobotParameter::getObstacleDetectionTime()
{	
	return m_nObstacleDetectionTime;
}
int RobotParameter::getLocalization()
{
	return m_nLocalization;
}
int RobotParameter::getAMRRunigtime()
{
	return m_nRunTime;
}
int RobotParameter::getLoadingMode()
{
	return m_nLoadingMode;
}
int RobotParameter::getFirstMuteOn()
{
	return m_nFirstMuteOn; 
}
//pjs 추가 - Battery Min, Max, ChargeRequired
double RobotParameter::getBatteryVoltageMax()
{
	return m_dBatteryVoltageMax;
}
double RobotParameter::getBatteryVoltageMin()
{
	return m_dBatteryVoltageMin;
}
double RobotParameter::getChargeRequiredPersent()
{
	return m_dChargeRequiredPersent;
}
//int RobotParameter::getUseAutoChargingStation()
//{
//	return m_nUseAutoChargingStation; 
//}
//double RobotParameter::getAutoChargingCompleteVoltage()
//{
//	return m_dAutoChargingCompleteVoltage;
//}
//double RobotParameter::getChargingAlarmCompleteVoltage()
//{
//	return m_dChargingAlarmCompleteVoltage;
//}
int RobotParameter::getUseDevicePowerSeparationMode()
{
	return m_nUseDevicePowerSeparationMode; 
}
int RobotParameter::getUseLowStandbyPowerMode()
{
	return m_nUseLowStandbyPowerMode; 
}

int RobotParameter::getUseBreakMode()
{
	return m_nUseBreakMode; 
}

int RobotParameter::getUseReset_EmergencyStopMode()
{
	return m_nUseReset_EmergencyStopMode; 
}


double RobotParameter::getLift_Up_Limit()
{
	return m_dLift_Up_Limit;
}



double RobotParameter::getLift_Down_Limit()
{
	return m_dLift_Down_Limit;
}
int RobotParameter::getLiftMotorType()
//pjs 2019.03.18 Lift Motor 개수에 따라서 다르게 적용 //0 : Lift Motor Count 2, 1 : Lift Motor Count 1
{
	return m_nLiftMotorType;
}

// Get [SENSOR]
int RobotParameter::getGyroComPort()
{
	// Gyro Com Port를 설정하는 함수
	return m_nGyroComPort;
}

int RobotParameter::getGuideSensorCount()
{
	// Guide Sensor의 개수를 받아옴
	return m_nGuideSensorCount;
}
int RobotParameter::getGuideComPort1()
{
	// Guide Com Port를 설정하는 함수
	return m_nGuideComPort1;
}
int RobotParameter::getGuideComPort2()
{
	// Guide Com Port를 설정하는 함수
	return m_nGuideComPort2;
}
int RobotParameter::getGuideCanPort()
{
	// Guide Com Port를 설정하는 함수
	return m_nGuideCanPort;
}
int RobotParameter::getGuideStopMarkSensorFront()
{
	return m_nGuideStopMarkSensorFront;
}
int RobotParameter::getGuideStopMarkSensorRear()
{
	return m_nGuideStopMarkSensorRear;
}
int RobotParameter::getUseMatchDualGuideStopMarkSensor()
{
	return m_nUseMatchDualGuideStopMarkSensor;
}

int RobotParameter::getMagneticGuideSensorXOffset()
{
	return m_nMagneticGuideSensorXOffset;
}
int RobotParameter::getMagneticGuideSensorWidth()
{
	return m_nMagneticGuideSensorWidth;
}

double RobotParameter::getMagneticGuideSensorMin()
{
	return m_dMagneticGuideSensorMin;
}
double RobotParameter::getMagneticGuideSensorMax()
{
	return m_dMagneticGuideSensorMax;
}
double RobotParameter::getMagneticGuideFindAngle()
{
	return m_dMagneticGuideFindAngle;
}

int RobotParameter::getBatteryChecker()
{
	return m_nBatteryChecker;
}

int RobotParameter::getLaserScannerType()
{	
	return m_nLaserScannerType;
}
int RobotParameter::getLaserScannerCount()
{
	return m_nLaserScannerCount;
}
int RobotParameter::getLaserScannerHead()
{
	return m_nLaserScannerHead;
}

string RobotParameter::getSICKLMSLaserIPAddress1()
{
	// Laser IP address를 넘겨줌
	return m_strSICKLMSLaserIP_1;
}
int RobotParameter::getLaserTCPPort1()
{
	return m_nLaserTCPPort1;
}
string RobotParameter::getSICKLMSLaserIPAddress2()
{
	// Laser IP address를 넘겨줌
	return m_strSICKLMSLaserIP_2;
}
int RobotParameter::getLaserTCPPort2()
{
	return m_nLaserTCPPort2;
}

int RobotParameter::getLaserMaxDist()
{
	//상단 레이저의 최대 탐지거리를 받아오는 함수
	return m_nLaserMaxDist;
}
int RobotParameter::getLaserMinDist()
{
	//상단 레이저의 최소 탐지거리를 받아오는 함수
	return m_nLaserMinDist;
}

int RobotParameter::getLaserHeight()
{
	//틸트 축으로 부터 상단 레이저까지의 높이값을 받아오는 함수
	return m_nLaserHeight;
}
int RobotParameter::getLaserXOffset()
{
	//틸트 축으로 부터 상단 레이저까지의 x offset을 받아오는 함수
	return m_nLaserXOffset;
}
int RobotParameter::getLaserYOffset()
{
	//틸트 축으로 부터 상단 레이저까지의 y offset을 받아오는 함수.
	return m_nLaserYOffset;
}


int RobotParameter::getLaserHeight1()
{
	//틸트 축으로 부터 상단 레이저까지의 높이값을 받아오는 함수
	return m_nLaserHeight1;
}
int RobotParameter::getLaserXOffset1()
{
	//틸트 축으로 부터 상단 레이저까지의 x offset을 받아오는 함수
	return m_nLaserXOffset1;
}
int RobotParameter::getLaserYOffset1()
{
	//틸트 축으로 부터 상단 레이저까지의 y offset을 받아오는 함수.
	return m_nLaserYOffset1;
}


int RobotParameter::getLaserHeight2()
{
	//틸트 축으로 부터 상단 레이저까지의 높이값을 받아오는 함수
	return m_nLaserHeight2;
}
int RobotParameter::getLaserXOffset2()
{
	//틸트 축으로 부터 상단 레이저까지의 x offset을 받아오는 함수
	return m_nLaserXOffset2;
}
int RobotParameter::getLaserYOffset2()
{
	//틸트 축으로 부터 상단 레이저까지의 y offset을 받아오는 함수.
	return m_nLaserYOffset2;
}

int RobotParameter::getLaser_Installation_Angle_Offset1()
{
	//
	return m_nLaser_Installation_Angle_Offset1;
}

int RobotParameter::getLaser_Start_Invalid_Count1()
{
	//
	return m_nLaser_Start_Invalid_Count1;
}

int RobotParameter::getLaser_End_Invalid_Count1()
{
	//
	return m_nLaser_End_Invalid_Count1;
}


int RobotParameter::getLaser_Installation_Angle_Offset2()
{
	//
	return m_nLaser_Installation_Angle_Offset2;
}

int RobotParameter::getLaser_Start_Invalid_Count2()
{
	//
	return m_nLaser_Start_Invalid_Count2;
}

int RobotParameter::getLaser_End_Invalid_Count2()
{
	//
	return m_nLaser_End_Invalid_Count2;
}

double RobotParameter::getLaser_Angle_Step1()
{
	//
	return m_dLaser_Angle_Step1;
}

double RobotParameter::getLaser_Angle_Step2()
{
	//
	return m_dLaser_Angle_Step2;
}






// Get [KANAYAMA_MOTION_CONTROL]
int RobotParameter::getTargetDistance()
{
	return m_nDistToTarget;
}
int RobotParameter::getDesiedVel()
{
	return m_nDesiredVel;
}
int RobotParameter::getGoalArea()
{
	return m_nGoalArea;
}
double RobotParameter::getdKX()
{
	return m_dKX;
}
double RobotParameter::getdKY()
{
	return m_dKY;
}
double RobotParameter::getdKT()
{
	return m_dKT;
}
int RobotParameter::getDirectionofRotation()
{
	return m_nDirectionofRotation;
}

// Get [PARTICLE_FILTER]
int RobotParameter::getMaxParticleNum()
{
	//최대 particle 갯수를 설정하는 함수
	return m_nMaxParticleNum;
}
int RobotParameter::getMinParticleNum()
{
	//최소 particle 갯수를 설정하는 함수
	return m_nMinParticleNum;
}
double RobotParameter::getDeviationforTrans()
{
	return m_dDevationforTrans;
}
double RobotParameter::getDeviationforRotate()
{
	return m_dDevationforRotate;
}
double RobotParameter::getDeviationforTransRotate()
{
	return m_dDevationforTransRotate;
}

double RobotParameter::getAccumDistGlobal()
{
	return m_Accum_Dist_Global;
}
double RobotParameter::getAccumAngleGlobal()
{
	return m_Accum_Angle_Global;
}
double RobotParameter::getAccumDistLocal()
{
	return m_Accum_Dist_Local;
}
double RobotParameter::getAccumAngleLocal()
{
	return m_Accum_Angle_Local;
}

// Get [MOTOR_DRIVER]
int RobotParameter::getMotorComport()
{
	return m_nRobotMotorComport;
}
int RobotParameter::getRoboCubeMotordriver()
{
	return m_nRoboCubeDriver;
}
int RobotParameter::getMotorDrection()
{
	return m_nMotorDirection;
}

// Get [DIGITAL_IO]
int RobotParameter::getIODevice()
{
	return m_nIo;
}

// Get [PHIDGETS]
int RobotParameter::getPhidgetsDeviceQuantity()
{
	return m_nQuantity;
}
int RobotParameter::getDeviceSerialNumber1()
{
	return m_PhidgetsSerialNumber1;
}
int RobotParameter::getDeviceSerialNumber2()
{
	return m_PhidgetsSerialNumber2;
}

int RobotParameter::getPhidget1InCh(int nChNum)
{
	return m_nPhidget1InCh[nChNum];
}
int RobotParameter::getPhidget1OutCh(int nChNum)
{
	return m_nPhidget1OutCh[nChNum];
}
int RobotParameter::getPhidget2InCh(int nChNum)
{
	return m_nPhidget2InCh[nChNum];
}
int RobotParameter::getPhidget2OutCh(int nChNum)
{
	return m_nPhidget2OutCh[nChNum];
}

// Get [OBJECT_FOLLOW]
int RobotParameter::getFindUseReflector()
{
	return m_nFindUseReflector;
}
int RobotParameter::getFindCountInvalidValueReflector()
{
	return m_nFindCountInvalidValueReflector;
}
int RobotParameter::getFindCountInvalidValueGap()
{
	return m_nFindCountInvalidValueGap;
}
double RobotParameter::getFindLimitPointGap()
{
	return m_dFindLimitPointGap;
}

int RobotParameter::getFilteringLimitObjectWidthMax()
{
	return m_nFilteringLimitObjectWidthMax;
}
int RobotParameter::getFilteringLimitObjectWidthMin()
{
	return m_nFilteringLimitObjectWidthMin;
}
int RobotParameter::getFilteringTraceRange()
{
	return m_nFilteringTraceRange;
}
int RobotParameter::getFilteringLimitTraceTime()
{
	return m_nFilteringLimitTraceTime;
}

double RobotParameter::getTargetingWeightValueDistance()
{
	return m_dTargetingWeightValueDistance;
}
double RobotParameter::getTargetingWeightValueThDeg()
{
	return m_dTargetingWeightValueThDeg;
}
int RobotParameter::getTargetingTergeting()
{
	return m_nTargetingTergeting;
}
int RobotParameter::getTargetingLimitObjectDistanceInitial()
{
	return m_nTargetingLimitObjectDistanceInitial;
}
int RobotParameter::getTargetingLimitTraceFailCount()
{
	return m_nTargetingLimitTraceFailCount;
}
int RobotParameter::getTargetTraceRange()
{
	return m_nTargetTraceRange;
}

double RobotParameter::getMoveObjectFlollowMaxV()
{
	return m_dMoveObjectFlollowMaxV;
}
double RobotParameter::getMoveFollowGap()
{
	return m_dMoveFollowGap;
}
double RobotParameter::getMoveMaxScanDistance()
{
	return m_dMoveMaxScanDistance;
}
double RobotParameter::getMoveRotateRange()
{
	return m_dMoveRotateRange;
}

// Get [TUNNEL_MOVE]
double RobotParameter::getTunnelFindScanLimiteDistance()
{
	return m_dTunnelFindScanLimiteDistance;
}
int RobotParameter::getTunnelFindLaserScanningObjectValidCount()
{
	return m_nTunnelFindLaserScanningObjectValidCount;
}
int RobotParameter::getTunnelFindCountInvalidValueGap()
{
	return m_nTunnelFindCountInvalidValueGap;
}
double RobotParameter::getTunnelFindLimitPointGap()
{
	return m_dTunnelFindLimitPointGap;
}

int RobotParameter::getTunnelFilteringWheelToHusingGap()
{
	return m_nTunnelFilteringWheelToHusingGap;
}
int RobotParameter::getTunnelFilteringCartSafetyGap()
{
	return m_nTunnelFilteringCartSafetyGap;
}
int RobotParameter::getTunnelFilteringObjectToSafetyGap()
{
	return m_nTunnelFilteringObjectToSafetyGap;
}

double RobotParameter::getTunnelTergetingScanLimiteDistance()
{
	return m_dTunnelTergetingScanLimiteDistance;
}
int RobotParameter::getTunnelTergetingScanLimiteDegree()
{
	return m_nTunnelTergetingScanLimiteDegree;
}
double RobotParameter::getTunnelTergetingWeightValueDistance()
{
	return m_dTunnelTergetingWeightValueDistance;
}
double RobotParameter::getTunnelTergetingWeightValueThDeg()
{
	return m_dTunnelTergetingWeightValueThDeg;
}
double RobotParameter::getTunnelTergetingWeightValueDirection()
{
	return m_dTunnelTergetingWeightValueDirection;
}
int RobotParameter::getTunnelTergetingLimitTraceFailCount()
{
	return m_nTunnelTergetingLimitTraceFailCount;
}

// Get [PRECISION_POSITION_MOVE]
double RobotParameter::getPrecisionPositionFindScanLimiteDistance()
{
	return m_dPrecisionPositionFindScanLimiteDistance;
}
int RobotParameter::getPrecisionPositionFindLaserScanningObjectValidCount()
{
	return m_nPrecisionPositionFindLaserScanningObjectValidCount;
}
int RobotParameter::getPrecisionPositionFindCountInvalidValueGap()
{
	return m_nPrecisionPositionFindCountInvalidValueGap;
}
double RobotParameter::getPrecisionPositionFindLimitPointGap()
{
	return m_dPrecisionPositionFindLimitPointGap;
}

int RobotParameter::getPrecisionPositionFilteringWheelToHusingGap()
{
	return m_nPrecisionPositionFilteringWheelToHusingGap;
}
int RobotParameter::getPrecisionPositionFilteringCartSafetyGap()
{
	return m_nPrecisionPositionFilteringCartSafetyGap;
}
int RobotParameter::getPrecisionPositionFilteringObjectToSafetyGap()
{
	return m_nPrecisionPositionFilteringObjectToSafetyGap;
}

double RobotParameter::getPrecisionPositionTergetingScanLimiteDistance()
{
	return m_dPrecisionPositionTergetingScanLimiteDistance;
}
int RobotParameter::getPrecisionPositionTergetingScanLimiteDegree()
{
	return m_nPrecisionPositionTergetingScanLimiteDegree;
}
double RobotParameter::getPrecisionPositionTergetingWeightValueDistance()
{
	return m_dPrecisionPositionTergetingWeightValueDistance;
}
double RobotParameter::getPrecisionPositionTergetingWeightValueThDeg()
{
	return m_dPrecisionPositionTergetingWeightValueThDeg;
}
double RobotParameter::getPrecisionPositionTergetingWeightValueDirection()
{
	return m_dPrecisionPositionTergetingWeightValueDirection;
}
int RobotParameter::getPrecisionPositionTergetingLimitTraceFailCount()
{
	return m_nPrecisionPositionTergetingLimitTraceFailCount;
}

// Get [GUI_DISP]
int RobotParameter::getLRF1_DISP()
{
	return LRF1_DISP;
}
int RobotParameter::getLRF2_DISP()
{
	return LRF2_DISP;
}
int RobotParameter::getNODE_DISP()
{
	return NODE_DISP;
}
int RobotParameter::getSTATION_DISP()
{
	return STATION_DISP;
}


int RobotParameter::getPATHDATA_DISP()
{
	return PATHDATA_DISP;
}
int RobotParameter::getPARTICLE_DISP()
{
	return PARTICLE_DISP;
}
int RobotParameter::getROBOT_DISP()
{
	return ROBOT_DISP;
}

// [OBSTACLE_RANGE]		//pjs 추가 - ObstacleDetect ini 설정
int RobotParameter::getObstacleRangeDisp()
{
	return m_nObstacleRangeDisp;
}
double RobotParameter::getObstacleStopDistanceMin()
{
	return m_dObstacleStopDistanceMin;
}
double RobotParameter::getObstacleStopDistanceMax()
{
	return m_dObstacleStopDistanceMax;
}
double RobotParameter::getObstacleWarnDistanceMax()
{
	return m_dObstacleWarnDistanceMax;
}
double RobotParameter::getObstacleStopWidthBasic()
{
	return m_dObstacleStopWidthBasic;
}
double RobotParameter::getObstacleStopWidthLoaded()
{
	return m_dObstacleStopWidthLoaded;
}

void RobotParameter::setObstacleRangeDisp(int nObstacleRangeDisp)
{
	m_nObstacleRangeDisp = nObstacleRangeDisp;
}
void RobotParameter::setObstacleStopDistanceMin(double dObstacleStopDistanceMin)
{
	m_dObstacleStopDistanceMin = dObstacleStopDistanceMin;
}
void RobotParameter::setObstacleStopDistanceMax(double dObstacleStopDistanceMax)
{
	m_dObstacleStopDistanceMax = dObstacleStopDistanceMax;
}
void RobotParameter::setObstacleWarnDistanceMax(double dObstacleWarnDistanceMax)
{
	m_dObstacleWarnDistanceMax = dObstacleWarnDistanceMax;
}
void RobotParameter::setObstacleStopWidthBasic(double dObstacleStopWidthBasic)
{
	m_dObstacleStopWidthBasic = dObstacleStopWidthBasic;
}
void RobotParameter::setObstacleStopWidthLoaded(double dObstacleStopWidthLoaded)
{
	m_dObstacleStopWidthLoaded = dObstacleStopWidthLoaded;
}

//double RobotParameter::getOBSTACLE_STOP_DISTANCE_MIN()
//{
//	return m_dObstacle_Stop_Distance_Min;
//}
//
//double RobotParameter::getOBSTACLE_STOP_DISTANCE_MAX()
//{
//	return m_dObstacle_Stop_Distance_Max;
//}
//
//double RobotParameter::getOBSTACLE_STOP_WIDTH_MIN()
//{
//	return m_dObstacle_Stop_Width_Min;
//}
//
//double RobotParameter::getOBSTACLE_STOP_WIDTH_MAX()
//{
//	return m_dObstacle_Stop_Width_Max;
//}
//
//
//double RobotParameter::getOBSTACLE_WARN_DISTANCE_MIN()
//{
//	return m_dObstacle_Warn_Distance_Min;
//}
//
//double RobotParameter::getOBSTACLE_WARN_DISTANCE_MAX()
//{
//	return m_dObstacle_Warn_Distance_Max;
//}
//
//double RobotParameter::getOBSTACLE_WARN_WIDTH_MIN()
//{
//	return m_dObstacle_Warn_Width_Min;
//}
//
//double RobotParameter::getOBSTACLE_WARN_WIDTH_MAX()
//{
//	return m_dObstacle_Warn_Width_Max;
//}
//
////Set
//
//void RobotParameter::setOBSTACLE_STOP_DISTANCE_MIN(double dist)
//{
//	m_dObstacle_Stop_Distance_Min = dist;
//}
//
//void RobotParameter::setOBSTACLE_STOP_DISTANCE_MAX(double dist)
//{
//	m_dObstacle_Stop_Distance_Max = dist;
//}
//
//
//void RobotParameter::setOBSTACLE_STOP_WIDTH_MIN(double width)
//{
//	m_dObstacle_Stop_Width_Min = width;
//}
//
//
//void RobotParameter::setOBSTACLE_STOP_WIDTH_MAX(double width)
//{
//	m_dObstacle_Stop_Width_Max = width;
//}
//
//
//void RobotParameter::setOBSTACLE_WARN_DISTANCE_MIN(double dist)
//{
//	m_dObstacle_Warn_Distance_Min = dist;
//}
//
//void RobotParameter::setOBSTACLE_WARN_DISTANCE_MAX(double dist)
//{
//	m_dObstacle_Warn_Distance_Max = dist;
//}
//
//
//void RobotParameter::setOBSTACLE_WARN_WIDTH_MIN(double width)
//{
//	m_dObstacle_Warn_Width_Min = width;
//}
//
//
//void RobotParameter::setOBSTACLE_WARN_WIDTH_MAX(double width)
//{
//	m_dObstacle_Warn_Width_Max = width;
//}