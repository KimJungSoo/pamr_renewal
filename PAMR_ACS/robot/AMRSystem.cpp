#include "stdafx.h"

#include <time.h>
#include "AMRSystem.h"
#include "sensor\SICKLaserScanner.h"
#include "sensor\GyroSensor.h"
#include "sensor\SICKGuide.h"
#include "util\PMap.h"

using namespace eventManager;
using namespace AMRSystem;
using namespace sensor;
using namespace cv;
using namespace std;

// ******************************* Public *******************************

CAMRSystem::CAMRSystem()
{
	m_bSwitch = false;
	m_kanayama.loadNodePoint();

	m_IOHub = NULL;
	m_sensor = NULL;

	m_nNumOfSensor = 4;
	m_sensor = DEBUG_NEW CSensorModule *[m_nNumOfSensor];
	m_sensor[0] = DEBUG_NEW CSICKLaserScanner("LMS_Front", LMS1xx, "192.168.1.160", 2111, true);
	m_sensor[1] = DEBUG_NEW CSICKLaserScanner("LMS_Rear", LMS1xx, "192.168.1.161", 2111, true);
	m_sensor[2] = DEBUG_NEW CSICKGuide("Guide", 1, 125, 100, 2);
	((CSICKGuide*)m_sensor[2])->setDeviceID(0, 0x18a);
	((CSICKGuide*)m_sensor[2])->setDeviceID(1, 0x18b);
	m_sensor[3] = DEBUG_NEW CGyroSensor("Gyro", 5, 38400);
	m_IOHub = DEBUG_NEW CIOHub("datafiles/inifiles/IO_List.ini");
	m_motionController.SetIO(m_IOHub);

	// Set Init Position
	m_nRobotInitPose_X = 14043;
	m_nRobotInitPose_Y = 19000;
	m_dRobotInitPose_ThetaRad = 0 * D2R;
	m_curPose.setX(m_nRobotInitPose_X);
	m_curPose.setY(m_nRobotInitPose_Y);
	m_curPose.setThetaRad(m_dRobotInitPose_ThetaRad);

	// Set MCL
	m_particleFilter.setNumOfLaserScanner(2);
	m_particleFilter.setSensor((CSICKLaserScanner*)m_sensor[0], 0);
	m_particleFilter.setSensor((CSICKLaserScanner*)m_sensor[1], 1);
	m_particleFilter.setRangeSensorParameter(181, -90, 1, 1000, 450, 450);

	// Set motion controller sensor
	m_motionController.SetSensor(m_sensor[0], 1);
	m_motionController.SetSensor(m_sensor[1], 2);
	m_motionController.SetIO(m_IOHub);


	m_bIsOnLine = false;

	// Set PreciseMove
	m_preciseMove.setSensor(m_sensor[0], 0);
	m_preciseMove.setSensor(m_sensor[1], 1);

	// RobotParameter::getInstance()->initialize();
	m_bStateMachineThreadLoop = false;
	m_bObstacleDetect = false;
	m_eState = SYSTEM_STATE_NOT_INIT;
	m_nThreadPeriod = 100;

	// Command Init
	m_nMaxV = 500; // mm/s
	m_nMaxW = 30; // deg/s
	m_nKeyV = 0;
	m_nKeyW = 0;
}

CAMRSystem::~CAMRSystem() {
	m_IOHub->bitSet(IO_List::DRIVE_SERVO_MOTOR_BRAKE_ON, false);
	m_bStateMachineThreadLoop = false;

	// Thread Terminate
	if (m_StateMachineThread.joinable())
	{
		if (m_StateMachineThread.joinable())
			m_StateMachineThread.join();
	}

	// Memory clear
	AutoCSLock cs(m_cs);
	//m_cmds = {};
	while (!m_cmds.empty()) m_cmds.pop();

	for (int i = 0; i < m_nNumOfSensor; i++)
		m_sensor[i]->Disconnect();

	// Signal 방식으로 교체
	int i = 0;
	while (true) {
		if (m_sensor[i]->getStatus() == SENSOR_STATE_INIT)
			i++;
		break;
	}

	//SAFE_DELETEA(m_IOHub);
	for (int i = 0; i < m_nNumOfSensor; i++) {
		m_sensor[i]->ThreadStop();
		//m_sensor[i]->~CSensorModule();
		//SAFE_DELETE(m_sensor[i]);
	}
	SAFE_DELETE(m_sensor);
}

// Command Function
int CAMRSystem::initialize()
{
	if (m_eState == SYSTEM_STATE_NOT_INIT)
	{
		PushCommand(SYSTEM_CMD_INITIALIZE);
		return RETURN_NONE_ERROR;
	}
	return RETURN_FAILED;
}

int CAMRSystem::pause()
{
	if (m_eState == SYSTEM_STATE_RUNNING)
	{
		PushCommand(SYSTEM_CMD_PAUSE);
		return RETURN_NONE_ERROR;
	}
	return RETURN_FAILED;
}

int CAMRSystem::resume()
{
	if (m_eState == SYSTEM_STATE_PAUSE)
	{
		PushCommand(SYSTEM_CMD_RESUME);
		return RETURN_NONE_ERROR;
	}
	return RETURN_FAILED;
}

int CAMRSystem::errorClear()
{
	if (m_eState == SYSTEM_STATE_ABORTED)
	{
		PushCommand(SYSTEM_CMD_ERROR_CLEAR);
		return RETURN_NONE_ERROR;
	}
	return RETURN_FAILED;
}
// 수동 주행
int CAMRSystem::manualDrive()
{
	if (m_eState == SYSTEM_STATE_READY || m_eState == SYSTEM_STATE_RUNNING)
	{
		PushCommand(SYSTEM_CMD_MANUAL_DRIVE);
		return RETURN_NONE_ERROR;
	}
	return RETURN_FAILED;
}

int CAMRSystem::manualDriveAct()
{
	/* Manual Switch가 On일때
	int errCode;
	if (m_IOHub->bitRead(IO_List::AUTO_MANUAL_SEL, &errCode))
		return RETURN_FAILED;
	*/
	m_motionController.SetVW(m_nKeyV, m_nKeyW* D2R); // mm/s, rad/s
	m_motionController.Move();
	return RETURN_NONE_ERROR;
}

// 자율 주행
int CAMRSystem::autoDrive()
{
	if (m_eState == SYSTEM_STATE_READY || m_eState == SYSTEM_STATE_RUNNING)
	{
		PushCommand(SYSTEM_CMD_AUTO_DRIVE);
		return RETURN_NONE_ERROR;
	}
	return RETURN_FAILED;
}

int CAMRSystem::autoDriveInitialize()
{
	m_kanayama.setKanayamaGain((float)0.12, (float)0.006, (float)0.01);
	if (m_bSwitch) {
		if (m_kanayama.PathPlanOnFile("datafiles/" + g_strAMRVersion + "/pathData1.txt", CPoint((int)m_curPose.getX(), (int)m_curPose.getY())) != RETURN_NONE_ERROR) {
			return RETURN_FAILED;
		}
	}
	else {
		if (m_kanayama.PathPlanOnFile("datafiles/" + g_strAMRVersion + "/pathData.txt", CPoint((int)m_curPose.getX(), (int)m_curPose.getY())) != RETURN_NONE_ERROR) {
			return RETURN_FAILED;
		}
	}
	m_bSwitch = !m_bSwitch;
	return RETURN_NONE_ERROR;
}

int CAMRSystem::autoDriveAct() {
	/* Manual Switch가 On일때
	int errCode;
	if (!m_IOHub->bitRead(IO_List::AUTO_MANUAL_SEL, &errCode))
		return RETURN_FAILED;
	*/
	int ret = m_kanayama.autoFunc(m_curPose);
	if (ret == RETURN_FAILED)
		return RETURN_FAILED;
	m_motionController.SetVW(m_kanayama.getV(), m_kanayama.getW());
	m_motionController.Autonomous();

	if (ret == RETURN_NONE_ERROR)
		cmd = SYSTEM_CMD_DONE;

	return ret;
}

// 정밀주행
int CAMRSystem::preciseMove()
{
	if (m_eState == SYSTEM_STATE_READY || m_eState == SYSTEM_STATE_RUNNING)
	{
		if (m_preciseMove.setTargetLandmarkIndex(0, 1) == RETURN_NONE_ERROR)
			PushCommand(SYSTEM_CMD_PRECISE_MOVE);
		else
		{
			g_eventManager.PushTask(MSG_WARN, WARN_PRECISEMOVE_INVALID_TARGET_LANDMARK_INDEX, true, true);
			return RETURN_FAILED;
		}
		return RETURN_NONE_ERROR;
	}
	return RETURN_FAILED;
}
int CAMRSystem::preciseMoveAct()
{
	/* Manual Switch가 On일때
	int errCode;
	if (!m_IOHub->bitRead(IO_List::AUTO_MANUAL_SEL, &errCode))
	return RETURN_FAILED;
	*/
	m_preciseMove.setRobotPose(m_curPose);
	int ret = m_preciseMove.doPreciseMoving();
	m_motionController.SetVW(m_preciseMove.getV(), m_preciseMove.getW());
	m_motionController.Autonomous();
	return ret;
}

// State Machine Functions
int CAMRSystem::PushCommand(eCommandType input)
{
	if (m_cmds.size() > 5)
		return RETURN_FAILED;
	m_cs.Lock();
	m_cmds.push(input);

	// thread start
	if (!m_StateMachineThread.joinable()) {
		m_bStateMachineThreadLoop = true;
		m_StateMachineThread = std::thread(ThreadFunction_StateMachine, this);
	}
	m_cs.Unlock();
	return RETURN_NONE_ERROR;
}


eCommandType CAMRSystem::PopCommand()
{
	m_cs.Lock();
	eCommandType cmd = m_cmds.front();
	m_cmds.pop();
	m_cs.Unlock();
	return cmd;
}


int CAMRSystem::ThreadFunction_StateMachine(CAMRSystem *system) {
	int nReconnectionCount = 0;
	int nDataCheckCount = 0;
	int nReturnValue = RETURN_NONE_ERROR;
	// Main Loop

	//20190905 SG_ADD
	try {
		while (system->m_bStateMachineThreadLoop) {
			// Clock Start
			clock_t start, runtime;
			start = clock();

			// State Machine Update
			if (!system->m_cmds.empty()) {
				system->cmd = system->PopCommand();
				switch (system->m_eState) {
				case SYSTEM_STATE_NOT_INIT:
					if (system->cmd == SYSTEM_CMD_INITIALIZE) {
						system->m_eState = SYSTEM_STATE_READING;
						if (system->initializeAct() != RETURN_NONE_ERROR)
							system->m_eState = SYSTEM_STATE_NOT_INIT;
					}
					break;
				case SYSTEM_STATE_READY:
					switch (system->cmd) {
					case SYSTEM_CMD_AUTO_DRIVE:
						if (system->m_bIsOnLine)
						{
							// ACS로 자율주행

						}
						else {
							// Path data 가져오기
							if (system->autoDriveInitialize() != RETURN_NONE_ERROR)
								system->m_eState = SYSTEM_STATE_ABORTED;
							else
								system->m_eState = SYSTEM_STATE_RUNNING;
						}
						break;
					case SYSTEM_CMD_MANUAL_DRIVE:
						// if manual switch == true
						system->manualDriveAct();
						system->m_eState = SYSTEM_STATE_RUNNING;
						break;
					case SYSTEM_CMD_GUIDE_DRIVE:
					{
						/*
						if (!system->m_GuideMove.GetThreadFlag())
						{
							system->gideDriveInitialize();
						}
						system->m_eState = SYSTEM_STATE_RUNNING;
						*/
						break;

					}
					case SYSTEM_CMD_STOP:
					{
						system->stopAct();

					case SYSTEM_CMD_PRECISE_MOVE:
						system->m_preciseMove.PreciseMoveStart();
						system->m_eState = SYSTEM_STATE_RUNNING;

						break;
					case SYSTEM_CMD_ESTOP:
						break;
					case SYSTEM_CMD_AUTO_CHARGE:
						break;
					}
					}
				case SYSTEM_STATE_RUNNING:
					switch (system->cmd) {
					case SYSTEM_CMD_PAUSE:
						system->m_eState = SYSTEM_STATE_PAUSE;
						break;
					case SYSTEM_CMD_MANUAL_DRIVE:
						// if manual switch == true
						system->manualDriveAct();
						break;
					case SYSTEM_CMD_STOP:
						if (system->stopAct() == RETURN_NONE_ERROR) {
							system->cmd = SYSTEM_CMD_STOP;
							//system->m_cmds = {};
							
							while (!system->m_cmds.empty()) system->m_cmds.pop();
							
							system->m_eState = SYSTEM_STATE_READY;
						}
						break;
					}
					break;
				case SYSTEM_STATE_PAUSE:
					if (system->cmd == SYSTEM_CMD_RESUME)
						if (system->resumeAct() == RETURN_NONE_ERROR)
							system->m_eState = SYSTEM_STATE_RUNNING;
					break;
				case SYSTEM_STATE_ABORTED:
					if (system->cmd == SYSTEM_CMD_ERROR_CLEAR)
						if (system->errorClearAct() == RETURN_NONE_ERROR)
							system->m_eState = SYSTEM_STATE_READY;
					break;
				}
			}

			// MCL은 항상 실행
			if (system->m_eState == SYSTEM_STATE_READY || system->m_eState == SYSTEM_STATE_RUNNING)
			{
				// Encoder update
				double x = 0.0, y = 0.0, theta = 0.0, yaw = 0.0;
				system->m_motionController.TractionPosition();
				system->m_motionController.GetPosition(&x, &y, &theta);
				//system->m_particleFilter.setDeltaEncoder(x, y, theta);
				//system->m_particleFilter.doThreadKMCL();
				//system->m_curPose = system->m_particleFilter.getRobotPose();

				// World Coor로 변환
				//system->m_curPose.setThetaRad(system->m_dRobotInitPose_ThetaRad *2-system->m_curPose.getThetaRad());
			}

			// 정밀주행
			if (system->m_eState == SYSTEM_STATE_READY || system->m_eState == SYSTEM_STATE_RUNNING)
			{
				system->m_preciseMove.setRobotPose(system->m_curPose);
				if (system->m_preciseMove.getPrecisePosition() == RETURN_NONE_ERROR)
					system->m_curPose = system->m_preciseMove.getPreciseRobotPose();
				CGyroSensor* gyrosensor = dynamic_cast<CGyroSensor*>(system->m_sensor[3]);
				double yaw = 0., d = 0.;
				gyrosensor->getData(&yaw, &d, &d);
				system->m_curPose.setThetaDeg(-yaw);
			}

			// 명령이 들어오지 않을 때 돌아가는 놈들
			switch (system->m_eState)
			{
			case SYSTEM_STATE_READING:
				if (system->stateCheck() == RETURN_CONTINUE)
					break;
				else if (system->stateCheck() == RETURN_FAILED)
					system->m_eState = SYSTEM_STATE_ABORTED;
				else
					system->m_eState = SYSTEM_STATE_READY;
				break;
			case SYSTEM_STATE_READY:
				// 무한 반복하기 위한 테스트 코드
				if (system->cmd == SYSTEM_CMD_DONE)
					system->autoDrive();

				if (system->stateCheck() != RETURN_NONE_ERROR)
					system->m_eState = SYSTEM_STATE_ABORTED;
				break;
			case SYSTEM_STATE_RUNNING:
				switch (system->cmd)
				{
				case SYSTEM_CMD_AUTO_DRIVE:
					nReturnValue = system->autoDriveAct();
					if (nReturnValue == RETURN_CONTINUE) {
						system->m_eState = SYSTEM_STATE_RUNNING;
						system->autoDrive();
					}
					else if (nReturnValue == RETURN_NONE_ERROR)
						system->m_eState = SYSTEM_STATE_READY;
					else
						system->m_eState = SYSTEM_STATE_ABORTED;
					break;
				case SYSTEM_CMD_PRECISE_MOVE:
					if (system->preciseMoveAct() == RETURN_NONE_ERROR)
						system->m_eState = SYSTEM_STATE_READY;
					system->m_eState = SYSTEM_STATE_RUNNING;
					break;
				case SYSTEM_CMD_GUIDE_DRIVE:
				{
					/*
					if (system->guideDriveAct() == RETURN_NONE_ERROR)
					{
						if (system->m_GuideMove.m_bThreadFlag)
						{
							system->guideDrive();
						}
						else
						{
							system->m_eState = SYSTEM_STATE_READY;
						}

					}
					else
					{
						system->m_eState = SYSTEM_STATE_ABORTED;
					}
					*/
					break;
				}

				}
				break;

			}

			// Keep the Thread Period
			runtime = clock() - start;
			if (system->m_nThreadPeriod - runtime > 0)
				Sleep(system->m_nThreadPeriod - runtime);
			else {
				// This thead is Delayed for (runtime - system->m_nThreadPeriod)ms
				Sleep(1);
			}
		}//while
	}//try
	catch (Exception ex)
	{
		std::cout << "ERROR : " << ex.what() << std::endl;
		std::cin.ignore();
	}
	//20190906 SG_ADD
	std::cout << "Thread End : ThreadFunction_StateMachine" << std::endl;

	return RETURN_NONE_ERROR;
}



// Command Act Function
int CAMRSystem::initializeAct()
{
	for (int i = 0; i < m_nNumOfSensor; i++)
		m_sensor[i]->Connect();
	m_IOHub->Connect();
	m_IOHub->bitSet(IO_List::DRIVE_SERVO_MOTOR_BRAKE_ON, true);
	int nErrorCode;
	if (!m_IOHub->bitRead(IO_List::DRIVE_SERVO_MOTOR_BRAKE_ON, &nErrorCode))
		m_IOHub->bitSet(IO_List::DRIVE_SERVO_MOTOR_BRAKE_ON, true);

	return RETURN_NONE_ERROR;
}

int CAMRSystem::resumeAct()
{
	Sleep(1000);
	return RETURN_NONE_ERROR;
}

int CAMRSystem::errorClearAct()
{
	for (int i = 0; i < m_nNumOfSensor; i++)
		m_sensor[i]->Reset();
	return RETURN_NONE_ERROR;
}

// System Function
int CAMRSystem::stateCheck()
{
	// Motion Controller State Check
	bool AllSensorReady = true;
	for (int i = 0; i < m_nNumOfSensor; i++)
	{
		// 센서 모듈 고장
		if (m_sensor[i]->getStatus() == SENSOR_STATE_ERROR)
			return RETURN_FAILED;
		if (m_sensor[i]->getStatus() != SENSOR_STATE_RUN)
			AllSensorReady = false;
	}

	// Sensor State Check
	if (AllSensorReady)
		return RETURN_NONE_ERROR;
	else
		return RETURN_CONTINUE;
}

int CAMRSystem::emergency()
{
	return RETURN_NONE_ERROR;
}

int CAMRSystem::stopAct()
{
	m_motionController.Stop();
	return RETURN_NONE_ERROR;
}

int CAMRSystem::stop()
{
	PushCommand(SYSTEM_CMD_STOP);
	return RETURN_NONE_ERROR;
}

int CAMRSystem::initMCL(Mat map)
{
	int ** nMap;
	nMap = DEBUG_NEW int *[map.cols];
	for (int i = 0; i < map.cols; i++)
		nMap[i] = DEBUG_NEW int[map.rows];

	// Map copy
	for (int i = 0; i < map.cols; i++) {
		for (int j = 0; j < map.rows; j++) {
			if (map.at<Vec3b>(j, i)[0] < 100)
				nMap[i][j] = PMap::WARNING_AREA;
			else if (map.at<Vec3b>(j, i)[0] > 200)
				nMap[i][j] = PMap::EMPTY_AREA;
			else
				nMap[i][j] = PMap::UNKNOWN_AREA;
		}
	}
	m_particleFilter.setRobotPose(m_curPose);
	m_particleFilter.setMap(map.cols, map.rows, nMap);
	m_particleFilter.setSamplesNearRobot(m_curPose.getX(), m_curPose.getY(), m_curPose.getThetaRad(), 300);
	m_particleFilter.ResetReservation();

	for (int i = 0; i < map.cols; i++)
		SAFE_DELETEA(nMap[i]);
	SAFE_DELETEA(nMap);
	return RETURN_NONE_ERROR;
}


// Getter & Setter
int CAMRSystem::getStatus()
{
	return m_eState;
}

int CAMRSystem::getSensor(int index, CSensorModule** sensor)
{
	if (index < 0 || index > m_nNumOfSensor)
		return RETURN_FAILED;
	sensor[index] = m_sensor[index];
	return RETURN_NONE_ERROR;
}

int CAMRSystem::getIOHub(CIOHub ** sensor)
{
	*sensor = m_IOHub;
	return RETURN_NONE_ERROR;
}

int CAMRSystem::getNumOfSensor()
{
	return m_nNumOfSensor;
}

int CAMRSystem::setNumOfSensor(int nNum)
{
	if (nNum < 0)
		return RETURN_FAILED;
	m_nNumOfSensor = nNum;
	return RETURN_NONE_ERROR;
}

void CAMRSystem::setKeyCommand(int nV, int nW)
{
	// 키보드 수동 조작
	// 선속도
	if (abs(nV) > m_nMaxV)
		nV > 0 ? m_nKeyV = m_nMaxV : m_nKeyV = -m_nMaxV;
	else
		m_nKeyV = nV;

	if (abs(nW) > m_nMaxW)
		nW > 0 ? m_nKeyW = m_nMaxW : m_nKeyW = -m_nMaxW;
	else
		m_nKeyW = nW;
}

int CAMRSystem::guideDriveAct()
{
	/*
	m_GuideMove.SetUseGyro(true);
	setGuideDataUpdate();

	m_motionController.SetVW(m_GuideMove.GetV(), m_GuideMove.GetW());
	m_motionController.Move();
	*/
	return RETURN_NONE_ERROR;
}

int CAMRSystem::guideDrive()
{
	if (m_eState == SYSTEM_STATE_READY || m_eState == SYSTEM_STATE_RUNNING)
	{
		PushCommand(SYSTEM_CMD_GUIDE_DRIVE);
		return RETURN_NONE_ERROR;
	}
	return RETURN_NONE_ERROR;
}

int CAMRSystem::setGuideDataUpdate()
{
	/*
	CSICKGuide* guidesensor = dynamic_cast<CSICKGuide*>(m_sensor[2]);
	CGyroSensor* gyrosensor = dynamic_cast<CGyroSensor*>(m_sensor[3]);

	short lcp1 = 0, lcp2 = 0, lcp3 = 0;
	double yaw = 0.0;
	int iTempNode = 301;

	m_GuideMove.SetFrontGuideLineExist(guidesensor->getLineExists(0));
	m_GuideMove.SetRearGuideLineExist(guidesensor->getLineExists(1));
	gyrosensor->getThetaDeg(eYaw, &yaw);
	m_GuideMove.SetGyroTheta(yaw);

	guidesensor->getLcpData(0, &lcp1, &lcp2, &lcp3);
	m_GuideMove.SetFrontLcpData(lcp1, lcp2, lcp3);
	lcp2 = 0.0;
	guidesensor->getLcpData(1, &lcp1, &lcp2, &lcp3);
	m_GuideMove.SetRearLcpData(lcp1, lcp2, lcp3);

	m_GuideMove.SetFrontMarker(guidesensor->getMarker(0));
	m_GuideMove.SetRearMarker(guidesensor->getMarker(1));

	m_GuideMove.SetRobotPose(getCurrentPose());
	m_GuideMove.SetTargetNode(iTempNode);
	*/
	return RETURN_NONE_ERROR;
}

int CAMRSystem::gideDriveInitialize()
{
	/*
	if (!m_GuideMove.m_bThreadFlag)
	{
		if (m_GuideMove.m_GuideMoveThread.joinable())
		{
			m_GuideMove.m_GuideMoveThread.join();
			m_GuideMove.initialize();
		}		
	}
	*/
	return RETURN_NONE_ERROR;
}