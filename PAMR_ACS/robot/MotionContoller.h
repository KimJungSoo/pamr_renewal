#pragma once

#include "sensor\SensorModule.h"
#include "sensor\IOHub.h"
#include "sensor\SICKLaserScanner.h"
#include "sensor\GyroSensor.h"
#include "ObstacleChecker.h"
#include "robot\RobotPose.h"

namespace robot
{
	enum eAMRMotionCommand
	{
		CMD_READY = 0,
		CMD_MOVE,
		CMD_AUTONOMOUS,
		CMD_ROTATE,
		CMD_STOP,
		CMD_EMG_STOP,
		CMD_ERROR,
	};

	enum eAMRStatus {
		STATE_AMR_INIT = 0,
		STATE_AMR_PROGRESSING,
		STATE_AMR_RUN,
		STATE_AMR_ERROR_SENSOR_STATE,
		STATE_AMR_ERROR_EMERGENCY,
		STATE_AMR_ERROR_CRASH,
		STATE_AMR_ERROR_OBSTACLE,
	};



	class CMotionController
	{
	private:
		int m_AMRState;
		std::string m_strModuleName;
		// Sensor
		int m_nNumOfSensor;
		sensor::CSensorModule ** m_sensor;
		sensor::CIOHub * m_IOHub;
		int m_nObstacle;
		int m_nThreadPeriod;
		int m_nTerminateTimeout;
		// PINIReadWriter *PINIReaderWriter;

		// Obstacle Checker
		CObstacleChecker m_obstacleChecker;
		int m_nObstacleX;
		int m_nObstacleY;

		// Main Control Thread
		std::queue<eAMRMotionCommand> m_cmds;
		int PushCommand(eAMRMotionCommand input);
		eAMRMotionCommand PopCommand();
		std::thread m_AMRControlThread;
		CriticalSection m_cs;
		bool m_bMotionControlThreadLoop;
		static int ThreadFunctionMotionControl(CMotionController *);

	public:
		CMotionController();
		virtual ~CMotionController();

		int m_iSystemCommand;
		double m_dAMRVelocity;
		double m_dAMR_W;
		bool m_bIsFirstTraction;
		bool m_bIsFirstEncoderInput;
		double m_dGyroValuePrev;
		bool m_bUseGyro;

		double m_dReferenceX;
		double m_dReferenceY;
		double m_dReferenceTh;
		RobotPose m_rDeltaEncoderPos;
		RobotPose r_pos;
		double m_dGeomThetaDeg;
		double m_dGeomDeltaDeg;
		double m_dGyroThetaDeg;
		double m_dGyroDeltaDeg;
		double m_dReferenceWheelEncoderCount[2];

		// Getter & Setter
		int getStatus();
		int getThreadPeriod();
		std::string getModuleName();
		int setThreadPeriod(int p);
		void setModuleName(std::string s);

		int getObstacleX() { return m_nObstacleX; }
		int getObstacleY() { return m_nObstacleY; }
		int getIOHub(sensor::CIOHub ** sensor);
		int getState();
		int getCommand();
		int setCommand(int iCmd) { return m_iSystemCommand = iCmd; }
		double getVelocity();
		double getW();
		int TractionPosition();
		bool setUseGyro(bool bUse) { return m_bUseGyro = bUse; }
		bool isUseGyro() { return m_bUseGyro; }
		int TrimAngle(std::string strUnit, double& dAngle);

		int GetGeomThetaDeg(double *dDeg);
		int GetGeomDeltaDeg(double *dDeg);
		int GetGyroThetaDeg(double *dDeg);
		int GetGyroThetaRad(double *dDeg);

		int SetVW(double dVelocity, double dW);
		int GetPosition(double *dX, double *dY, double *dAngle);
		int SetSensor(sensor::CSensorModule* LaserScanner, int index);
		int SetIO(sensor::CIOHub* IOHub);
		int SetGyro(sensor::CSensorModule* Gyro);
		int getSensor(int, sensor::CSensorModule**);
		int getObstacleObj(CObstacleChecker*);
		int Initialize();

		//Command Function
		void Move();
		void Rotate();
		void Autonomous();
		void Stop();
		void EmgergencyStop();


	};
}
