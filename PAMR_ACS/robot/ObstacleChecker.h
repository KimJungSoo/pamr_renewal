/**
@file ObstacleChecker.h

@date 2019/08/22

@author Soon ( sgchoi@potenit.com )

@version 1.0 
@brief obstacle check class 
*/

#pragma once

#include "PTN_DataStruct.h"
//#include "sensor\SensorData.h"

#define PI 3.14159265358979323846
#define D2R 0.017453292

#define AMRSIZE 11.f //AMR 크기는 10이나 쓰레기 값으로 인해 +1해줌

#define STOPZONE_STEPW	10	/** @def Stop zone 가로 */
#define STOPZONE_STEPH	10	/** @def Stop zone 세로 */


#define WARINGZONE_STEPW	10  /** @def Waring zone 가로 */
#define WARINGZONE_STEPH	10  /** @def Waring zone 세로 */

#define WARINGZONE_SIZE 4		/** @def waring zone 개수 */

#pragma comment(lib,"opencv_core2413d")
#pragma comment(lib,"opencv_highgui2413d")

//
//typedef struct _LaserScanData {
//	int nAngleResolution = 0;
//	int nStart_angle = 0;
//	int nEnd_angle = 0;
//	int nData_len = 0;
//
//	unsigned short *dist;
//	unsigned short *rssi;
//} LaserScanData;

namespace robot
{
	/**
	@enum	ObstacleZone
	@brief	obatacle 결과
	*/
	enum eObstacleZoneCheck
	{
		eNONE = 0,
		eWARNING = 1,
		eSTOP = 2
	};

	enum eAMRMotion
	{
		eGoWay = 0,
		eGoWay_TurnRight,
		eTurnRight,
		eBackWay_TurnRight,
		eBackWay,
		eBackWay_TurnLeft,
		eTurnLeft,
		eGoWay_TurnLeft,
		eReset
	};

	class CObstacleChecker
	{
	public:
		CObstacleChecker();
		~CObstacleChecker();

		
	private:
		/** @brief Stop / warning zone */
		ObstacleZone	m_sStopZone;
		ObstacleZone	m_sWarningZone[WARINGZONE_SIZE]; // 0 - 전방 / 1 - 우측 / 2 - 후방 / 3 - 좌측
		AMRModel		m_sAmrModel;
	private:
		void ObstacleZoneInit();
		void GetSickData2PointData(RobotPose sensorPos, LaserScanData* scanData, std::vector<PTN_Points>& _GetPoints);
		bool RectFindPoint(double x1, double y1, double x2, double y2, double x, double y);
		eObstacleZoneCheck WaringZoneResize(eAMRMotion _eNum, double _dVelocity, double _dW, std::vector<PTN_Points> _Points);
		eObstacleZoneCheck ObstacleZoneCheck(std::vector<PTN_Points> _Points);
		void ScannerToRobotCoor(RobotPose sensorPos, LaserScanData scanData, int index, double *X, double *Y);
	public:
		int GetObstacleZone(double _dVelocity, double _dW, LaserScanData * scanData_f, LaserScanData * scanData_r);
	};
}