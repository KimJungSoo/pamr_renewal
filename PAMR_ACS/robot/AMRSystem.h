#pragma once
#include <queue>
#include <string>
#include <thread>
#include "util\CriticalSection.h"
#include "sensor\SensorModule.h"
#include "sensor\IOHub.h"
#include "robot\RobotPose.h"
#include "MotionContoller.h"
#include "Algorithm\Kanayama\kanayama.h"
#include "Algorithm\ParticleFilter\ParticleFilter.h"
#include "AMRmove\PreciseMove.h"
#include "opencv2\opencv.hpp"
#include "AMRmove\GuideMove.h"

namespace AMRSystem {
	enum eCommandType {
		SYSTEM_CMD_INITIALIZE,
		// Job List
		SYSTEM_CMD_AUTO_DRIVE,
		SYSTEM_CMD_MANUAL_DRIVE,
		SYSTEM_CMD_PRECISE_MOVE,
		SYSTEM_CMD_GUIDE_DRIVE,
		SYSTEM_CMD_STOP,
		SYSTEM_CMD_ESTOP,
		SYSTEM_CMD_AUTO_CHARGE,
		// ***********
		SYSTEM_CMD_PAUSE,
		SYSTEM_CMD_RESUME,
		SYSTEM_CMD_ERROR_CLEAR,
		SYSTEM_CMD_DONE,
	};
	enum eStatus {
		SYSTEM_STATE_NOT_INIT,
		SYSTEM_STATE_READY,
		SYSTEM_STATE_READING,
		SYSTEM_STATE_RUNNING,
		SYSTEM_STATE_PAUSE,
		SYSTEM_STATE_ABORTED,
	};

	class CAMRSystem {
	private:
		// State Machine 변수
		eStatus m_eState;
		eCommandType cmd;
		std::thread m_StateMachineThread;
		bool m_bStateMachineThreadLoop;
		std::queue<eCommandType> m_cmds;
		CriticalSection m_cs;
		static int ThreadFunction_StateMachine(CAMRSystem *);
		int m_nThreadPeriod;

		// AMR System 변수
		std::string strRobotVersion;
		int m_nNumOfSensor;
		bool m_bObstacleDetect;
		bool m_bIsOnLine;
		int m_nMaxV;
		int m_nMaxW;
		bool m_bSwitch;

		// 로봇 위치
		RobotPose m_curPose;

		// 센서
		sensor::CSensorModule **m_sensor;
		sensor::CIOHub * m_IOHub;
		
		// State Machine Function
		eCommandType PopCommand();
		int PushCommand(eCommandType);

		// System Core Functiob
		int stateCheck();
		int emergency();

		// Keyboard Command
		int m_nKeyV;
		int m_nKeyW;

	public:
		int m_nRobotInitPose_X;
		int m_nRobotInitPose_Y;
		double m_dRobotInitPose_ThetaRad;

		// 주행 모듈
		// CGuideMove m_GuideMove;
		CParticleFilter m_particleFilter;
		robot::CMotionController m_motionController;
		AMRmove::CPreciseMove m_preciseMove;
		Ckanayama m_kanayama;

		CAMRSystem();
		~CAMRSystem();
		
		int initMCL(cv::Mat map);

		// Getter & Setter
		int getStatus();
		int getSensor(int index, sensor::CSensorModule** sensor);
		int getIOHub(sensor::CIOHub ** sensor);
		int getNumOfSensor();
		int setNumOfSensor(int nNum);
		void setCurrentPose(RobotPose input) { m_curPose = input; }
		RobotPose getCurrentPose() { return m_curPose; }
		void setKeyCommand(int nV, int nW);
		int setGuideDataUpdate();
		
		// Command Function
		int initialize();
		int pause();
		int resume();
		int errorClear();
		int autoDrive();
		int manualDrive();
		int guideDrive();
		int preciseMove();
		int stop();
	private:
		// Command Act Function
		int initializeAct();
		int resumeAct();
		int errorClearAct();
		int autoDriveInitialize();
		int autoDriveAct();
		int manualDriveAct();
		int preciseMoveAct();
		int guideDriveAct();
		int gideDriveInitialize();
		int stopAct();
		int emergencyStopAct();
		int autoChargeAct();
	};
}