#include "stdafx.h"
#include "ObstacleChecker.h"

#include <thread>
#include <vector>
#include <mutex>

#include "./SSV_OpenGLViwer.h"
#pragma comment(lib,"OpenGL_Win32x86d")

SSV_OpenGLViwer m_oviewer;
robot::CObstacleChecker::CObstacleChecker()
{
	m_oviewer.StartOpenGLViewer();

	/*AME Model Point info
	0	1
	3	2
	*/
	m_sAmrModel.fx[0] = 0.f;
	m_sAmrModel.fx[1] = AMRSIZE;
	m_sAmrModel.fx[2] = AMRSIZE;
	m_sAmrModel.fx[3] = 0.f;

	m_sAmrModel.fy[0] = 0.f;
	m_sAmrModel.fy[1] = 0.f;
	m_sAmrModel.fy[2] = -AMRSIZE;
	m_sAmrModel.fy[3] = -AMRSIZE;


	ObstacleZoneInit();				//초기화 함수
	//*
	m_oviewer.SetStopZone(m_sStopZone);	//Stop영역
	m_oviewer.SetWarningZone(m_sWarningZone);	//Stop영역
	m_oviewer.SetAMR_Model(m_sAmrModel);
	//*/
}


robot::CObstacleChecker::~CObstacleChecker()
{

}

/**
@fn void robot::CObstacleChecker::ObstacleZoneInit()
@brief obstacle zone init
@date 2019/08/26
*/
void robot::CObstacleChecker::ObstacleZoneInit()
{
	///New Vision
	///AMR 기준으로 변경
	{//StopZone - AMR Size 기준
		m_sStopZone.fLeftTopX = m_sAmrModel.fx[0] - STOPZONE_STEPW;
		m_sStopZone.fLeftTopY = m_sAmrModel.fy[0] + STOPZONE_STEPH;
		m_sStopZone.fRightBottomX = m_sAmrModel.fx[2] + STOPZONE_STEPW;
		m_sStopZone.fRightBottomY = m_sAmrModel.fy[2] - STOPZONE_STEPH;
	}

	{//Waring Zone init - Stop 기준
		//전방
		m_sWarningZone[0].fLeftTopX		= m_sStopZone.fLeftTopX - WARINGZONE_STEPW;
		m_sWarningZone[0].fLeftTopY		= m_sStopZone.fLeftTopY	+ WARINGZONE_STEPH;
		m_sWarningZone[0].fRightBottomX	= m_sStopZone.fRightBottomX + WARINGZONE_STEPH;
		m_sWarningZone[0].fRightBottomY	= m_sStopZone.fLeftTopY;

		//우측
		m_sWarningZone[1].fLeftTopX		= m_sStopZone.fRightBottomX ;
		m_sWarningZone[1].fLeftTopY		= m_sStopZone.fLeftTopY;
		m_sWarningZone[1].fRightBottomX = m_sStopZone.fRightBottomX + WARINGZONE_STEPW;
		m_sWarningZone[1].fRightBottomY = m_sStopZone.fRightBottomY;

		//후방
		m_sWarningZone[2].fLeftTopX		= m_sStopZone.fLeftTopX - WARINGZONE_STEPW;
		m_sWarningZone[2].fLeftTopY		= m_sStopZone.fRightBottomY;
		m_sWarningZone[2].fRightBottomX	= m_sStopZone.fRightBottomX + WARINGZONE_STEPW;
		m_sWarningZone[2].fRightBottomY	= m_sStopZone.fRightBottomY - WARINGZONE_STEPW;

		//좌측
		m_sWarningZone[3].fLeftTopX		= m_sStopZone.fLeftTopX - WARINGZONE_STEPW;
		m_sWarningZone[3].fLeftTopY		= m_sStopZone.fLeftTopY;
		m_sWarningZone[3].fRightBottomX = m_sStopZone.fLeftTopX;
		m_sWarningZone[3].fRightBottomY = m_sStopZone.fRightBottomY;
	}
}

bool robot::CObstacleChecker::RectFindPoint(double x1, double y1, double x2, double y2, double x, double y)
{
	if (x >= x1 && x <= x2 && y <= y1 && y >= y2)
		return true;
	return false;
}

/**
@fn robot::eObstacleZoneCheck robot::CObstacleChecker::ObstacleZoneCheck(std::vector<PTN_Points> _Points)
@brief 좌측탑 / 우측 바텀 point를 이용해 사각 영역내 검사 point check
@param _Points Sick points
@date 2019/08/26
*/
robot::eObstacleZoneCheck robot::CObstacleChecker::ObstacleZoneCheck(std::vector<PTN_Points> _Points)
{
	int iWaringDataCnt[WARINGZONE_SIZE] = {0,}; // 4방향의 waring존 전방 -> 우측 -> 후방 -> 좌측 / 0 -> 1 -> 2 -> 3
	int iWaringTotalCnt = 0;					// Waring존 개수
	int iStopDataCnt = 0;						// Stop존 개수
	
	for (int p = 0; p < (int)_Points.size(); ++p)//point  개수만큼
	{
		//std::cout << p << std::endl;
		//AMR 영역내 포인트 continue
		if (!RectFindPoint(m_sAmrModel.fx[0], m_sAmrModel.fy[0], m_sAmrModel.fx[2], m_sAmrModel.fy[2], _Points[p].fx, _Points[p].fy))
		{
			//Stop영역 내 포인트 check
			if (RectFindPoint(m_sStopZone.fLeftTopX, m_sStopZone.fLeftTopY, m_sStopZone.fRightBottomX, m_sStopZone.fRightBottomY, _Points[p].fx, _Points[p].fy))
				iStopDataCnt++;

			for (int i = 0; i < WARINGZONE_SIZE; ++i)//Waring존 만큼
			{
				//4 방향의 WarningZone 포인트 check
				if (RectFindPoint(m_sWarningZone[i].fLeftTopX, m_sWarningZone[i].fLeftTopY, m_sWarningZone[i].fRightBottomX, m_sWarningZone[i].fRightBottomY, _Points[p].fx, _Points[p].fy))
				{
					iWaringDataCnt[i]++;
					iWaringTotalCnt++;
				}
			}//for (int i = 0; i < WARINGZONE_SIZE; ++i)
		}//if (!RectFindPoint(m_sAmrModel.fx[0], m_sAmrModel.fy[0], m_sAmrModel.fx[2], m_sAmrModel.fy[2], x, y))
	}//for (int p = 0; p < 360; ++p)

	//{//Show Text
	//	std::cout << "iStopDataCnt : " << iStopDataCnt << std::endl;
	//	for (int i = 0; i < WARINGZONE_SIZE; ++i)
	//		std::cout << "iWaringDataCnt[" << i << "]" << iWaringDataCnt[i] << std::endl;
	//}

	if (iStopDataCnt > 0)			return robot::eSTOP;
	else if (iWaringTotalCnt > 0)	return robot::eWARNING;
	else return						robot::eNONE;
}
/**
@fn void robot::CObstacleChecker::WaringZoneResize(eAMRMotion _eNum, double _dVelocity, double _dW)
@brief obstacle zone Setting
@param _eNum AMR motion
@date 2019/08/22
*/
robot::eObstacleZoneCheck robot::CObstacleChecker::WaringZoneResize(eAMRMotion _eNum, double _dVelocity, double _dW, std::vector<PTN_Points> _Points)
{
	ObstacleZoneInit();
	float fgo_backfront		= (float)(_dVelocity*0.1);	//1% 직진 / 후진시 증가값
	float fgo_backSideStep	= (float)(_dVelocity*0.2);	//2% 직진 / 후진시 사이드 증가값
	float fVelocity			= (float)(_dVelocity*0.5);	//5% _dVelocity값 전방 / 후진 증가

	switch (_eNum)
	{
	case robot::eGoWay: // 직진
		/*전*/m_sWarningZone[0].fLeftTopY		+= fVelocity;
		/*전*/m_sWarningZone[0].fLeftTopX		-= fgo_backSideStep;
		/*전*/m_sWarningZone[0].fRightBottomX	+= fgo_backSideStep;
		/*우*/m_sWarningZone[1].fRightBottomX	+= fgo_backSideStep;
		/*후*/m_sWarningZone[2].fRightBottomY	-= fgo_backfront;//직진 / 후진시 증가값
		/*좌*/m_sWarningZone[3].fLeftTopX		-= fgo_backSideStep;
		break;
	case robot::eGoWay_TurnRight: //직우
		/*전*/m_sWarningZone[0].fLeftTopY		+= fVelocity;
		/*전*/m_sWarningZone[0].fRightBottomX	= m_sStopZone.fRightBottomX + fgo_backSideStep;
		/*우*/m_sWarningZone[1].fRightBottomX	= m_sStopZone.fRightBottomX + fgo_backSideStep;
		/*후*/m_sWarningZone[2].fRightBottomX	= m_sStopZone.fRightBottomX + fgo_backSideStep;
		/*후*/m_sWarningZone[2].fRightBottomY	-= fgo_backfront;//직진 / 후진시 증가값
		break;
	case robot::eTurnRight:	break;//우턴
	case robot::eBackWay_TurnRight://후우
		/*전*/m_sWarningZone[0].fRightBottomX	= m_sStopZone.fRightBottomX + fgo_backSideStep;
		/*전*/m_sWarningZone[0].fLeftTopY		+= fgo_backfront;//직진 / 후진시 증가값
		/*우*/m_sWarningZone[1].fRightBottomX	= m_sStopZone.fRightBottomX + fgo_backSideStep;
		/*후*/m_sWarningZone[2].fRightBottomX	= m_sStopZone.fRightBottomX + fgo_backSideStep;
		/*후*/m_sWarningZone[2].fRightBottomY	-= fVelocity;
		break;
	case robot::eBackWay://후
		/*전*/m_sWarningZone[0].fLeftTopY		+= fgo_backfront;//직진 / 후진시 증가값
		/*우*/m_sWarningZone[1].fRightBottomX	+= fgo_backSideStep;
		/*후*/m_sWarningZone[2].fRightBottomY	-= fVelocity;
		/*후*/m_sWarningZone[2].fRightBottomX	+= fgo_backSideStep;
		/*후*/m_sWarningZone[2].fLeftTopX		-= fgo_backSideStep;
		/*좌*/m_sWarningZone[3].fLeftTopX		-= fgo_backSideStep;
		break;
	case robot::eBackWay_TurnLeft://후좌
		/*전*/m_sWarningZone[0].fLeftTopY		+= fgo_backfront;//직진 / 후진시 증가값
		/*전*/m_sWarningZone[0].fLeftTopX		= m_sStopZone.fLeftTopX - fgo_backSideStep;
		/*후*/m_sWarningZone[2].fLeftTopX		= m_sStopZone.fLeftTopX - fgo_backSideStep;
		/*후*/m_sWarningZone[2].fRightBottomY	-= fVelocity;
		/*좌*/m_sWarningZone[3].fLeftTopX		= m_sStopZone.fLeftTopX - fgo_backSideStep;
		break;
	case robot::eTurnLeft:break;//좌턴
	case robot::eGoWay_TurnLeft://직좌
		/*전*/m_sWarningZone[0].fLeftTopY		+= fgo_backfront;//직진 / 후진시 증가값
		/*전*/m_sWarningZone[0].fLeftTopX		= m_sStopZone.fLeftTopX - fgo_backSideStep;
		/*후*/m_sWarningZone[2].fLeftTopX		= m_sStopZone.fLeftTopX - fgo_backSideStep;
		/*후*/m_sWarningZone[2].fRightBottomY	-= fgo_backSideStep;
		/*좌*/m_sWarningZone[3].fLeftTopX		= m_sStopZone.fLeftTopX - fgo_backSideStep;
		break;
	case robot::eReset:
		break;
	default:
		break;
	}

	
	m_oviewer.SetPoints(_Points);
	m_oviewer.SetWarningZone(m_sWarningZone);
	return ObstacleZoneCheck(_Points);
}

/**
@fn  robot::CObstacleChecker::GetSickData2PointData(LaserScanData* scanData, std::vector<PTN_Points>& _GetPoints)
@brief SickData를 xy 좌표로 변환
@param scanData sickdata
@param _GetPoints sick2point(x,y)
@date 2019/08/22
*/
void robot::CObstacleChecker::GetSickData2PointData(RobotPose sensorPos, LaserScanData* scanData, std::vector<PTN_Points>& _GetPoints)
{
	PTN_Points p;
	double dX, dY;
	for (int i = 0; i < scanData->nData_len; i++)
	{
		ScannerToRobotCoor(sensorPos, *scanData, i, &dX, &dY);
		p.fx = float(dX)/100.f;
		p.fy = float(dY)/100.f;
		_GetPoints.push_back(p);
	}
}

int robot::CObstacleChecker::GetObstacleZone(double _dVelocity, double _dW, LaserScanData * scanData_f, LaserScanData * scanData_r)
{
	PTN_Points testpoint;
	std::vector<PTN_Points> points;
	RobotPose LMS_Front_Pos, LMS_Rear_Pos;
	LMS_Front_Pos.setX(450);
	LMS_Front_Pos.setY(450);
	LMS_Front_Pos.setThetaDeg(-45);
	LMS_Rear_Pos.setX(-450);
	LMS_Rear_Pos.setY(-450);
	LMS_Rear_Pos.setThetaDeg(225);

	GetSickData2PointData(LMS_Front_Pos, scanData_f, points); //sick2points
	GetSickData2PointData(LMS_Rear_Pos, scanData_r, points); //sick2points
	//std::cout << points.size() << std::endl;
	eAMRMotion amrMotion = eReset;
	// 전진
	if(_dVelocity > 0.)
	{
		if (_dW > 0.)
			amrMotion = eGoWay_TurnRight;
		else if (_dW < 0.)
			amrMotion = eGoWay_TurnLeft;
		else
			amrMotion = eGoWay;
	}
	// 후진
	else if (_dVelocity < 0.)
	{
		if (_dW > 0.)
			amrMotion = eBackWay_TurnRight;
		else if (_dW < 0.)
			amrMotion = eBackWay_TurnLeft;
		else
			amrMotion = eBackWay;
	}
	// 제자리
	else
	{
		if (_dW > 0.)
			amrMotion = eTurnRight;
		else if (_dW < 0.)
			amrMotion = eTurnLeft;
		else
			amrMotion = eReset;
	}

	robot::eObstacleZoneCheck check = WaringZoneResize(amrMotion, _dVelocity, _dW, points);

	points.clear();
	return check;
}

void robot::CObstacleChecker::ScannerToRobotCoor(RobotPose sensorPos, LaserScanData scanData, int index, double *X, double *Y) {
	double deg = double(scanData.nStart_angle - sensorPos.getThetaDeg()) + (index / double(scanData.nAngleResolution));
	UINT16 dist = scanData.dist[index];
	*X = sensorPos.getX() + dist * cos(deg*D2R);
	*Y = sensorPos.getY() + dist * sin(deg*D2R);
}