#include "stdafx.h"
#include "RobotPose.h"


RobotPose::RobotPose(void)
{
	init();
}


RobotPose::~RobotPose(void)
{
}
void RobotPose::init()
{ 
	m_dPixX = m_dPixY = 0; //영상 픽셀값
	m_dX = 0.; //unit mm
	m_dY = 0.; //unit mm
	m_dZ = 0.; //unit mm
	m_dThetaDeg = 0.; //unit degree
	m_dThetaRad = 0.; //unit radian
	m_dDist=0.0;
	m_nID=-1;
	m_dPro=0.0;

}

/**
@brief Korean: 영상 픽셀의 X좌표를 다른 클래스에서 가져감
@brief English: 
*/
double RobotPose::getPixX()
{
	return m_dPixX;
}

/**
@brief Korean: 영상 픽셀의 Y좌표를 다른 클래스에서 가져감
@brief English: 
*/
double RobotPose::getPixY()
{
	return m_dPixY;
}

/**
@brief Korean: 다른 클래스에서 영상 픽셀의 X좌표를 가져온 후 저장함
@brief English : 
*/
void RobotPose::setPixX(double dPixX)
{
	m_dPixX = dPixX;
}

/**
@brief Korean:  다른 클래스에서 영상 픽셀의 Y좌표를 가져온 후 저장함
@brief English : 
*/
void RobotPose::setPixY(double dPixY)
{
	m_dPixY = dPixY;
}

/**
@brief Korean: 절대좌표상의 X(mm)좌표를 다른 클래스에서 가져감
@brief English : 
*/
double RobotPose::getX()
{
	return m_dX;
}

/**
@brief Korean: 절대좌표상의 Y(mm)좌표를 다른 클래스에서 가져감
@brief English : 
*/
double RobotPose::getY()
{
	return m_dY;
}		

/**
@brief Korean: 절대좌표상의 Z(mm)좌표를 다른 클래스에서 가져감
@brief English : 
*/
double RobotPose::getZ()
{
	return m_dZ;
}

/**
@brief Korean: 절대좌표상의 (degree)각도를 다른 클래스에서 가져감
@brief English : 
*/
double RobotPose::getThetaDeg() 
{ 
	return m_dThetaDeg; 
}

/**
@brief Korean: 절대좌표상의 (radian)각도를 다른 클래스에서 가져감
@brief English : 
*/
double RobotPose::getThetaRad() 
{ 
	return m_dThetaRad;
}

/**
@brief Korean: 다른 클래스에서 절대좌표상의 X(mm)좌표를 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setX(double dX)
{ 
	m_dX = dX; 
}

/**
@brief Korean: 다른 클래스에서 절대좌표상의 Y(mm)좌표를 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setY(double dY)
{ 
	m_dY = dY; 
}

/**
@brief Korean: 다른 클래스에서 절대좌표상의 Z(mm)좌표를 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setZ(double dZ)
{ 
	m_dZ = dZ; 
}

/**
@brief Korean: 다른 클래스에서 절대좌표상의 X(m)좌표를 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setXm(double dXm)
{
	m_dX = dXm*1000;
}

/**
@brief Korean: 다른 클래스에서 절대좌표상의 Y(m)좌표를 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setYm(double dYm)
{
	m_dY = dYm*1000;
}

/**
@brief Korean: 다른 클래스에서 절대좌표상의 Z(m)좌표를 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setZm(double dZm)
{
	m_dZ = dZm*1000;
}

/**
@brief Korean: 절대좌표상의 X(m)좌표를 다른 클래스에서 가져감
@brief English : 
*/
double RobotPose::getXm()
{
	return m_dX/1000.;
}

/**
@brief Korean: 절대좌표상의 Y(m)좌표를 다른 클래스에서 가져감
@brief English : 
*/
double RobotPose::getYm()
{
	return m_dY/1000.;
}

/**
@brief Korean: 절대좌표상의 Z(m)좌표를 다른 클래스에서 가져감
@brief English : 
*/
double RobotPose::getZm()
{
	return m_dZ/1000.;
}

/**
@brief Korean: 다른 클래스에서 절대좌표상의 (degree)각도를 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setThetaDeg(double dThetaDeg)
{ 
	m_dThetaDeg = dThetaDeg;  
	if(m_dThetaDeg > 180) m_dThetaDeg -=360;
	if(m_dThetaDeg < -180) m_dThetaDeg +=360;
	m_dThetaRad = m_dThetaDeg*3.141592/180;
} 

/**
@brief Korean: 다른 클래스에서 절대좌표상의 (radian)각도를 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setThetaRad(double dThetaRad)
{ 
	if (dThetaRad > PI)	m_dThetaRad = dThetaRad - 2.0*PI;
	if (dThetaRad < -PI) m_dThetaRad = dThetaRad + 2.0*PI;
	m_dThetaRad = dThetaRad;
	m_dThetaDeg = m_dThetaRad*180/PI;
}

/**
@brief Korean: 절대좌표상의 거리를 다른 클래스에서 가져감
@brief English : 
*/
double RobotPose::getDist()
{ 
	return m_dDist;
}

/**
@brief Korean: 지도에서 저장되어 있는 랜드마크들의 ID값을 다른 클래스에서 가져감
@brief English : 
*/
int RobotPose::getID()
{
	return m_nID;
}

/**
@brief Korean: 다른 클래스에서 절대좌표상의 거리를 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setDist(double dDist)
{
	m_dDist = dDist;	
}

/**
@brief Korean: 다른 클래스에서 지도에서 저장되어 있는 랜드마크들의 ID값을 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setID(int nID)
{
	m_nID = nID;
}

/**
@brief Korean: 다른 클래스에서 확률 값을 가져온 후 저장함 
@brief English : 
*/
void RobotPose::setPro(double dPro)
{
	m_dPro = dPro;
}

/**
@brief Korean:  저장된 확률 값을  다른 클래스에서 가져감
@brief English : 
*/
double RobotPose::getPro()
{
	return m_dPro;
}