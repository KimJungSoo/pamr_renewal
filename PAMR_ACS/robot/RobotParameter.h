#pragma once
#include "util/PINIReadWriter.h"
//#include "../util/PUtil.h"
#include "util/PSingletone.h"
#include "RobotPose.h"

typedef struct _tag_NodePointFromServer
{
	int no;
	int x;					// mm
	int y;					// mm
	int th_deg;				// 시작점 방향 정보를 추가
	int obs_flag;			// 장애물 감지 범위 - 0 : 사용안함, 1~n : 정의된 범위
	int max_v;				// 구간별 속도 설정(0 : 기존 제한 속도, 기타 : 구간별 제한 속도)
	int max_a;				// 구간별 각속도 설정(0 : 기존 제한 각속도, 기타 : 구간별 제한 각속도)
	int doorArea;			// Door 구간 설정(0 : 일반지역, 1 : Door Area)
	int reflector_flag;		// Reflector 구간 설정(0 : 일반지역, 1~ : Reflector Mode)
	// 1 : LASER_SCANNING_REFLECTOR_MODE_POINT_STOP : 반사판 이용하여 정지시키는 모드(반사판 2개 사용(2개서 1개로 변하는 시점에 정지, 간격은 약 1m)
	// 2 : LASER_SCANNING_REFLECTOR_MODE_GUIDE_FIND : 반사판 이용하여 반사판 방향으로 회전시키고, 설정 거리만큼 이동시키는 모드(반사판 1개 사용)
	// 3 : LASER_SCANNING_REFLECTOR_MODE_POINT_COUNT : 반사판 이용하여 위치를 Count or 보정하는 모드(반사판 1개 사용, 간격은 약 10m)
	int reflector_useSide;	// Reflector 구간 사용(0 : 전체사용, 1 : 왼쪽만 사용, 2 : 오른쪽만 사용)
} NodePointFromServer;

typedef struct _tag_NodePointForRobot
{
	int no;
	int x;					// grid
	int y;					// grid
	int th_deg;				// 시작점 방향 정보를 추가
	int obs_flag;			// 장애물 감지 범위 - 0 : 사용안함, 1~n : 정의된 범위
	int max_v;				// 구간별 속도 설정(0 : 기존 제한 속도, 기타 : 구간별 제한 속도)
	int max_a;				// 구간별 각속도 설정(0 : 기존 제한 각속도, 기타 : 구간별 제한 각속도)
	int doorArea;			// Door 구간 설정(0 : 일반지역, 1 : Door Area)
	int reflector_flag;		// Reflector 구간 설정(0 : 일반지역, 1~ : Reflector Mode)
	// 1 : LASER_SCANNING_REFLECTOR_MODE_POINT_STOP : 반사판 이용하여 정지시키는 모드(반사판 2개 사용(2개서 1개로 변하는 시점에 정지, 간격은 약 1m)
	// 2 : LASER_SCANNING_REFLECTOR_MODE_GUIDE_FIND : 반사판 이용하여 반사판 방향으로 회전시키고, 설정 거리만큼 이동시키는 모드(반사판 1개 사용)
	// 3 : LASER_SCANNING_REFLECTOR_MODE_POINT_COUNT : 반사판 이용하여 위치를 Count or 보정하는 모드(반사판 1개 사용, 간격은 약 10m)
	int reflector_useSide;	// Reflector 구간 사용(0 : 전체사용, 1 : 왼쪽만 사용, 2 : 오른쪽만 사용)
} NodePointForRobot;


typedef struct _tag_StationPointFromServer
{
	int no;
	int x;					// mm
	int y;					// mm
	int th_deg;				// 시작점 방향 정보를 추가
	int order;			// 1단, 2단, 3은 싱글랙

} StationPointFromServer;

typedef struct _tag_StationPointForRobot
{
	int no;
	int x;					// mm
	int y;					// mm
	int th_deg;				// 시작점 방향 정보를 추가
	int order;			// 1단, 2단, 3은 싱글랙
} StationPointForRobot;

typedef struct _tag_ChargerPoint
{
	int no;
	int x;					// mm 위치 및 초기화시 리셋할 값
	int y;					// mm
	int th_deg;				// 가이드 주행후 출발할 방향, 시작점 방향 정보를 추가
	int pre_node_no;		// 이전 노드 번호
	int guide_control_direction; // 가이드 제어 방향, 0: 전진, 1: 후진
	int obs_flag;			// 장애물 감지 범위 - 0 : 사용안함, 1~n : 정의된 범위
	int max_v;				// 구간별 속도 설정(0 : 기존 제한 속도, 기타 : 구간별 제한 속도)
	int max_a;				// 구간별 각속도 설정(0 : 기존 제한 각속도, 기타 : 구간별 제한 각속도)
	int doorArea;			// Door 구간 설정(0 : 일반지역, 1 : Door Area)
	int reflector_flag;		// Reflector 구간 설정(0 : 일반지역, 1~ : Reflector Mode)
							// 1 : LASER_SCANNING_REFLECTOR_MODE_POINT_STOP : 반사판 이용하여 정지시키는 모드(반사판 2개 사용(2개서 1개로 변하는 시점에 정지, 간격은 약 1m)
							// 2 : LASER_SCANNING_REFLECTOR_MODE_GUIDE_FIND : 반사판 이용하여 반사판 방향으로 회전시키고, 설정 거리만큼 이동시키는 모드(반사판 1개 사용)
							// 3 : LASER_SCANNING_REFLECTOR_MODE_POINT_COUNT : 반사판 이용하여 위치를 Count or 보정하는 모드(반사판 1개 사용, 간격은 약 10m)
	int reflector_useSide;	// Reflector 구간 사용(0 : 전체사용, 1 : 왼쪽만 사용, 2 : 오른쪽만 사용)
} ChargerPoint;

typedef struct _tag_WorkPlacePoint
{
	int no;
	int x;					// mm 위치 및 초기화시 리셋할 값
	int y;					// mm
	int th_deg;				// 가이드 주행후 출발할 방향, 시작점 방향 정보를 추가
	int pre_node_no;		// 이전 노드 번호
	int guide_control_direction; // 가이드 제어 방향, 0: 전진, 1: 후진
	int obs_flag;			// 장애물 감지 범위 - 0 : 사용안함, 1~n : 정의된 범위
	int max_v;				// 구간별 속도 설정(0 : 기존 제한 속도, 기타 : 구간별 제한 속도)
	int max_a;				// 구간별 각속도 설정(0 : 기존 제한 각속도, 기타 : 구간별 제한 각속도)
	int doorArea;			// Door 구간 설정(0 : 일반지역, 1 : Door Area)
	int reflector_flag;		// Reflector 구간 설정(0 : 일반지역, 1~ : Reflector Mode)
							// 1 : LASER_SCANNING_REFLECTOR_MODE_POINT_STOP : 반사판 이용하여 정지시키는 모드(반사판 2개 사용(2개서 1개로 변하는 시점에 정지, 간격은 약 1m)
							// 2 : LASER_SCANNING_REFLECTOR_MODE_GUIDE_FIND : 반사판 이용하여 반사판 방향으로 회전시키고, 설정 거리만큼 이동시키는 모드(반사판 1개 사용)
							// 3 : LASER_SCANNING_REFLECTOR_MODE_POINT_COUNT : 반사판 이용하여 위치를 Count or 보정하는 모드(반사판 1개 사용, 간격은 약 10m)
	int reflector_useSide;	// Reflector 구간 사용(0 : 전체사용, 1 : 왼쪽만 사용, 2 : 오른쪽만 사용)
} WorkPlacePoint;


using namespace std;

class RobotParameter : public PSingletone <RobotParameter>
{
public:
	RobotParameter();
	virtual ~RobotParameter();

private:
	PINIReadWriter* PINIReaderWriter;

	// [SERVER]
	string m_strServerIP;
	int m_nServerPort;

	// [MAP]
	string m_strMapNameNPath ;
	int m_nMapSizeXm ; //지도 x,y 크기 단위: m로 설정
	int m_nMapSizeYm ;
	int m_dMApOffset_X;
	int m_dMApOffset_Y;
	double m_dHeight;
	double m_dSDThres;

	// [PATH]
	string m_strNodePointNameNPath ;
	string m_strPathDataNameNPath;
	string m_strStationPointNameNPath;
	string m_strChargerPointNameNPath;
	string m_strWorkPlacePointNameNPath;
	string m_strLastRobotPoseNameNPath;
	string m_strInitRobotPoseNameNPath;			//pjs 추가 - 로봇 위치 초기화 설정


	// [ROBOT]
	int m_nRobotID;
	string m_strAMR_IP;
	int m_nAMR_Port;
	string m_strAMR_WLAN_Name;

	double m_dInitialPositionX;
	double m_dInitialPositionY;
	double m_dInitialPositionTh;

	double m_dRadiusofRobot;		//로봇 반경, 장애물 회피등에 사용(단위: mm)
	double m_dWheelBaseofRobot;
	double m_dGearRatio;
	int m_nEncoderResolution;
	double m_dWheelRadius;

	double m_dWeightAngleVelocity;
	double m_dMaxRobotVelocity ;		//로봇의 최대 속도, 로봇이 최소 속도
	double m_dMinRobotVelocity ;
	double m_dMaxRobotAngleVelocity;
	double m_dMaxRobotAutonomusVelocity;	//pjs 추가 - 자율 주행 최대 속도를 설정, 단 MAX_VELOCITY을 넘을 수 없음
	double m_dMaxRobotGuideVelocity ;
	double m_dMaxRobotGuideAngleVelocity ;
	double m_dMaxRobotGuideVelocitySlow ;
	double m_dMaxRobotGuideAngleVelocitySlow ;
	double m_dRobotRotationAngleSpeed;
	int m_nRobotStraightMoveSpeed;

	RobotPose m_initRobotPosForMap;
	int m_nUsingLastPose;
	RobotPose m_initRobotPosForNav;
	string m_strGlobalLocalization;
	int m_nObstacleDetectionTime;
	int m_nLocalization;

	int m_nRunTime;
	int m_nLoadingMode;
	int m_nFirstMuteOn;

	//pjs 추가 - Battery Min, Max, ChargeRequired
	double m_dBatteryVoltageMax;		
	double m_dBatteryVoltageMin;
	double m_dChargeRequiredPersent;
	//int m_nUseAutoChargingStation;
	//double m_dAutoChargingCompleteVoltage;
	//double m_dChargingAlarmCompleteVoltage;
	int m_nUseDevicePowerSeparationMode;
	int m_nUseLowStandbyPowerMode;

	int m_nUseBreakMode;

	int m_nUseReset_EmergencyStopMode;			// 0 = Pause, 1 = Reset

	double m_dLift_Up_Limit;
	double m_dLift_Down_Limit;
	//pjs 2019.03.18 Lift Motor 개수에 따라서 다르게 적용 //0 : Lift Motor Count 2, 1 : Lift Motor Count 1
	int m_nLiftMotorType;		//0 : Lift Motor Count 2, 1 : Lift Motor Count 1



	// [SENSOR]
	int m_nGyroComPort;

	int m_nGuideSensorCount;
	int m_nGuideComPort1;
	int m_nGuideComPort2;
	//pjs 2019.03.08 GuideCan Port 설정
	int m_nGuideCanPort;

	int m_nGuideStopMarkSensorFront;		// 0 = Default, 1 = Left, 2 = Right, 3 = Rear Left, 4 = Rear Right
	int m_nGuideStopMarkSensorRear;			// 0 = Default, 1 = Left, 2 = Right, 3 = Front Left, 4 = Front Right
	int m_nUseMatchDualGuideStopMarkSensor;		// Guide Stop Mark Sensor 입력이 동시에 들어오는 경우만 처리하는 모드 사용 여부

	int m_nMagneticGuideSensorXOffset;		// mm => Magnetic Guide Sensor 설치 위치와 AMR 중심(구동 축)
	int m_nMagneticGuideSensorWidth;		// mm => Magnetic 감지 Sensor 1번부터 끝번까지의 거리

	double m_dMagneticGuideSensorMin;
	double m_dMagneticGuideSensorMax;
	double m_dMagneticGuideFindAngle;

	int m_nBatteryChecker;

	int m_nLaserScannerType;
	int m_nLaserScannerCount;	// 사용하는 레이져 스케너의 개수
	int m_nLaserScannerHead;	// 레이져 스케너의 장착 방향(0 -> 정방향 / 1-> 역방향)

	string m_strSICKLMSLaserIP_1;
	int m_nLaserTCPPort1;
	string m_strSICKLMSLaserIP_2;
	int m_nLaserTCPPort2;

	int m_nLaserMaxDist;		// 상단 레이저의 최대 탐지거리를 저장하는 변수. mm단위
	int m_nLaserMinDist;		// 상단 레이저의 최소 탐지거리를 저장하는 변수. mm단위

	int m_nLaserHeight;			// 틸트 축으로 부터 상단 레이저까지의 높이값을 저장하는 변수. mm단위
	int m_nLaserXOffset;		// 틸트 축으로 부터 상단 레이저까지의 x offset을 저장하는 변수 mm단위
	int m_nLaserYOffset;		// 틸트 출으로 부터 상단 레이저까지의 y offset을 저장하는 변수 mm단위


	int m_nLaserHeight1;			// 틸트 축으로 부터 상단 레이저까지의 높이값을 저장하는 변수. mm단위
	int m_nLaserXOffset1;		// 틸트 축으로 부터 상단 레이저까지의 x offset을 저장하는 변수 mm단위
	int m_nLaserYOffset1;		// 틸트 출으로 부터 상단 레이저까지의 y offset을 저장하는 변수 mm단위

	int m_nLaserHeight2;			// 틸트 축으로 부터 상단 레이저까지의 높이값을 저장하는 변수. mm단위
	int m_nLaserXOffset2;		// 틸트 축으로 부터 상단 레이저까지의 x offset을 저장하는 변수 mm단위
	int m_nLaserYOffset2;		// 틸트 출으로 부터 상단 레이저까지의 y offset을 저장하는 변수 mm단위

	// [KANAYAMA_MOTION_CONTROL]
	int m_nDistToTarget;
	int m_nDesiredVel ;
	int m_nGoalArea;
	double m_dKX ;
	double m_dKY ;
	double m_dKT ;
	int m_nDirectionofRotation;
	
	// [PARTICLE_FILTER]
	int m_nMaxParticleNum;		// 최대, 최소 particle 갯수. 로봇의 위치추정에 사용
	int m_nMinParticleNum ;
	double m_dDevationforTrans ;
	double m_dDevationforRotate ;
	double m_dDevationforTransRotate ;

	double m_Accum_Dist_Global;
	double m_Accum_Angle_Global;
	double m_Accum_Dist_Local;
	double m_Accum_Angle_Local;

	//[MOTOR_DRIVER]
	int m_nRobotMotorComport;
	int m_nRoboCubeDriver;
	int m_nMotorDirection;

	//[DIGITAL_IO]
	int m_nIo;

	//[PHIDGETS]
	int m_nQuantity;
	int m_PhidgetsSerialNumber1;
	int m_PhidgetsSerialNumber2;

	// Phidget 정/역상 상태를 ini에서 불러온 데이터로 저장(0 : Forward, 1: Reverse)
	int m_nPhidget1InCh[16];
	int m_nPhidget1OutCh[16];
	int m_nPhidget2InCh[16];
	int m_nPhidget2OutCh[16];

	int m_nPhidget1InputMatching[50];
	int m_nPhidget1OutputMatching[50];
	int m_nPhidget2InputMatching[50];
	int m_nPhidget2OutputMatching[50];

/////////////////////////////////////////////////////////////////////////////////////////////
//
//	m_nPhidget1InputMatching[50];
//
//	00 : IN_EMO_SW
//	01 : IN_BUMPER
//	02 : IN_START_SW
//	03 : IN_STOP_SW
//	04 : IN_RESET_SW
//	05 : IN_LOW_BATTERY
//	06 : IN_LIFT_L_UP
//	07 : IN_LIFT_L_DOWN
//	08 : IN_LIFT_R_UP
//	09 : IN_LIFT_R_DOWN
//	10 : IN_CART_CHECK_F
//	11 : IN_CART_CHECK_R
//	12 : IN_MARK_SERSOR
//	13 : IN_MOTOR_LIFT_UP
//	14 : IN_MOTOR_LIFT_DOWN
//
//	m_nPhidget1OutputMatching[50];
//
//	00 : OUT_LEFT_LED_R
//	01 : OUT_LEFT_LED_G
//	02 : OUT_LEFT_LED_B
//	03 : OUT_RIGHT_LED_R
//	04 : OUT_RIGHT_LED_G
//	05 : OUT_RIGHT_LED_B
//	06 : OUT_SPECKER_CH1
//	07 : OUT_SPECKER_CH2
//	08 : OUT_SPECKER_CH3
//	09 : OUT_SPECKER_CH4
//	10 : OUT_LIFT_L
//	11 : OUT_LIFT_R
//	12 : OUT_START_SW_LED
//	13 : OUT_STOP_SW_LED
//	14 : OUT_RESET_SW_LED
//	15 : OUT_CENTER_LED_R
//	16 : OUT_CENTER_LED_G
//	17 : OUT_CENTER_LED_B
//	18 : OUT_MOTOR_RELEASE
//	19 : OUT_MOTOR_LIFT
//
/////////////////////////////////////////////////////////////////////////////////////////////

	// [OBJECT_FOLLOW]
	int m_nFindUseReflector;
	int m_nFindCountInvalidValueReflector;
	int m_nFindCountInvalidValueGap;
	double m_dFindLimitPointGap;

	int m_nFilteringLimitObjectWidthMax;
	int m_nFilteringLimitObjectWidthMin;
	int m_nFilteringTraceRange;
	int m_nFilteringLimitTraceTime;

	double m_dTargetingWeightValueDistance;
	double m_dTargetingWeightValueThDeg;
	int m_nTargetingTergeting;
	int m_nTargetingLimitObjectDistanceInitial;
	int m_nTargetingLimitTraceFailCount;
	int m_nTargetTraceRange;

	double m_dMoveObjectFlollowMaxV;
	double m_dMoveFollowGap;
	double m_dMoveMaxScanDistance;
	double m_dMoveRotateRange;

	//[TUNNEL_MOVE]
	double m_dTunnelFindScanLimiteDistance;
	int m_nTunnelFindLaserScanningObjectValidCount;
	int m_nTunnelFindCountInvalidValueGap;
	double m_dTunnelFindLimitPointGap;

	int m_nTunnelFilteringWheelToHusingGap;
	int m_nTunnelFilteringCartSafetyGap;
	int m_nTunnelFilteringObjectToSafetyGap;

	double m_dTunnelTergetingScanLimiteDistance;
	int m_nTunnelTergetingScanLimiteDegree;
	double m_dTunnelTergetingWeightValueDistance;
	double m_dTunnelTergetingWeightValueThDeg;
	double m_dTunnelTergetingWeightValueDirection;
	int m_nTunnelTergetingLimitTraceFailCount;

	// [PRECISION_POSITION_MOVE]
	double m_dPrecisionPositionFindScanLimiteDistance;
	int m_nPrecisionPositionFindLaserScanningObjectValidCount;
	int m_nPrecisionPositionFindCountInvalidValueGap;
	double m_dPrecisionPositionFindLimitPointGap;

	int m_nPrecisionPositionFilteringWheelToHusingGap;
	int m_nPrecisionPositionFilteringCartSafetyGap;
	int m_nPrecisionPositionFilteringObjectToSafetyGap;

	double m_dPrecisionPositionTergetingScanLimiteDistance;
	int m_nPrecisionPositionTergetingScanLimiteDegree;
	double m_dPrecisionPositionTergetingWeightValueDistance;
	double m_dPrecisionPositionTergetingWeightValueThDeg;
	double m_dPrecisionPositionTergetingWeightValueDirection;
	int m_nPrecisionPositionTergetingLimitTraceFailCount;

	// [GUI_DISP]
	int LRF1_DISP;
	int LRF2_DISP;
	int NODE_DISP;
	int STATION_DISP;

	int PATHDATA_DISP;
	int PARTICLE_DISP;
	int ROBOT_DISP;

public:
	bool initialize();		// 시스템 설정에 관련된 모든 변수들을 초기화 시켜주는 함수
	void saveParameter();

	// Set [SERVER]
	void setServerIP(string strServerIP);
	void setServerPort(int nServerPort);

	// Set [MAP]
	void setMapNameNPath(string sMapName);		// 작성할 지도의 이름
	void setMapSize(int nSizeXm, int nSizeYm);	// 지도크기를 설정하는 함수
	void setMapXOffset(int dXoffset);
	void setMapYOffset(int dYoffset);
	void setHeight(double dHeight);
	void setSDValue(double dSDValue);

	// Set [PATH]
	void setNodePointNameNPath(string sMapName);
	void setPathDataNameNPath(string sMapName);
	void setStationPointNameNPath(string sMapName);
	void setChargerPointNameNPath(string sMapName);
	void setWorkPlacePointNameNPath(string sMapName);
	void setLastRobotPoseNameNPath(string strLastRobotPoseNameNPath);		//pjs 추가 - 마지막 위치 저장 및 사용
	void setInitRobotPoseNameNPath(string strInitRobotPoseNameNPath);		//pjs 추가 - 로봇 위치 초기화 설정

	// Set [ROBOT]
	void setRobotID(int nID);		// 로봇의 ID를 설정해주는 함수.
	void setAMR_IP(string strAMR_IP);
	void setAMR_Port(int nAMR_Port);
	void setAMR_WLAN_Name(string strAMR_WLAN_Name);

	void setInitialPositionX(double dInitialPositionX);
	void setInitialPositionY(double dInitialPositionY);
	void setInitialPositionTh(double dInitialPositionTh);

	void setRobotRadius(double dRadius);		// 로봇 반경을 설정해주는 함수(단위 mm)
	void setWheelBaseofRobot(double dWheelBaseofRobot);
	void setGearRatio(double dGearRatio);
	void setEncoderResolution(int nEncoderResolution);
	void setWheelRadius(double dWheelRadius);

	void setWeightAngleVelocity(double dWeightAngleVelocity);
	void setMaxRobotVelocity(double dMaxVelocity);		// 로봇의 최대속도를 설정하는 함수
	void setMinRobotVelocity(double dMinVelocity);		// 로봇의 최소속도를 설정하는 함수
	void setMaxRobotAngleVelocity(double MaxRobotAngleVelocity);
	void setMaxRobotAutonomusVelocity(double MaxRobotAutonomusVelocity);	//pjs 추가 - 자율 주행 최대 속도를 설정, 단 MAX_VELOCITY을 넘을 수 없음
	void setMaxRobotGuideVelocity(double dMaxRobotGuideVelocity);
	void setMaxRobotGuideAngleVelocity(double dMaxRobotGuideAngleVelocity);
	void setMaxRobotGuideVelocitySlow(double dMaxRobotGuideVelocitySlow);
	void setMaxRobotGuideAngleVelocitySlow(double dMaxRobotGuideAngleVelocitySlow);
	void setRobotRotationAngleSpeed(double dRobotRotationAngleSpeed);
	void setRobotStraightMoveSpeed(int nRobotStraightMoveSpeed);

	void setInitRobotPoseForMap(RobotPose initRobotPos);
	void setUsingLastPose(int nUsingLastPose);
	void setInitRobotPoseForNav(RobotPose initRobotPos);
	//void setLastRobotPoseNameNPath(string strLastRobotPoseNameNPath);
	void setGlobalLocalization(string sGlobalLocalization);
	void setObstacleDetectionTime(int nObstacleDetectionTime);
	void setLocalization(int nLocalization);

	void setAMRRunigtime(int nRuningTime);
	void setLoadingMode(int nLoadingMode);
	void setFirstMuteOn(int nFirstMuteOn);
	//pjs 추가 - Battery Min, Max, ChargeRequired
	void setBatteryVoltageMax(double dBatteryVoltageMax);
	void setBatteryVoltageMin(double dBatteryVoltageMin);
	void setChargeRequiredPersent(double dChargeRequiredPersent);
	//void setUseAutoChargingStation(int nUseAutoChargingStation);
	//void setAutoChargingCompleteVoltage(double dAutoChargingCompleteVoltage);
	//void setChargingAlarmCompleteVoltage(double dChargingAlarmCompleteVoltage);
	void setUseDevicePowerSeparationMode(int nUseDevicePowerSeparationMode);
	void setUseLowStandbyPowerMode(int nUseLowStandbyPowerMode);

	void setUseBreakMode(int nUseBreakMode);

	void setUseReset_EmergencyStopMode(int nUseReset_EmergencyStopMode);

	// Set [SENSOR]
	void setGyroComPort(int nGyroComPort);		// Gyro Com Port를 설정하는 함수

	void setGuideSensorCount(int nGuideSensorCount);		// Guide Sensor의 개수를 설정하는 함수
	void setGuideComPort1(int nGuideComPort);		// Guide Com Port를 설정하는 함수
	void setGuideComPort2(int nGuideComPort);		// Guide Com Port를 설정하는 함수

	void setGuideStopMarkSensorFront(int nGuideStopMarkSensorFront);
	void setGuideStopMarkSensorRear(int nGuideStopMarkSensorRear);
	void setUseMatchDualGuideStopMarkSensor(int nUseMatchDualGuideStopMarkSensor);

	void setMagneticGuideSensorXOffset(int nMagneticGuideSensorXOffset);
	void setMagneticGuideSensorWidth(int nMagneticGuideSensorWidth);

	void setMagneticGuideSensorMin(double dMagneticGuideSensorMin);
	void setMagneticGuideSensorMax(double dMagneticGuideSensorMax);
	void setMagneticGuideFindAngle(double dMagneticGuideFindAngle);

	void setBatteryChecker(int nBatterChecker);

	void setLaserScannerType(int nLaserScannerType );
	void setLaserScannerCount(int nLaserScannerCount);		// 레이져 스케너의 개수를 설정하는 함수
	void setLaserScannerHead(int nLaserScannerHead);		// 레이져 스케너의 설치 방향을 설정하는 함수

	void setSICKLMSLaserComport1(string strLaserIP);
	void setLaserTCPPort1(int nLaserTCPPort);
	void setSICKLMSLaserComport2(string strLaserIP);
	void setLaserTCPPort2(int nLaserTCPPort);

	void setLaserMaxDist(int nMaxDist);		// 레이저의 최대 탐지거리를 설정하는 함수
	void setLaserMinDist(int nMinDist);		// 레이저의 최소 탐지거리를 설정하는 함수

	void setLaserHeight(int nHeight);		// 바닥에서 레이저까지의 높이값을 설정하는 함수
	void setLaserXOffset(int nXOffset);		// 로봇의 중심에서 레이저까지의 x offset을 설정하는 함수
	void setLaserYOffset(int nYOffset);		// 로봇의 중심에서 틸트 축으로 부터 상단 레이저까지의 y offset을 설정하는 함수

	void setLaserHeight1(int nHeight);		// 바닥에서 레이저까지의 높이값을 설정하는 함수
	void setLaserXOffset1(int nXOffset);		// 로봇의 중심에서 레이저까지의 x offset을 설정하는 함수
	void setLaserYOffset1(int nYOffset);		// 로봇의 중심에서 틸트 축으로 부터 상단 레이저까지의 y offset을 설정하는 함수

	void setLaserHeight2(int nHeight);		// 바닥에서 레이저까지의 높이값을 설정하는 함수
	void setLaserXOffset2(int nXOffset);		// 로봇의 중심에서 레이저까지의 x offset을 설정하는 함수
	void setLaserYOffset2(int nYOffset);		// 로봇의 중심에서 틸트 축으로 부터 상단 레이저까지의 y offset을 설정하는 함수


	// Set [KANAYAMA_MOTION_CONTROL]
	void setTargetDistance(int nDistToTarget);
	void setDesiedVel(int nDesiredVel);
	void setGoalArea(int nGoalArea);
	void setdKX(double  dKX);
	void setdKY(double  dKY);
	void setdKT(double  dKT);
	void setDirectionofRotation(int nDirectionofRotation);

	// Set [PARTICLE_FILTER]
	void setMaxParticleNum(int nMaxNum);		// 최대 particle 갯수를 설정하는 함수
	void setMinParticleNUm(int nMinNUm);		// 최소 particle 갯수를 설정하는 함수
	void setDeviationforTrans(double  dDevationforTrans);
	void setDeviationforRotae(double  dDevationforRotate);
	void setDeviationforTransRotae(double  dDevationforTransRotate);

	// Set [MOTOR_DRIVER]
	void setRobotMotorComport(int nRobotMotorComport);
	void setRoboCubeMotordriver(int nRoboCubeDriver);
	void setMotorDrection(int nMotorDirection);

	// Set [DIGITAL_IO]
	void setIODevice(int nIo);
		
	// Set [PHIDGETS]
	void setPhidgetsDeviceQuantity(int nQuantity);
	void setDeviceSerialNumber1(int PhidgetsSerialNumber1);
	void setDeviceSerialNumber2(int PhidgetsSerialNumber2);

	void setPhidget1InCh(int nChNum, int nChData);
	void setPhidget1OutCh(int nChNum, int nChData);
	void setPhidget2InCh(int nChNum, int nChData);
	void setPhidget2OutCh(int nChNum, int nChData);

	void setPhidget1InputMatching(int nNum, int nData);
	void setPhidget1OutputMatching(int nNum, int nData);
	void setPhidget2InputMatching(int nNum, int nData);
	void setPhidget2OutputMatching(int nNum, int nData);

	// Set [OBJECT_FOLLOW]
	void setFindUseReflector(int nFindUseReflector);
	void setFindCountInvalidValueReflector(int nFindCountInvalidValueReflector);
	void setFindCountInvalidValueGap(int nFindCountInvalidValueGap);
	void setFindLimitPointGap(double dFindLimitPointGap);

	void setFilteringLimitObjectWidthMax(int nFilteringLimitObjectWidthMax);
	void setFilteringLimitObjectWidthMin(int nFilteringLimitObjectWidthMin);
	void setFilteringTraceRange(int nFilteringTraceRange);
	void setFilteringLimitTraceTime(int nFilteringLimitTraceTime);

	void setTargetingWeightValueDistance(double dTargetingWeightValueDistance);
	void setTargetingWeightValueThDeg(double dTargetingWeightValueThDeg);
	void setTargetingTergeting(int nTargetingTergeting);
	void setTargetingLimitObjectDistanceInitial(int nTargetingLimitObjectDistanceInitial);
	void setTargetingLimitTraceFailCount(int nTargetingLimitTraceFailCount);
	void setTargetTraceRange(int nTargetTraceRange);

	void setMoveObjectFlollowMaxV(double dMoveObjectFlollowMaxV);
	void setMoveFollowGap(double dMoveFollowGap);
	void setMoveMaxScanDistance(double dMoveMaxScanDistance);
	void setMoveRotateRange(double dMoveRotateRange);

	// Set [TUNNEL_MOVE]
	void setTunnelFindScanLimiteDistance(double dTunnelFindScanLimiteDistance);
	void setTunnelFindLaserScanningObjectValidCount(int nTunnelFindLaserScanningObjectValidCount);
	void setTunnelFindCountInvalidValueGap(int nTunnelFindCountInvalidValueGap);
	void setTunnelFindLimitPointGap(double dTunnelFindLimitPointGap);

	void setTunnelFilteringWheelToHusingGap(int nTunnelFilteringWheelToHusingGap);
	void setTunnelFilteringCartSafetyGap(int nTunnelFilteringCartSafetyGap);
	void setTunnelFilteringObjectToSafetyGap(int nTunnelFilteringObjectToSafetyGap);

	void setTunnelTergetingScanLimiteDistance(double dTunnelTergetingScanLimiteDistance);
	void setTunnelTergetingScanLimiteDegree(int nTunnelTergetingScanLimiteDegree);
	void setTunnelTergetingWeightValueDistance(double dTunnelTergetingWeightValueDistance);
	void setTunnelTergetingWeightValueThDeg(double dTunnelTergetingWeightValueThDeg);
	void setTunnelTergetingWeightValueDirection(double dTunnelTergetingWeightValueDirection);
	void setTunnelTergetingLimitTraceFailCount(int nTunnelTergetingLimitTraceFailCount);

	// Set [PRECISION_POSITION_MOVE]
	void setPrecisionPositionFindScanLimiteDistance(double dPrecisionPositionFindScanLimiteDistance);
	void setPrecisionPositionFindLaserScanningObjectValidCount(int nPrecisionPositionFindLaserScanningObjectValidCount);
	void setPrecisionPositionFindCountInvalidValueGap(int nPrecisionPositionFindCountInvalidValueGap);
	void setPrecisionPositionFindLimitPointGap(double dPrecisionPositionFindLimitPointGap);

	void setPrecisionPositionFilteringWheelToHusingGap(int nPrecisionPositionFilteringWheelToHusingGap);
	void setPrecisionPositionFilteringCartSafetyGap(int nPrecisionPositionFilteringCartSafetyGap);
	void setPrecisionPositionFilteringObjectToSafetyGap(int nPrecisionPositionFilteringObjectToSafetyGap);

	void setPrecisionPositionTergetingScanLimiteDistance(double dPrecisionPositionTergetingScanLimiteDistance);
	void setPrecisionPositionTergetingScanLimiteDegree(int nPrecisionPositionTergetingScanLimiteDegree);
	void setPrecisionPositionTergetingWeightValueDistance(double dPrecisionPositionTergetingWeightValueDistance);
	void setPrecisionPositionTergetingWeightValueThDeg(double dPrecisionPositionTergetingWeightValueThDeg);
	void setPrecisionPositionTergetingWeightValueDirection(double dPrecisionPositionTergetingWeightValueDirection);
	void setPrecisionPositionTergetingLimitTraceFailCount(int nPrecisionPositionTergetingLimitTraceFailCount);

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Get [SERVER]
	string getServerIP();
	int getServerPort();

	// Get [MAP]
	string getMapNameNPath( ); 
	int getMapSizeXm();		// 지도크기를 설정하는 함수
	int getMapSizeYm();		// 지도크기를 설정하는 함수
	int getXoffset();
	int getYoffset();
	double getHeight();
	double getSDValue();

	// Get [PATH]
	string getNodePointNameNPath() ;
	string getPathDataNameNPath() ;
	string getStationPointNameNPath();
	string getChargerPointNameNPath();
	string getWorkPlacePointNameNPath();
	string getLastRobotPoseNameNPath();				//pjs 추가 - 마지막 위치 저장 및 사용
	string getInitRobotPoseNameNPath();				//pjs 추가 - 로봇 위치 초기화 설정

	// Get [ROBOT]
	int getRobotID();		// 로봇의 ID를 설정해주는 함수
	string getAMR_IP();
	int getAMR_Port();
	string getAMR_WLAN_Name();

	double getInitialPositionX();		// m
	double getInitialPositionY();		// m
	double getInitialPositionTh();		// degree

	double getRobotRadius();		//로봇 반경을 설정해주는 함수(단위 mm)
	double getWheelBaseofRobot();
	double getGearRatio();
	int getEncoderResolution();
	double getWheelRadius();

	double getWeightAngleVelocity();
	double getMaxRobotVelocity();		// 로봇의 최대속도를 설정하는 함수
	double getMinRobotVelocity();		// 로봇의 최소속도를 설정하는 함수
	double getMaxRobotAutonomusVelocity();		//pjs 추가 - 자율 주행 최대 속도를 설정, 단 MAX_VELOCITY을 넘을 수 없음
	double getMaxRobotAngleVelocity();
	double getMaxRobotGuideVelocity ();
	double getMaxRobotGuideAngleVelocity();
	double getMaxRobotGuideVelocitySlow();
	double getMaxRobotGuideAngleVelocitySlow();
	double getRobotRotationAngleSpeed();
	int getRobotStraightMoveSpeed();

	RobotPose getInitRobotPoseForMap();
	int getUsingLastPose();
	RobotPose getInitRobotPoseForNav();
	//string getLastRobotPoseNameNPath();				//pjs 추가 - 마지막 위치 저장 및 사용
	string getGlobalLocalization();
	int getObstacleDetectionTime();
	int getLocalization();

	int getAMRRunigtime();
	int getLoadingMode();
	int getFirstMuteOn();

	//pjs 추가 - Battery Min, Max, ChargeRequired
	double getBatteryVoltageMax();
	double getBatteryVoltageMin();
	double getChargeRequiredPersent();
	//int getUseAutoChargingStation();
	//double getAutoChargingCompleteVoltage();
	//double getChargingAlarmCompleteVoltage();

	int getUseDevicePowerSeparationMode();
	int getUseLowStandbyPowerMode();

	int getUseBreakMode();

	int getUseReset_EmergencyStopMode();

	double getLift_Up_Limit();
	double getLift_Down_Limit();
	//pjs 2019.03.18 Lift Motor 개수에 따라서 다르게 적용 //0 : Lift Motor Count 2, 1 : Lift Motor Count 1
	int getLiftMotorType();

	// Get [SENSOR]
	int getGyroComPort();		// Gyro Com Port를 넘겨줌

	int getGuideSensorCount();	// Guide Sensor의 개수를 받아옴
	int getGuideComPort1();		// Guide Com Port를 넘겨줌
	int getGuideComPort2();		// Guide Com Port를 넘겨줌
	
	//pjs 2019.03.08 GuideCan Port 설정
	int getGuideCanPort();

	int getGuideStopMarkSensorFront();
	int getGuideStopMarkSensorRear();
	int getUseMatchDualGuideStopMarkSensor();

	int getMagneticGuideSensorXOffset();
	int getMagneticGuideSensorWidth();

	double getMagneticGuideSensorMin();
	double getMagneticGuideSensorMax();
	double getMagneticGuideFindAngle();

	int getBatteryChecker();

	int getLaserScannerType();
	int getLaserScannerCount();		// 레이져 스케너의 개수를 받아옴
	int getLaserScannerHead();		// 레이져 스케너의 설치 방향을 설정

	string getSICKLMSLaserIPAddress1();
	int getLaserTCPPort1();
	string getSICKLMSLaserIPAddress2();
	int getLaserTCPPort2();

	int getLaserMaxDist();		// 상단 레이저의 최대 탐지거리를 설정하는 함수
	int getLaserMinDist();		// 상단 레이저의 최소 탐지거리를 설정하는 함수
	int getLaserHeight();		// 틸트 축으로 부터 상단 레이저까지의 높이값을 설정하는 함수
	int getLaserXOffset();		// 틸트 축으로 부터 상단 레이저까지의 x offset을 설정하는 함수
	int getLaserYOffset();		// 틸트 축으로 부터 상단 레이저까지의 y offset을 설정하는 함수

	int getLaserHeight1();		// 틸트 축으로 부터 상단 레이저까지의 높이값을 설정하는 함수
	int getLaserXOffset1();		// 틸트 축으로 부터 상단 레이저까지의 x offset을 설정하는 함수
	int getLaserYOffset1();		// 틸트 축으로 부터 상단 레이저까지의 y offset을 설정하는 함수

	int getLaserHeight2();		// 틸트 축으로 부터 상단 레이저까지의 높이값을 설정하는 함수
	int getLaserXOffset2();		// 틸트 축으로 부터 상단 레이저까지의 x offset을 설정하는 함수
	int getLaserYOffset2();		// 틸트 축으로 부터 상단 레이저까지의 y offset을 설정하는 함수

	int getLaser_Installation_Angle_Offset1();
	int getLaser_Start_Invalid_Count1();
	int getLaser_End_Invalid_Count1();

	int getLaser_Installation_Angle_Offset2();
	int getLaser_Start_Invalid_Count2();
	int getLaser_End_Invalid_Count2();

	double getLaser_Angle_Step1();
	double getLaser_Angle_Step2();

	
	int m_nLaser_Installation_Angle_Offset1;
	int m_nLaser_Start_Invalid_Count1;
	int m_nLaser_End_Invalid_Count1;

	int m_nLaser_Installation_Angle_Offset2;
	int m_nLaser_Start_Invalid_Count2;
	int m_nLaser_End_Invalid_Count2;

	double m_dLaser_Angle_Step1;
	double m_dLaser_Angle_Step2;



	// Get [KANAYAMA_MOTION_CONTROL]
	int getTargetDistance();
	int getDesiedVel();
	int getGoalArea();
	double getdKX();
	double getdKY();
	double getdKT();
	int getDirectionofRotation();

	// Get [PARTICLE_FILTER]
	int getMaxParticleNum();		// 최대 particle 갯수를 설정하는 함수
	int getMinParticleNum();		// 최소 particle 갯수를 설정하는 함수
	double getDeviationforTrans();
	double getDeviationforRotate();
	double getDeviationforTransRotate();
	
	double getAccumDistGlobal();
	double getAccumAngleGlobal();
	double getAccumDistLocal();
	double getAccumAngleLocal();

	// Get [MOTOR_DRIVER]
	int getMotorComport();
	int getRoboCubeMotordriver();
	int getMotorDrection();

	// Get [DIGITAL_IO]
	int getIODevice();

	// Get [PHIDGETS]
	int getPhidgetsDeviceQuantity();
	int getDeviceSerialNumber1();
	int getDeviceSerialNumber2();

	int getPhidget1InCh(int nChNum);
	int getPhidget1OutCh(int nChNum);
	int getPhidget2InCh(int nChNum);
	int getPhidget2OutCh(int nChNum);

	// Get [OBJECT_FOLLOW]
	int getFindUseReflector();
	int getFindCountInvalidValueReflector();
	int getFindCountInvalidValueGap();
	double getFindLimitPointGap();

	int getFilteringLimitObjectWidthMax();
	int getFilteringLimitObjectWidthMin();
	int getFilteringTraceRange();
	int getFilteringLimitTraceTime();

	double getTargetingWeightValueDistance();
	double getTargetingWeightValueThDeg();
	int getTargetingTergeting();
	int getTargetingLimitObjectDistanceInitial();
	int getTargetingLimitTraceFailCount();
	int getTargetTraceRange();

	double getMoveObjectFlollowMaxV();
	double getMoveFollowGap();
	double getMoveMaxScanDistance();
	double getMoveRotateRange();

	// Get [TUNNEL_MOVE]
	double getTunnelFindScanLimiteDistance();
	int getTunnelFindLaserScanningObjectValidCount();
	int getTunnelFindCountInvalidValueGap();
	double getTunnelFindLimitPointGap();

	int getTunnelFilteringWheelToHusingGap();
	int getTunnelFilteringCartSafetyGap();
	int getTunnelFilteringObjectToSafetyGap();

	double getTunnelTergetingScanLimiteDistance();
	int getTunnelTergetingScanLimiteDegree();
	double getTunnelTergetingWeightValueDistance();
	double getTunnelTergetingWeightValueThDeg();
	double getTunnelTergetingWeightValueDirection();
	int getTunnelTergetingLimitTraceFailCount();

	//Get [PRECISION_POSITION_MOVE]
	double getPrecisionPositionFindScanLimiteDistance();
	int getPrecisionPositionFindLaserScanningObjectValidCount();
	int getPrecisionPositionFindCountInvalidValueGap();
	double getPrecisionPositionFindLimitPointGap();

	int getPrecisionPositionFilteringWheelToHusingGap();
	int getPrecisionPositionFilteringCartSafetyGap();
	int getPrecisionPositionFilteringObjectToSafetyGap();

	double getPrecisionPositionTergetingScanLimiteDistance();
	int getPrecisionPositionTergetingScanLimiteDegree();
	double getPrecisionPositionTergetingWeightValueDistance();
	double getPrecisionPositionTergetingWeightValueThDeg();
	double getPrecisionPositionTergetingWeightValueDirection();
	int getPrecisionPositionTergetingLimitTraceFailCount();

	//Get [GUI_DISP]
	int getLRF1_DISP();
	int getLRF2_DISP();
	int getNODE_DISP();
	int getSTATION_DISP();

	int getPATHDATA_DISP();
	int getPARTICLE_DISP();
	int getROBOT_DISP();


	// [OBSTACLE_RANGE]		//pjs 추가 - ObstacleDetect ini 설정
private:
	int m_nObstacleRangeDisp;
	double m_dObstacleStopDistanceMin;
	double m_dObstacleStopDistanceMax;
	double m_dObstacleWarnDistanceMax;
	double m_dObstacleStopWidthBasic;
	double m_dObstacleStopWidthLoaded;

public:
	int getObstacleRangeDisp();
	double getObstacleStopDistanceMin();
	double getObstacleStopDistanceMax();
	double getObstacleWarnDistanceMax();
	double getObstacleStopWidthBasic();
	double getObstacleStopWidthLoaded();

	void setObstacleRangeDisp(int nObstacleRangeDisp);
	void setObstacleStopDistanceMin(double dObstacleStopDistanceMin);
	void setObstacleStopDistanceMax(double dObstacleStopDistanceMax);
	void setObstacleWarnDistanceMax(double dObstacleWarnDistanceMax);
	void setObstacleStopWidthBasic(double dObstacleStopWidthBasic);
	void setObstacleStopWidthLoaded(double dObstacleStopWidthLoaded);


	//double m_dObstacle_Stop_Distance_Min;
	//double m_dObstacle_Stop_Distance_Max;
	//double m_dObstacle_Stop_Width_Min;
	//double m_dObstacle_Stop_Width_Max;

	//double m_dObstacle_Warn_Distance_Min;
	//double m_dObstacle_Warn_Distance_Max;
	//double m_dObstacle_Warn_Width_Min;
	//double m_dObstacle_Warn_Width_Max;

	//double getOBSTACLE_STOP_DISTANCE_MIN();
	//double getOBSTACLE_STOP_DISTANCE_MAX();
	//double getOBSTACLE_STOP_WIDTH_MIN();
	//double getOBSTACLE_STOP_WIDTH_MAX();

	//double getOBSTACLE_WARN_DISTANCE_MIN();
	//double getOBSTACLE_WARN_DISTANCE_MAX();
	//double getOBSTACLE_WARN_WIDTH_MIN();
	//double getOBSTACLE_WARN_WIDTH_MAX();

	//void setOBSTACLE_STOP_DISTANCE_MIN(double dist);
	//void setOBSTACLE_STOP_DISTANCE_MAX(double dist);
	//void setOBSTACLE_STOP_WIDTH_MIN(double width);
	//void setOBSTACLE_STOP_WIDTH_MAX(double width);

	//void setOBSTACLE_WARN_DISTANCE_MIN(double dist);
	//void setOBSTACLE_WARN_DISTANCE_MAX(double dist);
	//void setOBSTACLE_WARN_WIDTH_MIN(double width);
	//void setOBSTACLE_WARN_WIDTH_MAX(double width);

};
