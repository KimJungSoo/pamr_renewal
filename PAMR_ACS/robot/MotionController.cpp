#include "stdafx.h"
#include "robot\MotionContoller.h"
#include "sensor\SICKLaserScanner.h"
#include "sensor\GyroSensor.h"
#include "sensor\SICKGuide.h"
#include "sensor\ComizoaMotionCard.h"
#include "robot\RobotParameter.h"

using namespace eventManager;
using namespace robot;
using namespace sensor;
using namespace std;

#define ENCMAX (2147483647.0)
#define PI 3.14159265358979323846
#define D2R 0.017453292
#define R2D 57.29579143

CMotionController::CMotionController()
{

	if (Initialize() != RETURN_NONE_ERROR)
	{
		g_eventManager.PushTask(MSG_ERROR, ERROR_MOTIONCTR_INITIALIZE_FAILED, true, false);
		m_AMRState = STATE_AMR_ERROR_SENSOR_STATE;
	}
	else
	{
		m_sensor[0]->Connect();
		m_AMRState = STATE_AMR_PROGRESSING;
	}
}

CMotionController::~CMotionController() {

	// Thread Terminate
	m_bMotionControlThreadLoop = false;
	if (m_AMRControlThread.joinable())
		m_AMRControlThread.join();

	//AutoCSLock cs(m_cs);

	while (!m_cmds.empty())
	{
		delete &(m_cmds.front());
		m_cmds.pop();
	}
	// Memory delete
	SAFE_DELETE(m_sensor[0]);
	SAFE_DELETE(m_IOHub);
	SAFE_DELETEA(m_sensor);

	//g_eventManager.PushTask(MSG_INFO, getModuleName(), INFO_MODULE_DELETE_SUCCEED, true, false);
}

int CMotionController::Initialize()
{
	m_AMRState = STATE_AMR_INIT;
	
	//ini file read
	m_nNumOfSensor = 3;
	m_sensor = DEBUG_NEW CSensorModule *[m_nNumOfSensor];
	for (int i = 0; i < m_nNumOfSensor; i++)
		m_sensor[i] = NULL;
	m_sensor[0] = DEBUG_NEW CComizoaMotionCard("Motion Card");
	m_IOHub = NULL;
	// Variable Initialize
	m_iSystemCommand = 0;
	m_dAMRVelocity = 0.0;
	m_dAMR_W = 0.0;
	//Encoder Initialize
	m_bIsFirstTraction = true;
	m_bIsFirstEncoderInput = false;
	m_dGyroValuePrev = 0.0;	
	m_dReferenceX = 0.0;
	m_dReferenceY = 0.0;
	m_dReferenceTh = 0.0;

	m_dGeomThetaDeg = 0.0;
	m_dGeomDeltaDeg = 0.0;
	m_dGyroThetaDeg = 0.0;
	m_dGyroDeltaDeg = 0.0;

	m_dReferenceWheelEncoderCount[2] = { 0, };

	setUseGyro(true);

	//RobotPosition Initialize
	r_pos.setX(RobotParameter::getInstance()->getInitialPositionX() * 1000.0);		// m => mm
	r_pos.setY(RobotParameter::getInstance()->getInitialPositionY() * 1000.0);		// m => mm
	r_pos.setThetaDeg(RobotParameter::getInstance()->getInitialPositionTh());		// degree

																					// Thread
	m_nThreadPeriod = 100;
	m_bMotionControlThreadLoop = true;
	m_AMRControlThread = std::thread(ThreadFunctionMotionControl, this);

	return RETURN_NONE_ERROR;
}

int CMotionController::ThreadFunctionMotionControl(CMotionController * controller) {

	while (controller->m_bMotionControlThreadLoop) {
		// Clock Start
		clock_t start, runtime;
		start = clock();

		// Obstacle Check
		if (controller->m_sensor[1] != NULL &&controller->m_sensor[2] != NULL)
		{
			if (controller->m_sensor[1]->getStatus() == SENSOR_STATE_RUN && controller->m_sensor[2]->getStatus() == SENSOR_STATE_RUN)
			{
				// Get Laser Scan data
				CSICKLaserScanner* lms_front = dynamic_cast<CSICKLaserScanner*>(controller->m_sensor[1]);
				CSICKLaserScanner* lms_rear = dynamic_cast<CSICKLaserScanner*>(controller->m_sensor[2]);
				LaserScanData scan_front, scan_rear;
				lms_front->getData(&scan_front, 1);
				lms_rear->getData(&scan_rear, 1);

				int ret = controller->m_obstacleChecker.GetObstacleZone(controller->m_dAMRVelocity, controller->m_dAMR_W, &scan_front, &scan_rear);
				/*if (ret == robot::eSTOP)
				{
					controller->m_dAMRVelocity = 0.;
					controller->m_dAMR_W = 0.;
				}
				else if (ret == robot::eWARNING)
				{
					controller->m_dAMRVelocity /= 2.;
					controller->m_dAMR_W /= 2.;
				}*/
			}
		}

		// State Machine 
		CComizoaMotionCard* motion = dynamic_cast<CComizoaMotionCard*>(controller->m_sensor[0]);
		if (!controller->m_cmds.empty())
		{
			eAMRMotionCommand cmd = controller->PopCommand();
			switch (cmd)
			{
			case CMD_MOVE:
				motion->Move(controller->getVelocity(), controller->getW());
				break;
			case CMD_ROTATE:
				motion->Rotate(controller->getVelocity(), controller->getW());
				break;
			case CMD_STOP:
				motion->AMRStop(false);
				break;
			case CMD_EMG_STOP:
				motion->AMRStop(true);
				break;
			case CMD_AUTONOMOUS:
				motion->Move(controller->getVelocity(), controller->getW());
				break;
			default:
				break;
			}
		}

		// Sensor Error Check
		bool bAllSensorIsReady = true;
		for (int i = 0; i < controller->m_nNumOfSensor; i++)
		{
			if (controller->m_sensor[i] == NULL) break;
			if (controller->m_sensor[i]->getStatus() == SENSOR_STATE_ERROR)
			{
				controller->m_AMRState = STATE_AMR_ERROR_SENSOR_STATE;
				break;
			}
			else if (controller->m_sensor[i]->getStatus() == SENSOR_STATE_PROGRESSING)
				controller->m_AMRState = STATE_AMR_PROGRESSING;
			else if (controller->m_sensor[i]->getStatus() != SENSOR_STATE_RUN)
				bAllSensorIsReady = false;
		}
		if (controller->m_IOHub == NULL)
			continue;
		if(controller->m_IOHub->getStatus() != SENSOR_STATE_RUN)
			bAllSensorIsReady = false;
		

		if (bAllSensorIsReady)
			controller->m_AMRState = STATE_AMR_RUN;
		// Run 이 아니면 모터 정지후 아무것도 안함 (continue)
		else
		{
			controller->m_AMRState = STATE_AMR_ERROR_SENSOR_STATE;
			// Motion Stop
			((CComizoaMotionCard*)(controller->m_sensor[0]))->MotionStop(false, LEFT_DRIVE_MOTOR, 0, 0);
			((CComizoaMotionCard*)(controller->m_sensor[0]))->MotionStop(false, RIGHT_DRIVE_MOTOR, 0, 0);
			continue;
		}

		// 평소에 도는 놈
		if (controller->m_AMRState == SENSOR_STATE_RUN)
		{
			// IO Error Check
			if (controller->m_IOHub->getStatus() == SENSOR_STATE_RUN)
			{
				int errorCode;
				vector<bool> errorVector;
				vector<bool> valueVector;
				valueVector.push_back(controller->m_IOHub->bitRead(IO_List::EMS_TP, &errorCode));
				errorCode == RETURN_NONE_ERROR ? errorVector.push_back(true) : errorVector.push_back(false);
				valueVector.push_back(controller->m_IOHub->bitRead(IO_List::EMS1, &errorCode));
				errorCode == RETURN_NONE_ERROR ? errorVector.push_back(true) : errorVector.push_back(false);
				valueVector.push_back(controller->m_IOHub->bitRead(IO_List::EMS2, &errorCode));
				errorCode == RETURN_NONE_ERROR ? errorVector.push_back(true) : errorVector.push_back(false);
				valueVector.push_back(controller->m_IOHub->bitRead(IO_List::EMS3, &errorCode));
				errorCode == RETURN_NONE_ERROR ? errorVector.push_back(true) : errorVector.push_back(false);
				valueVector.push_back(controller->m_IOHub->bitRead(IO_List::EMS4, &errorCode));
				errorCode == RETURN_NONE_ERROR ? errorVector.push_back(true) : errorVector.push_back(false);
				valueVector.push_back(controller->m_IOHub->bitRead(IO_List::BUMPER1, &errorCode));
				errorCode == RETURN_NONE_ERROR ? errorVector.push_back(true) : errorVector.push_back(false);
				valueVector.push_back(controller->m_IOHub->bitRead(IO_List::BUMPER2, &errorCode));
				errorCode == RETURN_NONE_ERROR ? errorVector.push_back(true) : errorVector.push_back(false);
				valueVector.push_back(controller->m_IOHub->bitRead(IO_List::BUMPER3, &errorCode));
				errorCode == RETURN_NONE_ERROR ? errorVector.push_back(true) : errorVector.push_back(false);
				valueVector.push_back(controller->m_IOHub->bitRead(IO_List::BUMPER4, &errorCode));
				errorCode == RETURN_NONE_ERROR ? errorVector.push_back(true) : errorVector.push_back(false);
				valueVector.push_back(controller->m_IOHub->bitRead(IO_List::SAFETY_PLC_OUT, &errorCode));
				errorCode == RETURN_NONE_ERROR ? errorVector.push_back(true) : errorVector.push_back(false);

				// E-Stop
				for (int i = 0; i < 5; i++)
					if (errorVector[i] & valueVector[i])
						controller->m_AMRState = STATE_AMR_ERROR_EMERGENCY;

				// Bumper
				for (int i = 5; i < 10; i++)
					if (errorVector[i] & valueVector[i])
						controller->m_AMRState = STATE_AMR_ERROR_CRASH;
			}	

			//Motion 위치 값 Update

			if (controller->m_IOHub->getStatus() == SENSOR_STATE_RUN)
			{
				switch (controller->m_AMRState) {
				case STATE_AMR_ERROR_OBSTACLE:
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH1, false);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH2, false);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH3, false);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH4, true);
					break;
				case STATE_AMR_ERROR_CRASH:
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH1, true);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH2, true);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH3, false);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH4, true);
					break;
				case STATE_AMR_ERROR_EMERGENCY:
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH1, true);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH2, false);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH3, false);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH4, true);
					break;
				default:
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH1, false);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH2, false);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH3, false);
					controller->m_IOHub->bitSet(IO_List::SPEAKER_CH4, false);
					break;
				}
			}
		}

		// Run 이 아니면 모터 정지후 아무것도 안함 (continue)
		if (controller->m_AMRState != STATE_AMR_RUN)
		{
			// Motion Stop
			((CComizoaMotionCard*)(controller->m_sensor[0]))->MotionStop(false, LEFT_DRIVE_MOTOR, 0, 0);
			((CComizoaMotionCard*)(controller->m_sensor[0]))->MotionStop(false, RIGHT_DRIVE_MOTOR, 0, 0);

			continue;
		}

		// Keep the Thread Period
		runtime = clock() - start;
		if (controller->m_nThreadPeriod - runtime > 0)
			Sleep(controller->m_nThreadPeriod - runtime);
		//else
		//g_eventManager.PushTask(MSG_ERROR, "Motion Controller", ERROR_EXCEED_THREAD_CYCLE, true, false);
	}
	//20190906 SG_ADD
	std::cout << "Thread End : ThreadFunctionMotionControl" << std::endl;
	
	return RETURN_NONE_ERROR;
}

int CMotionController::PushCommand(eAMRMotionCommand input)
{
	if (m_cmds.size() > 5)
		return RETURN_FAILED;
	m_cs.Lock();

	m_cmds.push(input);

	m_cs.Unlock();
	return RETURN_NONE_ERROR;
}

eAMRMotionCommand CMotionController::PopCommand()
{
	m_cs.Lock();
	eAMRMotionCommand cmd = m_cmds.front();
	m_cmds.pop();
	m_cs.Unlock();
	return cmd;
}

int CMotionController::getState() {
	return m_AMRState;
}

int CMotionController::getCommand()
{
	return m_iSystemCommand;
}

double CMotionController::getVelocity()
{
	return m_dAMRVelocity;
}

double CMotionController::getW()
{

	return m_dAMR_W;
}

int CMotionController::TractionPosition()
{
	//TractionPosition을 처음 호출 하는 함수에서 bool값을 false로 변경 해줘야 함
	if (m_bIsFirstTraction)
	{
		m_bIsFirstTraction = false;
		return RETURN_NONE_ERROR;
	}

	double dWheelEncoderCount[2] = { 0. };
	double dLeftwheelEnc = 0.0, dRightwheelEnc = 0.0;

	CComizoaMotionCard* motion = dynamic_cast<CComizoaMotionCard*>(m_sensor[0]);
	motion->GetPosition(LEFT_DRIVE_MOTOR, eCounter_Feed, &(dLeftwheelEnc));
	motion->GetPosition(RIGHT_DRIVE_MOTOR, eCounter_Feed, &(dRightwheelEnc));

	if (!m_bIsFirstEncoderInput)
	{
		m_dReferenceWheelEncoderCount[0] = dWheelEncoderCount[0] = dLeftwheelEnc;
		m_dReferenceWheelEncoderCount[1] = dWheelEncoderCount[1] = -dRightwheelEnc;
		m_bIsFirstEncoderInput = true;
	}

	dWheelEncoderCount[0] = dLeftwheelEnc;
	dWheelEncoderCount[1] = -dRightwheelEnc;


	double encleftdelta, encrightdelta;
	double diffleft, diffright;

	diffleft = dWheelEncoderCount[0] - m_dReferenceWheelEncoderCount[0];
	diffright = dWheelEncoderCount[1] - m_dReferenceWheelEncoderCount[1];

	if (abs(diffleft) > 100000.0)
		diffleft = 0.0;

	if (abs(diffright) > 100000.0)
		diffright = 0.0;

	if (diffleft < -2000000000)
	{
		if (dWheelEncoderCount[0] > 0)
		{
			encleftdelta = (ENCMAX - m_dReferenceWheelEncoderCount[0]) + dWheelEncoderCount[0];
		}
		else
		{
			encleftdelta = (dWheelEncoderCount[0] + ENCMAX) - m_dReferenceWheelEncoderCount[0];
		}
	}
	else if (diffleft > 2000000000)
	{
		if (dWheelEncoderCount[0] > 0)
		{
			encleftdelta = (-ENCMAX - m_dReferenceWheelEncoderCount[0]) + dWheelEncoderCount[0];
		}
		else
		{
			encleftdelta = dWheelEncoderCount[0] - ENCMAX - m_dReferenceWheelEncoderCount[0];
		}
	}
	else
	{
		encleftdelta = diffleft;
	}

	//////////////////////////
	if (diffright < -2000000000)
	{
		if (dWheelEncoderCount[1] > 0)
		{
			encrightdelta = (ENCMAX - m_dReferenceWheelEncoderCount[1]) + dWheelEncoderCount[1];
		}
		else
		{
			encrightdelta = (dWheelEncoderCount[1] + ENCMAX) - m_dReferenceWheelEncoderCount[1];
		}
	}
	else if (diffright > 2000000000)
	{
		if (dWheelEncoderCount[1] > 0)
		{
			encrightdelta = (-ENCMAX - m_dReferenceWheelEncoderCount[1]) + dWheelEncoderCount[1];
		}
		else
		{
			encrightdelta = dWheelEncoderCount[1] - ENCMAX - m_dReferenceWheelEncoderCount[1];
		}
	}
	else
	{
		encrightdelta = diffright;
	}

	double dGearRatio = 0.0, dEncoderResolution = 0.0, dWheelRadius = 0.0;
	double dWheelDistance[2] = { 0. };

	dGearRatio = RobotParameter::getInstance()->getGearRatio();
	dEncoderResolution = RobotParameter::getInstance()->getEncoderResolution();
	dWheelRadius = RobotParameter::getInstance()->getWheelRadius();

	dWheelDistance[0] = (encleftdelta / (double)(dGearRatio * dEncoderResolution)) * 2. * PI * dWheelRadius;

	dWheelDistance[1] = (encrightdelta / (double)(dGearRatio * dEncoderResolution)) * 2. * PI * dWheelRadius;

	if ((fabs(dWheelDistance[0]) < 1.0) && (fabs(dWheelDistance[1]) < 1.0))
	{
		dWheelDistance[0] = 0.0;
		dWheelDistance[1] = 0.0;
	}
	else
	{
		m_dReferenceWheelEncoderCount[0] = dWheelEncoderCount[0];
		m_dReferenceWheelEncoderCount[1] = dWheelEncoderCount[1];
	}
	//현재 전역변수로 선언된 g_nOdometryCnt 증가 하고 있지만 사용하거나 Check 하는곳이 없어서 Comment out 처리 
	//if (g_nOdometryCnt % 10 == 0) {
	//	//WriteLog(0x11, "Encoder value: L=%d, R=%d\n", m_dWheelEncoderCount[0], m_dWheelEncoderCount[1]);
	//	//_RPTN(_CRT_WARN,"enc>L=%d, R=%d, dis>L=%.2f, R=%.2f, diff>L=%.2f, R=%.2f\n", leftwheelenc, rightwheelenc, m_dWheelDistance[0], m_dWheelDistance[1],diffleft, diffright);
	//}
	//g_nOdometryCnt++;

	//m_dReferenceWheelEncoderCount[0] = m_dWheelEncoderCount[0];
	//m_dReferenceWheelEncoderCount[1] = m_dWheelEncoderCount[1];


	double dDeltaX = 0.0, dDeltaY = 0.0, dDeltaTRad = 0.0, dDeltaGyroThetaRad = 0.0, dDeltaGeomThetaRad = 0.0, dAverageWheelDistance = 0.0;
	double dGyroValue = 0.0, dWheelBase = 0.0, dDistance2RobotCenter = 0.0;
	int iStatus = ((CGyroSensor*)m_sensor[1])->getStatus();
	((CGyroSensor *)m_sensor[1])->getThetaDeg(eYaw, &dGyroValue);
	dWheelBase = RobotParameter::getInstance()->getWheelBaseofRobot();
	dAverageWheelDistance = (dWheelDistance[0] + dWheelDistance[1]) / 2;
	dDeltaGyroThetaRad = D2R * (m_dGyroValuePrev - dGyroValue);
	m_dGyroValuePrev = dGyroValue;
	dDeltaGeomThetaRad = (dWheelDistance[1] - dWheelDistance[0]) / dWheelBase;
	dDeltaTRad = isUseGyro() ? dDeltaGyroThetaRad : dDeltaGeomThetaRad;
	//dDeltaTRad = dDeltaGeomThetaRad; //경동욱 수정
	
	/*dDistance2RobotCenter = dAverageWheelDistance / dDeltaTRad;
	dDeltaY = dDistance2RobotCenter - (dDistance2RobotCenter * cos(dDeltaTRad));
	dDeltaX = dDistance2RobotCenter * sin(dDeltaTRad);*/
	
	if (fabs(dDeltaTRad) > 0.f) { // Lhopital Th. //경동욱 0.0017 값의 의미?
		dDistance2RobotCenter = dAverageWheelDistance / dDeltaTRad;
		dDeltaY = dDistance2RobotCenter - (dDistance2RobotCenter * cos(dDeltaTRad));
		dDeltaX = dDistance2RobotCenter * sin(dDeltaTRad);
	}
	else {
		dDistance2RobotCenter = 0.0;
		dDeltaY = 0;
		dDeltaX = dAverageWheelDistance;
	}

	m_dReferenceTh += dDeltaTRad;
	m_dReferenceX += dDeltaX * cos(m_dReferenceTh) - dDeltaY * sin(m_dReferenceTh);
	m_dReferenceY += dDeltaX * sin(m_dReferenceTh) + dDeltaY * cos(m_dReferenceTh);
	TrimAngle("rad", m_dReferenceTh);

	double dRefGeomThetaRad = m_dReferenceTh;
	double dRefGyroThetaRad = D2R * (RobotParameter::getInstance()->getInitialPositionTh() + dGyroValue);
	TrimAngle("rad", dRefGyroThetaRad);

	if (isUseGyro()) m_dReferenceTh = dRefGyroThetaRad;

	// Encoder delta pose for robot coord. used for PF
	m_rDeltaEncoderPos.setX(dDeltaX);
	m_rDeltaEncoderPos.setY(dDeltaY);
	m_rDeltaEncoderPos.setThetaRad(dDeltaTRad);
	// Encoder delta pose for world coord. used for GUI, map building...
	r_pos.setX(m_dReferenceX);
	r_pos.setY(m_dReferenceY);
	r_pos.setThetaRad(m_dReferenceTh);

	m_dGeomThetaDeg = dRefGeomThetaRad * R2D;
	m_dGeomDeltaDeg = dDeltaGeomThetaRad * R2D;
	m_dGyroThetaDeg = dRefGyroThetaRad * R2D;
	m_dGyroDeltaDeg = dDeltaGyroThetaRad * R2D;

	//Dialog에 Gyro값 쓰는 부분은 추후에 
	//str_gyro.Format(_T("Theta Deg (Global/Delta)\n")
	//	_T("Geom: %.2f / %.2f \n")
	//	_T("Gyro: %.2f / %.2f \n"),
	//	dRefGeomThetaRad*R2D, dDeltaGeomThetaRad*R2D,
	//	dRefGyroThetaRad*R2D, dDeltaGyroThetaRad*R2D);
	//m_static_gyro.SetWindowText(str_gyro);

	return RETURN_NONE_ERROR;
}

int CMotionController::GetGeomThetaDeg(double *dDeg)
{
	*dDeg = m_dGeomThetaDeg * D2R;
	return RETURN_NONE_ERROR;
}

int CMotionController::GetGeomDeltaDeg(double *dDeg)
{
	*dDeg = m_dGeomDeltaDeg * D2R;
	return RETURN_NONE_ERROR;
}

int CMotionController::GetGyroThetaDeg(double *dDeg)
{
	*dDeg = m_dGyroThetaDeg;
	return RETURN_NONE_ERROR;
}

int CMotionController::GetGyroThetaRad(double *dDeg)
{
	*dDeg = m_dGyroDeltaDeg*D2R;
	return RETURN_NONE_ERROR;
}
//
//int CMotionController::GetEncoderDelPosRobotCoord()
//{
//	// Encoder delta pose for robot coord. used for PF
//	m_rDeltaEncoderPos.setX(dDeltaX);
//	m_rDeltaEncoderPos.setY(dDeltaY);
//	m_rDeltaEncoderPos.setThetaRad(dDeltaTRad);
//
//	return RETURN_NONE_ERROR;
//}
//
//int CMotionController::GetEncoderDelPosWorldCoord()
//{
//	// Encoder delta pose for world coord. used for GUI, map building...
//	r_pos.setX(m_dReferenceX);
//	r_pos.setY(m_dReferenceY);
//	r_pos.setThetaRad(m_dReferenceTh);
//}


int CMotionController::TrimAngle(std::string strUnit, double& dAngle)
{
	if (strUnit != "rad" && strUnit != "deg") strUnit = "rad";
	double dLimit = (strUnit == "rad") ? PI : 180.0;
	if (dAngle > dLimit) { dAngle -= 2 * dLimit; }
	else if (dAngle < -dLimit) { dAngle += 2 * dLimit; }

	return RETURN_NONE_ERROR;
}

int CMotionController::SetVW(double dVelocity, double dW)
{
	m_dAMRVelocity = dVelocity;
	m_dAMR_W = dW;

	return RETURN_NONE_ERROR;
}

int CMotionController::GetPosition(double *dX, double *dY, double *dAngle)
{
	*dX = m_rDeltaEncoderPos.getX();
	*dY = m_rDeltaEncoderPos.getY();
	*dAngle = m_rDeltaEncoderPos.getThetaRad();

	return RETURN_NONE_ERROR;
}

int CMotionController::SetIO(CIOHub* IOHub)
{
	m_IOHub = IOHub;
	return RETURN_NONE_ERROR;
}

int CMotionController::SetSensor(sensor::CSensorModule* sensor, int index)
{
	if (index < 0 || index >= m_nNumOfSensor)
		return RETURN_FAILED;

	m_sensor[index] = sensor;

	return RETURN_NONE_ERROR;
}

int CMotionController::getSensor(int index, CSensorModule** sensor)
{
	if (index < 0 || index > m_nNumOfSensor)
		return RETURN_FAILED;
	*sensor = m_sensor[index];
	return RETURN_NONE_ERROR;
}

int CMotionController::getObstacleObj(CObstacleChecker* obstacle)
{
	*obstacle = m_obstacleChecker;
	return RETURN_NONE_ERROR;
}

void CMotionController::Move() { PushCommand(CMD_MOVE); }
void CMotionController::Rotate() { PushCommand(CMD_ROTATE); }
void CMotionController::Autonomous() { PushCommand(CMD_AUTONOMOUS); }
void CMotionController::Stop() { PushCommand(CMD_STOP); }
void CMotionController::EmgergencyStop() { PushCommand(CMD_EMG_STOP); }

std::string CMotionController::getModuleName() { return m_strModuleName; }