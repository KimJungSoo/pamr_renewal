﻿
#include "stdafx.h"
#include "GuideMove.h"

#define M_PI 3.14159265358979323846

//#define COUT_ON

CGuideMove::CGuideMove()
{
	initialize();
	m_WP = NULL;
}

CGuideMove::~CGuideMove()
{
	if (m_GuideMoveThread.joinable())
	{
		m_GuideMoveThread.join();
	}		

	SAFE_DELETE(m_WP);
}

int CGuideMove::initialize( )
{
	m_bThreadFlag = false;

	m_nState = 1;

	m_dV = 0.0;
	m_dW = 0.0;

	m_dTargetX = 0.0;
	m_dTargetY = 0.0;
	m_dTargetTh = 0.0;
	
	m_PosAlign = true;
	m_SearchGuide = false;
	m_GuideTracking = false;
	m_FindMarker = false;

	m_RobotPos = GetRobotPose();

	//lrfreflect_toPose variable


	//Front = 0, Rear = 1
	guide_control_direction = 0;
	guide_blocked_count = 0;

	m_iWorkPlacePointCount = 0;


	first_marker = false;
	second_marker = false;
	marker_trigger = true;

	LoadWorkPlacePoint();
	m_nThreadPeriod = 100;

	if (!m_GuideMoveThread.joinable())
	{
		CreateThread();
	}

	return RETURN_NONE_ERROR;
}

int CGuideMove::ThreadFunctionGuideMove(void* arg)
{
	CGuideMove* pGGB = (CGuideMove*)arg;

	while (pGGB->m_bThreadFlag)
	{
		// Clock Start
		clock_t start, runtime;
		start = clock();

		double turn_angle = 0.0;

		//KA는 회전시 Gain
		//	double KA = 7.5;

		//	pGGB->m_maindlg->led_number |= LED_GUIDE_STATE_ON;

		pGGB->m_RobotPos = pGGB->GetRobotPose();


		double guide_front_lcp2;

		int guide_front_line_exists;
		int guide_front_marker;

		double guide_rear_lcp2;

		int guide_rear_line_exists;
		int guide_rear_marker;
		short slcp1 = 0, sFrontlcp2 = 0, slcp3 = 0, sRearlcp2 = 0;
		pGGB->GetFrontLcpData(&slcp1, &sFrontlcp2, &slcp3);
		pGGB->GetRearLcpData(&slcp1, &sRearlcp2, &slcp3);

		guide_front_lcp2 = sFrontlcp2;

		guide_front_line_exists = pGGB->GetFrontGuideLineExist();
		guide_front_marker = pGGB->GetFrontMarker();


		guide_rear_lcp2 = sRearlcp2;

		guide_rear_line_exists = pGGB->GetRearGuideLineExist();
		guide_rear_marker = pGGB->GetRearMarker();

		pGGB->guide_control_direction = pGGB->m_iTargetDirection;

		//First Setp: 
		//가이드 감지 함수
		if (pGGB->m_PosAlign == true) // 첫번째 단계: 회전 후 가이드 탐지
		{
			// 노드 도착 후 첫 실행되는 부분
			// Case 1은 회전 후 가이드 찾기
			switch (pGGB->m_nState)
			{
			case 1: // 
			{

				_RPT0(_CRT_WARN, "case 1(Charger) 180deg turn\n");

				//double target_angle_diff = pGGB->m_maindlg->target_workplace.th_deg - pGGB->m_RobotPos.getThetaDeg();
				double target_angle_diff = pGGB->m_dTargetTh - pGGB->m_RobotPos.getThetaDeg();

				if (target_angle_diff < -180.0) target_angle_diff += 360.0;
				if (target_angle_diff > 180.0) target_angle_diff -= 360.0;


				if (fabs(target_angle_diff) < 30.0)
				{
					pGGB->m_PosAlign = false;
					pGGB->m_SearchGuide = true;
					pGGB->m_nState = 1;
					break;
				}

				turn_angle = target_angle_diff;

				_RPTN(_CRT_WARN, "\n case 1: turn_angle[%f]\n", turn_angle);

				pGGB->m_nState += 1;
				double dAngleDiff = pGGB->m_RobotPos.getThetaDeg() + turn_angle;
				if (dAngleDiff > 180.0)	dAngleDiff -= 360.0;
				else if (dAngleDiff < -180.0)		dAngleDiff += 360.0;
				pGGB->m_TargetPos.setThetaDeg(dAngleDiff);

				pGGB->gyro_target = pGGB->GetGyroTheta() + turn_angle;

				if (pGGB->gyro_target < -180.0) pGGB->gyro_target += 360.0;
				if (pGGB->gyro_target > 180.0) pGGB->gyro_target -= 360.0;

				if (turn_angle >= 0.0)
				{
					pGGB->m_setdirection = true;
				}
				else
				{
					pGGB->m_setdirection = false;
				}

				break;
			}

			case 2: // turning 
			{
				_RPT0(_CRT_WARN, "case 2(CGuideMove) turning to reverse angle \n");

				double dAngleDiff = pGGB->m_TargetPos.getThetaDeg() - pGGB->m_RobotPos.getThetaDeg();

				if (dAngleDiff > 180.0)	dAngleDiff -= 360.0;
				else if (dAngleDiff < -180.0)		dAngleDiff += 360.0;

				double dAngleDiff_gyro = pGGB->gyro_target - pGGB->GetGyroTheta();

				if (dAngleDiff_gyro > 180.0)	dAngleDiff_gyro -= 360.0;
				else if (dAngleDiff_gyro < -180.0)		dAngleDiff_gyro += 360.0;

				if (pGGB->isUseGyro())
				{
					dAngleDiff = dAngleDiff_gyro;
				}

				double turning_velocity = 0.0;

				if (pGGB->m_setdirection)
				{
					if (dAngleDiff > 20.0)
					{
						turning_velocity = 20.0;
					}
					else if (dAngleDiff > 5.0)
					{
						turning_velocity = 5.0;
					}
					else if (dAngleDiff > 1.0)
					{
						turning_velocity = 3.0;
					}
					else
					{
						pGGB->AMRStop();
						pGGB->m_PosAlign = false;
						pGGB->m_SearchGuide = true;
						pGGB->m_nState = 1;
					}
				}
				else
				{
					if (dAngleDiff < -20.0)
					{
						turning_velocity = -20.0;
					}
					else if (dAngleDiff < -5.0)
					{
						turning_velocity = -5.0;
					}
					else if (dAngleDiff < -1.0)
					{
						turning_velocity = -3.0;
					}
					else
					{
						pGGB->AMRStop();						
						pGGB->m_PosAlign = false;
						pGGB->m_SearchGuide = true;
						pGGB->m_nState = 1;
					}
				}

				//pGGB->TRVelInput(0, turning_velocity);
				pGGB->SetVW(0.0, turning_velocity);
				break;
			}

			default: break;

			}
		}

		if (pGGB->m_SearchGuide == true)
		{
			switch (pGGB->m_nState)
			{
			case 1:
			{
				if (pGGB->guide_control_direction == 0) //fwd
				{
					if ((guide_front_line_exists > 0) && (fabs(guide_front_lcp2) <= 250.0))
					{
						pGGB->m_SearchGuide = false;
						pGGB->m_GuideTracking = true;
					}
					else
					{
						turn_angle = 45.0;
						pGGB->gyro_target = pGGB->GetGyroTheta() + turn_angle;

						if (pGGB->gyro_target < -180.0) pGGB->gyro_target += 360.0;
						if (pGGB->gyro_target > 180.0) pGGB->gyro_target -= 360.0;

						pGGB->m_setdirection = true;
						pGGB->m_nState += 1;

					}
				}
				else if (pGGB->guide_control_direction == 1) //reverse
				{
					if ((guide_rear_line_exists > 0) && (fabs(guide_rear_lcp2) <= 250.0))
					{
						pGGB->m_SearchGuide = false;
						pGGB->m_GuideTracking = true;
					}
					else
					{
						turn_angle = 45.0;
						pGGB->gyro_target = pGGB->GetGyroTheta() + turn_angle;

						if (pGGB->gyro_target < -180.0) pGGB->gyro_target += 360.0;
						if (pGGB->gyro_target > 180.0) pGGB->gyro_target -= 360.0;

						pGGB->m_setdirection = true;
						pGGB->m_nState += 1;
					}


				}
				break;
			}

			case 2:
			{
				if (pGGB->guide_control_direction == 0) //fwd
				{
					if ((guide_front_line_exists > 0) && (fabs(guide_front_lcp2) <= 250.0))
					{
						pGGB->m_SearchGuide = false;
						pGGB->m_GuideTracking = true;
					}
					else
					{
						_RPT0(_CRT_WARN, "case 2(CGuideMove) m_SearchGuide \n");

						double dAngleDiff_gyro = pGGB->gyro_target - pGGB->GetGyroTheta();


						if (dAngleDiff_gyro > 180.0)	dAngleDiff_gyro -= 360.0;
						else if (dAngleDiff_gyro < -180.0)		dAngleDiff_gyro += 360.0;

						double dAngleDiff = 0.0;

						if (pGGB->isUseGyro())
						{
							dAngleDiff = dAngleDiff_gyro;
						}

						int turning_velocity = 0;

						if (pGGB->m_setdirection)
						{
							if (dAngleDiff > 1.0)
							{
								turning_velocity = 3;
							}
							else
							{
								pGGB->AMRStop();
								turn_angle = -90.0;

								pGGB->gyro_target = pGGB->GetGyroTheta() + turn_angle;

								if (pGGB->gyro_target < -180.0) pGGB->gyro_target += 360.0;
								if (pGGB->gyro_target > 180.0) pGGB->gyro_target -= 360.0;

								pGGB->m_setdirection = false;
								pGGB->m_nState += 1;
							}
						}
						pGGB->SetVW(0, turning_velocity);

					}
				}
				else if (pGGB->guide_control_direction == 1) //reverse
				{
					if ((guide_rear_line_exists > 0) && (fabs(guide_rear_lcp2) <= 250.0))
					{
						pGGB->m_SearchGuide = false;
						pGGB->m_GuideTracking = true;
					}
					else
					{
						_RPT0(_CRT_WARN, "case 2(CGuideMove) m_SearchGuide \n");

						double dAngleDiff_gyro = pGGB->gyro_target - pGGB->GetGyroTheta();

						if (dAngleDiff_gyro > 180.0)	dAngleDiff_gyro -= 360.0;
						else if (dAngleDiff_gyro < -180.0)		dAngleDiff_gyro += 360.0;

						double dAngleDiff = 0.0;

						if (pGGB->isUseGyro())
						{
							dAngleDiff = dAngleDiff_gyro;
						}

						int turning_velocity = 0;

						if (!pGGB->m_setdirection)
						{
							if (dAngleDiff < -1.0)
							{
								turning_velocity = -3;
							}
							else
							{
								pGGB->AMRStop();
								turn_angle = -90.0;

								pGGB->gyro_target = pGGB->GetGyroTheta() + turn_angle;

								if (pGGB->gyro_target < -180.0) pGGB->gyro_target += 360.0;
								if (pGGB->gyro_target > 180.0) pGGB->gyro_target -= 360.0;

								pGGB->m_setdirection = false;
								pGGB->m_nState += 1;
							}
						}
						pGGB->SetVW(0, turning_velocity);
						_RPTN(_CRT_WARN, "target_turning_velocity : %d\n", turning_velocity);
					}
				}

				break;
			}

			case 3:
			{
				if (pGGB->guide_control_direction == 0) //fwd
				{
					if ((guide_front_line_exists > 0) && (fabs(guide_front_lcp2) <= 250.0))
					{
						pGGB->m_SearchGuide = false;
						pGGB->m_GuideTracking = true;
					}
					else
					{
						_RPT0(_CRT_WARN, "case 3(CGuideMove) m_SearchGuide \n");


						double dAngleDiff_gyro = pGGB->gyro_target - pGGB->GetGyroTheta();;

						if (dAngleDiff_gyro > 180.0)	dAngleDiff_gyro -= 360.0;
						else if (dAngleDiff_gyro < -180.0)		dAngleDiff_gyro += 360.0;

						double dAngleDiff = 0.0;

						if (pGGB->isUseGyro())
						{
							dAngleDiff = dAngleDiff_gyro;
						}

						double turning_velocity = 0.0;

						if (dAngleDiff < -1.0)
						{
							turning_velocity = -3.0;
						}
						else
						{
							pGGB->AMRStop();

							pGGB->m_nState += 1;
						}

						pGGB->SetVW(0.0, turning_velocity);

					}
				}
				else if (pGGB->guide_control_direction == 1) //reverse
				{
					if ((guide_rear_line_exists > 0) && (fabs(guide_rear_lcp2) <= 250.0))
					{
						pGGB->m_SearchGuide = false;
						pGGB->m_GuideTracking = true;
					}
					else
					{
						_RPT0(_CRT_WARN, "case 3(CGuideMove) m_SearchGuide \n");

						double dAngleDiff_gyro = pGGB->gyro_target - pGGB->GetGyroTheta();

						if (dAngleDiff_gyro > 180.0)	dAngleDiff_gyro -= 360.0;
						else if (dAngleDiff_gyro < -180.0)		dAngleDiff_gyro += 360.0;

						double dAngleDiff = 0.0;


						if (pGGB->isUseGyro())
						{
							dAngleDiff = dAngleDiff_gyro;
						}


						double turning_velocity = 0.0;

						if (dAngleDiff < -1.0)
						{
							turning_velocity = -3.0;
						}
						else
						{
							pGGB->AMRStop();

							pGGB->robot_status0 = 1; // fail

						}

						pGGB->SetVW(0.0, turning_velocity);
					}
				}

				break;
			} // case3

			default: break;
			} // switch
		}//if

		if (pGGB->m_GuideTracking == true)
		{
			double linear_velocity = 0.0;
			double turning_velocity = 0.0;


			if (pGGB->guide_control_direction == 0) //fwd
			{
				if (guide_front_marker > 0)
				{
					if (pGGB->marker_trigger && !pGGB->first_marker)
					{
						pGGB->first_marker = true;
						pGGB->marker_trigger = false;
					}

					if (pGGB->marker_trigger && pGGB->first_marker && !pGGB->second_marker)
					{
						pGGB->second_marker = true;
						pGGB->marker_trigger = false;
					}


					if (pGGB->second_marker)
					{
						pGGB->AMRStop();
						pGGB->m_GuideTracking = false;
						pGGB->m_FindMarker = true;
						pGGB->m_nState = 1;
						pGGB->robot_status0 = 0; //success
					}
				}
				else
				{
					pGGB->marker_trigger = true;
				}


				if ((guide_front_line_exists > 0) && (fabs(guide_front_lcp2) <= 250.0)) // mm  ( -, +)
				{// guide control forward
					pGGB->guide_blocked_count = 0;

					double dMin_v = 50.0;

					double dMaxRobotGuideVelocity = 200.0; //  --> 추후에 변경 RobotParameter::getInstance()->getMaxRobotGuideVelocity();

					//linear_velocity = 100.0;
					//turning_velocity = -guide_front_lcp2 * 0.05; // [deg/sec]
					double diff = -guide_front_lcp2;

					double diff_th = atan2(diff, 460.0);
					double fabs_diff_th = fabs(diff_th);
					double diff_th_max = atan2(250.0, 460.0);

					if (fabs_diff_th <= (diff_th_max * 0.05))
					{
						linear_velocity = dMaxRobotGuideVelocity;
					}
					else if ((fabs_diff_th > (diff_th_max * 0.05)) && (fabs_diff_th <= (diff_th_max * 0.1)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.95);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.1)) && (fabs_diff_th <= (diff_th_max * 0.2)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.9);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.2)) && (fabs_diff_th <= (diff_th_max * 0.3)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.8);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.3)) && (fabs_diff_th <= (diff_th_max * 0.4)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.7);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.4)) && (fabs_diff_th <= (diff_th_max * 0.5)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.6);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.5)) && (fabs_diff_th <= (diff_th_max * 0.6)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.5);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.6)) && (fabs_diff_th <= (diff_th_max * 0.7)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.4);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.7)) && (fabs_diff_th <= (diff_th_max * 0.8)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.2);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.8)) && (fabs_diff_th <= (diff_th_max * 0.9)))
					{
						//AMR_drive_v = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.1);
						linear_velocity = dMin_v;
					}
					else if (fabs_diff_th > (diff_th_max * 0.9))
					{
						//AMR_drive_v = dMin_v;
						linear_velocity = dMin_v;
					}

					turning_velocity = diff_th * 180.0 / M_PI * 1.2; //0.8

					if (pGGB->first_marker)
					{
						linear_velocity = 30.0;
					}

					pGGB->SetVW(linear_velocity, turning_velocity);


				}
				else
				{
					pGGB->guide_blocked_count++;
				}

			}//if (pGGB->guide_control_direction == 0) //fwd
			else if (pGGB->guide_control_direction == 1) //reverse
			{
				if (guide_rear_marker > 0)
				{
					if (pGGB->marker_trigger && !pGGB->first_marker)
					{
						pGGB->first_marker = true;
						pGGB->marker_trigger = false;
					}

					if (pGGB->marker_trigger && pGGB->first_marker && !pGGB->second_marker)
					{
						pGGB->second_marker = true;
						pGGB->marker_trigger = false;
					}


					if (pGGB->second_marker)
					{
						pGGB->AMRStop();
						pGGB->m_GuideTracking = false;
						pGGB->m_FindMarker = true;
						pGGB->m_nState = 1;
						pGGB->robot_status0 = 0; //success
					}
				}
				else
				{
					pGGB->marker_trigger = true;
				}


				if ((guide_rear_line_exists > 0) && (fabs(guide_rear_lcp2) <= 250.0)) // mm  ( -, +)
				{// guide control forward
					pGGB->guide_blocked_count = 0;

					double dMin_v = -50.0;
					double dMaxRobotGuideVelocity = -200.0; //--> 추후에 변경 RobotParameter::getInstance()->getMaxRobotGuideVelocity();

					//linear_velocity = 100.0;
					//turning_velocity = -guide_front_lcp2 * 0.05; // [deg/sec]
					double diff = -guide_rear_lcp2;

					double diff_th = atan2(diff, 460.0);
					double fabs_diff_th = fabs(diff_th);
					double diff_th_max = atan2(250.0, 460.0);

					if (fabs_diff_th <= (diff_th_max * 0.05))
					{
						linear_velocity = dMaxRobotGuideVelocity;
					}
					else if ((fabs_diff_th > (diff_th_max * 0.05)) && (fabs_diff_th <= (diff_th_max * 0.1)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.95);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.1)) && (fabs_diff_th <= (diff_th_max * 0.2)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.9);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.2)) && (fabs_diff_th <= (diff_th_max * 0.3)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.8);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.3)) && (fabs_diff_th <= (diff_th_max * 0.4)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.7);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.4)) && (fabs_diff_th <= (diff_th_max * 0.5)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.6);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.5)) && (fabs_diff_th <= (diff_th_max * 0.6)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.5);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.6)) && (fabs_diff_th <= (diff_th_max * 0.7)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.4);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.7)) && (fabs_diff_th <= (diff_th_max * 0.8)))
					{
						linear_velocity = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.2);
					}
					else if ((fabs_diff_th > (diff_th_max * 0.8)) && (fabs_diff_th <= (diff_th_max * 0.9)))
					{
						//AMR_drive_v = dMin_v + ((dMaxRobotGuideVelocity - dMin_v) * 0.1);
						linear_velocity = dMin_v;
					}
					else if (fabs_diff_th > (diff_th_max * 0.9))
					{
						//AMR_drive_v = dMin_v;
						linear_velocity = dMin_v;
					}

					turning_velocity = diff_th * 180.0 / M_PI * 0.8;

					if (pGGB->first_marker)
					{
						linear_velocity = -30.0;
					}

					pGGB->SetVW(linear_velocity, turning_velocity);


				}
				else
				{
					pGGB->guide_blocked_count++;
				}
			}

			if (pGGB->guide_blocked_count > 20)
			{
				pGGB->AMRStop();

				_RPT0(_CRT_WARN, " (pGGB->guide_blocked_count > 20) \n");
				pGGB->m_GuideTracking = false;
				pGGB->m_SearchGuide = true;
				pGGB->guide_blocked_count = 0;

				if (pGGB->guide_control_direction == 1)
				{
					pGGB->m_SearchGuide = false;
					pGGB->m_GuideTracking = false;
					pGGB->m_FindMarker = false;
					pGGB->m_nState = 1;
				}
			}

		}

		// Second Step :WorkPlace Sequence

		/////////////////////////////////////////////////////////////////////////////////////////////////
		if (pGGB->m_FindMarker) // WorkPlace Sequence
		{
			pGGB->AMRStop();
			pGGB->m_bThreadFlag = false;

			pGGB->m_nState = 1;
		}

		// Keep the Thread Period
		runtime = clock() - start;
		if (pGGB->m_nThreadPeriod - runtime > 0)
			Sleep(pGGB->m_nThreadPeriod - runtime);

	}	
	//20190906 SG_ADD
	std::cout << "Thread End : ThreadFunctionGuideMove" << std::endl;

	return RETURN_NONE_ERROR;

} // doThread End


void CGuideMove::terminate()
{
	AMRStop();

	double dX = 0.0, dY = 0.0, dTh = 0.0;
	GetTargetNode(&dX, &dY, &dTh);
	
	
	// particle timing ????????????????
	// 추가 해야한다
	//OdoMetryResetFrom(dX, dY, (dTh * 100.0));



	//Sound는 박선임에게 물어보고 처리
	//m_maindlg->sound_number |= SOUND_WORKPLACE_ARRIVE_STATE_ON;
	//m_maindlg->workplace_arrive_repeat_enable = true;
	//m_maindlg->workplace_arrive_enable_count = 0;


	m_bThreadFlag = false;

	m_nState = 1;

	if (m_GuideMoveThread.joinable())
	{
		m_GuideMoveThread.join();
	}

	//이것도 나중에 수정
	//m_maindlg->led_number &= LED_GUIDE_STATE_OFF;

	//m_maindlg->AGV_Command = 0;

	//_RPT0(_CRT_WARN, "CGuideMove terminate!!\n");
	// 주행 종료 상태 설정 ok

	//현재 State 정의가 하나도 안돼 있다. 
	//m_maindlg->m_pStatusSock->WorkFinishStatusSet();
	//m_maindlg->m_pStatusSock->StatusSet(190, 192);

}


int CGuideMove::AMRStop()
{
	m_dV = 0;
	m_dW = 0;

	return RETURN_NONE_ERROR;
}

int CGuideMove::SetFrontLcpData(short lcp1, short lcp2, short lcp3)
{
	m_sFrontGuideLcp1 = lcp1;
	m_sFrontGuideLcp2 = lcp2;
	m_sFrontGuideLcp3 = lcp3;
	return RETURN_NONE_ERROR;
}

int CGuideMove::SetRearLcpData(short lcp1, short lcp2, short lcp3)
{
	m_sRearGuideLcp1 = lcp1;
	m_sRearGuideLcp2 = lcp2;
	m_sRearGuideLcp3 = lcp3;
	return RETURN_NONE_ERROR;
}

int CGuideMove::GetFrontLcpData(short* lcp1, short* lcp2, short* lcp3)
{	
	*lcp1 = m_sFrontGuideLcp1;
	*lcp2 = m_sFrontGuideLcp2;
	*lcp3 = m_sFrontGuideLcp3;

	return RETURN_NONE_ERROR;
}

int CGuideMove::GetRearLcpData(short* lcp1, short* lcp2, short* lcp3)
{
	*lcp1 = m_sRearGuideLcp1;
	*lcp2 = m_sRearGuideLcp2;
	*lcp3 = m_sRearGuideLcp3;

	return RETURN_NONE_ERROR;
}

int CGuideMove::SetTargetNode(int iNode)
{
	WorkPlacePoint WP;
	if (SetTargetNodeToConvertNode(iNode, &WP) != RETURN_NONE_ERROR)
	{
		return RETURN_FAILED;
	}
	
	m_dTargetX = WP.x;
	m_dTargetY = WP.y;
	m_dTargetTh = WP.th_deg;
	m_iTargetDirection = WP.guide_control_direction;

	return RETURN_NONE_ERROR;
}

int	CGuideMove::SetTargetNodeToConvertNode(int inputNode, WorkPlacePoint* rWP)
{
	int seq = 0;
	bool bFindWorkplace = false;
	for (int i = 0; i < m_iWorkPlacePointCount; i++)
	{
		if (inputNode == m_WP[i].pre_node_no)
		{
			seq = i;
			bFindWorkplace = true;
			break;
		}
	}

	if (!bFindWorkplace)
	{
		return 1;
	}
	else
	{
		*rWP = m_WP[seq];
	}

	return RETURN_NONE_ERROR;	
}


int CGuideMove::GetTargetNode(double *dX, double *dY, double *dTh)
{
	*dX = m_dTargetX;
	*dY = m_dTargetY;
	*dTh = m_dTargetTh;

	return RETURN_NONE_ERROR;
}

int CGuideMove::LoadWorkPlacePoint()
{
	ifstream fin;
	char inputString[100];
	char* type;
	//char* token;
	//int i;

	//fin.open("./data/workplacepoint.txt");
	string workplacepointname = RobotParameter::getInstance()->getWorkPlacePointNameNPath();
	fin.open(workplacepointname.c_str());

	int no, x, y, th_deg, pre_node_no, guide_control_direction, obs_flag, max_v, max_a, doorArea, reflector_flag, reflector_useSide;

	fin.getline(inputString, 100);
	m_iWorkPlacePointCount = atoi(inputString);

	m_WP = new WorkPlacePoint[m_iWorkPlacePointCount];

	int nodecountvar = 0;
	//while(!fin.eof())
	// bwk 2017.01.17 - 수정 : 마지막행 공백시 에러 발생 해결

	for (nodecountvar = 0; nodecountvar < m_iWorkPlacePointCount; nodecountvar++)
	{
		fin.getline(inputString, 100);
		char* token = strtok_s(inputString, ",", &type);
		no = atoi(token);

		token = strtok_s(NULL, ",", &type);
		x = atoi(token);

		token = strtok_s(NULL, ",", &type);
		y = atoi(token);

		token = strtok_s(NULL, ",", &type);
		th_deg = atoi(token);

		token = strtok_s(NULL, ",", &type);
		pre_node_no = atoi(token);

		token = strtok_s(NULL, ",", &type);
		guide_control_direction = atoi(token);

		token = strtok_s(NULL, ",", &type);
		obs_flag = atoi(token);

		token = strtok_s(NULL, ",", &type);
		max_v = atoi(token);

		token = strtok_s(NULL, ",", &type);
		max_a = atoi(token);

		token = strtok_s(NULL, ",", &type);			//pjs 2019.03.13 노드의 속성 추가, Input 1, middle 2, output 3
		doorArea = atoi(token);

		token = strtok_s(NULL, ",", &type);
		reflector_flag = atoi(token);

		token = strtok_s(NULL, ",", &type);
		reflector_useSide = atoi(token);

		m_WP[nodecountvar].no = no;
		m_WP[nodecountvar].x = x;								// mm
		m_WP[nodecountvar].y = y;								// mm
		m_WP[nodecountvar].th_deg = th_deg;					// bwk 2017.04.10 - 수정 : 시작점 방향 정보를 추가
		m_WP[nodecountvar].pre_node_no = pre_node_no;					// bwk 2017.04.10 - 수정 : 시작점 방향 정보를 추가
		m_WP[nodecountvar].guide_control_direction = guide_control_direction;		// bwk 2017.04.10 - 수정 : 시작점 방향 정보를 추가

		m_WP[nodecountvar].obs_flag = obs_flag;					// bwk 2017.04.05 - 수정 : 장애물 감지 범위 - 0 : 사용안함, 1~n : 정의된 범위
		m_WP[nodecountvar].max_v = max_v;						// bwk 2017.03.29 - 수정 : 구간별 속도 설정(0 : 기존 제한 속도, 기타 : 구간별 제한 속도)
		m_WP[nodecountvar].max_a = max_a;						// bwk 2017.03.29 - 수정 : 구간별 각속도 설정(0 : 기존 제한 각속도, 기타 : 구간별 제한 각속도)
		m_WP[nodecountvar].doorArea = doorArea;					// bwk 2017.03.29 - 수정 : Door 구간 설정(0 : 일반지역, 1 : Door Area)
		m_WP[nodecountvar].reflector_flag = reflector_flag;		// bwk 2017.06.16 - 수정 : Reflector 구간 설정(0 : 일반지역, 1~ : Reflector Mode)
																// 1 : LASER_SCANNING_REFLECTOR_MODE_POINT_STOP : 반사판 이용하여 정지시키는 모드(반사판 2개 사용(2개서 1개로 변하는 시점에 정지, 간격은 약 1m)
																// 2 : LASER_SCANNING_REFLECTOR_MODE_GUIDE_FIND : 반사판 이용하여 반사판 방향으로 회전시키고, 설정 거리만큼 이동시키는 모드(반사판 1개 사용)
																// 3 : LASER_SCANNING_REFLECTOR_MODE_POINT_COUNT : 반사판 이용하여 위치를 Count or 보정하는 모드(반사판 1개 사용, 간격은 약 10m)
		m_WP[nodecountvar].reflector_useSide = reflector_useSide;	// bwk 2017.06.16 - 수정 : Reflector 구간 사용(0 : 전체사용, 1 : 왼쪽만 사용, 2 : 오른쪽만 사용)

																//nodecountvar++;		// bwk 2017.01.17 - 수정 : 마지막행 공백시 에러 발생 해결
	}

	fin.close();

	return RETURN_NONE_ERROR;
}

int CGuideMove::CreateThread()
{
	m_bThreadFlag = true;
	m_GuideMoveThread = std::thread(ThreadFunctionGuideMove, this);

	return RETURN_NONE_ERROR;
}