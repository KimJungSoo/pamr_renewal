#pragma once

#include "sensor\SensorModule.h"
#include "robot\RobotPose.h"
#include "util\PUtil.h"

#ifndef M_PI
#define M_PI		3.14159265358979323846	// pi 
#endif
#define D2R 0.017453292
#define R2D 57.29579143
#define LASER_SCANNING_VALUE_REFLECTOR_OBJECT 60

namespace AMRmove {
	typedef struct _tag_PointObject
	{
		int nIndex;
		int nScanToObjectDist;
		int nPointCount;
		int nWidth;				// mm

		int nFirst_x;			// mm
		int nFirst_y;			// mm

		int nEnd_x;				// mm
		int nEnd_y;				// mm

		int nCenter_x;			// mm
		int nCenter_y;			// mm
		int nCenter_th_deg;		// degree
		int nCenter_Distance;	// mm
	} PointObject;
	typedef struct _tag_LandmarkPoint
	{
		int x;
		int y;
		double dist;
		int objectindex = -1;
	} LandmarkPoint;
	enum ePreciseMoveState {
		PreciseMove_Initial,
		PreciseMove_GoToReflectorCentor,
		PreciseMove_Turning,
		PreciseMove_MoveFront,
		PreciseMove_StandBy,
		PreciseMove_MoveBack,
		PreciseMove_Done,
		PreciseMove_Error,
	};
	class CPreciseMove {
	private:
		int m_nState;
		double m_dV, m_dW, m_dMaxV, m_dMaxW;
		int m_nTargetOffset;

		RobotPose m_RobotPos, m_PrecisePos;
		std::vector<LandmarkPoint> m_Landmark;
		std::vector<LandmarkPoint> m_DetectedLandmark;
		int m_nTargetLandmarkIndex1, m_nTargetLandmarkIndex2;
		PointObject m_TargetLine;

		// Object
		int m_dGroupLimit;
		int m_nMaxObjectDist;
		int m_nObjectsize;
		int m_nPositionError;
		int m_nMinObjectDetectionSize;
		std::vector<PointObject> m_PointObject;

		int m_nNumOfLaserScanner;
		sensor::CSensorModule ** m_sensor;

		int getLandmark(std::string fileName);
		int searchReflector();
		std::vector<PointObject> findObject(LaserScanData scanData, RobotPose sensorPose);
		LaserScanData medianFilter(LaserScanData scanData, int nWindowSize);
		void ScannerToRobotCoor(RobotPose sensorPos, LaserScanData scanData, int index, double *X, double *Y);
		void RobotCoorToWorldCoor(RobotPose curPos, double nRobotX, double nRobotY, double *nImageX, double *nImageY);
		void bubbleSort(UINT16 * list, int size);
		int userDefinedKanayama(RobotPose currentPos, RobotPose referencePos, double Kx, double Ky, double Kt);
		int userDefinedTurnningKanayama(RobotPose currentPos, RobotPose referencePos, double Kx, double Ky, double Kt);

		// Ransac Algorithm
		int RansacCircleCentor(std::vector<POINT> point, PointObject * po);

	public:
		CPreciseMove();
		~CPreciseMove();
		int getPrecisePosition();
		int doPreciseMoving();
		void PreciseMoveStart() { m_nState = PreciseMove_Initial; }
		void setRobotPose(RobotPose pos) { m_RobotPos = pos; }
		void setSensor(sensor::CSensorModule * sensor, int index) { m_sensor[index] = sensor; }
		int setTargetLandmarkIndex(int, int);
		double getV() { return m_dV; }
		double getW() { return m_dW; }
		std::vector<LandmarkPoint> getLandmark() { return m_Landmark; }
		std::vector<PointObject> getObjectPoint() { return m_PointObject; }
		PointObject CPreciseMove::getTargetLine() { return m_TargetLine; }
		RobotPose getPreciseRobotPose() { return m_PrecisePos; }
	};
}
