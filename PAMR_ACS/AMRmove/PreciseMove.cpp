#include "stdafx.h"
#include "PreciseMove.h"
#include "sensor\SICKLaserScanner.h"
#include <string>
#include <fstream>
#include <iostream>

using namespace sensor;
using namespace AMRmove;
using namespace eventManager;

CPreciseMove::CPreciseMove()
{
	m_nNumOfLaserScanner = 2;
	m_sensor = DEBUG_NEW CSensorModule*[m_nNumOfLaserScanner];
	for (int i = 0; i < m_nNumOfLaserScanner; i++)
		m_sensor[i] = NULL;

	// 주행관련
	m_dV = m_dW = 0;
	m_dMaxV = 200;
	m_dMaxW = 20 * D2R;
	m_nTargetOffset = 300 + 500; // 반사봉에서 offset만큼 떨어진 위치에서 정지

	m_dGroupLimit = 50;
	m_nMinObjectDetectionSize = 3;
	m_nMaxObjectDist = 20000; // 20m 이내의 반사봉 감지
	m_nObjectsize = 90;	// 반사봉 지름
	m_nPositionError = 500;

	m_nTargetLandmarkIndex1 = -1;
	m_nTargetLandmarkIndex2 = -1;

	//m_Landmark = {};
	//m_DetectedLandmark = {};
	//m_PointObject = {};
	m_Landmark.clear();
	m_DetectedLandmark.clear();
	m_PointObject.clear();

	m_PrecisePos.setX(0);
	m_PrecisePos.setY(0);

	// Landmark 정보 가져오기
	if (getLandmark("datafiles/" + g_strAMRVersion + "/landmark.txt") == RETURN_NONE_ERROR)
		m_nState = PreciseMove_Initial;
	else
		m_nState = PreciseMove_Error;
}

CPreciseMove::~CPreciseMove()
{
	m_nState = PreciseMove_Initial;
	m_dV = m_dW = 0;

	SAFE_DELETEA(m_sensor);

}

int CPreciseMove::getPrecisePosition()
{
	if (searchReflector() == RETURN_NONE_ERROR)
	{
		if (m_DetectedLandmark.size() < 3) return RETURN_NONE_ERROR;
		vector<LandmarkPoint> temp_Landmark = m_DetectedLandmark;
		vector<vector<double>> A(temp_Landmark.size() - 1, vector<double>(2, 0));
		vector<vector<double>> tranA(2, vector<double>(temp_Landmark.size() - 1, 0));
		vector<vector<double>> ProductA(2, vector<double>(2, 0));
		vector<vector<double>> InvProductA(2, vector<double>(2, 0));
		vector<double> ATB(2, 0);
		vector<double> B(temp_Landmark.size() - 1, 0);

		// Matrix 구성
		for (int i = 0; i < temp_Landmark.size() - 1; i++)
		{
			int xi = temp_Landmark[i].x;
			int yi = temp_Landmark[i].y;
			int di = sqrt(pow(temp_Landmark[i].x - m_RobotPos.getX(), 2) + pow(temp_Landmark[i].y - m_RobotPos.getY(), 2));
			int xi1 = temp_Landmark[i + 1].x;
			int yi1 = temp_Landmark[i + 1].y;
			int di1 = sqrt(pow(temp_Landmark[i + 1].x - m_RobotPos.getX(), 2) + pow(temp_Landmark[i + 1].y - m_RobotPos.getY(), 2));

			// A, A^T
			A[i][0] = tranA[0][i] = 2 * (xi - xi1);
			A[i][1] = tranA[1][i] = 2 * (yi - yi1);

			// (A^T * A)
			ProductA[0][0] += tranA[0][i] * A[i][0];
			ProductA[0][1] += tranA[0][i] * A[i][1];
			ProductA[1][0] += tranA[1][i] * A[i][0];
			ProductA[1][1] += tranA[1][i] * A[i][1];

			// B
			B[i] = (xi1*xi1 + yi1*yi1 - di1*di1) - (xi*xi + yi*yi - di*di);

			// A^T * B
			ATB[0] -= tranA[0][i] * B[i];
			ATB[1] -= tranA[1][i] * B[i];
		}

		// 역행렬 (A^T * A)^-1
		double det = ProductA[0][0] * ProductA[1][1] - ProductA[1][0] * ProductA[0][1];
		if (det == 0.)
		{
			temp_Landmark.clear();
			B.clear();
			ATB.clear();
			InvProductA.clear();
			ProductA.clear();
			tranA.clear();
			A.clear();
			return RETURN_FAILED;
		}
		InvProductA[0][0] = ProductA[1][1] / det;
		InvProductA[0][1] = -ProductA[0][1] / det;
		InvProductA[1][0] = -ProductA[1][0] / det;
		InvProductA[1][1] = ProductA[0][0] / det;

		// Solve Linear EQ: X = (A^T * A )^-1 * A^T * B
		m_PrecisePos.setX((int)(InvProductA[0][0] * ATB[0] + InvProductA[0][1] * ATB[1]));
		m_PrecisePos.setY((int)(InvProductA[1][0] * ATB[0] + InvProductA[1][1] * ATB[1]));

		temp_Landmark.clear();
		B.clear();
		ATB.clear();
		InvProductA.clear();
		ProductA.clear();
		tranA.clear();
		A.clear();
	}
	else
		return RETURN_FAILED;
	return RETURN_NONE_ERROR;
}

int CPreciseMove::doPreciseMoving()
{
	double Theta_e;

	switch (m_nState)
	{
	case PreciseMove_Initial:
	case PreciseMove_GoToReflectorCentor:
		if (searchReflector() == RETURN_NONE_ERROR)
		{
			double dTargetSlope = (m_TargetLine.nFirst_y - m_TargetLine.nEnd_y) / double(m_TargetLine.nFirst_x - m_TargetLine.nEnd_x);
			double dRobotSlope = tan(m_RobotPos.getThetaRad());

			// TargetLine의 직교선과 로봇의 연장선과의 만나는 점 Target(x, y)을 구함
			RobotPose Target;
			Target.setX((int)(((m_TargetLine.nCenter_x / dTargetSlope) + m_TargetLine.nCenter_y + (dRobotSlope*m_RobotPos.getX()) - m_RobotPos.getY())
				/ ((1 / dTargetSlope) + dRobotSlope)));
			Target.setY((int)(dRobotSlope*(Target.getX() - m_RobotPos.getX()) + m_RobotPos.getY()));
			Target.setThetaRad(m_RobotPos.getThetaRad());

			// V, W 를 구함
			
			if (userDefinedKanayama(m_RobotPos, Target, 0.4, 0.01, 0.2) == RETURN_NONE_ERROR)
				m_nState = PreciseMove_Turning;
		}
		// Target 반사봉을 못찾았을 때 정지
		else
		{
			m_dV = 0.;
			m_dW = 0.;
		}
		break;
		// Step2. Turning 
	case PreciseMove_Turning:
		if (searchReflector() == RETURN_NONE_ERROR)
		{
			RobotPose Target;
			Target.setThetaRad(atan2(m_TargetLine.nCenter_y - m_RobotPos.getY(), m_TargetLine.nCenter_x - m_RobotPos.getX()));

			// V, W 를 구함
			if (userDefinedTurnningKanayama(m_RobotPos, Target, 0., 0., 0.2) == RETURN_NONE_ERROR)
				m_nState = PreciseMove_MoveFront;

		}
		// Target 반사봉을 못찾았을 때 정지
		else
		{
			m_dV = 0.;
			m_dW = 0.;
		}
		break;
		// Step3. Precise Move
	case PreciseMove_MoveFront:
		if (searchReflector() == RETURN_NONE_ERROR)
		{
			double dTargetSlope = (m_TargetLine.nFirst_y - m_TargetLine.nEnd_y) / double(m_TargetLine.nFirst_x - m_TargetLine.nEnd_x);
			double dRobotSlope = tan(m_RobotPos.getThetaRad());

			// TargetLine에서 Offset만큼 떨어진 지점까지 주행
			RobotPose Target;
			Target.setX((int)(m_TargetLine.nCenter_x));
			Target.setY((int)(m_TargetLine.nCenter_y) - m_nTargetOffset);
			Target.setThetaRad(m_RobotPos.getThetaRad());

			// V, W 를 구함
			if (userDefinedKanayama(m_RobotPos, Target, 0.2, 0.006, 0.01) == RETURN_NONE_ERROR)
				m_nState = PreciseMove_Done;
		}
		// Target 반사봉을 못찾았을 때 정지
		else
		{
			m_dV = 0.;
			m_dW = 0.;
		}
		break;
	case PreciseMove_StandBy:
		m_nState = PreciseMove_MoveBack;
		break;
	case PreciseMove_MoveBack:
		m_nState = PreciseMove_Done;
		break;
	case PreciseMove_Done:
		m_dV = 0.;
		m_dW = 0.;
		return RETURN_NONE_ERROR;
		break;
	case PreciseMove_Error:
		break;
	}
	return RETURN_CONTINUE;
}

// *************************** private ***************************

int CPreciseMove::getLandmark(string fileName)
{
	ifstream fin;
	char inputString[100];
	fin.open(fileName.c_str());
	if (!fin.is_open()) {
		//		g_eventManager.PushTask(MSG_ERROR, ERROR_NO_LANDMARK_FILE, true, false);
		return RETURN_FAILED;
	}

	// Number of Nodes
	fin.getline(inputString, 100);
	int nLandmarkSize = atoi(inputString);

	for (int i = 0; i < nLandmarkSize; i++)
	{
		LandmarkPoint p;
		char * context = NULL;
		fin.getline(inputString, 100);
		strtok_s(inputString, ",", &context);
		p.x = atoi(inputString);
		p.y = atoi(context);
		m_Landmark.push_back(p);
	}
	return RETURN_NONE_ERROR;
}

int CPreciseMove::searchReflector()
{
	int ret = RETURN_NONE_ERROR;

	// 스캔 데이터 가져오기
	CSICKLaserScanner * lms_front = dynamic_cast<CSICKLaserScanner *>(m_sensor[0]);
	CSICKLaserScanner * lms_rear = dynamic_cast<CSICKLaserScanner *>(m_sensor[1]);

	LaserScanData scanData_front, filteredScanData_front;
	LaserScanData scanData_rear, filteredScanData_rear;

	//20190906 SG_ADD
	if (!lms_front->getData(&scanData_front, 4)) return RETURN_FAILED;
	if (!lms_rear->getData(&scanData_rear, 4)) return RETURN_FAILED;

	//20190906 SG_수정
	scanData_front.dist = DEBUG_NEW UINT16[scanData_front.nData_len];
	scanData_front.rssi = DEBUG_NEW UINT16[scanData_front.nData_len];// Median Filter

	scanData_rear.dist = DEBUG_NEW UINT16[scanData_rear.nData_len];
	scanData_rear.rssi = DEBUG_NEW UINT16[scanData_rear.nData_len];// Median Filter

	filteredScanData_front = medianFilter(scanData_front, 3);
	filteredScanData_rear = medianFilter(scanData_rear, 3);

	// 반사봉 찾고 없으면 failed
	// front scanner
	m_PointObject.clear();
	RobotPose sensorPose;
	sensorPose.setX(450);
	sensorPose.setY(450);
	sensorPose.setThetaDeg(45);
	vector<PointObject> vtTempPointObject = findObject(filteredScanData_front, sensorPose);
	for (int i = 0; i < (int)vtTempPointObject.size(); i++)
		m_PointObject.push_back(vtTempPointObject[i]);
	//vtTempPointObject = {};
	vtTempPointObject.clear();

	// rear scanner
	sensorPose.setX(-450);
	sensorPose.setY(-450);
	sensorPose.setThetaDeg(-135);
	vtTempPointObject = findObject(filteredScanData_rear, sensorPose);
	for (int i = 0; i < (int)vtTempPointObject.size(); i++)
		m_PointObject.push_back(vtTempPointObject[i]);
	//vtTempPointObject = {};
	vtTempPointObject.clear();

	if (m_PointObject.size() == 0) ret = RETURN_FAILED;
	 
	// landmark와 매칭
	m_DetectedLandmark.clear();
	for (int i = 0; i < (int)m_Landmark.size(); i++)
	{
		m_Landmark[i].dist = m_nObjectsize + m_nPositionError;
		m_Landmark[i].objectindex = -1;
		for (int j = 0; j < (int)m_PointObject.size(); j++)
		{
			double nDist = sqrt(pow(m_Landmark[i].x - m_PointObject[j].nCenter_x, 2) + pow(m_Landmark[i].y - m_PointObject[j].nCenter_y, 2));
			if (m_Landmark[i].dist > nDist)
			{
				m_Landmark[i].dist = nDist;
				m_Landmark[i].objectindex = j;
				m_DetectedLandmark.push_back(m_Landmark[i]);
			}
		}
	}

	// 반사봉을 찾으면 타겟포인트를 설정
	if (m_nTargetLandmarkIndex1 != -1 && m_nTargetLandmarkIndex2 != -1)
	{
		if (m_Landmark[m_nTargetLandmarkIndex1].objectindex != -1 && m_Landmark[m_nTargetLandmarkIndex2].objectindex != -1)
		{
			m_TargetLine.nFirst_x = m_PointObject[m_Landmark[m_nTargetLandmarkIndex1].objectindex].nCenter_x;
			m_TargetLine.nFirst_y = m_PointObject[m_Landmark[m_nTargetLandmarkIndex1].objectindex].nCenter_y;
			m_TargetLine.nEnd_x = m_PointObject[m_Landmark[m_nTargetLandmarkIndex2].objectindex].nCenter_x;
			m_TargetLine.nEnd_y = m_PointObject[m_Landmark[m_nTargetLandmarkIndex2].objectindex].nCenter_y;
			m_TargetLine.nCenter_x = (m_TargetLine.nFirst_x + m_TargetLine.nEnd_x) / 2;
			m_TargetLine.nCenter_y = (m_TargetLine.nFirst_y + m_TargetLine.nEnd_y) / 2;
		}
		else
			ret = RETURN_FAILED;
	}
	else
		ret = RETURN_FAILED;

	// 메모리 해제
	vtTempPointObject.clear();

	//SAFE_DELETEA(filteredScanData_rear.rssi);
	//SAFE_DELETEA(filteredScanData_rear.dist);
	//SAFE_DELETEA(filteredScanData_front.rssi);
	//SAFE_DELETEA(filteredScanData_front.dist);
	
	//20190906 SG_수정
	SAFE_DELETEA(scanData_front.rssi);
	SAFE_DELETEA(scanData_front.dist);
	SAFE_DELETEA(scanData_rear.rssi);
	SAFE_DELETEA(scanData_rear.dist);
	return ret;
}

vector<PointObject> CPreciseMove::findObject(LaserScanData scanData, RobotPose sensorPose)
{
	vector<PointObject> vtPointObject;
	int nObjectCheckIndex = 0;
	int nObjectPointCount = 0;
	int nObjectCount = 0;
	for (int i = 0; i < scanData.nData_len; i++)
	{
		// 20mm 보다 가깝거나 m_nMaxObjectDist 보다 멀면 제외
		if (scanData.dist[i] < 20 || scanData.dist[i] > m_nMaxObjectDist)
		{
			nObjectPointCount = 0;
			continue;
		}

		// 반사판 여부 판단
		if (scanData.rssi[i] < LASER_SCANNING_VALUE_REFLECTOR_OBJECT)		// 반사판 광량 수치와 비교
		{
			nObjectPointCount = 0;
			continue;
		}
		nObjectPointCount++;

		int nCheckInvalidValue_Reflector = 0;
		int nCheckInvalidValue_Gap = 0;

		for (int nCount = i + 1; nCount < scanData.nData_len; nCount++)
		{
			nObjectCheckIndex = nCount;
			// 반사판 여부 판단
			if (scanData.rssi[nCount] < LASER_SCANNING_VALUE_REFLECTOR_OBJECT)		// 반사판 광량 수치와 비교
			{
				// 연속하여 튀는 값을 몇회까지 허용해줌
				if (nCheckInvalidValue_Reflector >= 1)
					break;
				else
					nCheckInvalidValue_Reflector++;
			}
			// 이전 Data와 거리 값이 제한 값 이내일 경우
			if (fabs(scanData.dist[nCount - 1] - scanData.dist[nCount]) < m_dGroupLimit)
			{
				nObjectPointCount++;
				nCheckInvalidValue_Gap = 0;			// 연속되지 않은 값은 체크값을 초기화
			}
			else
			{
				// 연속하여 튀는 값을 몇회까지 허용해줌
				if (nCheckInvalidValue_Gap >= 1)
					break;
				// 거리 제한 값은 한번이라도 허용하면, 뒷쪽의 물체와 구분이 안되므로 허용하면 안되나, 경사면의 경우 거리가 멀어질수록 포인트 간격이 넓어져 다른 물체로 인식되므로, 한번 더 체크하여 같은 물체인지 체크함(해결해야 함)
				else
				{
					if (nCount < scanData.nData_len - 1)
					{
						// 튀는 값 이후 Data와 거리 값이 제한 값 이내일 경우 같은 물체로 확정
						if (fabs(scanData.dist[nCount - 1] - scanData.dist[nCount + 1]) < m_dGroupLimit)
						{
							nCheckInvalidValue_Gap++;
						}
						else
						{
							// 튀는 값과 튀는 값 이후 Data와 거리 값이 제한 값 이내일 경우, 이전 조건으로 인해 다른 물체로 확정
							if (fabs(scanData.dist[nCount] - scanData.dist[nCount + 1]) < m_dGroupLimit)
								break;
							else
								nCheckInvalidValue_Gap++;
						}
					}
					else
						break;
				}
			}
		}

		if (nObjectPointCount >= m_nMinObjectDetectionSize)		// 일정 크기 이상의 물체
		{
			// 측정점 좌표구하기
			int nIndex_First = i;
			int nIndex_End = i + nObjectPointCount - 1;
			int nIndex_Center = nIndex_First + (int)((nIndex_End - nIndex_First) / 2.);

			// Ransac Circle Fit
			vector<POINT> ReflectorScanData;
			for (int i = nIndex_First; i < nIndex_End; i++)
			{
				double dX, dY;
				POINT p;
				ScannerToRobotCoor(sensorPose, scanData, i, &dX, &dY);
				RobotCoorToWorldCoor(m_RobotPos, dX, dY, &dX, &dY);
				p.x = dX;
				p.y = dY;
				ReflectorScanData.push_back(p);
			}
			PointObject tempPointObject;
			RansacCircleCentor(ReflectorScanData, &tempPointObject);
			vtPointObject.push_back(tempPointObject);
			nObjectCount++;

			ReflectorScanData.clear();
		}

		i = nObjectCheckIndex;
		nObjectPointCount = 0;
	}
	return vtPointObject;
}

LaserScanData CPreciseMove::medianFilter(LaserScanData scanData, int nWindowSize)
{
	LaserScanData FilteredScanData = scanData;

	//20190906 SG_수정
	//FilteredScanData.dist = DEBUG_NEW UINT16[scanData.nData_len];
	//FilteredScanData.rssi = DEBUG_NEW UINT16[scanData.nData_len];

	UINT16 * distBuffer = DEBUG_NEW UINT16[nWindowSize];
	UINT16 * rssiBuffer = DEBUG_NEW UINT16[nWindowSize];
	int nWindowCentor = nWindowSize / 2;
	for (int i = nWindowCentor; i < scanData.nData_len - nWindowCentor; i++)
	{
		// 데이터 가져오기
		for (int j = 0; j < nWindowSize; j++)
		{
			distBuffer[j] = scanData.dist[i + j - nWindowCentor];
			rssiBuffer[j] = scanData.rssi[i + j - nWindowCentor];
		}

		// 버블 Sort
		bubbleSort(distBuffer, nWindowSize);
		bubbleSort(rssiBuffer, nWindowSize);

		// 대입
		FilteredScanData.dist[i] = distBuffer[nWindowCentor];
		FilteredScanData.rssi[i] = rssiBuffer[nWindowCentor];
	}
	SAFE_DELETEA(rssiBuffer);
	SAFE_DELETEA(distBuffer);
	return FilteredScanData;
}

void CPreciseMove::bubbleSort(UINT16 * list, int size) {
	UINT16 temp;
	for (int i = size - 1; i > 0; i--) {
		for (int j = 0; j < i; j++) {
			if (list[j]<list[j + 1]) {
				temp = list[j];
				list[j] = list[j + 1];
				list[j + 1] = temp;
			}
		}
	}
}

void CPreciseMove::ScannerToRobotCoor(RobotPose sensorPos, LaserScanData scanData, int index, double *X, double *Y) {
	double deg = double(scanData.nStart_angle - sensorPos.getThetaDeg()) + (index / double(scanData.nAngleResolution));
	UINT16 dist = scanData.dist[index];
	*X = sensorPos.getX() + dist * cos(deg*D2R);
	*Y = sensorPos.getY() + dist * sin(deg*D2R);
}

void CPreciseMove::RobotCoorToWorldCoor(RobotPose curPos, double nRobotX, double nRobotY, double *nImageX, double *nImageY)
{
	// x축 대칭 행렬 * Theta 회전 행렬 = (cos, sin; sin, -cos)
	double x = nRobotX*cos(curPos.getThetaRad()) + nRobotY*sin(curPos.getThetaRad());
	double y = nRobotX*sin(curPos.getThetaRad()) - nRobotY*cos(curPos.getThetaRad());
	*nImageX = curPos.getX() + x;
	*nImageY = curPos.getY() + y;
}

int CPreciseMove::setTargetLandmarkIndex(int index1, int index2)
{
	if (index1 < 0 || index1 >= m_Landmark.size() || index2 < 0 || index2 >= m_Landmark.size())
		return RETURN_FAILED;
	m_nTargetLandmarkIndex1 = index1;
	m_nTargetLandmarkIndex2 = index2;
	return RETURN_NONE_ERROR;
}

int CPreciseMove::userDefinedKanayama(RobotPose currentPos, RobotPose referencePos, double Kx, double Ky, double Kt)
{
	double dV = 0, dW = 0;
	double Theta_c = currentPos.getThetaRad();
	double X_e = (referencePos.getX() - currentPos.getX()) * cos(Theta_c) + (referencePos.getY() - currentPos.getY()) * sin(Theta_c);
	double Y_e = -(referencePos.getX() - currentPos.getX()) * sin(Theta_c) + (referencePos.getY() - currentPos.getY()) * cos(Theta_c);
	double Theta_e = referencePos.getThetaRad() - currentPos.getThetaRad();
	double v_ref = 0.0;

	if (abs(X_e) < 10)
	{
		dV = 0.;
		dW = 0.;
		return RETURN_NONE_ERROR;
	}
	else if (abs(X_e) < 150)
		v_ref = 0.3;
	else if (abs(X_e) < 300)
		v_ref = 0.6;
	else
		v_ref = 1.;

	dV = v_ref * m_dMaxV;
	m_dW = 0.;

	X_e > 0 ? m_dV = dV : m_dV = -dV;

	return RETURN_CONTINUE;
}

int CPreciseMove::userDefinedTurnningKanayama(RobotPose currentPos, RobotPose referencePos, double Kx, double Ky, double Kt)
{

	double dErrorTh_rad = referencePos.getThetaRad() - m_RobotPos.getThetaRad();

	if (fabs(dErrorTh_rad) > PI)
		dErrorTh_rad = dErrorTh_rad - dErrorTh_rad / fabs(dErrorTh_rad) * 2 * PI;

	// 제동거리를 호로 변환하여 각 계산
	double v_ref = 1.0, dW = 0.;
	if (fabs(dErrorTh_rad) < (1.0 * D2R))
	{
		m_dV = 0.;
		m_dW = 0.;
		return RETURN_NONE_ERROR;
	}
	else if (fabs(dErrorTh_rad) < (5.0 * D2R))
		v_ref = 0.3;
	else if (fabs(dErrorTh_rad) < (10.0 * D2R))
		v_ref = 0.6;
	else
		v_ref = 1.;

	// Apply speed
	dW = v_ref*m_dMaxW;

	m_dV = 0.;
	dErrorTh_rad > 0 ? m_dW = -dW : m_dW = dW;

	return RETURN_CONTINUE;
}
int CPreciseMove::RansacCircleCentor(vector<POINT> point, PointObject * po)
{
	int iter = point.size() / 5 + 1;
	vector<POINT> candidate;
	// 양 끝단에서 랜덤한 두 샘플 선택
	while (iter)
	{
		int index1, index2;
		index1 = (int)(rand() % point.size() / 5);
		index2 = point.size() - 1 - (int)(rand() % point.size() / 5);


		// 두 샘플의 중앙을 지나 원 중심으로 가는 선분
		double dCentorX = (point[index1].x + point[index2].x) / 2;
		double dCentorY = (point[index1].y + point[index2].y) / 2;
		double P1toP2 = sqrt(pow(point[index1].y - point[index2].y, 2) + pow(point[index1].x - point[index2].x, 2));
		double dTheta;
		if ((point[index1].x - point[index2].x) == 0)
		{
			dTheta = 0;
			// 로봇이 두 샘플을 지나는 선분 위인지 아래인지?
			if (m_RobotPos.getX() - dCentorX > 0)
				dTheta = PI;
		}
		else
		{
			double a = ((point[index1].y - point[index2].y) / (point[index1].x - point[index2].x));
			dTheta = atan(a);
			// 로봇이 두 샘플을 지나는 선분 위인지 아래인지?
			if (a * (m_RobotPos.getX() - dCentorX) + dCentorY - m_RobotPos.getY() > 0)
				dTheta += PI / 2;
			else
				dTheta -= PI / 2;
		}

		// 로봇의 반대 방향으로 직진
		// JKPark abs 빼는 방법 없나?
		double dist = sqrt(abs(pow(m_nObjectsize / 2, 2) - pow(P1toP2 / 2, 2)));
		POINT p;
		p.x = (int)(dCentorX + dist*cos(dTheta));
		p.y = (int)(dCentorY + dist*sin(dTheta));
		candidate.push_back(p);
		--iter;
	}
	// Candidate 마다 스코어를 매김
	double dMaxWeight = 0.;
	int nIndex = 0;
	for (int i = 0; i < candidate.size(); i++)
	{
		// 임의의 10개의 샘플에 대해서 거리를 계산함
		int nSampleSize = 10;
		double dSigma = 1000., dWeightSum = 0;
		for (int j = 0; j < nSampleSize; j++) {
			int index = ((int)(rand() % point.size()));
			dWeightSum += exp(-(pow(candidate[i].x - point[index].x, 2) + pow(candidate[i].y - point[index].y, 2)) / dSigma);
		}
		dWeightSum /= nSampleSize;
		if (dMaxWeight < dWeightSum)
		{
			dMaxWeight = dWeightSum;
			nIndex = i;
		}
	}
	po->nCenter_x = candidate[nIndex].x;
	po->nCenter_y = candidate[nIndex].y;
	candidate.clear();

	return RETURN_NONE_ERROR;
}
