﻿#pragma once
#include "afxwin.h"
#include <iostream>
#include <conio.h>

#include"../robot/RobotPose.h"

#include "../util/PMath.h"


#include "../util/PUtil.h"

#include "../struct_define.h"

using namespace std;

class CGuideMove
{

private:
	RobotPose m_RobotPos;
	RobotPose m_TargetPos;

	int m_nThreadPeriod;

	bool m_PosAlign;
	bool m_SearchGuide;
	bool m_GuideTracking;
	bool m_FindMarker;

	
	bool m_setdirection;


	int AMRStop();
	static int ThreadFunctionGuideMove(void*);
	int iWaitCount;

public:
	int initialize();
	void terminate();
	int m_nState;
	bool m_bThreadFlag;

public:
	CGuideMove();
	~CGuideMove();

	std::thread m_GuideMoveThread; 


	//HCE add variables

	double gyro_target;

	////////////////////

	int guide_control_direction;

	int guide_blocked_count;

	int robot_status0;

	bool first_marker;
	bool second_marker;
	bool marker_trigger;
	//int s_CGuideMove_state;

	double m_dV;
	double m_dW;
	short m_sFrontGuideLcp1;
	short m_sFrontGuideLcp2;
	short m_sFrontGuideLcp3;
	short m_sRearGuideLcp1;
	short m_sRearGuideLcp2;
	short m_sRearGuideLcp3;

	double m_dTargetX;
	double m_dTargetY;
	double m_dTargetTh;
	int m_iTargetDirection;
	double m_dGytoTh;
	bool m_bUseGyro;
	int m_iRearGuideLineExist;
	int m_iFrontGuideLineExist;
	int m_iFrontGuideMarker;
	int m_iReareGuideMarker;
	int m_iWorkPlacePointCount;
	WorkPlacePoint* m_WP;
	void SetRobotPose(RobotPose inputPos) { m_RobotPos = inputPos; }
	RobotPose GetRobotPose() { return m_RobotPos; }

	int SetFrontLcpData(short lcp1, short lcp2, short lcp3);
	int SetRearLcpData(short lcp1, short lcp2, short lcp3);
	int GetFrontLcpData(short* lcp1, short* lcp2, short* lcp3);
	int GetRearLcpData(short* lcp1, short* lcp2, short* lcp3);	
	int SetTargetNode(int iNode);
	int GetTargetNode(double *dX, double *dY, double *dTh);
	double SetGyroTheta(double dGyroTh) { return m_dGytoTh = dGyroTh; }
	double GetGyroTheta() { return m_dGytoTh; }
	int SetUseGyro(bool bUse) { return m_bUseGyro = bUse; }
	bool isUseGyro() { return m_bUseGyro; }
	double GetV() { return m_dV; }
	double GetW() { return m_dW; }
	double SetVW(double dV, double dW) { return m_dV = dV, m_dW = dW * D2R; }

	int SetFrontGuideLineExist(int input) { return m_iFrontGuideLineExist = input; }
	int SetRearGuideLineExist(int input) { return m_iRearGuideLineExist  = input; }
	int GetFrontGuideLineExist() { return m_iFrontGuideLineExist; }
	int GetRearGuideLineExist() { return m_iRearGuideLineExist; }

	int GetFrontMarker() { return m_iFrontGuideMarker; }
	int GetRearMarker() { return m_iReareGuideMarker; }

	int SetFrontMarker(int inputMarker) { return m_iFrontGuideMarker = inputMarker; }
	int SetRearMarker(int inputMarker) { return m_iReareGuideMarker = inputMarker; }

	bool GetThreadFlag() { return m_bThreadFlag; }
	int LoadWorkPlacePoint();
	int	SetTargetNodeToConvertNode(int inputNode, WorkPlacePoint* rWP);	
	int CreateThread();
};

