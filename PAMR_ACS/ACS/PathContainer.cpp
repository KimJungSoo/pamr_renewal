#include "stdafx.h"
#include "PathContainer.h"
#include "mm.h"

#pragma warning(disable:4996)

// 두 vertex를 하나로 취급하기 위한 두 vertex간의 최대 거리 
#define SAME_VERTEX_TOLERENCE		0.01
#define MIN_DISTANCE_THRESHOLD		5.0


CPathContainer::CPathContainer(const char *fileName)
{
	_lastVertexID = -1;

	::ZeroMemory(_nodePoints, sizeof(_nodePoints));
	::ZeroMemory(_nodePoints_modified, sizeof(_nodePoints));

	if(fileName && *fileName)
	{
		LoadPathFile(fileName);
	}
}

CPathContainer::CPathContainer(const char *fileName, const char *fileName_Magnet)
{
	_lastVertexID = -1;

	::ZeroMemory(_nodePoints, sizeof(_nodePoints));
	::ZeroMemory(_nodePoints_modified, sizeof(_nodePoints_modified));

	::ZeroMemory(_edgeProps, sizeof(_edgeProps));
	::ZeroMemory(_bothEnds, sizeof(_bothEnds));

	if((fileName && *fileName) && (fileName_Magnet && *fileName_Magnet))
	{
		LoadPathFile(fileName, fileName_Magnet);
	}
}

CPathContainer::~CPathContainer()
{
	Clear();
}

void CPathContainer::Clear()
{
	_lastVertexID = -1;

	_vertexList.clear();
	_edgeList.clear();
}

int CPathContainer::LoadPathFile(const char *fileName)
{
	_vertexList.reserve(1000);
	_edgeList.reserve(1000);

	FILE *fp = fopen(fileName, "rt");
	if(!fp)		return -1;

	char *p;
	char text[1024 + 1] = "";
	double px = -1., py = -1.; 

	while(fgets(text, 1024, fp))
	{
		text[1024] = '\0';

		for(p = text; (*p == ' ') || (*p == '\t') || (*p == '\r') || (*p == '\n'); ++p);

		if(*p == '\0')		continue;
		if(*p == '#')		continue;		// # 기호는 주석을 의미한다.
		if(*p == ';')						// ; 기호는 새로운 경로의 시작을 의미
		{
			px = py = -1.; 
			NewPath();
		}

		int id = -1;

		double x = -1.;
		double y = -1.;
		double th = 0.;

		int obs = 0;

		int max_v = 0;
		int max_a = 0;
		int door = 0;
		int reflector_mode = 0;
		int reflector_useSide = 0;

		char name[256] = "";

		sscanf(text, "%d %lf %lf %lf %d %d %d %d %d %d %s", &id, &x, &y, &th, &obs, &max_v, &max_a, &door, &reflector_mode, &reflector_useSide, name);

		if(x != -1. && y != -1.)
		{
			int n = (int)ceil(Distance(x, y, px, py) / MIN_DISTANCE_THRESHOLD);
			if(name[0] || 0 <= n)
			{
				//AttachPath(x, y, max_v, name);
				AttachPath(id, x, y, th, obs, max_v, max_a, door, reflector_mode, reflector_useSide, name);

				px = x;
				py = y;
			}
		}
	}

	TRACE("vertex: %d, edge: %d\n", _vertexList.size(), _edgeList.size());

	fclose (fp);

	return 0;
}


int CPathContainer::SplitString(CUIntArray & pArrArg, char *pData, int nDataSize)
{
	int nReturn = 0;
	char *pTemp = NULL;
	CString strTemp;

	pTemp = strtok(pData, ",");
	while (NULL != pTemp)
	{
		if (nReturn >= nDataSize)
		{
			break;
		}

		//nodeTemp[nIdx] = atoi(pTemp);
		pArrArg.Add(atoi(pTemp));
		pTemp = strtok(NULL, ",");
		//printf("[%d]%d\n", nIdx, nodeTemp[nIdx]);
		//printf("[%d]%d\n", nIdx, pArrArg.GetAt(nIdx));
		nReturn++;
	}

	//printf("%d data parsed.\n", nReturn);

	return nReturn;
}

int CPathContainer::LoadPathFile(const char *fileName, const char *fileName_Magnet)
{
	_vertexList.reserve(1000);
	_edgeList.reserve(1000);
	char *p;
	char text[1024 + 1] = "";
	double px = -1., py = -1.;

#if 1
	const int kMaxIdx = 4096;
	DWORD dReturn = 0;
	char nodeName[kMaxIdx] = { 0, };
	char nodePosX[kMaxIdx] = { 0, };
	char nodePosY[kMaxIdx] = { 0, };
	char nodeProp[kMaxIdx] = { 0, };
	char edgeFrom[kMaxIdx] = { 0, };
	char edgeTo[kMaxIdx] = { 0, };
	char edgeTheta[kMaxIdx] = { 0, };
	char edgeDist[kMaxIdx] = { 0, };
	char edgeMaxSpeed[kMaxIdx] = { 0, };
	char edgeProp[kMaxIdx] = { 0, };

	//char* pmap_path = "..\\data\\mapNodeEdge.pmap"; //pmap data directory
	char* pmap_path = "C:\\data\\map\\mapNodeEdge.pmap";

	printf("Loading pmap information as \n");
	int nNodeCnt = GetPrivateProfileIntA("node", "count", 0, pmap_path);
	g_nNodeCnt = nNodeCnt;
	printf("Number of Nodes: %d\n", nNodeCnt);
	dReturn = GetPrivateProfileStringA("node", "name", NULL, nodeName, kMaxIdx, pmap_path);
	dReturn = GetPrivateProfileStringA("node", "xPos", NULL, nodePosX, kMaxIdx, pmap_path);
	dReturn = GetPrivateProfileStringA("node", "yPos", NULL, nodePosY, kMaxIdx, pmap_path);
	dReturn = GetPrivateProfileStringA("node", "properties", NULL, nodeProp, kMaxIdx, pmap_path);

	int nEdgeCount = GetPrivateProfileIntA("edge", "count", 0, pmap_path);
	g_nEdgeCnt = nEdgeCount;
	printf("Number of Edges: %d\n", nEdgeCount);
	dReturn = GetPrivateProfileStringA("edge", "from", NULL, edgeFrom, kMaxIdx, pmap_path);
	dReturn = GetPrivateProfileStringA("edge", "to", NULL, edgeTo, kMaxIdx, pmap_path);
	dReturn = GetPrivateProfileStringA("edge", "theta", NULL, edgeTheta, kMaxIdx, pmap_path);
	dReturn = GetPrivateProfileStringA("edge", "distance", NULL, edgeDist, kMaxIdx, pmap_path);
	dReturn = GetPrivateProfileStringA("edge", "maxSpeed", NULL, edgeMaxSpeed, kMaxIdx, pmap_path);
	dReturn = GetPrivateProfileStringA("edge", "properties", NULL, edgeProp, kMaxIdx, pmap_path);

	///////////////////////////////////////////////////////

	const int kMaxNode = 2048;
	CUIntArray carrNodeName;
	CUIntArray carrNodePosX;
	CUIntArray carrNodePosY;
	CUIntArray carrNodeProp;
	CString strTemp;

	printf("\nNode data loading.\n");
	int nError = 0;
	strTemp = _T("[Error] map loading fail, check pmap file in [node]");
	if (nNodeCnt != SplitString(carrNodeName, nodeName, kMaxNode))
	{
		nError = -1;
		strTemp += _T("node name");
	}
	if (nNodeCnt != SplitString(carrNodePosX, nodePosX, kMaxNode))
	{
		nError = -1;
		strTemp += _T(", xPos");
	}
	if (nNodeCnt != SplitString(carrNodePosY, nodePosY, kMaxNode))
	{
		nError = -1;
		strTemp += _T(", yPos");
	}
	if (nNodeCnt != SplitString(carrNodeProp, nodeProp, kMaxNode))
	{
		nError = -1;
		strTemp += _T(", properties");
	}
	if (0 != nError)
	{
		MessageBox(NULL, strTemp, NULL, 0);
	}

	if (0 != nError)
	{
		AttachPath(
			1,
			0,
			0, 0, 0, 0, 0, 0, 0, 0, "");
	}
	else
	{
		for (int nIdx = 0; nIdx < nNodeCnt; nIdx++)
		{
			AttachPath(
				carrNodeName.GetAt(nIdx),
				(double)(carrNodePosX.GetAt(nIdx)) / 1000,
				(double)(carrNodePosY.GetAt(nIdx)) / 1000, 0, 0, 0, 0, 0, 0, 0, "");
		}
	}

	/*for (int idx = 0; idx < nNodeCnt; idx++)
	{
		printf("[%d] NodeProp value %d\n", idx, carrNodeProp.GetAt(idx));
	}*/


	//AttachPath(carrNodeName.GetAt(0), x, y, th, obs, max_v, max_a, door, reflector_mode, reflector_useSide, name);

	CUIntArray carrEdgeFrom;
	CUIntArray carrEdgeTo;
	CUIntArray carrEdgeTheta;
	CUIntArray carrEdgeDist;
	CUIntArray carrEdgeMaxSpeed;
	CUIntArray carrEdgeProp;
	
	printf("\nEdge data loading.\n");

	nError = 0;
	strTemp = _T("[Error] map loading fail, check pmap file in [edge]");
	
	if (nEdgeCount != SplitString(carrEdgeFrom, edgeFrom, kMaxNode))
	{
		nError = -1;
		strTemp += _T("from");
	}
	if (nEdgeCount != SplitString(carrEdgeTo, edgeTo, kMaxNode))
	{
		nError = -1;
		strTemp += _T(", to");
	}
	if (nEdgeCount != SplitString(carrEdgeTheta, edgeTheta, kMaxNode))
	{
		nError = -1;
		strTemp += _T(", theta");
	}
	if (nEdgeCount != SplitString(carrEdgeDist, edgeDist, kMaxNode))
	{
		nError = -1;
		strTemp += _T(", distance");
	}
	if (nEdgeCount != SplitString(carrEdgeMaxSpeed, edgeMaxSpeed, kMaxNode))
	{
		nError = -1;
		strTemp += _T(", maxSpeed");
	}
	if (nEdgeCount != SplitString(carrEdgeProp, edgeProp, kMaxNode))
	{
		nError = -1;
		strTemp += _T(", properties");
	}
	if (0 != nError)
	{
		MessageBox(NULL, strTemp, NULL, 0);
	}


	/*for (int idx = 0; idx < nEdgeCount; idx++)
	{
		printf("[%d] EdgeProp value %d\n", idx, carrEdgeProp.GetAt(idx));
	}
*/
	////// For non-ordered node : for CJ hayang smart hub 2018.10.12 ////////////////////////////

	//pArrArg.Add(atoi(pTemp));
	char temp_str[10] = { '\0', };
	CUIntArray carr_nodeID;
	CUIntArray carr_egFrom;
	CUIntArray carr_egTo;

	for (int ind = 0; ind < nNodeCnt; ind++)
	{
		carr_nodeID.Add(ind);
	    _RPTN(_CRT_WARN,"Node [%d] (%d : %d)\n", ind, carrNodeName.GetAt(ind), carr_nodeID.GetAt(ind));
	}

	for (int ind = 0; ind < nEdgeCount; ind++)
	{
		int eg_num = carrEdgeFrom.GetAt(ind);
		sprintf(temp_str, "%d", eg_num);
		int eg_ind1 = FindVertexName((const char*)temp_str);
		carr_egFrom.Add(eg_ind1);
		_RPTN(_CRT_WARN,"EdgeFrom [%d] (%d : %d)\n", ind, eg_num, carr_egFrom.GetAt(ind));
	}

	for (int ind = 0; ind < nEdgeCount; ind++)
	{
		int eg_num = carrEdgeTo.GetAt(ind);
		sprintf(temp_str, "%d", eg_num);
		int eg_ind1 = FindVertexName((const char*)temp_str);
		carr_egTo.Add(eg_ind1);
		_RPTN(_CRT_WARN, "EdgeTo [%d] (%d : %d)\n", ind, eg_num, carr_egTo.GetAt(ind));
	}
	
	// ----------------------------------------------------------------------------------------------------------------//
	// ------------------------------------  load  _nodePoints & _edgeProp------------------------//
	// ----------------------------------------------------------------------------------------------------------------//
	//int nodeCount1 = _vertexList.size(); //nodeCount1 is equal to nNodeCnt
	//	printf("\nNode Properties %d.\n", nodeCount1);
	for (int i = 0; i < nNodeCnt; i++)
	{
		//token = strtok_s(NULL, "\0", &ptype);
		/*_nodePoints[j] = (char)token[0];*/
		_nodePoints[i] = nodeProp[i * 3];
		_nodePoints_modified[i] = _nodePoints[i];
	//	printf("[%d] %c\n", i, _nodePoints[i]); //node properties space
	}

	int edgeCount1 = _edgeList.size();
	printf("\nEdge Properties %d.\n", edgeCount1);
	for (int i = 0; i < nEdgeCount; i++)
	{
		_edgeProps[i] = edgeProp[i * 3];
	//	printf("[%d] %c\n", i, _edgeProps[i]); //node properties space
	}
	
	//int _bothEnds[kMaxNode] = { 0, };
	int from_bNode = -1;
	int to_bNode = -1;
	int fr_index = -1;
	int to_index = -1;
	int bNode_ind = 0;
	for (int i = 0; i < nEdgeCount; i++)
	{
		if (_edgeProps[i] == 'b')
		{
			bNode_ind++;
			from_bNode	=  carrEdgeFrom.GetAt(i) ; // i 인덱스에 해당하는 실제노드값 알려줌
			to_bNode		=  carrEdgeTo.GetAt(i) ;

			fr_index = FindVertexID(from_bNode); // 실제 노드값에 대한 노드인덱스(0~) 알려줌
			to_index = FindVertexID(to_bNode);

			_bothEnds[fr_index] = bNode_ind;
			_bothEnds[to_index] = bNode_ind;
			printf("[%d(%d) -- %d(%d)] is set to be one-way edge.\n", from_bNode, fr_index, to_bNode, to_index);
		}
		
	}
	// ----------------------------------------------------------------------------------------------------------------//
	
	if (0 != nError)
	{
		for (int nIdxR = 0, nIdxV = 0; nIdxR < VERTICES_MAX; nIdxR++)
		{
			for (; nIdxV < VERTICES_MAX; nIdxV++)
			{
				g_graph[nIdxR][nIdxV] = 0;
				g_graph_modified[nIdxR][nIdxV] = 0;
				b_graph[nIdxR][nIdxV] = 0;
				b_graph_modified[nIdxR][nIdxV] = 0;

			}
		}
	}
	else
	{
		/*
		for (int nIdx = 0, nIdxFrom = 0, nIdxTo = 0; nIdx < nEdgeCount; nIdx++)
		{
			nIdxFrom = carrEdgeFrom.GetAt(nIdx) - 1;
			nIdxTo = carrEdgeTo.GetAt(nIdx) - 1;
			g_graph[nIdxFrom][nIdxTo] = carrEdgeDist.GetAt(nIdx);
		}
		*/
		for (int nIdx = 0, nIdxFrom = 0, nIdxTo = 0; nIdx < nEdgeCount; nIdx++)
		{
			nIdxFrom = carr_egFrom.GetAt(nIdx)  ;
			nIdxTo = carr_egTo.GetAt(nIdx) ;
			g_graph[nIdxFrom][nIdxTo] = carrEdgeDist.GetAt(nIdx);  // new g_graph for non-ordered node : 2018.10.12
			//printf("Edge Dist. [%d: %d--%d] %d\n", nIdx, nIdxFrom, nIdxTo, carrEdgeDist.GetAt(nIdx));

			if (_edgeProps[nIdx] == 'b')
			{
				g_graph[nIdxFrom][nIdxTo] = 100* carrEdgeDist.GetAt(nIdx); // 'both-way' link is not considered in the path.
			}
		}

		for (int nIdx = 0, nIdxFrom = 0, nIdxTo = 0; nIdx < nEdgeCount; nIdx++)
		{
			nIdxFrom = carr_egFrom.GetAt(nIdx) ;
			nIdxTo = carr_egTo.GetAt(nIdx) ;
			b_graph[nIdxFrom][nIdxTo] = carrEdgeDist.GetAt(nIdx);
			//b_graph[nIdxTo][nIdxFrom] = carrEdgeDist.GetAt(nIdx); // both ways g_graph for non-ordered node : 2018.10.13

			int nIdxFrom1 = carr_egTo.GetAt(nIdx) ; 
			int nIdxTo1 = carr_egFrom.GetAt(nIdx) ;
			b_graph[nIdxFrom1][nIdxTo1] = carrEdgeDist.GetAt(nIdx);
			
			
			if ( (_nodePoints[nIdxFrom] == 'd') |  (_nodePoints[nIdxTo] == 'd') )
			{
				b_graph[nIdxFrom][nIdxTo] = 100 * carrEdgeDist.GetAt(nIdx); // edge including 'disable' node.
				b_graph[nIdxTo][nIdxFrom] = 100 * carrEdgeDist.GetAt(nIdx);
			}
						
			if (_edgeProps[nIdx] == 'b')
			{
				b_graph[nIdxFrom][nIdxTo] = 100 * carrEdgeDist.GetAt(nIdx); // 'both-way' link is not considered in the path.
				b_graph[nIdxTo][nIdxFrom] = 100 * carrEdgeDist.GetAt(nIdx); // 'both-way' link is not considered in the path.
			}
		
		}

	}

#else
	// 일반 PathNode
	FILE *fp = fopen(fileName, "rt");
	if(!fp)		return -1;

	char *p;
	char text[1024 + 1] = "";
	double px = -1., py = -1.; 

	while(fgets(text, 1024, fp))
	{
		text[1024] = '\0';

		for(p = text; (*p == ' ') || (*p == '\t') || (*p == '\r') || (*p == '\n'); ++p);

		if(*p == '\0')		continue;
		if(*p == '#')		continue;		// # 기호는 주석을 의미한다.
		if(*p == ';')						// ; 기호는 새로운 경로의 시작을 의미
		{
			px = py = -1.; 
			NewPath();
		}

		int id = -1;

		double x = -1.;
		double y = -1.;
		double th = 0.;

		int obs = 0;

		int max_v = 0;
		int max_a = 0;
		int door = 0;
		int reflector_mode = 0;
		int reflector_useSide = 0;

		char name[256] = "";

		sscanf(text, "%d %lf %lf %lf %d %d %d %d %d %d %s", &id, &x, &y, &th, &obs, &max_v, &max_a, &door, &reflector_mode, &reflector_useSide, name);

		if(x != -1. && y != -1.)
		{
			int n = (int)ceil(Distance(x, y, px, py) / MIN_DISTANCE_THRESHOLD);
			if(name[0] || 0 <= n)
			{
				//AttachPath(x, y, max_v, name);
				AttachPath(id, x, y, th, obs, max_v, max_a, door, reflector_mode, reflector_useSide, name);

				px = x;
				py = y;
			}
		}
	}

	TRACE("vertex: %d, edge: %d\n", _vertexList.size(), _edgeList.size());

	fclose (fp);
#endif

	// Magnet PathNode
	int type = 1;		// Magnet PathNode 를 의미함

	FILE *fp_magnet = fopen(fileName_Magnet, "rt");
	//if(!fp_magnet)		return -1;
	if(!fp_magnet)
		return 0;		// 이미 일반 PathNode 성공했으므로 정상임

	// PathNode 와 MagnetNode 사이에 ";"를 넣어줌
	px = py = -1.; 
	NewPath();

	while(fgets(text, 1024, fp_magnet))
	{
		text[1024] = '\0';

		for(p = text; (*p == ' ') || (*p == '\t') || (*p == '\r') || (*p == '\n'); ++p);

		if(*p == '\0')		continue;
		if(*p == '#')		continue;		// # 기호는 주석을 의미한다.
		if(*p == ';')						// ; 기호는 새로운 경로의 시작을 의미
		{
			px = py = -1.; 
			NewPath();
		}

		int id = -1;

		double x = -1.;
		double y = -1.;
		double th = 0.;

		int obs = 0;

		int max_v = 0;
		int max_a = 0;
		int door = 0;
		int reflector_mode = 0;
		int reflector_useSide = 0;

		char name[256] = "";

		int pol = -1;

		sscanf(p, "%d %lf %lf %d %s", &id, &x, &y, &pol, name);

		if(x != -1. && y != -1.)
		{
			int n = (int)ceil(Distance(x, y, px, py) / MIN_DISTANCE_THRESHOLD);
			if(name[0] || 0 <= n)
			{
				//AttachPath(x, y, max_v, name);
				AttachPath(id, x, y, th, obs, max_v, max_a, door, reflector_mode, reflector_useSide, name, type);

				px = x;
				py = y;
			}
		}
	}

	TRACE("vertex(added): %d, edge(added): %d\n", _vertexList.size(), _edgeList.size());

	fclose(fp_magnet);




	return 0;
}

int CPathContainer::FindVertexName( const char *name)
{
	
	char * name_temp = (char*)name;
	for(unsigned int i = 0; i < _vertexList.size(); ++i)
	{
	//	if(strcmpi(_vertexList[i].name.c_str(), name) == 0)
		//printf("%d\n", _vertexList[i].id);
		if (_vertexList[i].id == atoi(name_temp))
		{
			return i;
		}
	}
	
	return -1;
}

int CPathContainer::FindVertexID(int name)
{
	//실제 노드번호를 입력하면, 일치하는 노드인덱스(0~) 를 알려줌.
	char  name_temp[10] = {'\0',};
	sprintf(name_temp, "%d", name);
	for (unsigned int i = 0; i < _vertexList.size(); ++i)
	{
		//	if(strcmpi(_vertexList[i].name.c_str(), name) == 0)
		//printf("%d\n", _vertexList[i].id);
		if (_vertexList[i].id == atoi(name_temp))
		{
			return i;
		}
	}

	return -1;
}

int CPathContainer::FindVertexNum(int num)
{
	if (num < 0) return 0;
	return _vertexList[num].id;
	
}



int CPathContainer::FindNearVertex(double x, double y, double *distance, bool haveName)
{
	int min_i = -1;
	double min_d = 1e6;

	for(unsigned int i = 0; i < _vertexList.size(); ++i)
	{
		if(!haveName || (haveName && _vertexList[i].name.size()))
		{
			double d = Distance(_vertexList[i].x, _vertexList[i].y, x, y);
			
			if(d < min_d)
			{
				min_i = i;
				min_d = d;
			}
		}
	}
	if(distance)
	{
		*distance = min_d;
	}
	return min_i;
}

vector<int> CPathContainer::FindNearVertexNames(int toNode, double dist_threshold)
{
	//toNode 인근의 dist 범위 안에 가까운 노드들을 모두 찾음.
	vector<int> _nearNodes;
	int toIndex = FindVertexID(toNode);
	if (toIndex < 0)
	{
		printf("No near vertex node found.\n");
		_nearNodes.push_back(toIndex);
		return _nearNodes;
	}
	for (unsigned int ind = 0; ind < _vertexList.size(); ind++)
	{
		if (_vertexList[ind].id > 500 || ind == toIndex) continue;
		
		double d = Distance(_vertexList[toIndex].x, _vertexList[toIndex].y, _vertexList[ind].x, _vertexList[ind].y);
		if (d < dist_threshold )
		{
			_nearNodes.push_back(_vertexList[ind].id);
		}
	}

	return _nearNodes;
}



vector<vaWayPoint> CPathContainer::ConvertPathWp(vector<int> &path)
{
	assert(path.size());

	vector<vaWayPoint> wp_path;
	wp_path.reserve(path.size());

	for(unsigned int i = 0; i < path.size(); ++i)
	{
		int nIndexNode = path[i];
		sVertex &vertex = _vertexList[nIndexNode];

		//double node_[4] = { vertex.x, vertex.y, 0., vertex.max_v };
		//vaWayPoint node(node_, 4);
		double node_[6] = { vertex.x, vertex.y, 0., 
			static_cast<double>(vertex.max_v), static_cast<double>(nIndexNode), static_cast<double>(vertex.id) };
		vaWayPoint node(node_, 6);

		wp_path.push_back(node);
	}
	return wp_path;
}

void CPathContainer::NewPath()
{
	_lastVertexID = -1;
}

void CPathContainer::AttachPath(double x, double y, double max_v, char *name)
{
	// vertexList를 뒤져보고 만일 겹친다면 기존 vertex를 사용한다.
	// 겹치지 않는다면 새로운 vertex로 vertexList 끝에 삽입한다.
	int vertexID = FindVertex(x, y);
	if(vertexID < 0)
	{
		_vertexList.push_back(sVertex(x, y, (int)max_v, name));
		vertexID = _vertexList.size() - 1;
	}
	else
	{
		// 새로운 vertex에 이름이 있는 경우, 이름만 복사
		if(name && *name)
			_vertexList[vertexID].name = name;
	}

	// 경로상의 이전 vertex와 현재 vertex를 edge로 연결한다.
	if(_lastVertexID != -1)
	{
		_edgeList.push_back(sEdge(_lastVertexID, vertexID));
	}
	_lastVertexID = vertexID;
}

void CPathContainer::AttachPath(int id, double x, double y, double th, int obs, int max_v, int max_a, int door, int reflector_mode, int reflector_useSide, char *name)
{
	// vertexList를 뒤져보고 만일 겹친다면 기존 vertex를 사용한다.
	// 겹치지 않는다면 새로운 vertex로 vertexList 끝에 삽입한다.
	int vertexID = FindVertex(x, y);
	if(vertexID < 0)
	{
		_vertexList.push_back(sVertex(id, x, y, th, obs, max_v, max_a, door, reflector_mode, reflector_useSide, name));
		vertexID = _vertexList.size() - 1;
	}
	else
	{
		// 새로운 vertex에 이름이 있는 경우, 이름만 복사
		if(name && *name)		_vertexList[vertexID].name = name;
	}

	// 경로상의 이전 vertex와 현재 vertex를 edge로 연결한다.
	if(_lastVertexID != -1)
	{
		_edgeList.push_back(sEdge(_lastVertexID, vertexID));
	}
	_lastVertexID = vertexID;
}

void CPathContainer::AttachPath(int id, double x, double y, double th, int obs, int max_v, int max_a, int door, int reflector_mode, int reflector_useSide, char *name, int type)
{
	// vertexList를 뒤져보고 만일 겹친다면 기존 vertex를 사용한다.
	// 겹치지 않는다면 새로운 vertex로 vertexList 끝에 삽입한다.
	int vertexID = FindVertex(x, y);
	if(vertexID < 0)
	{
		_vertexList.push_back(sVertex(id, x, y, th, obs, max_v, max_a, door, reflector_mode, reflector_useSide, name, type));
		vertexID = _vertexList.size() - 1;
	}
	else
	{
		if(type == 0)		// 일반 PathNode 만 적용
		{
			// 새로운 vertex에 이름이 있는 경우, 이름만 복사
			if(name && *name)
				_vertexList[vertexID].name = name;
		}
	}

	// 경로상의 이전 vertex와 현재 vertex를 edge로 연결한다.
	if(_lastVertexID != -1)
	{
		_edgeList.push_back(sEdge(_lastVertexID, vertexID));
	}
	_lastVertexID = vertexID;
}

int CPathContainer::FindVertex(double x, double y)
{
	for(unsigned int i = 0; i < _vertexList.size(); ++i)
	{
		double l = Distance(_vertexList[i].x, _vertexList[i].y, x, y);

		if(l < SAME_VERTEX_TOLERENCE)
		{
			return i;
		}
	}
	return -1;
}
