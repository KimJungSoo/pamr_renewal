#include "stdafx.h"
#include "MapEntity.h"

double EPSILON = 1e-6;


char *skip_space (char *p)
{
	while(*p==' ' || *p=='\t' || *p==',') p++;
	return p;
}

bool EOS(char c)
{
	return (c=='\r' || c=='\n' || c=='\0');
}