#include "stdafx.h"
#include "Astar.h"

#define FORWARD_SEARCH		1		// 경로를 정방향으로 탐색하도록 함
#define REVERSE_SEARCH		0		// 경로를 역방향으로 탐색하도록 함
									// 둘 다 1인 경우는 양방향으로 탐색함


CAstar::CAstar(const char *fileName)
: CPathContainer(fileName)
{ 
}

CAstar::CAstar(const char *fileName, const char *fileName_Magnet)
: CPathContainer(fileName, fileName_Magnet)
{ 
}

int CAstar::PopOpenSet()
{
	// f_score가 가장 적은 요소 하나를 꺼낸다.

	int min_i = -1;
	double min_f = 1.e10;
	vector<int>::iterator min_it;
	
	for(vector<int>::iterator it = _openSet.begin(); it != _openSet.end(); ++it)
	{
		if(_vertexList[*it].f_score < min_f)
		{
			min_i = *it;
			min_f = _vertexList[*it].f_score;
			min_it = it;
		}
	}
	assert(min_i != -1);

	// 꺼내간 요소는 삭제한다.
	_openSet.erase(min_it);
	
	return min_i;
}

vector<int> CAstar::ReverseFollow(int start, int goal)
{
	vector<int> path;
	path.reserve(1000);

	for(int i = goal; i != start; i = _vertexList[i].came_from)
	{
		if(i == -1)
		{
			path.clear();
			return path;
		}
		path.push_back(i);
	}
	path.push_back(start);

	return path;
}

vector<int> CAstar::Reverse(vector<int> &path)
{
	vector<int> rev_path;
	rev_path.reserve(path.size());

	for(int i = (int)path.size() - 1; 0 <= i; --i)
	{
		rev_path.push_back(path[i]);
	}
	return rev_path;
}


bool CAstar::SearchOpenSet(int id)
{
	for(unsigned int i = 0 ; i < _openSet.size(); i++)
	{
		if(id == _openSet[i])
		{
			return true;
		}
	}
	return false;
}

bool CAstar::SearchCloseSet(int id)
{
	for(unsigned int i = 0 ; i<_closedSet.size(); i++)
	{
		if(id == _closedSet[i])
		{
			return true;
		}
	}
	return false;
}

double CAstar::dist_between(int start, int goal)
{
	// start에서 goal 까지의 거리를 계산한다.

	double dx = _vertexList[start].x - _vertexList[goal].x;
	double dy = _vertexList[start].y - _vertexList[goal].y;

	return sqrt(dx * dx + dy * dy);
}

double CAstar::heuristic_cost_estimate(int start, int goal)
{
	// start에서 goal 까지의 휴리스틱 코스트를 추정한다.
	// 여기서는 간단히 start와 goal 간의 거리를 사용하였다.

	double dx = _vertexList[start].x - _vertexList[goal].x;
	double dy = _vertexList[start].y - _vertexList[goal].y;

	return sqrt(dx * dx + dy * dy);
}

vector<int> CAstar::FindPath(int start, int goal)
{
	_closedSet.clear();
	_openSet.clear();

	for(unsigned int i = 0; i < _vertexList.size(); ++i)
	{
		_vertexList[i].came_from = -1;
		_vertexList[i].g_score = 0.;
		_vertexList[i].h_score = 0.;
		_vertexList[i].f_score = 0.;
	}

	// 출발 위치를 openset에 넣고 초기값을 설정한다.
	_openSet.push_back(start);

	_vertexList[start].g_score = 0.;
	_vertexList[start].h_score = heuristic_cost_estimate(start, goal);
	_vertexList[start].f_score = _vertexList[start].h_score + _vertexList[start].g_score;
	
	// openset(방문 예정인 node를 기록해 둠)이 비어있지 않은 경우 실행.
	while(!_openSet.empty())
	{
		// openset에서 f_score가 가장 적은 node를 하나 꺼내온다.
		int x = PopOpenSet();

		// goal에 도달했다면 while 루프를 중단한다.
		if(x == goal)
		{
			break;
		}

		// closedset(이미 방문한 node를 기록해둠)에 x를 등록한다.
		_closedSet.push_back(x);

		// x에 연결된 node를 찾는다.
		for(unsigned int i = 0; i < _edgeList.size(); ++i)
		{
			int y = -1;

#if FORWARD_SEARCH
			if(_edgeList[i].s == x) y = _edgeList[i].e; // 정방향 탐색
#endif
#if REVERSE_SEARCH
			if(_edgeList[i].e == x) y = _edgeList[i].s; // 역방향 탐색
#endif

			if(y < 0)
				continue;

			// 찾은 node y가 이미 방문한 node이면 다음 과정을 수행하지 않는다.
			if(SearchCloseSet(y))
				continue;

			bool tentative_is_better = false;
			// node y 까지의 잠정적인 g_score를 계산한다.
			double tentative_g_score = _vertexList[x].g_score + dist_between(x, y);
			
			// node y가 openset에 없다면 openset에 추가하고 y의 상태를 업데이트 한다.
			if(!SearchOpenSet(y))
			{
				_openSet.push_back(y);
				tentative_is_better = true;
			}
			// node y가 openset에 있으면서 잠정적인 g_score가 y의 현재 g_socre보다 적은 경우
			// y의 상태를 업데이트 한다.
			else if(tentative_g_score < _vertexList[y].g_score)
			{
				tentative_is_better = true;
			}
			else
			{
				tentative_is_better = false;
			}

			if(tentative_is_better)
			{
				// y의 상태를 업데이트 한다.
				_vertexList[y].came_from = x;
				_vertexList[y].g_score = tentative_g_score;
				_vertexList[y].h_score = heuristic_cost_estimate(y, goal);
				_vertexList[y].f_score = _vertexList[y].g_score + _vertexList[y].h_score;
			}
		}
	}

	return Reverse(ReverseFollow(start, goal));
}

bool CAstar::FindPath(vector<vaWayPoint> &path, const CPosture &robotPos, const char *targetName)
{
	SCOPED_LOCK (&_lock);

	// 로봇의 위치로부터 가장 가까운 노드를 찾는다.
	// 그리고 목적지 이름에 해당하는 노드를 찾는다.
	int start = FindNearVertex(robotPos.x, robotPos.y);
	int goal = FindVertexName(targetName);

	if(start < 0 || goal < 0)
		return false;
	
	vector<int> i_path;

	if(start == goal)
	{
		// 동일한 지점이다. 즉 목적지가 경로 없이 바로 선택되었다.
		i_path.push_back(start);
	}
	else
	{
		// 둘 다 찾았다면 이 둘을 연결하는 path를 가져온다.
		i_path = FindPath(start, goal);
	}

	if(!i_path.size())
		return false;
	
	path = ConvertPathWp(i_path);

	return true;
}
