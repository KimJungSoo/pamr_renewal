#pragma once

using namespace std;

struct sMagnetNode
{
	bool close;				// Path 연속성 구분자(true : Path 구분자(;), false : 연속된 PathNode)

	int id;

	double x, y;

	int p;					// Magnet 의 극성(0 : +, 1 : -)

	string name;			// 경유점 이름

	sMagnetNode(double x_, double y_, const char *name_)
		: close(false), id(0), x(x_), y(y_), p(0), name(name_) { }
	sMagnetNode(int id_, double x_, double y_, int p_, const char *name_)
		: close(false), id(id_), x(x_), y(y_), p(p_), name(name_) { }
	sMagnetNode(bool close_)
		: close(true), x(0), y(0), p(0), name("") { }
};


class CMagnetContainer
{
public:
	CMagnetContainer(const char *fileName);
	~CMagnetContainer();

	void Clear();
	int LoadMagnetFile(const char *fileName);
	int SaveMagnetFile(const char *fileName);

	vector<sMagnetNode> _magnets;
};
