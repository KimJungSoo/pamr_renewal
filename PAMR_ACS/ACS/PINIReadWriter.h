﻿#ifndef PAMR_INIREAD_WRITER_H_
#define PAMR_INIREAD_WRITER_H_

//#include "ini.h"

using namespace std;

// Read an INI file into easy-to-access name/value pairs. (Note that I've gone
// for simplicity here rather than speed, but it should be pretty decent.)
class PINIReadWriter
{
private:
    int _error;
    map<std::string, std::string> _values;
    static string MakeKey(std::string section, std::string name);
    static int ValueHandler(void* user, const char* section, const char* name, const char* value);

public:

	// Return the result of ini_parse(), i.e., 0 on success, line number of
	// first error on parse error, or -1 on file open error.
	int ParseError();

	 // Get a string value from INI file, returning default_value if not found.
	string getStringValue(string section, string name, string default_value);

	// Get an integer (long) value from INI file, returning default_value if
	// not found or not a valid integer (decimal "1234", "-1234", or hex "0x4d2").
	long getIntValue(string section, string name, long default_value);

	// Get an double  value from INI file, returning default_value if
	// not found or not a valid double (decimal "0.1", "-0.2").
	double getDoubleValue(string section, string name, double default_value=0.);

    // Get a boolean value from INI file, returning default_value if not found or if
    // not a valid true/false value. Valid true values are "true", "yes", "on", "1",
    // and valid false values are "false", "no", "off", "0" (not case sensitive).
    bool GetBoolean(std::string section, std::string name, bool default_value);

    // Construct PAMRINIReadWriter and parse given filename.
    // See ini.h for more info about the parsing.
	PINIReadWriter(string strFileName);
	virtual ~PINIReadWriter();
};

#endif /* PAMR_INIREAD_WRITER_H_ */

////////////////////////////////////////////////////////////////////////////////////////////////////

/* ini.h -- simple .INI file parser

inih is released under the New BSD license (see LICENSE.txt). Go to the project
home page for more info:

http://code.google.com/p/inih/

*/

#ifndef __INI_H__
#define __INI_H__

/* Make this header file easier to include in C++ code */
#ifdef __cplusplus
extern "C" {
#endif


#include <stdio.h>

/* Parse given INI-style file. May have [section]s, name=value pairs
   (whitespace stripped), and comments starting with ';' (semicolon). Section
   is "" if name=value pair parsed before any section heading. name:value
   pairs are also supported as a concession to Python's ConfigParser.

   For each name=value pair parsed, call handler function with given user
   pointer as well as section, name, and value (data only valid for duration
   of handler call). Handler should return nonzero on success, zero on error.

   Returns 0 on success, line number of first error on parse error (doesn't
   stop on first error), or -1 on file open error.
*/
int gini_parse(const char* filename,
              int (*handler)(void* user, const char* section,
                             const char* name, const char* value),
              void* user);

/* Same as ini_parse(), but takes a FILE* instead of filename. This doesn't
   close the file when it's finished -- the caller must do that. */
int gini_parse_file(FILE* file,
                   int (*handler)(void* user, const char* section,
                                  const char* name, const char* value),
                   void* user);

/* Nonzero to allow multi-line value parsing, in the style of Python's
   ConfigParser. If allowed, ini_parse() will call the handler with the same
   name for each subsequent line parsed. */
#ifndef INI_ALLOW_MULTILINE
#define INI_ALLOW_MULTILINE 1
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INI_H__ */

////////////////////////////////////////////////////////////////////////////////////////////////////
