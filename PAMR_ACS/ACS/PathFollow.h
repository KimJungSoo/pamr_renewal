#pragma once
#include "MapEntity.h"

using namespace std;

struct sVelocity2WD 
{
	double v;			// linear velocity
	double w;			// angular velocity

	sVelocity2WD (double v_ = 0, double w_ = 0) : v(v_), w(w_) { }

	void Zero() { v = w = 0; }
};

struct sInterpolatedWp 
{
	double dist;		// 목적지 까지의 거리
	sVelocity2WD vel;	// 경유점에서의 선속도와 각속도
	CPosture pos;		// 경유점의 위치와 방위
	int toNodeIndex;	// 해당 위치에서 목표 Node의 Index => Node의 속성 정보 이용을 위해 필요
	int toNodeId;		// 해당 위치에서 목표 Node의 Id => Node의 속성 정보 이용을 위해 필요
};


class CPathFollow
{
public:
	CPathFollow();
	~CPathFollow();

	void Reset();
	void SmoothPath(vector<vaWaypoint> &path, int nodeSize);
	void SetPath(const vector<vaWaypoint> &path, const CPosture &robotPos);

	bool FollowPath(sVelocity2WD *desiredVel, const CPosture &robotPos, double dt);
	bool Turning(sVelocity2WD *desiredVel, const CPosture &robotPos, double targetHeading, double dt);

public:
	// 로봇이 따라가야 할 경로를 저장하고 있다.
	// 로봇은 경로상의 한 점에 도달하면 _curIndex를 증가시켜 다음 점을 목표로 이동한다. 
	// _curIndex가 _wpSize에 도달할 때까지 상기 과정을 반복한다.
	int      _curIndex;		// 로봇이 현재 참조하고 있는 _wp의 경유점에 대한 인덱스
	double   _curDist;		// 로봇이 경로상에서 목적지에 도달하기까지 남은 거리
	int      _curToNodeIndex;	// 해당 위치에서 목표 Node의 Index => Node의 속성 정보 이용을 위해 필요
	int      _curToNodeId;	// 해당 위치에서 목표 Node의 Id => Node의 속성 정보 이용을 위해 필요

	int	_wpSize;			// _wp를 구성하는 경유점(waypoint)의 개수
	sInterpolatedWp *_wp;	// 보간된 경로

	int _pfMode;

	// FollowPath에서 계산한 로봇의 이동 속도
	sVelocity2WD _ffVel;		// Feed-forward velocity
	sVelocity2WD _fbVel;		// Feed-back velocity
	sVelocity2WD _desiredVel;	// 로봇에 요청한 구동 속도

	double _obstacleDistance;

private:
	double DetectObstacle();

	void CalculateDistance();
	void LinearVelLimitByCurvature();
	void LinearVelLimitByDeceleration();
	void LinearVelLimitByAcceleration();
	void CalculateAngularVelocity();

	void MoveToNextWaypoint(double dt);
	CPosture GetTargetPos();
	double DistanceToTarget(const CPosture &robotPos);

	sVelocity2WD FeedforwardValueToZero(double dt);
	sVelocity2WD FeedforwardVelocity(double vel_limit, double dt);
	sVelocity2WD NonlinearFeedbackControl(const CPosture &error, const sVelocity2WD &ffVel);
	sVelocity2WD ReturnToPath(const CPosture &error, double vel_limit, double dt);
	sVelocity2WD AlignToPath(const CPosture &error, double dt);

	double TrapezoidalProfile(double velCurrent, double velDesired, double acceleration, double deceleration, double dt);
};


