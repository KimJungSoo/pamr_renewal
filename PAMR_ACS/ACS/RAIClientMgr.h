#pragma once

#include <iostream>
#include <deque>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include "Protocol.h"
#include <string>

enum eOREQ_CMD
{
	OREQ_CMD = 30,				// CJ작업 지시
	OREQ_MON = 40,
	OREQ_SQN = 50,
};

enum eOREQ_CMD_VAL1
{
	OREQ_CMD_V1_NODECON_AMR = 10, // 노드주행조작 AMR
	OREQ_CMD_V1_NODECON = 11,	// 노드 주행 조작
	OREQ_CMD_V1_MANUAL	= 20,	// 수동
	OREQ_CMD_V1_DOCKING	= 30,	// 도킹
	OREQ_CMD_V1_LIFTING	= 40,	// 리프팅
	OREQ_CMD_V1_POSCORR	= 60,	// 위치 보정
	OREQ_CMD_V1_CHARGE	= 70,	// 충전
};

enum eOREQ_CMD_VAL2_NODECON
{
	OREQ_CMD_V2_NC_DIJKSTRA	= 2,	// 주행 다익스트라(노드 주행 조작)
	OREQ_CMD_V2_NC_ARRNODE	= 1,	// 주행 경로(노드 주행 조작)
};

enum eOREQ_CMD_VAL2_MANUAL
{
	OREQ_CMD_V2_MN_STANDBY		= 1,	// 대기(수동)
	OREQ_CMD_V2_MN_FOWARD		= 2,	// 전진(수동)
	OREQ_CMD_V2_MN_BACKWARD		= 3,	// 후진(수동)
	OREQ_CMD_V2_MN_LEFTTURN		= 4,	// 좌회전(수동)
	OREQ_CMD_V2_MN_RIGHTTURN	= 5,	// 우회전(수동)
	OREQ_CMD_V2_MN_STOP			= 6,	// 정지(수동)
	OREQ_CMD_V2_MN_ANG_VEL		= 7,	// 각속도 초기화(수동)
	OREQ_CMD_V2_MN_ROTATE_CCW	= 8,	// 각도좌 회전(수동)
	OREQ_CMD_V2_MN_ROTATE_CW	= -1,	// 각도우 회전(수동)
	OREQ_CMD_V2_MN_STRAIGHT		= 9,	// 거리 이동(수동)
};

enum eOREQ_CMD_VAL2
{
	OREQ_CMD_V2_DOCKING		= 1,	// 도킹
	OREQ_CMD_V2_LIFTING0	= 0,	// 리프팅
	OREQ_CMD_V2_LIFTING1	= 1,	// 리프팅
	OREQ_CMD_V2_POSCORR		= 3000,	// 위치 보정
	OREQ_CMD_V2_CHARGE0		= 0,	// 충전
	OREQ_CMD_V2_CHARGE1		= 1,	// 충전
};

typedef struct _tag_OREQDATA
{
	int				nRemoteID;
	int				nAMRID;
	int				nCMD;
	int				nCMD_Value1;
	int				nCMD_Value2;
	std::string	sCMD_Value3;
	std::string	sCMD_Value4;
	std::string	nWorkID;
	std::string	nRequestTime;

	void SetData(int nIdx, char* bufVal)
	{
		switch (nIdx)
		{
		case 0:		this->nRemoteID				= atoi(	bufVal);	break;
		case 1:		this->nAMRID					= atoi(	bufVal);	break;
		case 2:		this->nCMD						= atoi(	bufVal);	break;
		case 3:		this->nCMD_Value1			= atoi(	bufVal);	break;
		case 4:		this->nCMD_Value2			= atoi(	bufVal);	break;
		case 5:		this->sCMD_Value3			=		bufVal;		break;
		case 6:		this->sCMD_Value4			=		bufVal;		break;
		case 7:		this->nWorkID					=		bufVal;		break;
		case 8:		this->nRequestTime		=		bufVal; 	break;

		default: break;
		}
	}

} OREQDATA;

class CRAIClientMgr
{
	// ================ Singtone ================
private:
	static CRAIClientMgr* pInst;

public:
	static CRAIClientMgr* GetInst() {
		if (pInst == nullptr)
			pInst = new CRAIClientMgr;

		return pInst;
	}

	static void Destroy() {
		if (pInst != nullptr)
			delete pInst;
		pInst = nullptr;
	}
	// ============================================

public:
	static boost::asio::io_service io_service_rai;
	static boost::asio::ip::tcp::endpoint endpoint_rai;
	static boost::thread* pthread_run;

public:

	bool IsConnecting();
	void LoginOK();
	bool IsLogin();
	void Connect(boost::asio::ip::tcp::endpoint endpoint);
	void Close();

	void PostSend(char* pData, const int nSize, const bool bImmediately = false);
	void Start();
	void Stop();

	void OrderProcess(const char* pOrderData, size_t nSize);

	bool IsOrderDataEmpty();
	unsigned int CRAIClientMgr::GetOrderDataSize();
	void GetOrderData(char* buff, size_t nSizeOfBuff);

private:
	void PostReceive();
	void handle_connect(const boost::system::error_code& error);
	void handle_write(const boost::system::error_code& error, size_t bytes_transferred);
	void handle_receive(const boost::system::error_code& error, size_t bytes_transferred);
	
public: // private:
	void ProcessPacket(const char*pData, size_t nSize);


private:
	boost::asio::io_service m_IOService;
	boost::asio::io_service::work m_Work;
	boost::asio::ip::tcp::socket m_Socket;
	boost::thread_group m_ioThreads;

	std::array<char, 512> m_ReceiveBuffer;
	
public: //private:
	int m_nPacketBufferMark;
	char m_PacketBuffer[MAX_RECEIVE_BUFFER_LEN];

	CRITICAL_SECTION m_lock;
	std::deque< char* > m_SendDataQueue;

	bool m_bIsLogin;

	CRITICAL_SECTION m_lock_order;
	std::deque< char* > m_OrderDataQueue;
	
private:
	OREQDATA m_tLastOREQData;//

public:

	CRITICAL_SECTION m_lock_OREQData;

	void setLastOREQData(OREQDATA& _tOREQData) {
		// 크리티컬 섹션	
		EnterCriticalSection(&m_lock);
		m_tLastOREQData = _tOREQData;
		LeaveCriticalSection(&m_lock);
	}

	OREQDATA getLastOREQData() {
		EnterCriticalSection(&m_lock);
		OREQDATA temp = m_tLastOREQData;
		LeaveCriticalSection(&m_lock);
		return temp;
	}

	void SendPauseToAmr(int nId, OREQDATA& tOREQData);


private:
	CRAIClientMgr();

public:
	~CRAIClientMgr();
};


