#include "StdAfx.h"
#include "PathFollow.h"
#include "ConstParameters.h"


void CPathFollow::Reset()
{
	_pfMode = 0;

	_curIndex = 0;
	_curDist = 0;
	
	_wpSize = 0;
	
	if(_wp) {
		delete [] _wp;
		_wp = NULL;
	}

	_ffVel.Zero();
	_fbVel.Zero();
	_desiredVel.Zero();
}

void CPathFollow::CalculateDistance()
{
	// 각 waypoint에서 목적지 까지의 거리 계산	
	double dist_sum = 0.;

	for(int i = _wpSize - 1; i >= 0; --i) {
		_wp[i].dist = dist_sum;

		if(i > 0) {
			dist_sum += _wp[i].pos.Distance(_wp[i - 1].pos);
		}
	}
}

void CPathFollow::LinearVelLimitByCurvature()
{
	// 구부러진 경로상의 곡률로부터 로봇이 회전할 때의 구심력을 계산한 후 
	// 구심력이 특정 값(ROBOT_CENTRIPETAL_ACCEL)을 넘어가지 않도록 구부러진 경로 진입 속도를 계산한다.

	for(int i = 1; i < _wpSize; ++i) {
		const CPosture &p1 = _wp[i - 1].pos;
		const CPosture &p2 = _wp[i - 0].pos;

		// 곡률반경 r을 계산: r = ds/dtheta
		double dth = DeltaRad(p1.th, p2.th);
		double ds  = p2.Distance(p1);
		double r   = (fabs(dth) < EPSILON) ? ds/EPSILON : ds/fabs(dth);

		// 곡률반경에 따라 낼 수 있는 속도 v계산
		// 구심가속도(a): a = v^2/r
		// v = sqrt(a*r)
		double v = sqrt(ROBOT_CENTRIPETAL_ACCEL*r);
		v = max (v, ROBOT_MIN_VELOCITY);

		// 경로상에 설정된 최대 속도와 비교하여 작은값 선택
		_wp[i].vel.v = min(_wp[i].vel.v, v);
	}
}

void CPathFollow::LinearVelLimitByDeceleration()
{
	for(int i = _wpSize - 2; i >= 0; --i) {
		// 로봇이 일정한 감속도(a)로 감속하여 정지하고자 할 때
		// 거리(s)를 남겨두고 현재 낼 수 있는 속도(v) 계산: 
		// 아래 식에서 t를 소거하여 v에 대해서 푼다:
		// v = v0 + a*t
		// s = v0*t + 1/2*a*t^2
		// ==> v = sqrt(v0^2 + 2*a*s) 

		double v0 = _wp[i + 1].vel.v;
		double s  = _wp[i + 0].dist - _wp[i + 1].dist;
		double v = sqrt(v0*v0 + 2*ROBOT_DECELERATION*s);
		
		// 경로상에 설정된 최대 속도와 비교하여 작은값 선택
		_wp[i].vel.v = min(_wp[i].vel.v, v);
	}
}

void CPathFollow::LinearVelLimitByAcceleration()
{
	for(int i = 1; i< _wpSize; ++i) {
		double v0 = _wp[i - 1].vel.v;
		double s  = _wp[i - 1].dist - _wp[i - 0].dist;
		double v = sqrt(v0*v0 + 2*ROBOT_ACCELERATION*s);
		
		// 경로상에 설정된 최대 속도와 비교하여 작은값 선택
		_wp[i].vel.v = min(_wp[i].vel.v, v);
	}
}

void CPathFollow::CalculateAngularVelocity()
{
	_wp[0].vel.w = 0;

	for(int i=1; i< _wpSize; ++i)
	{
		const CPosture &p_cur  = _wp[i - 0].pos;
		const CPosture &p_prev = _wp[i - 1].pos;

		// 경로상의 현재 점과 바로 앞 점간의 거리 계산
		double l = p_cur.Distance(p_prev);
		if(l < EPSILON)
			l = EPSILON;	// l이 0이되면 안된다.

		// 경로상의 현재 점과 바로 앞 점간의 각도 차이 계산
		double theta = DeltaRad(p_cur.th, p_prev.th);

		// 원호의 길이(l)은 원의 반지름(r)과 각도(theta)의 곱이다: l = r*theta
		// 원호에서의 선속도(v)은 원의 반지름(r)과 각속도(w)의 곱이다: v = r*w
		// 그러면 w = v/r = v/(l/theta) = v/l*theta 가 된다
		double w = _wp[i - 0].vel.v / l * theta;
		w = BOUND (w, -ROBOT_MAX_ANGULARVEL, ROBOT_MAX_ANGULARVEL);

		_wp[i - 0].vel.w = w;
	}
}


void CPathFollow::SmoothPath(vector<vaWaypoint> &path, int nodeSize)
{
	vector<vaWaypoint> new_path;

	int n = path.size();
	new_path.resize (n);

	for(int i = 0; i < n; i++) {
		int d = min(min(i, n - 1 - i), nodeSize);

		int s = i - d;
		int e = i + d + 1;
	
		vaWaypoint w(5);

		for(int j = s; j < e; j++)
		{
			w += path[j];
		}
		w /= (e - s);

		new_path[i] = w;
	}
	path.swap(new_path);
}


void CPathFollow::SetPath(const vector<vaWaypoint> &path, const CPosture &robotPos)
{
	Reset();

	int n_path = path.size();
	_wp = new sInterpolatedWp[n_path];

	// vector 컨테이너에 들어있는 path를 배열로 옮긴다.
	for(int i = 0; i < n_path; ++i)
	{
		// 방위 계산: 현재 waypoint와 전방 waypoint가 이루는 각도 계산
		double th = 0;

		if(n_path == 1)
		{
			th = atan2(path[i][1] - robotPos.y, path[i][0] - robotPos.x);
		}
		else if(n_path > 1)
		{
			if(i < n_path-1)
			{
				th = atan2(path[i + 1][1] - path[i][1], path[i + 1][0] - path[i][0]); 
			}
			else
			{
				th = atan2(path[i][1] - path[i - 1][1], path[i][0] - path[i - 1][0]); 
			}
		}
		_wp[i].pos = CPosture(path[i][0], path[i][1], th);		// 위치와 방위 복사
		_wp[i].vel.v = path[i][3];				// 최고속도 복사
		_wp[i].toNodeIndex = (int)path[i][4];		// 해당 위치에서 목표 Node의 Index => Node의 속성 정보 이용을 위해 필요
		_wp[i].toNodeId = (int)path[i][5];			// 해당 위치에서 목표 Node의 Id => Node의 속성 정보 이용을 위해 필요
	}

	_wpSize = n_path;	

	// 각 waypoint에서 목적지까지의 거리 계산
	CalculateDistance();
	_curDist = _wp[0].dist;
	_curToNodeIndex = _wp[0].toNodeIndex;
	_curToNodeId = _wp[0].toNodeId;

	// 처음과 끝 waypoint의 속도는 최소 속도가 되어야 한다.
	_wp[0        ].vel.v = ROBOT_MIN_VELOCITY;
	_wp[_wpSize-1].vel.v = ROBOT_MIN_VELOCITY;

	// 전방 경로의 곡률에 따라 속도 제한
	LinearVelLimitByCurvature ();
	// 사다리꼴 속도 프로파일 적용
	LinearVelLimitByDeceleration();
	LinearVelLimitByAcceleration();

	// 두 waypoint간의 곡률과 선속도로부터 각속도 계산
	CalculateAngularVelocity();

	// ###########################################################################
	/*
	// For debugging
	_unlink ("_path.csv");

	FILE *fp = fopen("_path.csv", "wt");
	if(fp) {
		for(int i = 0; i < _wpSize; ++i) {
			fprintf(fp, "%10.3f, %10.3f, %10.3f, %10.3f, %10.3f, %10.3f\n", _wp[i].dist, _wp[i].vel.v, _wp[i].vel.w, _wp[i].pos.x, _wp[i].pos.y, _wp[i].pos.th );
		}
		fclose (fp);
	}
	*/
}

