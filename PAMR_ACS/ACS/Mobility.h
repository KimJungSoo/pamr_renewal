#pragma once
#include "Posture.h"


class CMobility {
public:
	CMobility();
	~CMobility();

	void Drive (double linearVel, double angularVel, double dt);
	void QuickStop();

public:
	// Dead-reckoning으로 계산한 로봇의 위치와 방위
	CPosture _posture;

private:
	double _linearVel;
	double _angularVel;

	double VelLimit(double curVel, double tgtVel, double accel, double dt);
};
