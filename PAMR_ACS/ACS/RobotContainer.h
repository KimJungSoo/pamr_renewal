#pragma once

using namespace std;

struct sRobot
{
	int Id;
	double x, y, th;
	int fromNode, toNode;
	string name;

	string Ip;
	int Port;
	int FTP_Port;
	string FTP_Id;
	string FTP_Pw;

	int Length;
	int Width;
	int Height;

	int Payload;
	int WorkType;
	int OperatingTime;

	string ImageFile;

	sRobot(int Id_, double x_, double y_, double th_, const char *name_, const char *Ip_, int Port_, int FTP_Port_, const char *FTP_Id_, const char *FTP_Pw_, int Length_, int Width_, int Height_, int Payload_, int WorkType_, int OperatingTime_, const char *ImageFile_)
		: Id(Id_), x(x_), y(y_), th(th_), name(name_), Ip(Ip_), Port(Port_), FTP_Port(FTP_Port_), FTP_Id(FTP_Id_), FTP_Pw(FTP_Pw_), Length(Length_), Width(Width_), Height(Height_), Payload(Payload_), WorkType(WorkType_), OperatingTime(OperatingTime_), ImageFile(ImageFile_) { }
	sRobot()
		: Id(1), x(0), y(0), th(0), name("Empty_Value"), Ip("Empty_Value"), Port(5000), FTP_Port(5001), FTP_Id("Empty_Value"), FTP_Pw("Empty_Value"), Length(0), Width(0), Height(0), Payload(0), WorkType(0), OperatingTime(0), ImageFile("Empty_Value") { }
};


class CRobotContainer
{
public:
	CRobotContainer();

	CRobotContainer(const char *fileName);
	~CRobotContainer();

	void Clear();
	int LoadRobotFile(const char *fileName);
	int SaveRobotFile(const char *fileName);

	vector<sRobot> _robots;
};
