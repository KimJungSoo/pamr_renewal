#pragma once

#include "ConstParameters.h"
//#include "PAMR.h"
#include "GlobalMap.h"
#include "WallContainer.h"
#include "MagnetContainer.h"
#include "RobotContainer.h"
#include "TaskContainer.h"
#include "Astar.h"
#include "StationContainer.h"




extern CGlobalMap			*g_globalMap;

extern int					g_PamrNo;
//extern CPAMR				*g_Pamr[MAX_VEHICLE];

extern CAstar				*g_route;
extern CWallContainer		*g_wallContainer;
extern CMagnetContainer		*g_magnetContainer;

extern CRobotContainer		*g_robotContainer;

extern CTaskContainer		*g_taskContainer;
extern CStationContainer	*g_stationContainer;

extern void InitInstance();
extern void TerminateInstance();

////////////////////////////////////////////////////////////////////////////////////////////////////

#include "Packet.h"
