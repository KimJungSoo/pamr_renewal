#pragma once
#include "mm.h"
#include "Posture.h"

using namespace std;

struct sWallNode 
{
	bool close;				// Path 楷加己 备盒磊(true : Path 备盒磊(;), false : 楷加等 PathNode)

	double x, y, z;

	sWallNode(double x_, double y_, double z_)
		: close(false), x(x_), y(y_), z(z_) { }
	sWallNode(bool close_)
		: close(true), x(0), y(0), z(0) { }
};

struct sLine 
{
	double sx, sy;
	double mx, my;

	sLine(const sLine &line)
		: sx(line.sx), sy(line.sy), mx(line.mx), my(line.my) { }
	sLine(double sx_ = 0., double sy_ = 0., double mx_ = 0., double my_ = 0.)
		: sx(sx_), sy(sy_), mx(mx_), my(my_) { }
};

struct sMapEntity : sLine 
{
	double rho;
	double alpha;

	sMapEntity(const sLine &line, bool rhoMustPositive = false)
		: sLine(line) {	CalcRhoAlpha (rhoMustPositive);	}

	sMapEntity(double sx = 0., double sy = 0., double ex = 0., double ey = 0., bool rhoMustPositive = false)
		: sLine(sx, sy, ex - sx, ey - sy) {	CalcRhoAlpha(rhoMustPositive);	}

	void CalcRhoAlpha(bool rhoMustPositive = false)
	{
		rho = (sx * my - sy * mx) / sqrt(mx * mx + my * my);
		alpha = atan2(my, mx) - PI / 2;
		
		if(rhoMustPositive && rho < 0.)
		{
			rho = -rho;
			alpha += PI;
		}
		alpha = DeltaRad(alpha, 0.);
	}
};


class CWallContainer
{
public:
	CWallContainer(const char *fileName);
	~CWallContainer();

	void Clear();
	int LoadWallFile(const char *fileName);
	int SaveWallFile(const char *fileName);

	vector<sWallNode> _walls;
	vector<sMapEntity> _entities;
	vector<sMapEntity> MapTransform(CPosture &pose);
};
