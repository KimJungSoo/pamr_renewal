#pragma once
#include "Posture.h"
#include "Mobility.h"
#include "PathFollow.h"


////////////////////////////////////////////////////////////////////////////////////////////////////

#include "Packet.h"
#include "RAIClientMgr.h"

//g_Pamr[i]->m_stAMRStatus.nStatus 
enum ePAMR_WORK_STATUS
{
	STATUS_READY			=		0,		// 대기상태
	STATUS_MANUAL_OPERATION	=		1,		// 수동조작중
	STATUS_WORKING			=		2,		// 작업중
	STATUS_CHARGING			=		3,		// 충전조치중
	STATUS_WORK_BACK		=		4,		// 작업중 특정 위치(Load/Unload)로 되돌아 오는 기능
	STATUS_WARNING			=		6,		// WARNING
	STATUS_ERROR			=		9,		// ERROR
};

//g_Pamr[i]->m_nSatus
enum eAmrStatus
{
	AS_DISCONNECT,
	AS_IDLE,
	AS_WORKING,
	AS_CHARGING,
	AS_WARNING,
	AS_ERROR,
	AS_AMR_MANUAL,
	AS_AMR_IDLE,
	AS_AMR_WORKING,
	AS_AMR_CHARGING,
	AS_AMR_WARNING,
	AS_AMR_ERROR,
	AS_UNKNOWN,
};


enum ePAMR_WORK_DIV
{
	WORK_READY					 =	0,		// 대기상태
	WORK_LIFT_UP				 =	1,		// Lift Up			(nValue : 현재 Node)
	WORK_LIFT_DOWN				 =	2,		// Lift Down		(nValue : 현재 Node)
	WORK_DOOR_OPEN				 =	3,		// Door Open		(nValue : 현재 Node)
	WORK_DOOR_CLOSE				 =	4,		// Door Close		(nValue : 현재 Node)
	WORK_GUIDE_MOVE				 =	5,		// Guide Move		(nValue : 목표 Node,		nLocalizationUseArea : 분기시 주행방향(0 : 디폴트, 1 : 왼쪽주행, 2 : 오른쪽주행), nObstacleRange : 장애물 감지 범위)
	WORK_AUTONOMOUS				 =	6,		// Autonomous Move	(nValue : Path No.,			nLocalizationUseArea : 위치추정 사용 범위(0 : 전체사용, 1 : 왼쪽만 사용, 2 : 오른쪽만 사용))
	WORK_ROTATION				 =	7,		// 회전				(nValue : 현재 Node,		nLocalizationUseArea : 회전각도(Degree))
	WORK_STRAIGHT_MOVE			 =	8,		// 직선주행			(nValue : 목표 Node,		nLocalizationUseArea : 이동거리(mm))
	WORK_GUIDE_LIFT_UP			 =	9,		// Guide Move Slow & Lift Up(nValue : 목표 Node,	nLocalizationUseArea : 분기시 주행방향(0 : 디폴트, 1 : 왼쪽주행, 2 : 오른쪽주행), nObstacleRange : Cart 감지 체크 시간(초)) => 장애물 감지X
	WORK_GUIDE_MOVE_SLOW		 =	10,		// Guide Move Slow	(nValue : 목표 Node,		nLocalizationUseArea : 분기시 주행방향(0 : 디폴트, 1 : 왼쪽주행, 2 : 오른쪽주행), nObstacleRange : 장애물 감지 범위)		// bwk 2017.04.18 - 추가 : 느린 속도 제어를 위해 추가
	WORK_TUNNEL_MOVE			 =	11,     // Tunnel Move		(nValue : Path No.,			nLocalizationUseArea : 편향 이동 정보(0 : 중심 이동, 1 : 왼쪽 편향 이동, 2 : 오른쪽 편향 이동) => 복도와 같이 단순한 구조의 긴 구간을 주행하는 모드(설정된 좌우측의 간격을 일정하게 유지하도록 함)
	WORK_AUTO_CHARGING			 =	12,		// Auto Charging	(nValue : 충전 Station No.,	X) => 충전이 끝나면 대기장소로 이동(WORK_GUIDE_MOVE_SLOW)		// jdh 2017.12.01 - 추가 : 충전스테이션에서 자동충전하는 기능
	WORK_BACK					 =	13,		// 작업중 취소 및 시작위치로 이동(미개발)
	WORK_PRECISION_POSITION		 =	14,		// 정밀위치_전진	(nValue : 목표 Node,		X)																															// bwk 2018.01.03 - 추가 : 정밀 위치 이동(전진)을 위해 반사판 2개를 사용하여, 중앙 위치로 찾아가는 작업
	WORK_GUIDE_BACK_MOVE		 =	15,		// Guide Back Move	(nValue : 목표 Node,		nLocalizationUseArea : 분기시 주행방향(0 : 디폴트, 1 : 왼쪽주행, 2 : 오른쪽주행), nObstacleRange : 장애물 감지 범위)		// bwk 2018.01.02 - 추가 : 기능 추가
	WORK_PRECISION_POSITION_BACK =	16,		// 정밀위치_후진	(nValue : 목표 Node,		X)																															// bwk 2018.03.27 - 추가 : 정밀 위치 이동(후진)을 위해 반사판 2개를 사용하여, 중앙 위치로 찾아가는 작업
	WORK_GUIDE_BACK_MOVE_SLOW	 =	20,		// Guide Back Move Slow(nValue : 목표 Node,		nLocalizationUseArea : 분기시 주행방향(0 : 디폴트, 1 : 왼쪽주행, 2 : 오른쪽주행), nObstacleRange : 장애물 감지 범위)		// bwk 2018.01.02 - 추가 : 기능 추가
	WORK_LOADING = 30,
	WORK_UNLOADING = 31,
	WORK_WAIT					 =	31,		// 작업중 대기
	WORK_MANUAL_CHARGING		 =	32,		// 충전중
	WORK_MANUAL_MOVE			 =	51,		// 수동조작 Move
	WORK_MANUAL_FOLLOW			 =	52,		// Object Follow Move
};


enum ePAMR_WARNING
{
	WARNING_CART_WAIT				= 61,		// Cart 대기로 인한 정지
	WARNING_CART_POSITION			= 62,		// Cart Position 에러로 인한 정지
	WARNING_CHARGING_WAIT			= 63,		// 충전장소에서 충전대기중
	WARNING_PAUSE					= 64,		// 일시정지
	WARNING_CHARGING_STATION_ERROR	= 65,		// 충전 스테이션 고장상태		// bwk 2017.12.20 - 수정 : 충전 스테이션 고장 상태 추가
};


enum ePAMR_ERROR
{
	ERROR_OBSTACLE_STOP		= 91,		// 장애물 감지로 인한 정지
	ERROR_BUMPER_STOP		= 92,		// 범퍼 충돌로 인한 정지
	ERROR_EMERGENCY_STOP	= 93,		// 비상정지로 인한 정지
	ERROR_GUIDE_BREAK_AWAY	= 94,		// Guide 이탈로 인한 정지
	ERROR_DEVICE_FAULT		= 95,		// 장비(AMR) 고장
};

////////////////////////////////////////////////////////////////////////////////////////////////////

enum ePAMR_CMD
{
	CMD_MONITOR = 0,		// 모니터링
	CMD_WORK	= 1,		// 작업명령
	CMD_MANUAL	= 2,		// 수동조작
	CMD_INIT	= 3,		// 위치초기화
};

enum ePAMR_CMD_TYPE
{
	CMD_TYPE_MOVE = 1,		// 이동
	CMD_TYPE_WORK = 2,		// 작업(Load / Unload)
};

enum ePAMR_CMD_TYPE_WORK_LAYER
{
	CMD_TYPE_WORK_LAYER_GROUND	= 0,		// 바닥
	CMD_TYPE_WORK_LAYER_1		= 1,		// 1단
	CMD_TYPE_WORK_LAYER_2		= 2,		// 2단
};

enum ePAMR_CMD_WORK
{
	CMD_POS_INITIALIZE = 1,
	CMD_WORK_MOVE = 2,
	CMD_WORK_WORK = 3,
	CMD_WORK_RESET = 4,
	CMD_WORK_START = 5,
};

enum e_CMD_VALUE_AMR_DEMO
{
	CMD_AMR_DEMO_START			=	0,
	CMD_VALUE_AMR_DEMO_PAUSE	=	0,
	CMD_VALUE_AMR_DEMO_START	=	1,
	CMD_VALUE_AMR_DEMO_INIT		=	2,
};

////////////////////////////////////////////////////////////////////////////////////////////////////


enum eAmrCommand
{
	AC_WAIT,
	AC_MOVE_AUTONOMOUS,
	AC_MOVE_GUIDE,
	AC_LOAD,
	AC_UNLOAD,
	AC_STOP,
	AC_RESUME,
};

struct sAmrCommand
{
	eAmrCommand cmd;
	long lparam;
	string sparam;

	sAmrCommand(eAmrCommand cmd_, long lparam_ = 0, string sparam_ = "") 
		: cmd(cmd_), lparam(lparam_), sparam(sparam_) { }
};


extern const char *GetAmrStatus(int nStatus);
extern const char *GetAmrStatusDetail(int nStatusDetail);


typedef struct tag_work_list
{
	int nNo;
	int nWork;
	int nValue;
	int nLocalizationUseArea;	
	int nObstacleRange;		
	std::string strPath;
} work_list;


class CPAMR
{
public:
	CPAMR();
	~CPAMR();

	void AmrCommand(eAmrCommand cmd, long param);
	void AmrCommand(eAmrCommand cmd, string &param);

	CPosture GetMobilityPosture();
	void SetMobilityPosture(const CPosture &posture);

	bool DoNavigation(double dt);

	bool FindPath(const char *targetName);

	bool Wait(int to);
	bool Load(int to);
	bool Unload(int to);
	//bool NavigationTo(int tn);
	bool NavigationTo(const char *targetName);
	void NavigationStop();

	//vector<CPosture> GetTracePath();

public:
	deque<sAmrCommand> _command;
	CMobility _mobility;
	CPathFollow _pathFollow;

	int m_nStatus;

	CTime m_ctmRcvTime;
	bool m_bConnected;

	// AMRStatus m_stAMRStatus; // BY KJS.
	HWStatus  m_stHWStatus;
	OREQDATA m_OREQData;

	vector<int> m_vtWorkData;

	bool m_bCheckStart_SendCmd;
	DWORD m_dwCheckStart_SendCmd;
	int m_nCountWorkData_SendCmd;
	
	// For multiple AMRs
	/*
	vector<WorkList> m_workListData;
	*/
	int  m_nWorkIdx;
	bool m_bSeqwork = false;
	bool m_bColrisk = false;
	bool m_bPickclr = false;
	bool m_bPickstatus = false;
	bool m_bSeqclr = false;
	bool m_bCharge = false;
	bool bFirstRun = false;
	bool bObsAvoid = false;
	int nprevStatusValue;

	work_list work_list_file[10];
	int _pathList[1024] = { 0, };
	int _countList = 0;
	//std::string strPath;
	//test-start
//public:
//	int getWorkListSize_();
//	WorkList* LoadWorkList_(int WorkNum);
//	void InsertWorkList_(const std::string& sWork);
//
//
private:
//	
//	int m_nWorkListCount_;
	// test-end
	
	//int nprevStatusValue;
	int nCntWorkList;
	
	////////////////////////////////////////

private:
	void StatWait(double dt);
	void StatLoad(double dt);
	void StatUnload(double dt);
	void StatDriving(double dt);
	void StatAbortNavigation(double dt);

private:
	int _navState;
	long _navTimeout;
};
