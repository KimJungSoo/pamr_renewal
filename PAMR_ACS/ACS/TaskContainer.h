#pragma once

using namespace std;

struct sTaskInfo {
	int status;
	vector<string> todo;
};

class CTaskContainer
{
public:
	CTaskContainer(const char *fileName);
	~CTaskContainer();

	void Clear();
	int LoadFile(const char *fileName);


	vector<sTaskInfo> _storage;
};
