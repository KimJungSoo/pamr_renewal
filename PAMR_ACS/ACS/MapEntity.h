#pragma once
#include "mm.h"
#include "Posture.h"
#include <valarray>

using namespace std;

extern double EPSILON;


// 배열형 way point(0 - x위치, 1 - y위치, 2-방위, 3-최고속도 )
typedef	valarray<double>	vaWaypoint;	


extern char *skip_space (char *p);
extern bool EOS(char c);
