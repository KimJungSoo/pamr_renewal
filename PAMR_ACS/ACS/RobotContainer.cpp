#include "stdafx.h"
#include "RobotContainer.h"


CRobotContainer::CRobotContainer()
{
}

CRobotContainer::CRobotContainer(const char *fileName)
{
	if(fileName && *fileName)
	{
		LoadRobotFile(fileName);
	}
}

CRobotContainer::~CRobotContainer()
{
	Clear();
}

void CRobotContainer::Clear()
{
	_robots.clear();
}

int CRobotContainer::LoadRobotFile(const char *fileName)
{
	_robots.reserve(1000);

	FILE *fp = fopen(fileName, "rt");
	if(!fp) return -1;

	char *p;
	char text[1024 + 1];

	while(fgets(text, 1024, fp))
	{
		text[1024] = '\0';

		for(p = text; (*p == ' ') || (*p == '\t') || (*p == '\r') || (*p == '\n'); ++p);

		if(*p == '\0')		continue;
		if(*p == '#')		continue;		// # 기호는 주석을 의미한다.
		if(*p == ';')		continue;		// ; 기호는 새로운 경로의 시작을 의미한다.

		int Id = 0;
		double x = -1.;
		double y = -1.;
		double th = -1.;
		char name[256] = "";

		char Ip[256] = "Empty_Value";
		int Port = 0;
		int FTP_Port = 0;
		char FTP_Id[256] = "Empty_Value";
		char FTP_Pw[256] = "Empty_Value";

		int Length = 0;
		int Width = 0;
		int Height = 0;

		int Payload;
		int WorkType;
		int OperatingTime;

		char ImageFile[256] = "Empty_Value";

		sscanf(p, "%d %lf %lf %lf %s %s %d %d %s %s %d %d %d %d %d %d %s", &Id, &x, &y, &th, name, Ip, &Port, &FTP_Port, FTP_Id, FTP_Pw, &Length, &Width, &Height, &Payload, &WorkType, &OperatingTime, ImageFile);
		_robots.push_back(sRobot(Id, x, y, th, name, Ip, Port, FTP_Port, FTP_Id, FTP_Pw, Length, Width, Height, Payload, WorkType, OperatingTime, ImageFile));
		printf("Robot Id %d:[%s]\n", Id, Ip);
	}

	TRACE("SizeofData(_robots): %d\n", _robots.size());

	fclose(fp);

	return 0;
}

int CRobotContainer::SaveRobotFile(const char *fileName)
{

	FILE *fp = fopen(fileName, "wt");
	if(!fp) return -1;

	for(unsigned int i = 0; i < _robots.size(); ++i)
	{
		sRobot &robot = _robots[i];

		if(robot.name == "")
			robot.name = "Empty_Value";
		if(robot.Ip == "")
			robot.Ip = "Empty_Value";
		if(robot.FTP_Id == "")
			robot.FTP_Id = "Empty_Value";
		if(robot.FTP_Pw == "")
			robot.FTP_Pw = "Empty_Value";
		if(robot.ImageFile == "")
			robot.ImageFile = "Empty_Value";

		fprintf(fp, "%d %10.3f %10.3f %10.3f %s %s %d %d %s %s %d %d %d %d %d %d %s\n"
			, robot.Id
			, robot.x
			, robot.y
			, robot.th
			, robot.name.c_str()
			, robot.Ip.c_str()
			, robot.Port
			, robot.FTP_Port
			, robot.FTP_Id.c_str()
			, robot.FTP_Pw.c_str()
			, robot.Length
			, robot.Width
			, robot.Height
			, robot.Payload
			, robot.WorkType
			, robot.OperatingTime
			, robot.ImageFile.c_str());
	}

	fclose(fp);

	return 0;
}

