#pragma once
#include "stdafx.h"
#include "RAIClientMgr.h"
//----------[IOCP 관련]---------->>
#include "IocpSrvMgr.h"
//----------[IOCP 관련]----------<<

#include "AppInstance.h"

#include "OperationMgr.h"


CRAIClientMgr* CRAIClientMgr::pInst = nullptr;

CRAIClientMgr::CRAIClientMgr()
	: m_IOService(), 
	m_Socket(m_IOService), 
	m_Work(m_IOService)
{
	m_bIsLogin = false;
	InitializeCriticalSectionAndSpinCount(&m_lock, 4000);
	InitializeCriticalSectionAndSpinCount(&m_lock_order, 4000);

	InitializeCriticalSectionAndSpinCount(&m_lock_OREQData, 4000);
}

CRAIClientMgr::~CRAIClientMgr()
{
	Stop();
	Close();

	EnterCriticalSection(&m_lock);
	while (m_SendDataQueue.empty() == false)
	{
		delete[] m_SendDataQueue.front();
		m_SendDataQueue.pop_front();
	}
	LeaveCriticalSection(&m_lock);
	DeleteCriticalSection(&m_lock);

	EnterCriticalSection(&m_lock_order);
	while (m_OrderDataQueue.empty() == false)
	{
		delete[] m_OrderDataQueue.front();
		m_OrderDataQueue.pop_front();
	}
	LeaveCriticalSection(&m_lock_order);
	DeleteCriticalSection(&m_lock_order);

	DeleteCriticalSection(&m_lock_OREQData);

}

void CRAIClientMgr::Start()
{
	m_ioThreads.create_thread(boost::bind(&boost::asio::io_service::run, &m_IOService));
}

void CRAIClientMgr::Stop()
{
	m_IOService.stop();
	m_ioThreads.join_all();
	m_IOService.reset();
}

bool CRAIClientMgr::IsConnecting() 
{
	return m_Socket.is_open();
}

void CRAIClientMgr::LoginOK()
{
	m_bIsLogin = true;
}
bool CRAIClientMgr::IsLogin()
{
	return m_bIsLogin;
}

void CRAIClientMgr::Connect(boost::asio::ip::tcp::endpoint endpoint)
{
	m_nPacketBufferMark = 0;

	m_Socket.async_connect(
		endpoint,
		boost::bind(
			&CRAIClientMgr::handle_connect,
			this,
			boost::asio::placeholders::error)
	);
}

void CRAIClientMgr::Close()
{
	if (m_Socket.is_open())
	{
		m_Socket.close();
	}
}

void CRAIClientMgr::PostSend(char* pData, const int nSize, const bool bImmediately/*= false*/)
{
	char* pSendData = nullptr;

	EnterCriticalSection(&m_lock);

	if (bImmediately == false)
	{
		pSendData = new char[nSize];
		memcpy(pSendData, pData, nSize);

		m_SendDataQueue.push_back(pSendData);
	}
	else
	{
		pSendData = pData;
	}

	if (bImmediately || m_SendDataQueue.size() < 2)
	{
		boost::asio::async_write(m_Socket, boost::asio::buffer(pSendData, nSize),
			boost::bind(&CRAIClientMgr::handle_write, this,
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred)
		);
	}

	LeaveCriticalSection(&m_lock);
}

void CRAIClientMgr::PostReceive()
{
	memset(&m_ReceiveBuffer, '\0', sizeof(m_ReceiveBuffer));

	m_Socket.async_read_some
	(
		boost::asio::buffer(m_ReceiveBuffer),
		boost::bind(
			&CRAIClientMgr::handle_receive,
			this,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred)
	);
}

void CRAIClientMgr::handle_connect(const boost::system::error_code& error)
{
	if (!error)
	{
		PostReceive();
	}
	else
	{
		std::cout << "[RAI]: 서버 접속 실패. error No: " << error.value() << std::endl;
		std::cout << "[RAI]: error Message: " << error.message() << std::endl;
	}
}

void CRAIClientMgr::handle_write(const boost::system::error_code& error, size_t bytes_transferred)
{
	EnterCriticalSection(&m_lock);

	if (m_SendDataQueue.empty() == false)
	{
		delete[] m_SendDataQueue.front();
		m_SendDataQueue.pop_front();
	}

	char* pData = nullptr;

	if (m_SendDataQueue.empty() == false)
	{
		pData = m_SendDataQueue.front();
	}

	LeaveCriticalSection(&m_lock);


	if (pData != nullptr)
	{
		std::string strTemp = pData;
		PostSend((char*)strTemp.c_str(), strTemp.size(), true);
	}
}

void CRAIClientMgr::handle_receive(const boost::system::error_code& error, size_t bytes_transferred)
{
	if (error)
	{
		if (error == boost::asio::error::eof)
		{
			std::cout << "[RAI] : disconnect client.." << std::endl;
		}
		else
		{
			std::cout << "[RAI] : error No: " << error.value() << " error Message: " << error.message() << std::endl;
		}

		Close();
	}
	else
	{
		memcpy(&m_PacketBuffer[m_nPacketBufferMark], m_ReceiveBuffer.data(), bytes_transferred);

		std::string strTemp;
		if ((bytes_transferred > MAX_HEADER_LEN) &&
			(0 == strncmp(m_PacketBuffer, HEADER_OREQ, MAX_HEADER_LEN)))
		{
			strTemp = m_PacketBuffer;
			std::cout << '[' << strTemp.c_str() << ']' << std::endl;
			ProcessPacket(m_PacketBuffer, bytes_transferred);
		}
		else
		{
			strTemp = m_PacketBuffer;
			std::cout << '^' << strTemp.c_str() << '^' << std::endl;
		}

		for (int nIdx = 0; nIdx < bytes_transferred; nIdx++)
		{
			printf("%c", m_PacketBuffer[nIdx]);
		}
		printf("\n");

		memset(m_PacketBuffer, 0x00, MAX_RECEIVE_BUFFER_LEN);

		PostReceive();
	}
}

void CRAIClientMgr::ProcessPacket(const char*pData, size_t nSize)
{
	char* pOrderData = nullptr;

	EnterCriticalSection(&m_lock_order);
	pOrderData = new char[nSize];
	memcpy(pOrderData, pData, nSize);

	// 명령 처리
	OrderProcess(pOrderData, nSize);

	m_OrderDataQueue.push_back(pOrderData);
	LeaveCriticalSection(&m_lock_order);

#if 0
	PACKET_HEADER* pheader = (PACKET_HEADER*)pData;

	switch (pheader->nID)
	{
	case RES_IN:
	{
		PKT_RES_IN* pPacket = (PKT_RES_IN*)pData;

		LoginOK();

		std::cout << "login success ?: " << pPacket->bIsSuccess << std::endl;
	}
	break;
	case NOTICE_CHAT:
	{
		PKT_NOTICE_CHAT* pPacket = (PKT_NOTICE_CHAT*)pData;

		std::cout << pPacket->szName << ": " << pPacket->szMessage << std::endl;
	}
	break;
	}
#endif
}

bool CRAIClientMgr::IsOrderDataEmpty()
{
	return m_OrderDataQueue.empty();
}

unsigned int CRAIClientMgr::GetOrderDataSize()
{
	return m_OrderDataQueue.size();
}

void CRAIClientMgr::GetOrderData(char* pBuff, size_t nSizeOfBuff)
{
	if (false == m_OrderDataQueue.empty())
	{
		memcpy_s(pBuff, nSizeOfBuff, m_OrderDataQueue.front(), MAX_RECEIVE_BUFFER_LEN);
		m_OrderDataQueue.pop_front();
	}
}

void Packet2OREQDATA(const char* pData, OREQDATA& tOREQData)
{

	// TODO : 프로토콜 변경 시킨거 전부 적용 해서 넣어야 함

	// pData : 헤더 포함 패킷 전체
	// [0]~[3] : Header
	// [4]~[7] : Length
	// [8]~ :	 데이터
	// ',' 구분문자
	// ['@','@','@','@'] : 종료 문자
	char bufVal[BYTE_MAX] = { '\0', };
	int nPacketLen = 0;

	memset(bufVal, 0, sizeof(bufVal));
	memcpy(&bufVal, &pData[4], 4);

	nPacketLen = atoi(bufVal);

	// 시작 위치 8
	// idxBuf : 값 하나의 시작 위치
	int idxBuf = 8;
	int idxSet = 0;


	while (true)
	{
		// pData 한계 접근 제한
		if (idxBuf >= nPacketLen)
			break;

		// 종료자 만나면 끝!
		if (pData[idxBuf] == '@')
			break;

		// 한 데이터 값의 길이
		int idxLen = 0;

		// 종료자나 구분문자 찾음
		while (idxBuf + idxLen < nPacketLen)
		{
			char temp = pData[idxBuf + idxLen];
			if (temp == ',' || temp == '@')
				break;
			++idxLen;
		}

		// bufVal를 초기화 하고 값을 넣어줌
		memset(bufVal, 0, sizeof(bufVal));
		memcpy_s(bufVal, sizeof(bufVal), &pData[idxBuf], sizeof(char)*idxLen);

		// OREQData에 값을 넣어줌
		tOREQData.SetData(idxSet, bufVal); //
		++idxSet;

		// 다음 위치
		idxBuf = idxBuf + idxLen + 1;
	}

}

// OREQ를 AMR로 전송
void SendOREQToAmr(int nId, const char* pOrderData, DWORD dwSize)
{
	DWORD dwSize1 = 0;
	CIocpSrvMgr* pSrv = CIocpSrvMgr::GetInst();

	PSOCKET_DATA pSockets = pSrv->GetSockets();

	std::string sRobotIP;

	if (pSockets == NULL)
		return;

	printf("Number of Client robots: %d\n", g_robotContainer->_robots.size());

	for (int i = 0; i < g_robotContainer->_robots.size(); ++i)
	{
		if (g_robotContainer->_robots[i].Id == nId)
		{
			sRobotIP = g_robotContainer->_robots[i].Ip;
			
			printf("[Index: Robot Id] %d, %d\n", nId, g_robotContainer->_robots[i].Id);
			break;
		}
	}

	printf("Size of IndexQ: %d\n", pSrv->m_SocketIndexQ.Size());

	//for (int i = 0; i < 1; i++)
	for (int i = 0; i < pSrv->m_SocketIndexQ.Size(); i++)
	{
		if (!sRobotIP.compare(pSockets[i].IpAddress))
		{
			printf("[RobotIP:] %s\t [SocketIP:] %s\n", sRobotIP.c_str(), pSockets[i].IpAddress);

			DWORD dwFlags = 0;
			dwSize1 = 0;

			pSockets[i].IOData[1].WsaBuf.buf = (char *)pOrderData;
			pSockets[i].IOData[1].WsaBuf.len = dwSize;


			int nRtn = WSASend(
				pSockets[i].Socket
				, &(pSockets[i].IOData[1].WsaBuf)
				, 1
				, &dwSize1
				, dwFlags
				, &(pSockets[i].IOData[1].Overlapped)
				, NULL);
			
			printf("[To AMR] Before Send [%d] and  After Send [%d]\n", dwSize, dwSize1);


			int result = WSAGetLastError();
		}
	}
}

// 다익스트라로 Path 문자열
std::string MakeOREQPath(int nRobotIdx, int nSrc, int nDst)
{
	int nResultPath0[PATH_MAX];
	int nResultPath[PATH_MAX];
	memset(nResultPath0, 0x00, sizeof(nResultPath0));
	memset(nResultPath, 0x00, sizeof(nResultPath));
	
	int nSrc1 = g_route->FindVertexID(nSrc); //입력 노드에 대한 노드인덱스(0부터) 찾음
	int nDst1 = g_route->FindVertexID(nDst);
	if (g_route->_nodePoints[nDst1] == 'd')
	{
		printf("\nUnaccessible Station due to an Obstacle !.\n");
		string strPath0 = "";
		return strPath0;
	}

	if (nSrc1 < 0)
	{
		printf("[Error] Src node is %d\n", nSrc1);
		return "";
	}

	dijkstra(g_graph, nSrc1, nDst1, nResultPath0); //입력값: 노드 인덱스(0~)
	if (nResultPath0[0] <1)
	{
		printf("[Error] S-Path is not available!\n");
		return "";
	}
	// If there is 'disable' node ///////////////////////////////////////////////////////////////////
	int brk_num = 0;
	int brk_idx[100] = { 0, }; 
	// 1. check the number of 'd' nodes and each node index
	for (int i = 1; i <= nResultPath0[0]; i++)
	{
		int ind1 = g_route->FindVertexID(nResultPath0[i]);
		if (g_route->_nodePoints[ind1] == 'd')
		{
			brk_idx[brk_num] = i;
			printf("\nDisable node: [%d] %d\n", brk_num, nResultPath0[brk_idx[brk_num]]);

			brk_num++;
		}
	}

	if (g_Pamr[nRobotIdx]->bObsAvoid == true)
	{
		if (brk_num > 0) // 'd' 노드가 있을 때
		{
			memset(nResultPath0, 0x00, sizeof(nResultPath0));
			memset(nResultPath, 0x00, sizeof(nResultPath));

			//MessageBox(_T("Disble node exists."), _T("Error"), MB_OK);

			dijkstra(b_graph, nSrc1, nDst1, nResultPath0);

			if (nResultPath0[0] < 1)
			{
				printf("[Error] Bi-Path is not available!\n");
				return "";
			}
			printf("\n[Bi-Path] %d points:  ", nResultPath0[0]);
		}
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	nResultPath[0] = nResultPath0[0]; //얻어진 d-path 노드점의 개수

	for (int i = 1; i < PATH_MAX; i++)
	{
		nResultPath[i] = g_route->FindVertexNum(nResultPath0[i]); 
		//ordered 노드점을 원래 노드점으로 변경함: _vertexList[num-1].id;
	}
	//========================================
	g_Pamr[nRobotIdx]->_countList = nResultPath[0];
	memcpy(g_Pamr[nRobotIdx]->_pathList, nResultPath, sizeof(int)* (nResultPath[0] + 1));
	//nSrc = nSrc1+1; // d-path 변환 path 의 첫번째 노드
	//--------------------------------------------------------------------------------

	string strPath = to_string(nSrc); // check

	for (int nCnt = 1; nCnt <= nResultPath[0]; nCnt++)
	{
		strPath += ';' + (to_string(nResultPath[nCnt]));
	}

	return strPath;
}

//
void SendOREQDESTRUNToAmr(int nId,  OREQDATA& tOREQData)
{
	char buf[1024] = {'\0', };
	char bufSize[5];
	std::string sData;
	memset(buf, 0, sizeof(buf));

	// 시작 위치 찾음
	int nSrc = -1;
	int nRobotIdx = -1;
	for (int i = 0; i < g_PamrNo; ++i)
	{
		if (g_Pamr[i]->m_stAMRStatus.nId == tOREQData.nAMRID)
		{
			nSrc = g_Pamr[i]->m_stAMRStatus.nToNodeNo;
			nRobotIdx = i;
			break;
		}
	}

	if (nSrc == -1)
		return;

	//--------------------------------------------------
	/*if (tOREQData.nCMD_Value1 == 11)
	{
		tOREQData.nCMD_Value1 = 10;
	}*/

	string sPath;
	if (tOREQData.nCMD_Value2 == 1)
	{
		sPath = tOREQData.sCMD_Value3;
	}
	else if (tOREQData.nCMD_Value2 == 2)
	{
		// nSrc, nDst 는 실제 노드값
		sPath = MakeOREQPath(nRobotIdx, nSrc, atoi(tOREQData.sCMD_Value3.c_str()));
	}
	//--------------------------------------------------
	// 다익스트라로 패스 만듦
	//string sPath = MakeOREQPath(nSrc, atoi(tOREQData.sCMD_Value3.c_str()));

	printf("Obtained path(Djkstra): %s\n", sPath.c_str());

	// 프로토콜 조합
	sprintf_s(buf, "%s", "OREQ");

	sData += std::to_string(tOREQData.nRemoteID) + ',';
	sData += std::to_string(tOREQData.nAMRID) + ',';
	sData += std::to_string(tOREQData.nCMD) + ',';
	sData += std::to_string(tOREQData.nCMD_Value1) + ',';
	sData += std::to_string(tOREQData.nCMD_Value2) + ',';
	sData += sPath + ',';
	sData += tOREQData.sCMD_Value4 + ',';
	sData += tOREQData.nWorkID + ',';
	sData += tOREQData.nRequestTime;
	sData += "@@@@";

	int nDataSize = sData.length();

	sprintf(bufSize, "%04d", nDataSize+8);

	memcpy_s(buf+8, sizeof(buf)-8, sData.c_str(), nDataSize);

	memcpy_s(buf+4, sizeof(char)*4, bufSize, sizeof(char)*4);

	// AMR 던짐
	SendOREQToAmr(nId, buf, nDataSize + 8);
}

void SendOREQDESTRUNToAmr1(int nId, OREQDATA& tOREQData)
{
	char buf[256] = { '\0', };
	char bufSize[5];
	std::string sData;
	memset(buf, 0, sizeof(buf));

	// 시작 위치 찾음
	int nSrc = -1;
	int nRobotIdx = -1;
	for (int i = 0; i < g_PamrNo; ++i)
	{
		if (g_Pamr[i]->m_stAMRStatus.nId == nId) // tOREQData.nAMRID
		{
			nSrc = g_Pamr[i]->m_stAMRStatus.nToNodeNo;
			nRobotIdx = i;
			break;
		}
	}

	if (nSrc == -1)
		return;

	//--------------------------------------------------
	//if (tOREQData.nCMD_Value1 == 11)
	//{
	//	tOREQData.nCMD_Value1 = 10;
	//}

	string sPath;
	/*if (tOREQData.nCMD_Value2 == 1)
	{
		sPath = tOREQData.sCMD_Value3;
	}*/
	 if (tOREQData.nCMD_Value2 == 2)
	{
		sPath = MakeOREQPath(nRobotIdx, nSrc, atoi(tOREQData.sCMD_Value3.c_str()));
	}
	 else
	 {
		 return;
	 }
	 printf("Obtained path : %s\n", sPath.c_str());

	//--------------------------------------------------
	 //변환
	tOREQData.nCMD = 88; //30
	 tOREQData.nCMD_Value1 = 6; //11
	 //tOREQData.nCMD_Value2 = 1;
	//--------------------------------------------------
	
	// 프로토콜 조합
	sprintf_s(buf, "%s", "OREQ");

	sData += std::to_string(tOREQData.nRemoteID) + ',';
	sData += std::to_string(nId) + ','; //tOREQData.nAMRID
	sData += std::to_string(tOREQData.nCMD) + ',';
	sData += std::to_string(tOREQData.nCMD_Value1) + ',';
	sData += sPath + ',';
	sData += tOREQData.sCMD_Value3 + ',';
	sData += tOREQData.sCMD_Value4 + ',';
	sData += tOREQData.nWorkID + ',';
	sData += tOREQData.nRequestTime;
	sData += "@@@@";

	int nDataSize = sData.length();

	sprintf(bufSize, "%04d", nDataSize + 8);

	memcpy_s(buf + 8, sizeof(buf) - 8, sData.c_str(), nDataSize);

	memcpy_s(buf + 4, sizeof(char) * 4, bufSize, sizeof(char) * 4);

	// AMR 던짐
	SendOREQToAmr(nId, buf, nDataSize + 8);
}



void SendOREQDESTRUNToAmr2(int nId, OREQDATA& tOREQData)
{
	char buf[256] = { '\0', };
	char bufSize[5];
	std::string sData;
	memset(buf, 0, sizeof(buf));

	// 시작 위치 찾음
	int nSrc = -1;
	int nRobotIdx = -1;
	for (int i = 0; i < g_PamrNo; ++i)
	{
		if (g_Pamr[i]->m_stAMRStatus.nId == nId) // tOREQData.nAMRID
		{
			nSrc = g_Pamr[i]->m_stAMRStatus.nToNodeNo;
			nRobotIdx = i;
			break;
		}
	}

	if (nSrc == -1)
		return;

	string sPath;
	/*if (tOREQData.nCMD_Value2 == 1)
	{
	sPath = tOREQData.sCMD_Value3;
	}*/
	if (tOREQData.nCMD_Value2 == 2)
	{
		sPath = MakeOREQPath(nRobotIdx, nSrc, atoi(tOREQData.sCMD_Value3.c_str()));
	}
	else
	{
		return;
	}
	printf("Obtained path : %s\n", sPath.c_str());

	//--------------------------------------------------
	//변환
	tOREQData.nCMD = 88; //30
	tOREQData.nCMD_Value1 = 6; //11
	//tOREQData.nCMD_Value2 = 1;
	//--------------------------------------------------

							   // 프로토콜 조합
	sprintf_s(buf, "%s", "OREQ");

	sData += std::to_string(tOREQData.nRemoteID) + ',';
	sData += std::to_string(nId) + ','; //tOREQData.nAMRID
	sData += std::to_string(tOREQData.nCMD) + ',';
	sData += std::to_string(tOREQData.nCMD_Value1) + ',';
	sData += sPath + ',';
	sData += tOREQData.sCMD_Value3 + ',';
	sData += tOREQData.sCMD_Value4 + ',';
	sData += tOREQData.nWorkID + ',';
	sData += tOREQData.nRequestTime;
	sData += "@@@@";

	int nDataSize = sData.length();

	sprintf(bufSize, "%04d", nDataSize + 8);

	memcpy_s(buf + 8, sizeof(buf) - 8, sData.c_str(), nDataSize);

	memcpy_s(buf + 4, sizeof(char) * 4, bufSize, sizeof(char) * 4);

	// AMR 던짐
	SendOREQToAmr(nId, buf, nDataSize + 8);
}



void SendOREQDESTRUNToAmr_CS(int nId, OREQDATA& tOREQData, string sPath)
{
	char buf[256] = { '\0', };
	char bufSize[5];
	std::string sData;
	memset(buf, 0, sizeof(buf));

	// 시작 위치 찾음
	int nSrc = -1;
	for (int i = 0; i < g_PamrNo; ++i)
	{
		if (g_Pamr[i]->m_stAMRStatus.nId == nId) // tOREQData.nAMRID
		{
			nSrc = g_Pamr[i]->m_stAMRStatus.nToNodeNo;
			break;
		}
	}

	if (nSrc == -1)
		return;

	
	//변환
	tOREQData.nCMD = 88;
	tOREQData.nCMD_Value1 = 6;
	//tOREQData.nCMD_Value2 = 1;
	//--------------------------------------------------
	// 다익스트라로 패스 만듦
	//string sPath = MakeOREQPath(nSrc, atoi(tOREQData.sCMD_Value3.c_str()));

	//printf("Obtained path(Djkstra): %s\n", sPath.c_str());

	// 프로토콜 조합
	sprintf_s(buf, "%s", "OREQ");

	sData += std::to_string(tOREQData.nRemoteID) + ',';
	sData += std::to_string(nId) + ','; //tOREQData.nAMRID
	sData += std::to_string(tOREQData.nCMD) + ',';
	sData += std::to_string(tOREQData.nCMD_Value1) + ',';
	sData += sPath+ ',';
	sData += std::to_string(0) + ',';
	sData += std::to_string(0) + ',';
	sData += tOREQData.nWorkID + ',';
	sData += tOREQData.nRequestTime;
	sData += "@@@@";

	int nDataSize = sData.length();

	sprintf(bufSize, "%04d", nDataSize + 8);

	memcpy_s(buf + 8, sizeof(buf) - 8, sData.c_str(), nDataSize);

	memcpy_s(buf + 4, sizeof(char) * 4, bufSize, sizeof(char) * 4);

	// AMR 던짐
	SendOREQToAmr(nId, buf, nDataSize + 8);
}




void SendResumeToAmr(int nId, OREQDATA& tOREQData)
{
	char buf[1024] = {'\0',};
	char bufSize[5];
	std::string sData;
	memset(buf, 0, sizeof(buf));

	// 시작 위치 찾음
	int nSrc = -1;
	for (int i = 0; i < g_PamrNo; ++i)
	{
		if (g_Pamr[i]->m_stAMRStatus.nId == nId) // nId = tOREQData.nAMRID
		{
			nSrc = g_Pamr[i]->m_stAMRStatus.nToNodeNo;
			break;
		}
	}

	if (nSrc == -1)
		return;

	//--------------------------------------------------
	tOREQData.nCMD = 88;
	tOREQData.nCMD_Value1 = 0;
	tOREQData.nCMD_Value2 = 2; //재시작
	//--------------------------------------------------

	// 프로토콜 조합
	sprintf_s(buf, "%s", "OREQ");

	sData += std::to_string(tOREQData.nRemoteID) + ',';
	sData += std::to_string(nId) + ','; //tOREQData.nAMRID
	sData += std::to_string(tOREQData.nCMD) + ','; //88
	sData += std::to_string(tOREQData.nCMD_Value1) + ','; //0
	sData += std::to_string(tOREQData.nCMD_Value2) + ','; //2
	sData += tOREQData.sCMD_Value3 + ',';
	sData += tOREQData.sCMD_Value4 + ',';
	sData += tOREQData.nWorkID + ',';
	sData += tOREQData.nRequestTime;
	sData += "@@@@";

	int nDataSize = sData.length();

	sprintf(bufSize, "%04d", nDataSize + 8);

	memcpy_s(buf + 8, sizeof(buf) - 8, sData.c_str(), nDataSize);

	memcpy_s(buf + 4, sizeof(char) * 4, bufSize, sizeof(char) * 4);

	// AMR 던짐
	SendOREQToAmr(nId, buf, nDataSize + 8);
}

void SendPauseToAmr(int nId, OREQDATA& tOREQData)
{
	char buf[1024];
	char bufSize[5];
	std::string sData;
	memset(buf, 0, sizeof(buf));

	// 시작 위치 찾음
	int nSrc = -1;
	for (int i = 0; i < g_PamrNo; ++i)
	{
		if (g_Pamr[i]->m_stAMRStatus.nId == nId) // tOREQData.nAMRID
		{
			nSrc = g_Pamr[i]->m_stAMRStatus.nToNodeNo;
			break;
		}
	}

	if (nSrc == -1)
		return;

	//--------------------------------------------------
	tOREQData.nCMD = 88;// 30;
	tOREQData.nCMD_Value1 = 0;// 11;
	tOREQData.nCMD_Value2 = 2;// 4; //재시작
	//--------------------------------------------------

							   // 프로토콜 조합
	sprintf_s(buf, "%s", "OREQ");

	sData += std::to_string(tOREQData.nRemoteID) + ',';
	sData += std::to_string(nId) + ','; //tOREQData.nAMRID
	sData += std::to_string(tOREQData.nCMD) + ',';
	sData += std::to_string(tOREQData.nCMD_Value1) + ',';
	sData += std::to_string(tOREQData.nCMD_Value2) + ',';
	sData += tOREQData.sCMD_Value3 + ',';
	sData += tOREQData.sCMD_Value4 + ',';
	sData += tOREQData.nWorkID + ',';
	sData += tOREQData.nRequestTime;
	sData += "@@@@";

	int nDataSize = sData.length();

	sprintf(bufSize, "%04d", nDataSize + 8);

	memcpy_s(buf + 8, sizeof(buf) - 8, sData.c_str(), nDataSize);

	memcpy_s(buf + 4, sizeof(char) * 4, bufSize, sizeof(char) * 4);

	// AMR 던짐
	SendOREQToAmr(nId, buf, nDataSize + 8);
}

void CRAIClientMgr::OrderProcess(const char* pOrderData, size_t nSize)
{
	OREQDATA tOREQData;
	Packet2OREQDATA(pOrderData, tOREQData);

	int nRobotIdx = -1;
	for (int indx = 0; indx < g_PamrNo; indx++)
	{
		if (g_Pamr[indx]->m_stAMRStatus.nId == tOREQData.nAMRID)
		{
			nRobotIdx = indx;
			break;
		}
	}
	printf("Ordered Robot [Index : ID] = [%d:%d]\n", nRobotIdx, g_Pamr[nRobotIdx]->m_stAMRStatus.nId);
	
	if (nRobotIdx == -1) {
		printf("No matching Robot ID.\n");
		return;
	}

	CPAMR* pAMR = g_Pamr[nRobotIdx];
	pAMR->m_OREQData = tOREQData;
	
	printf("[Robot ID: %d] OREQ Packet data: %s\n", tOREQData.nAMRID, pOrderData);// ok

	printf("CMD value3: %s\n",  tOREQData.sCMD_Value3.c_str());
		
	const int CMD = 50;
	const int Val1 = 10;
	const int Val2_1 = 1;
	const int Val2_2 = 2;

	const int Val2_7 = 7;
	const int Val2_11 = 11;

	setLastOREQData(tOREQData);
	
	switch (tOREQData.nCMD)
	{
	case (OREQ_SQN) :
		
		if (tOREQData.nCMD_Value1 == Val1)
		{
			if ( tOREQData.nCMD_Value2 == Val2_1)
			{
				printf("Sequence work ordered.\n");
								
				if (strlen(tOREQData.sCMD_Value3.c_str()) > 5)
				{
					//SendOREQToAmr(tOREQData.nAMRID, pOrderData, (DWORD)nSize); //for virtual AMR only
				}

				//COperationMgr::GetInst()->InsertWorkList(tOREQData.sCMD_Value3);  //test

				
				//CPAMR* pAMR = g_Pamr[nRobotIndex];
				WorkList* work_list = COperationMgr::GetInst()->InsertWorkList(tOREQData.sCMD_Value3);  //test
				int m_nWorkListCount = COperationMgr::GetInst()->getWorkListSize();
				for (int idx = 0; idx < m_nWorkListCount; idx++)
				{
					memcpy(&(pAMR->work_list_file[idx]), &(work_list[idx]), sizeof(work_list[idx]));
					memcpy( &(g_Pamr[nRobotIdx]->work_list_file[idx]), &(work_list[idx]), sizeof(work_list[idx]));
					//g_Pamr[nRobotIndex]->work_list_file[idx] = work_list[idx];
					printf("Index [%d] %d, %d, %d\n", idx,
						pAMR->work_list_file[idx].nNo,
						pAMR->work_list_file[idx].nWork,
						pAMR->work_list_file[idx].nValue);
				}
				pAMR->m_bSeqwork = true;
				pAMR->m_bSeqclr = false;
				pAMR->bFirstRun = true;
				
				
				//to-do
				//pAMR->strPath = pAMR->work_list_file[idx].strPath;


				return;
			}
			else if (tOREQData.nCMD_Value2 == Val2_2) //target 자율주행
			{
				SendOREQDESTRUNToAmr1(tOREQData.nAMRID, tOREQData);
			}
			else if (tOREQData.nCMD_Value2 == Val2_7)
			{

				/*int nRobotIndex = -1;

				for (int i = 0; i < g_PamrNo; i++)
				{
					if (g_robotContainer->_robots[i].Id == tOREQData.nAMRID)
					{
						nRobotIndex = i;
						break;
					}
				}
				if (nRobotIndex == -1)
				{
					printf("Error: Can not find correct Robot IP !\n");
					return;
				}
				CPAMR* pAMR = g_Pamr[nRobotIndex];*/
								
				if (g_Pamr[nRobotIdx]->m_stAMRStatus.nId != tOREQData.nAMRID)
				{
					printf("Different Robot ID: %d , %d \n", g_Pamr[nRobotIdx]->m_stAMRStatus.nId, tOREQData.nAMRID);
				}

				pAMR->m_bPickclr = true;
				pAMR->m_bPickstatus = false;
				pAMR->m_bCharge = false;

				//SendOREQToAmr(tOREQData.nAMRID, pOrderData, (DWORD)nSize); //for virtual AMR only

			}
			else if (tOREQData.nCMD_Value2 == Val2_11)
			{
				//SendOREQToAmr(tOREQData.nAMRID, pOrderData, (DWORD)nSize); //for virtual AMR only
				SendResumeToAmr(tOREQData.nAMRID, tOREQData);
			}
			else
			{
				/*tOREQData.nCMD = 30;
				tOREQData.nCMD_Value1 = 11;
				tOREQData.nCMD_Value2 += 1;*/
				//SendOREQDESTRUNToAmr2(tOREQData.nAMRID, tOREQData);
				//SendOREQToAmr(tOREQData.nAMRID, pOrderData, (DWORD)nSize);
			}

		}
		break;

	case (OREQ_CMD) :

		if ((tOREQData.nCMD_Value1 == OREQ_CMD_V1_NODECON) |
			(tOREQData.nCMD_Value1 == OREQ_CMD_V1_NODECON_AMR)) //	&& (tOREQData.nCMD_Value2 == OREQ_CMD_V2_NC_DIJKSTRA))
		{
			if ((tOREQData.nCMD_Value2 == 1) | (tOREQData.nCMD_Value2 == 2))
			{
				SendOREQDESTRUNToAmr(tOREQData.nAMRID, tOREQData);
			}
			else
			{
				SendOREQToAmr(tOREQData.nAMRID, pOrderData, (DWORD)nSize);
			}
			//if (strlen(tOREQData.sCMD_Value3.c_str()) > 1) // for path list  2018.0905
			//{
			//	SendOREQToAmr(tOREQData.nAMRID, pOrderData, (DWORD)nSize);
			//}
			//else // for target node only
			//{
			//	SendOREQDESTRUNToAmr(tOREQData.nAMRID, tOREQData); //ok
			//}

		}
		else if (tOREQData.nCMD_Value1 == 70 && tOREQData.nCMD_Value2 ==1)  // go to charge
		{
			//-------------------------------------------------------------------------------------------------
			//int nRobotIndex = -1;
			//for (int i = 0; i < g_PamrNo; i++)
			//{
			//	if (g_robotContainer->_robots[i].Id == tOREQData.nAMRID)
			//	{
			//		nRobotIndex = i;
			//		break;
			//	}
			//}
			//if (nRobotIndex == -1)
			//{
			//	printf("Error: Can not match correct Robot IP !\n");
			//	return;
			//}
			//CPAMR* pAMR = g_Pamr[nRobotIndex];
			//--------------------------------------------------------------------------------------------------
			int nSrc = g_Pamr[nRobotIdx]->m_stAMRStatus.nToNodeNo;
			int charge_station = 301; //301;  //306
			if (g_Pamr[nRobotIdx]->m_stAMRStatus.nId == 1)
			{
				charge_station = 301;
			}
			else
			{
				charge_station = 306;
			}
			int nDst = charge_station;
			string sPath = MakeOREQPath(nRobotIdx,nSrc, nDst);
			SendOREQDESTRUNToAmr_CS(tOREQData.nAMRID, tOREQData, sPath);
		}
		else
		{
			SendOREQToAmr(tOREQData.nAMRID, pOrderData, (DWORD)nSize);
		}
		
		break;
	case (OREQ_MON):

		break;
	default:

		break;
	}

	/*if ((tOREQData.nCMD == CMD)
		&& (tOREQData.nCMD_Value1 == Val1)
		&& (tOREQData.nCMD_Value2 == Val2))
	{
		printf("Sequence work ordered.\n");
		COperationMgr::GetInst()->InsertWorkList(tOREQData.sCMD_Value3);
		bSeqwork = true;
		return;
	}*/


	/*
	if ( (tOREQData.nCMD_Value1 == OREQ_CMD_V1_NODECON) | 
		(tOREQData.nCMD_Value1 == OREQ_CMD_V1_NODECON_AMR) ) //	&& (tOREQData.nCMD_Value2 == OREQ_CMD_V2_NC_DIJKSTRA))
	{
		SendOREQDESTRUNToAmr(tOREQData.nAMRID, tOREQData);
		//if (strlen(tOREQData.sCMD_Value3.c_str()) > 1) // for path list  2018.0905
		//{
		//	SendOREQToAmr(tOREQData.nAMRID, pOrderData, (DWORD)nSize);
		//}
		//else // for target node only
		//{
		//	SendOREQDESTRUNToAmr(tOREQData.nAMRID, tOREQData); //ok
		//}
		
	}
	else
	{
		SendOREQToAmr(tOREQData.nAMRID, pOrderData, (DWORD)nSize);
	}

	*/


	/*switch (tOREQData.nCMD_Value1)
	{
	case OREQ_CMD_V1_NODECON:
		switch (tOREQData.nCMD_Value2)
		{
		case OREQ_CMD_V2_NC_DIJKSTRA:	SendOREQDESTRUNToAmr(tOREQData.nAMRID, tOREQData);		break;
		case OREQ_CMD_V2_NC_ARRNODE:	SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		}
		break;
	case OREQ_CMD_V1_MANUAL:
		switch (tOREQData.nCMD_Value2)
		{
		case OREQ_CMD_V2_MN_STANDBY:			SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		case OREQ_CMD_V2_MN_FOWARD:			SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		case OREQ_CMD_V2_MN_BACKWARD:		SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		case OREQ_CMD_V2_MN_LEFTTURN:		SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		case OREQ_CMD_V2_MN_RIGHTTURN:		SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		case OREQ_CMD_V2_MN_STOP:				SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		case OREQ_CMD_V2_MN_ANG_VEL:		break;
		case OREQ_CMD_V2_MN_ROTATE_CCW:	SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		case OREQ_CMD_V2_MN_ROTATE_CW:		SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		case OREQ_CMD_V2_MN_STRAIGHT:			break;
		}
		break;
	case OREQ_CMD_V1_DOCKING:
		switch (tOREQData.nCMD_Value2)
		{
		case OREQ_CMD_V2_DOCKING:	break;
		}
		break;
	case OREQ_CMD_V1_LIFTING:
		switch (tOREQData.nCMD_Value2)
		{
		case OREQ_CMD_V2_LIFTING0:			SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		case OREQ_CMD_V2_LIFTING1:			SendOREQToAmr(tOREQData.nAMRID, pOrderData, nSize);	break;
		}
		break;
	case OREQ_CMD_V1_POSCORR:
		switch (tOREQData.nCMD_Value2)
		{
		case OREQ_CMD_V2_POSCORR:	break;
		}
		break;
	case OREQ_CMD_V1_CHARGE:
		switch (tOREQData.nCMD_Value2)
		{
		case OREQ_CMD_V2_CHARGE0:	break;
		case OREQ_CMD_V2_CHARGE1:	break;
		}
		break;
	}*/
}

#if 0
int main()
{
	boost::asio::io_service io_service;

	auto endpoint = boost::asio::ip::tcp::endpoint(
		boost::asio::ip::address::from_string("127.0.0.1"),
		PORT_NUMBER);

	CRAIClientMgr Cliet(io_service);
	Cliet.Connect(endpoint);

	boost::thread thread(boost::bind(&boost::asio::io_service::run, &io_service));


	char szInputMessage[MAX_MESSAGE_LEN * 2] = { 0, };

	while (std::cin.getline(szInputMessage, MAX_MESSAGE_LEN))
	{
		if (strnlen_s(szInputMessage, MAX_MESSAGE_LEN) == 0)
		{
			break;
		}

		if (Cliet.IsConnecting() == false)
		{
			std::cout << "connecting to server.." << std::endl;
			continue;
		}

		if (Cliet.IsLogin() == false)
		{
			PKT_REQ_IN SendPkt;
			SendPkt.Init();
			strncpy_s(SendPkt.szName, MAX_NAME_LEN, szInputMessage, MAX_NAME_LEN - 1);

			Cliet.PostSend(false, SendPkt.nSize, (char*)&SendPkt);
		}
		else
		{
			PKT_REQ_CHAT SendPkt;
			SendPkt.Init();
			strncpy_s(SendPkt.szMessage, MAX_MESSAGE_LEN, szInputMessage, MAX_MESSAGE_LEN - 1);

			Cliet.PostSend(false, SendPkt.nSize, (char*)&SendPkt);
		}
	}

	io_service.stop();

	Cliet.Close();

	thread.join();

	std::cout << "client end" << std::endl;

	return 0;
};
#endif
