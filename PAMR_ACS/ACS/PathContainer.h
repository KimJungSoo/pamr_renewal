#pragma once

#include <valarray>
#include "Dijkstras.h"

using namespace std;

extern int g_nNodeCnt;
extern int g_nEdgeCnt;
extern int g_graph[VERTICES_MAX][VERTICES_MAX];
extern int g_graph_modified[VERTICES_MAX][VERTICES_MAX];
extern int b_graph[VERTICES_MAX][VERTICES_MAX];
extern int b_graph_modified[VERTICES_MAX][VERTICES_MAX];


// 배열형 way point( 0 - x위치, 1 - y위치, 2 - 방위(th), 3 - 속도 )
typedef	valarray<double> vaWayPoint;	

struct sVertex
{
	int id;					// 경유점 ID

	double x, y;			// 위치정보
	double th;				// 방향정보

	int obs;				// 장애물 감지 범위 - 0 : 사용안함, 1~n : 정의된 범위
	int max_v;				// 경유점을 지나가는 최고 속도
	int max_a;				// 경유점을 지나가는 최고 각속도
	int door;				// Door 구간 설정(0 : 일반지역, 1 : Door Area)
	int reflector_mode;		// Reflector 구간 설정(0 : 일반지역, 1 : Reflector Stop Area)
	int reflector_useSide;	// Reflector 구간 사용 구간(0 : 전체사용, 1 : 왼쪽만 사용, 2 : 오른쪽만 사용)

	string name;			// 경유점 이름

	sVertex(double x_, double y_, int max_v_, const char *name_)
		: x(x_), y(y_), max_v(max_v_), name(name_)
		, came_from(-1) , g_score(0.), h_score(0.), f_score(0.)
		, type(0) { }

	sVertex(int id_, double x_, double y_, double th_, int obs_, int max_v_, int max_a_, int door_, int reflector_mode_, int reflector_useSide_, const char *name_)
		: id(id_), x(x_), y(y_), th(th_), obs(obs_), max_v(max_v_), max_a(max_a_), door(door_), reflector_mode(reflector_mode_), reflector_useSide(reflector_useSide_), name(name_)
		, came_from(-1) , g_score(0.), h_score(0.), f_score(0.)
		, type(0) { }

	sVertex(int id_, double x_, double y_, double th_, int obs_, int max_v_, int max_a_, int door_, int reflector_mode_, int reflector_useSide_, const char *name_, int type_)
		: id(id_), x(x_), y(y_), th(th_), obs(obs_), max_v(max_v_), max_a(max_a_), door(door_), reflector_mode(reflector_mode_), reflector_useSide(reflector_useSide_), name(name_)
		, came_from(-1) , g_score(0.), h_score(0.), f_score(0.)
		, type(type_) { }

public:
	// 아래 변수들은 A* 알고리즘에서 사용
	int came_from;			// The map of navigated nodes.
	double g_score;			// Cost from start along best known path.
	double h_score;			// Estimated heuristic cost from this to goal
	double f_score;			// Estimated total cost from start to goal

public:
	int type;				// 경유점 type
};

struct sEdge
{
	int s, e;	// path가 연결하는 두 city의 index

	sEdge(int s_, int e_) 
		: s(s_), e(e_) { }
};


class CPathContainer
{
public:
	CPathContainer(const char *fileName);
	CPathContainer(const char *fileName, const char *fileName_Magnet);
	~CPathContainer();

	void Clear();
	int LoadPathFile(const char *fileName);
	int LoadPathFile(const char *fileName, const char *fileName_Magnet);

	vector<sVertex> _vertexList;
	//vector<int> _nearNodes;
	//const int kMaxNode = 2048;

	char _nodePoints[2048];
	char _nodePoints_modified[2048];

	char _edgeProps[1024];
	int _bothEnds[2048] = { 0, };

public:
	int FindVertexName(const char *name);
	int FindVertexID(int name);
	int FindVertexNum(int name);
	int FindNearVertex(double x, double y, double *distance = NULL, bool haveName = false);
	vector<int> FindNearVertexNames(int toNode, double dist_threshold);


	vector<sEdge> _edgeList;
	vector<vaWayPoint> ConvertPathWp(vector<int> &path);
	

private:
	int SplitString(CUIntArray & pArrArg, char * pData, int nDataSize);

public:
	void NewPath();
	void AttachPath(double x, double y, double max_v, char *name);
	void AttachPath(int id, double x, double y, double th, int obs, int max_v, int max_a, int door, int reflector_mode, int reflector_useSide, char *name);
	void AttachPath(int id, double x, double y, double th, int obs, int max_v, int max_a, int door, int reflector_mode, int reflector_useSide, char *name, int type);

	int FindVertex(double x, double y);



private:
	int _lastVertexID;
};
