#include "StdAfx.h"
#include "PathFollow.h"
#include "ConstParameters.h"


CPathFollow::CPathFollow()
{
	_wp = NULL;

	Reset();

	_obstacleDistance = 10;
}

CPathFollow::~CPathFollow()
{
}

void CPathFollow::MoveToNextWaypoint(double dt)
{
	// 로봇이 경로를 따라 이동하면서 로봇이 따라가야 할 경로상의 위치와 남은 거리를 계산한다.
	_curDist -= _ffVel.v * dt;
	if(_curDist < 0)
		_curDist = 0;

	// 남은 거리보다 짧은 거리의 최대 거리를 가지는 waypoint를 선택한다.
	while(_curIndex < _wpSize)
	{
		if(_curDist < _wp[_curIndex].dist)
		{
			++_curIndex;
			_curToNodeIndex = _wp[_curIndex].toNodeIndex;
			_curToNodeId = _wp[_curIndex].toNodeId;
		}
		else
		{
			break;
		}
	}
}

CPosture CPathFollow::GetTargetPos()
{
	// 로봇이 추종해야 할 위치점을 선형보간으로 계산한다.
	// ## 주의:  MoveToNextWaypoint() 함수에 의해 _curDist가 먼저 계산되어야 한다.
	if(0 < _curIndex)
	{
		const CPosture &x1 = _wp[_curIndex - 1].pos;
		const CPosture &x2 = _wp[_curIndex + 0].pos;
		double d1 = _wp[_curIndex - 1].dist;
		double d2 = _wp[_curIndex - 0].dist;

		double ratio = (d1 - _curDist) / (d1 - d2);
	
		TRACE("curindex, %d, d1 : %f , d2: %f, ratio: %f\n", _curIndex, d1, d2, ratio);

		return x1 + (x2 - x1) * ratio;
	}
	else
	{
		return _wp[0].pos;
	}
}

double CPathFollow::DistanceToTarget(const CPosture &robotPos) 
{
	// 목적지 까지 남은 step이 3 step 이하일 때
	if(_wpSize - 1 - 3 <= _curIndex)
	{	
		// 로봇에서 목적지로 향하는 벡터의 각도 계산
		CPosture d = _wp[_curIndex].pos - robotPos;
		double length = sqrt(d.x * d.x + d.y * d.y);
		double angle = atan2(d.y, d.x);

		// 현재 경유지에서 남은 거리와 로봇에서 경유점으로 향하는 벡터에서 경로에 수평성분의 거리를 더한다.
		double delta =  DeltaRad(_wp[_curIndex].pos.th, angle);

		return _wp[_curIndex].dist + length * cos(delta);
	}
	else
	{
		return _wp[_curIndex].dist;
	}
}

sVelocity2WD CPathFollow::FeedforwardValueToZero(double dt)
{
	// Feed-forward 선속도와 각속도 모두 deceleration으로 감속하여 정지하도록 한다.
	double v = 0;
	double w = 0;

	v = TrapezoidalProfile(_ffVel.v, v, ROBOT_ACCELERATION, ROBOT_DECELERATION, dt);
	w = TrapezoidalProfile(_ffVel.w, w, ROBOT_ACCELERATION, ROBOT_DECELERATION, dt);

	// TRACE("FeedforwardValueToZero v = %f w = %f\n", v, w);

	return sVelocity2WD (v, w);
}

sVelocity2WD CPathFollow::FeedforwardVelocity(double vel_limit, double dt)
{
	// ### 로봇의 Feed-forward 선속도 계산
	// 먼저 경로상에 설정된 속도 설정
	double v = _wp[_curIndex].vel.v;
	// 가속할 때의 속도로 제한
	v = min(v, _desiredVel.v + ROBOT_ACCELERATION * dt);
	// 장애물에 의한 속도로 제한
	v = min(v, vel_limit);

	// ### 로봇의 Feed-forward 각속도 계산
	// 선속도가 원래 경로상의 속도에서 얼마나 낮은지 비율을 계산한다.
	double ratio = v / _wp[_curIndex].vel.v;
	if(ratio > 1)
		ratio = 1;	// ratio는 1보다 커질 수 없다.

	// 선속도가 제한된 양만큼 각속도도 제한한다.
	double w = ratio * _wp[_curIndex].vel.w;

	// TRACE("FeedforwardVelocity v = %f w = %f\n", v, w);

	return sVelocity2WD(v, w);
}

sVelocity2WD CPathFollow::NonlinearFeedbackControl(const CPosture &error, const sVelocity2WD &ffVel)
{
	double zeta = 0.7;	// zeta는 0 ~ 1 사이의 수
	double b = 1.5;		// b는 0보다 큰 수

	double k1 = 2*zeta*sqrt(ffVel.w*ffVel.w + b*ffVel.v*ffVel.v);
	double k2 = b*fabs(ffVel.v);
	double k3 = k1;

	double v = k1*error.x;
	// error.th가 0근처값이면 sin(error.th)/error.th는 1이 된다.
	double w = (-EPSILON < error.th && error.th < EPSILON) ?
		k2*ffVel.v*error.y + k3*error.th :								
		k2*ffVel.v*sin(error.th)/error.th*error.y + k3*error.th ;

	// Feedback 속도는 정상적인 최대 속도의 1/4을 넘지 않도록 한다.
	v = BOUND (v, -ROBOT_MAX_VELOCITY/4,   ROBOT_MAX_VELOCITY/4);
	w = BOUND (w, -ROBOT_MAX_ANGULARVEL/4, ROBOT_MAX_ANGULARVEL/4);

	// TRACE("NonlinearFeedbackControl v = %f w = %f\n", v, w);

	return sVelocity2WD (v, w);
}

sVelocity2WD CPathFollow::ReturnToPath(const CPosture &error, double vel_limit, double dt)
{
	// 목적 위치로 이동한다.
	double k1 = 1.0;
	double k2 = 1.0;

	double rho   = error.Length();
	double alpha = atan2(error.y, error.x);

	double v = k1*rho;
	double w = k2*alpha;

	// 추종 각도가 크다면 로봇의 전진속도를 줄여준다.
	double gamma = exp(-(alpha*alpha)/(2*(M_PI/12)*(M_PI/12)));		// 정규분포함수
	v = min(v*gamma, vel_limit);

	// 경로 복귀중일 때는 정상적인 최대 속도의 1/2을 넘지 않도록 한다.
	v = BOUND (v, -ROBOT_MAX_VELOCITY/2,   ROBOT_MAX_VELOCITY/2);
	w = BOUND (w, -ROBOT_MAX_ANGULARVEL/2, ROBOT_MAX_ANGULARVEL/2);

	v = TrapezoidalProfile(_fbVel.v, v, ROBOT_ACCELERATION, ROBOT_DECELERATION, dt);
	w = TrapezoidalProfile(_fbVel.w, w, ROBOT_ACCELERATION, ROBOT_DECELERATION, dt);

	// TRACE("ReturnToPath v = %f w = %f\n", v, w);

	return sVelocity2WD (v, w);
}

sVelocity2WD CPathFollow::AlignToPath(const CPosture &error, double dt)
{
	// 목적 방향으로 정렬한다.
	double k1 = 0.0;
	double k2 = 1.0;

	double v = k1*0;
	double w = k2*error.th;

	// 경로 복귀중일 때는 정상적인 최대 속도의 1/2을 넘지 않도록 한다.
	w = BOUND (w, -ROBOT_MAX_ANGULARVEL/2, ROBOT_MAX_ANGULARVEL/2);

	v = TrapezoidalProfile(_fbVel.v, v, ROBOT_ACCELERATION, ROBOT_DECELERATION, dt);
	w = TrapezoidalProfile(_fbVel.w, w, ROBOT_ACCELERATION, ROBOT_DECELERATION, dt);

	// TRACE("AlignToPath v = %f w = %f\n", v, w);
	return sVelocity2WD (0, w);
}

bool CPathFollow::FollowPath(sVelocity2WD *desiredVel, const CPosture &robotPos, double dt)
{
	// 로봇이 현재 추종해야 할 경로상의 위치를 계산한다.
	MoveToNextWaypoint(dt);

	// 로봇이 목적지까지 남은 거리 계산. 
	// 목적지를 지나게 되면 0보다 작은 수 리턴
	double remain = DistanceToTarget(robotPos);
	if(remain <= 0) {
		_ffVel.Zero();
		_fbVel.Zero();
		_desiredVel.Zero();
		return false;
	}

	// 로봇의 전방에 있는 장애물과의 최소 거리를 측정하여 
	// 장애물과 충돌하지 않도록 속도를 줄여준다.
	double obstacleDistance = DetectObstacle ();
	// 현재속도(_desiredVel.v)와 정지 거리(obstacleDistance)에 대해서 
	// 얼마의 감속도(decel)로 정지해야 할지 계산한 후
	// 현재 로봇의 속도를 decel*dt 만큼 줄여준다.
	double velLimitByObstacle = ROBOT_MAX_VELOCITY;
	if(obstacleDistance > 0) {
		double decel = _desiredVel.v*_desiredVel.v/(2*obstacleDistance);
		if(decel > ROBOT_DECELERATION) {
			velLimitByObstacle = _desiredVel.v - decel*dt;
			if(velLimitByObstacle < 0) velLimitByObstacle = 0;
		}
	}
	else {
		velLimitByObstacle = 0;
	}

	CPosture targetPos = GetTargetPos();

	// 로봇좌표계 기준으로 목적 위치와 로봇 위치간 오차 계산
	CPosture error = targetPos - robotPos;
	error.Rotate(-robotPos.th);

	sVelocity2WD ffVel, fbVel;
	
	if(_pfMode == 0) {
PF_MODE0:
		if(error.Length3 () > MAX_TRACE_PATH_ERROR) { _pfMode = 1; goto PF_MODE1; }

		// 경로를 추종할 때의 Feed-forward 선속도와 각속도 계산
		ffVel = FeedforwardVelocity(velLimitByObstacle, dt);
		// 로봇이 경로상에서 얼마나 벗어났는지에 따라 Feed-back 선속도와 각속도 계산
		// 목적지의 위치와 방위(x, y, theta)를 추종하는 follower
		fbVel = NonlinearFeedbackControl(error, ffVel);
	}
	else if(_pfMode == 1) {
PF_MODE1:
		if(error.Length() < MAX_TRACE_PATH_ERROR/5) { _pfMode = 2; goto PF_MODE2; }

		// 경로를 많이 벗어난 경우, 먼저 경로상의 waypoint로 복귀해야한다.
		ffVel = FeedforwardValueToZero(dt);
		fbVel = ReturnToPath(error, velLimitByObstacle, dt);
	}
	else if(_pfMode == 2) {
PF_MODE2:
		if(fabs(error.th) < MAX_TRACE_PATH_ERROR/20) { _pfMode = 0; goto PF_MODE0; }

		// 경로의 방향과 로봇의 방향을 일치시킨다.
		ffVel = FeedforwardValueToZero(dt);
		fbVel = AlignToPath(error, dt);
	}

	_ffVel.v = ffVel.v*cos(error.th);
	_ffVel.w = ffVel.w;

	_fbVel.v = fbVel.v;
	_fbVel.w = fbVel.w;

	_desiredVel.v = BOUND (_ffVel.v + _fbVel.v, -velLimitByObstacle, velLimitByObstacle);
	_desiredVel.w = BOUND (_ffVel.w + _fbVel.w, -ROBOT_MAX_ANGULARVEL, ROBOT_MAX_ANGULARVEL);

	desiredVel->v = _desiredVel.v;
	desiredVel->w = _desiredVel.w;
	return true;
}

bool CPathFollow::Turning(sVelocity2WD *desiredVel, const CPosture &robotPos, double targetHeading, double dt)
{
	// 목적 방향과 로봇 방향간 오차 계산
	double error_th = DeltaRad(targetHeading, robotPos.th);
	if(fabs(error_th) < MAX_TRACE_PATH_ERROR/100) return false;

	CPosture error (0, 0, error_th);

	sVelocity2WD fbVel = AlignToPath(error, dt);

	_ffVel.v = 0;
	_ffVel.w = 0;

	_fbVel.v = 0;
	_fbVel.w = fbVel.w;

	_desiredVel.w = BOUND (_fbVel.w, -ROBOT_MAX_ANGULARVEL, ROBOT_MAX_ANGULARVEL);

	desiredVel->v = _desiredVel.v;
	desiredVel->w = _desiredVel.w;
	return true;
}

double CPathFollow::TrapezoidalProfile(double velCurrent, double velDesired, double acceleration, double deceleration, double dt)
{
	if(velCurrent == velDesired);
	else if(velCurrent > 0) {
		if(velDesired > velCurrent) {
			velCurrent += acceleration*dt;
			if(velDesired < velCurrent) velCurrent = velDesired; 
		}
		else {
			velCurrent -= deceleration*dt;
			if(velDesired > velCurrent) velCurrent = velDesired;
		}
	}
	else if(velCurrent < 0) {
		if(velDesired < velCurrent) {
			velCurrent -= acceleration*dt;
			if(velDesired > velCurrent) velCurrent = velDesired;
		}
		else {
			velCurrent += deceleration*dt;
			if(velDesired < velCurrent) velCurrent = velDesired;
		}
	}
	else {
		if(velDesired > velCurrent) {
			velCurrent += acceleration*dt;
			if(velDesired < velCurrent) velCurrent = velDesired; 
		}
		else {
			velCurrent -= acceleration*dt;
			if(velDesired > velCurrent) velCurrent = velDesired;
		}
	}
	return velCurrent;
}
	