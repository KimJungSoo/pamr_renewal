// Packet.cpp: implementation of the CPacket class.
//
//////////////////////////////////////////////////////////////////////
#pragma once
#include "stdafx.h"
#include "Packet.h"
#include "AppInstance.h"
//#include "PamrServerDlg.h"
#include "PAMRDlg.h"


//////////////////////////////////////////////////////////////////////
// Construction / Destruction
//////////////////////////////////////////////////////////////////////


//void Packet2AMRStatus(char* pData, AMRStatus& tAMRStat)
//{

	//// TODO : 프로토콜 변경 시킨거 전부 적용 해서 넣어야 함

	//// pData : 헤더 포함 패킷 전체
	//// [0]~[3] : Header
	//// [4]~[7] : Length
	//// [8]~ : amr 데이터
	//// ',' 구분문자
	//// ['@','@','@','@'] : 종료 문자
	//char bufVal[BYTE_MAX] = {'\0', };

	//int nPacketLen = 0;
	//memset(bufVal, 0, sizeof(bufVal));
	//memcpy(&bufVal, &pData[4], 4);

	//nPacketLen = atoi(bufVal);

	//// 시작 위치 8
	//// idxBuf : 값 하나의 시작 위치
	//int idxBuf = 8;
	//int idxSet = 0;


	//while (true)
	//{
	//	// pData 한계 접근 제한
	//	if (idxBuf >= nPacketLen)
	//		break;

	//	// 종료자 만나면 끝!
	//	if (pData[idxBuf] == '@')
	//		break;

	//	// 한 데이터 값의 길이
	//	int idxLen = 0;

	//	// 종료자나 구분문자 찾음
	//	while (idxBuf + idxLen < nPacketLen)
	//	{
	//		char temp = pData[idxBuf + idxLen];
	//		if (temp == ',' || temp == '@')
	//			break;
	//		++idxLen;
	//	}

	//	// bufVal를 초기화 하고 값을 넣어줌
	//	memset(bufVal, 0, sizeof(bufVal));
	//	memcpy_s(bufVal, sizeof(bufVal), &pData[idxBuf], sizeof(char)*idxLen);

	//	// AMRStatus에 값을 넣어줌
	//	tAMRStat.SetData(idxSet, bufVal);
	//	++idxSet;
	//	
	//	// 다음 위치
	//	idxBuf = idxBuf + idxLen + 1;
	//}
//}

//void Packet2HWStatus(char* pData, HWStatus& tHWStat) 
//{
	//// TODO : 프로토콜 변경 시킨거 전부 적용 해서 넣어야 함

	//// pData : 헤더 포함 패킷 전체
	//// [0]~[3] : Header
	//// [4]~[7] : Length
	//// [8]~ : amr 데이터
	//// ',' 구분문자
	//// ['@','@','@','@'] : 종료 문자
	//char bufVal[BYTE_MAX] = { '\0', };

	//int nPacketLen = 0;
	//memset(bufVal, 0, sizeof(bufVal));
	//memcpy(&bufVal, &pData[4], 4);

	//nPacketLen = atoi(bufVal);

	//// 시작 위치 8
	//// idxBuf : 값 하나의 시작 위치
	//int idxBuf = 8;
	//int idxSet = 0;

	//while (true)
	//{
	//	// pData 한계 접근 제한
	//	if (idxBuf >= nPacketLen)
	//		break;

	//	// 종료자 만나면 끝!
	//	if (pData[idxBuf] == '@')
	//		break;

	//	// 한 데이터 값의 길이
	//	int idxLen = 0;

	//	// 종료자나 구분문자 찾음
	//	while (idxBuf + idxLen < nPacketLen)
	//	{
	//		char temp = pData[idxBuf + idxLen];
	//		if (temp == ',' || temp == '@')
	//			break;
	//		++idxLen;
	//	}

	//	// bufVal를 초기화 하고 값을 넣어줌
	//	memset(bufVal, 0, sizeof(bufVal));
	//	memcpy_s(bufVal, sizeof(bufVal), &pData[idxBuf], sizeof(char)*idxLen);


	//	tHWStat.SetData(idxSet, bufVal);
	//	++idxSet;

	//	// 다음 위치
	//	idxBuf = idxBuf + idxLen + 1;
	//}
//}

//AMRStatus* CPacket::EncodePacket(char* pData, int index)
//{

	//if (pData == nullptr)
	//	return nullptr;
	//AMRStatus stAMRStatus;

	//Packet2AMRStatus(pData, stAMRStatus);

	////-- 데이터 판별...
	////m_PacketData.stAMRStatus = data->stAMRStatus;

	//int nIndex = -1;
	//for(int i = 0; i < g_PamrNo; ++i)
	//{
	//	if(g_robotContainer->_robots[i].Id == stAMRStatus.nId)
	//	{
	//		nIndex = i;

	//		break;
	//	}
	//}

	//if(nIndex != -1)
	//{
	//	g_Pamr[nIndex]->m_ctmRcvTime = CTime::GetCurrentTime();
	//	g_Pamr[nIndex]->m_bConnected = true;
	//	g_Pamr[nIndex]->m_stAMRStatus = stAMRStatus;
	//	//memcpy(&(g_Pamr[nIndex]->m_stAMRStatus), &stAMRStatus, sizeof(stAMRStatus));
	//	//if((g_Pamr[nIndex]->m_stAMRStatus.nStatus == STATUS_READY) && (g_Pamr[nIndex]->m_stAMRStatus.nStatusValue == WORK_READY) && (g_Pamr[nIndex]->m_stAMRStatus.nFromNodeNo == 0) && (g_Pamr[nIndex]->m_stAMRStatus.nToNodeNo == 0))
	//	//{
	//	//	g_Pamr[nIndex]->_mobility._posture.x = g_robotContainer->_robots[nIndex].x;		// mm
	//	//	g_Pamr[nIndex]->_mobility._posture.y = g_robotContainer->_robots[nIndex].y;		// mm
	//	//	g_Pamr[nIndex]->_mobility._posture.th = g_robotContainer->_robots[nIndex].th * _DEG2RAD;	// Degree => Radian
	//	//}
	//	//else
	//	{
	//		g_Pamr[nIndex]->_mobility._posture.x = stAMRStatus.nPos_X / 1000.;		// mm => m
	//		g_Pamr[nIndex]->_mobility._posture.y = stAMRStatus.nPos_Y / 1000.;		// mm => m

	//		//pjs 2018.05.23 AMR radian으로 설정...
	//		//g_Pamr[nIndex]->_mobility._posture.th = m_PacketData.stAMRStatus.nPos_Th / 100. * _DEG2RAD;		// radian * 100을 받아서 변환해줌
	//		g_Pamr[nIndex]->_mobility._posture.th = stAMRStatus.nPos_Th / 100.;		// radian * 100을 받아서 변환해줌
	//		//<<--
	//	}
	//	return &(g_Pamr[nIndex]->m_stAMRStatus);
	//}

//	return nullptr;
//}

//HWStatus*  CPacket::EncodeHWPacket(char* pData, int index)
//{
	//if (pData == nullptr)
	//	return nullptr;
	//HWStatus stHWStatus;

	//Packet2HWStatus(pData, stHWStatus);

	////-- 데이터 판별...
	////m_PacketData.stAMRStatus = data->stAMRStatus;

	//int nIndex = -1;
	//for (int i = 0; i < g_PamrNo; ++i)
	//{
	//	if (g_robotContainer->_robots[i].Id == stHWStatus.nId)
	//	{
	//		nIndex = i;

	//		break;
	//	}
	//}

	//if (nIndex != -1)
	//{
	//	
	//	return &(g_Pamr[nIndex]->m_stHWStatus);
	//}

//	return nullptr;
//}

CPacket::CPacket()
{
}

CPacket::~CPacket()
{
}
