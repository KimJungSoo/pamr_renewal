#pragma once

//#include <opencv/cv.h>
#include "opencv2\opencv.hpp"

#include "mm.h"

class CGlobalMap {
public:
	CGlobalMap(const char *imageFileName, double pixelWidth, int pixelOriginX, int pixelOriginY);
	~CGlobalMap();

	bool GetPixel (double x, double y, unsigned char pixel[3]);
	char *pGetPixel (double x, double y);
	float GetWallProb (double x, double y, float *angle);
	void PaintMap(HDC hDC, int sx, int sy, double scale);

	inline IplImage *GetMapImage () { return _imgMap; }
	inline double GetMapWidth()  { return _imgMap->width*_pixelWidth; }
	inline double GetMapHeight() { return _imgMap->height*_pixelWidth; }

public:
	// 한 픽셀의 미터단위 크기:
	// 만일 pixelWidth가 0.01이라면 한 픽셀의 가로 세로 크기가 0,01m가 된다는 것이다.
	double _pixelWidth;
	int _pixelOriginX;
	int _pixelOriginY;

private:
	CRITICAL_SECTION m_lock;
	// 미터 단위를 픽셀 단위로 변경한다.
	inline int M2CUx(double x)	{ return INTEGER(x/_pixelWidth) + _pixelOriginX; }
	inline int M2CUy(double y)	{ return INTEGER(y/_pixelWidth) + _pixelOriginY; }
	// 픽셀 단위를 미터 단위로 변경한다.
	inline double CU2Mx(int x)	{ return (x - _pixelOriginX)*_pixelWidth; }
	inline double CU2My(int y)	{ return (y - _pixelOriginY)*_pixelWidth; }

private:
	// bitmap 파일에서 읽어들이 이미지가 그대로 저장된다.
	IplImage *_imgMap;
	// imgMap과 크기가 같으며 imgWall은 벽의 확률분포를 표시한다.
	IplImage *_imgWall;
	// imgMap과 크기가 같으며 벽면의 노멀 벡터의 방향을 표시한다.
	// 왼쪽:0, 오른쪽: 180, 위쪽: 90, 아래쪽: 270
	IplImage *_imgAngle;

	void ExtractWall ();
	void MakeWallProb ();
};
 
