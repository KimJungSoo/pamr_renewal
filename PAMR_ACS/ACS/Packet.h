// Packet.h: interface for the CPacket class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PACKET_H__643EAE08_3798_43A6_B1C9_9BC6AAF6D0B4__INCLUDED_)
#define AFX_PACKET_H__643EAE08_3798_43A6_B1C9_9BC6AAF6D0B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define AMR_SELF	0
#define SERVER		1000

//#pragma pack(1)		// 바이트 정렬을 최소 1바이트로 설정
//#pragma pack()		// 바이트 정렬을 옵션에 설정된 디폴트로 복구
//typedef struct _tag_AMRStatus
//{	
//	int nId;				// AMR Id
//	int nCtrlOwnership;		// AMR 제어권(0 : AMR Self, 1000 : Server)
//	int nOrderClientId;		// 조작하는 대상의 Id, 0 : SCV에서 조작, 1~10 : 조작하는 태블릿의 Id, 1000~ : 조작하는 서버의 ID
//	int nWorkNo;			// 현재 작업 중인 WorkList No, 0 : 작업을 선택 안함, 1~12 : 선택된 작업 번호
//							// => Battery Alarm 으로인한 WORK_LIST_CHARGING 작업 : 99+xxx => xxx 는 해당 작업 No
//							// => Work Back 으로인한 STATUS_WORK_BACK 작업 : 44+xxx => xxx 는 해당 작업 No
//	int nWorkIndex;			// 현재 작업 중인 Work Index(+1 해준 값), 0 : 작업을 선택 안함, 1~ : 작업중인 Index + 1, -1 : 출발준비중, -2 : 대기장소 이동중
//	int nStatus;			// 상태 정보
//	int nStatusValue;		// 상태 상세 정보
//	//int nprevStatusValue;	// 이전 상태 상세 정보
//	int nFromNodeNo;		// 현재 혹은 이전 Node Number
//	int nToNodeNo;			// 목표 Node Number
//	int nPos_X;				// mm
//	int nPos_Y;				// mm
//	int nPos_Th;			// radian * 100
//	int nBattery;			// V(100을 곱하여 소수2째리를 이상 변환됨(예, 26.17V => 2617))
//	int nLoadUnload;		// 0: Unload, 1 : Load
//
//	std::string sWorkId;	// WorkId
//	std::string sReqTime;	// RequestTime
//	std::string sRespTime;	// ResponseTime
//
//	std::string sPassNode;	// pass_node
//
//	int nVelocity;			// velocity 
//
//	void SetData(int nIdx, char* bufVal)
//	{
//		switch (nIdx)
//		{
//		case 0:		this->nId				= atoi(	bufVal);	break;
//		case 1:		this->nCtrlOwnership	= atoi(	bufVal);	break;
//		case 2:		this->nOrderClientId	= atoi(	bufVal);	break;
//		case 3:		this->nWorkNo			= atoi(	bufVal);	break;
//		case 4:		this->nWorkIndex		= atoi(	bufVal);	break;
//		case 5:		this->nStatus			= atoi(	bufVal);	break;
//		case 6:		this->nStatusValue		= atoi(	bufVal); break;
//		//case 19:	this->nprevStatusValue = atoi(bufVal);	break;
//		case 7:		this->nFromNodeNo		= atoi(	bufVal);	break;
//		case 8:		this->nToNodeNo			= atoi(	bufVal);	break;
//		case 9:		this->nPos_X			= atoi(	bufVal);	break;
//		case 10:	this->nPos_Y			= atoi(	bufVal);	break;
//		case 11:	this->nPos_Th			= atoi(	bufVal);	break;
//		case 12:	this->nBattery			= atoi(	bufVal);	break;
//		case 13:	this->nLoadUnload		= atoi(	bufVal);	break;
//		case 14:	this->sWorkId			=		bufVal;		break;
//		case 15:	this->sReqTime			=		bufVal;		break;
//		case 16:	this->sRespTime			=		bufVal;		break;
//		case 17:	this->sPassNode			=		bufVal;		break;
//		case 18:	this->nVelocity			= atoi(	bufVal);	break;
//		
//		default: break;
//		}
//	}
//
//} AMRStatus;

typedef struct _tag_HWStatus
{
	int			nId;
	std::string	LeftEnc;
	std::string	RightEnc;
	std::string	GyroAng;
	std::string	ErrCode;
	std::string	ErrCodeDetail;
	std::string	Odomatry;
	std::string	Operationtime;

	void SetData(int nIdx, char* bufVal)
	{
		switch (nIdx)
		{
		case 0:		this->nId = atoi(bufVal);	break;
		case 1:		this->LeftEnc = bufVal;	break;
		case 2:		this->RightEnc = bufVal;	break;
		case 3:		this->GyroAng = bufVal;	break;
		case 4:		this->ErrCode = bufVal;	break;
		case 5:		this->ErrCodeDetail = bufVal;	break;
		case 6:		this->Odomatry = bufVal;	break;
		case 7:		this->Operationtime = bufVal;	break;
		default:	break;
		}
	}

} HWStatus;

class CPacket
{

public:

	//static AMRStatus* EncodePacket(char* pData, int index);
	//static HWStatus*  EncodeHWPacket(char* pData, int index);
	CPacket();
	virtual ~CPacket();
};

#endif // !defined(AFX_PACKET_H__643EAE08_3798_43A6_B1C9_9BC6AAF6D0B4__INCLUDED_)
