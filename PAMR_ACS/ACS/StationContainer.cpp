#include "stdafx.h"
#include "StationContainer.h"
//#include "MyFTP.h"

CStationContainer::CStationContainer()
{
	_stations.clear();
}

CStationContainer::CStationContainer(const char *fileName)
{
	_stations.clear();

	// BY KJS, delete.
/*
	if(fileName && *fileName)
	{
		CMyFTP myFTP;

		CString strDirName;
		strDirName = (CString)fileName;

		CString strFindFileName = _T("station");

		if(myFTP.FindFileListOnDir(strDirName, strFindFileName) == TRUE)
		{
			for(int i = 0; i < (int)myFTP.m_vtFileNameIndexList.size(); i++)
			{
				if(strDirName.Right(1) == _T("\\"))
				{
					LoadStationFile(strDirName + myFTP.m_vtFileNameIndexList.at(i).strName, myFTP.m_vtFileNameIndexList.at(i).nIndex);
				}
				else
				{
					LoadStationFile(strDirName + _T("\\") + myFTP.m_vtFileNameIndexList.at(i).strName, myFTP.m_vtFileNameIndexList.at(i).nIndex);
				}
			}
		}
	}
*/
}

CStationContainer::~CStationContainer()
{
	Clear();
}

void CStationContainer::Clear()
{
	_stations.clear();
}

bool CStationContainer::LoadStationFile(CString strFileName, int nIndex)
{
	_stations.reserve(1000);

	char szFileName[1000] = {0};
	//CStringToChar(strFileName, szFileName, 1000);
	PINIReaderWriter = DEBUG_NEW PINIReadWriter(szFileName);		// 설정파일 읽기

	if(PINIReaderWriter->ParseError() < 0)			//  파일을 읽지 못한 경우
	{
		delete PINIReaderWriter;
		PINIReaderWriter = NULL;

		return false;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	int Id = nIndex;
	double x = -1.;
	double y = -1.;
	double th = -1.;
	int nRelationNode1 = 0;
	int nRelationNode2 = 0;
	int nMaxLayer = 0;

	double dLay0_Un_P1;
	double dLay0_Un_P2;
	double dLay0_Un_P3;
	double dLay0_Lo_P1;
	double dLay0_Lo_P2;
	double dLay0_Lo_P3;

	double dLay1_Un_P1;
	double dLay1_Un_P2;
	double dLay1_Un_P3;
	double dLay1_Lo_P1;
	double dLay1_Lo_P2;
	double dLay1_Lo_P3;

	double dLay2_Un_P1;
	double dLay2_Un_P2;
	double dLay2_Un_P3;
	double dLay2_Lo_P1;
	double dLay2_Lo_P2;
	double dLay2_Lo_P3;

	double dLay3_Un_P1;
	double dLay3_Un_P2;
	double dLay3_Un_P3;
	double dLay3_Lo_P1;
	double dLay3_Lo_P2;
	double dLay3_Lo_P3;

	double dLay4_Un_P1;
	double dLay4_Un_P2;
	double dLay4_Un_P3;
	double dLay4_Lo_P1;
	double dLay4_Lo_P2;
	double dLay4_Lo_P3;

	double dLay5_Un_P1;
	double dLay5_Un_P2;
	double dLay5_Un_P3;
	double dLay5_Lo_P1;
	double dLay5_Lo_P2;
	double dLay5_Lo_P3;

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Get [INFO]
	nRelationNode1 = PINIReaderWriter->getIntValue("INFO", "RELATION_NODE1", 0);
	nRelationNode2 = PINIReaderWriter->getIntValue("INFO", "RELATION_NODE2", 0);
	nMaxLayer = PINIReaderWriter->getIntValue("INFO", "MAX_UNLOAD_LAYER", 0);

	x = PINIReaderWriter->getDoubleValue("INFO", "UNLOADING_POS1", 150000.0) / 1000.0;
	y = PINIReaderWriter->getDoubleValue("INFO", "UNLOADING_POS2", 150000.0) / 1000.0;
	th = PINIReaderWriter->getDoubleValue("INFO", "UNLOADING_POS3", 0.0);

	// Get [UNLOAD0]
	dLay0_Un_P1 = PINIReaderWriter->getDoubleValue("UNLOAD0", "LIFT1", 0.0);
	dLay0_Un_P2 = PINIReaderWriter->getDoubleValue("UNLOAD0", "LIFT2", 0.0);
	dLay0_Un_P3 = PINIReaderWriter->getDoubleValue("UNLOAD0", "LIFT3", 0.0);
	// Get [LOAD0]
	dLay0_Lo_P1 = PINIReaderWriter->getDoubleValue("LOAD0", "LIFT1", 0.0);
	dLay0_Lo_P2 = PINIReaderWriter->getDoubleValue("LOAD0", "LIFT2", 0.0);
	dLay0_Lo_P3 = PINIReaderWriter->getDoubleValue("LOAD0", "LIFT3", 0.0);

	// Get [UNLOAD1]
	dLay1_Un_P1 = PINIReaderWriter->getDoubleValue("UNLOAD1", "LIFT1", 0.0);
	dLay1_Un_P2 = PINIReaderWriter->getDoubleValue("UNLOAD1", "LIFT2", 0.0);
	dLay1_Un_P3 = PINIReaderWriter->getDoubleValue("UNLOAD1", "LIFT3", 0.0);
	// Get [LOAD1]
	dLay1_Lo_P1 = PINIReaderWriter->getDoubleValue("LOAD1", "LIFT1", 0.0);
	dLay1_Lo_P2 = PINIReaderWriter->getDoubleValue("LOAD1", "LIFT2", 0.0);
	dLay1_Lo_P3 = PINIReaderWriter->getDoubleValue("LOAD1", "LIFT3", 0.0);

	// Get [UNLOAD2]
	dLay2_Un_P1 = PINIReaderWriter->getDoubleValue("UNLOAD2", "LIFT1", 0.0);
	dLay2_Un_P2 = PINIReaderWriter->getDoubleValue("UNLOAD2", "LIFT2", 0.0);
	dLay2_Un_P3 = PINIReaderWriter->getDoubleValue("UNLOAD2", "LIFT3", 0.0);
	// Get [LOAD2]
	dLay2_Lo_P1 = PINIReaderWriter->getDoubleValue("LOAD2", "LIFT1", 0.0);
	dLay2_Lo_P2 = PINIReaderWriter->getDoubleValue("LOAD2", "LIFT2", 0.0);
	dLay2_Lo_P3 = PINIReaderWriter->getDoubleValue("LOAD2", "LIFT3", 0.0);

	// Get [UNLOAD3]
	dLay3_Un_P1 = PINIReaderWriter->getDoubleValue("UNLOAD3", "LIFT1", 0.0);
	dLay3_Un_P2 = PINIReaderWriter->getDoubleValue("UNLOAD3", "LIFT2", 0.0);
	dLay3_Un_P3 = PINIReaderWriter->getDoubleValue("UNLOAD3", "LIFT3", 0.0);
	// Get [LOAD3]
	dLay3_Lo_P1 = PINIReaderWriter->getDoubleValue("LOAD3", "LIFT1", 0.0);
	dLay3_Lo_P2 = PINIReaderWriter->getDoubleValue("LOAD3", "LIFT2", 0.0);
	dLay3_Lo_P3 = PINIReaderWriter->getDoubleValue("LOAD3", "LIFT3", 0.0);

	// Get [UNLOAD4]
	dLay4_Un_P1 = PINIReaderWriter->getDoubleValue("UNLOAD4", "LIFT1", 0.0);
	dLay4_Un_P2 = PINIReaderWriter->getDoubleValue("UNLOAD4", "LIFT2", 0.0);
	dLay4_Un_P3 = PINIReaderWriter->getDoubleValue("UNLOAD4", "LIFT3", 0.0);
	// Get [LOAD4]
	dLay4_Lo_P1 = PINIReaderWriter->getDoubleValue("LOAD4", "LIFT1", 0.0);
	dLay4_Lo_P2 = PINIReaderWriter->getDoubleValue("LOAD4", "LIFT2", 0.0);
	dLay4_Lo_P3 = PINIReaderWriter->getDoubleValue("LOAD4", "LIFT3", 0.0);

	// Get [UNLOAD5]
	dLay5_Un_P1 = PINIReaderWriter->getDoubleValue("UNLOAD5", "LIFT1", 0.0);
	dLay5_Un_P2 = PINIReaderWriter->getDoubleValue("UNLOAD5", "LIFT2", 0.0);
	dLay5_Un_P3 = PINIReaderWriter->getDoubleValue("UNLOAD5", "LIFT3", 0.0);
	// Get [LOAD5]
	dLay5_Lo_P1 = PINIReaderWriter->getDoubleValue("LOAD5", "LIFT1", 0.0);
	dLay5_Lo_P2 = PINIReaderWriter->getDoubleValue("LOAD5", "LIFT2", 0.0);
	dLay5_Lo_P3 = PINIReaderWriter->getDoubleValue("LOAD5", "LIFT3", 0.0);

////////////////////////////////////////////////////////////////////////////////////////////////////

	_stations.push_back(sStation(Id, x, y, th, nRelationNode1, nRelationNode2, nMaxLayer
								, dLay0_Un_P1, dLay0_Un_P2, dLay0_Un_P3, dLay0_Lo_P1, dLay0_Lo_P2, dLay0_Lo_P3
								, dLay1_Un_P1, dLay1_Un_P2, dLay1_Un_P3, dLay1_Lo_P1, dLay1_Lo_P2, dLay1_Lo_P3
								, dLay2_Un_P1, dLay2_Un_P2, dLay2_Un_P3, dLay2_Lo_P1, dLay2_Lo_P2, dLay2_Lo_P3
								, dLay3_Un_P1, dLay3_Un_P2, dLay3_Un_P3, dLay3_Lo_P1, dLay3_Lo_P2, dLay3_Lo_P3
								, dLay4_Un_P1, dLay4_Un_P2, dLay4_Un_P3, dLay4_Lo_P1, dLay4_Lo_P2, dLay4_Lo_P3
								, dLay5_Un_P1, dLay5_Un_P2, dLay5_Un_P3, dLay5_Lo_P1, dLay5_Lo_P2, dLay5_Lo_P3
								)
		);

	TRACE("SizeofData(_stations): %d\n", _stations.size());

	delete PINIReaderWriter;

	return true;
}

bool CStationContainer::SaveStationFile(CString strFileName, sStation tempStation)
{
	CString strTemp;

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Save [INFO]
	strTemp.Format(L"%d", tempStation.nRelationNode1);
	WritePrivateProfileString(_T("INFO"), _T("RELATION_NODE1"), strTemp, strFileName);
	strTemp.Format(L"%d", tempStation.nRelationNode2);
	WritePrivateProfileString(_T("INFO"), _T("RELATION_NODE2"), strTemp, strFileName);

	strTemp.Format(L"%d", tempStation.nMaxLayer);
	WritePrivateProfileString(_T("INFO"), _T("MAX_UNLOAD_LAYER"), strTemp, strFileName);
	WritePrivateProfileString(_T("INFO"), _T("MAX_LOAD_LAYER "), strTemp, strFileName);

	strTemp.Format(L"%0.1f", tempStation.x * 1000);
	WritePrivateProfileString(_T("INFO"), _T("UNLOADING_POS1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.y * 1000);
	WritePrivateProfileString(_T("INFO"), _T("UNLOADING_POS2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.th);
	WritePrivateProfileString(_T("INFO"), _T("UNLOADING_POS3"), strTemp, strFileName);

	strTemp.Format(L"%0.1f", tempStation.x * 1000);
	WritePrivateProfileString(_T("INFO"), _T("LOADING_POS1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.y * 1000);
	WritePrivateProfileString(_T("INFO"), _T("LOADING_POS2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.th);
	WritePrivateProfileString(_T("INFO"), _T("LOADING_POS3"), strTemp, strFileName);

	// Save [UNLOAD0]
	strTemp.Format(L"%0.1f", tempStation.dLay0_Un_P1);
	WritePrivateProfileString(_T("UNLOAD0"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay0_Un_P1);
	WritePrivateProfileString(_T("UNLOAD0"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay0_Un_P1);
	WritePrivateProfileString(_T("UNLOAD0"), _T("LIFT3"), strTemp, strFileName);
	// Save [LOAD0]
	strTemp.Format(L"%0.1f", tempStation.dLay0_Lo_P1);
	WritePrivateProfileString(_T("LOAD0"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay0_Lo_P1);
	WritePrivateProfileString(_T("LOAD0"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay0_Lo_P1);
	WritePrivateProfileString(_T("LOAD0"), _T("LIFT3"), strTemp, strFileName);

	// Save [UNLOAD1]
	strTemp.Format(L"%0.1f", tempStation.dLay1_Un_P1);
	WritePrivateProfileString(_T("UNLOAD1"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay1_Un_P1);
	WritePrivateProfileString(_T("UNLOAD1"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay1_Un_P1);
	WritePrivateProfileString(_T("UNLOAD1"), _T("LIFT3"), strTemp, strFileName);
	// Save [LOAD1]
	strTemp.Format(L"%0.1f", tempStation.dLay1_Lo_P1);
	WritePrivateProfileString(_T("LOAD1"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay1_Lo_P1);
	WritePrivateProfileString(_T("LOAD1"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay1_Lo_P1);
	WritePrivateProfileString(_T("LOAD1"), _T("LIFT3"), strTemp, strFileName);

	// Save [UNLOAD2]
	strTemp.Format(L"%0.1f", tempStation.dLay2_Un_P1);
	WritePrivateProfileString(_T("UNLOAD2"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay2_Un_P1);
	WritePrivateProfileString(_T("UNLOAD2"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay2_Un_P1);
	WritePrivateProfileString(_T("UNLOAD2"), _T("LIFT3"), strTemp, strFileName);
	// Save [LOAD2]
	strTemp.Format(L"%0.1f", tempStation.dLay2_Lo_P1);
	WritePrivateProfileString(_T("LOAD2"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay2_Lo_P1);
	WritePrivateProfileString(_T("LOAD2"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay2_Lo_P1);
	WritePrivateProfileString(_T("LOAD2"), _T("LIFT3"), strTemp, strFileName);

	// Save [UNLOAD3]
	strTemp.Format(L"%0.1f", tempStation.dLay3_Un_P1);
	WritePrivateProfileString(_T("UNLOAD3"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay3_Un_P1);
	WritePrivateProfileString(_T("UNLOAD3"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay3_Un_P1);
	WritePrivateProfileString(_T("UNLOAD3"), _T("LIFT3"), strTemp, strFileName);
	// Save [LOAD3]
	strTemp.Format(L"%0.1f", tempStation.dLay3_Lo_P1);
	WritePrivateProfileString(_T("LOAD3"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay3_Lo_P1);
	WritePrivateProfileString(_T("LOAD3"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay3_Lo_P1);
	WritePrivateProfileString(_T("LOAD3"), _T("LIFT3"), strTemp, strFileName);

	// Save [UNLOAD4]
	strTemp.Format(L"%0.1f", tempStation.dLay4_Un_P1);
	WritePrivateProfileString(_T("UNLOAD4"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay4_Un_P1);
	WritePrivateProfileString(_T("UNLOAD4"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay4_Un_P1);
	WritePrivateProfileString(_T("UNLOAD4"), _T("LIFT3"), strTemp, strFileName);
	// Save [LOAD4]
	strTemp.Format(L"%0.1f", tempStation.dLay4_Lo_P1);
	WritePrivateProfileString(_T("LOAD4"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay4_Lo_P1);
	WritePrivateProfileString(_T("LOAD4"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay4_Lo_P1);
	WritePrivateProfileString(_T("LOAD4"), _T("LIFT3"), strTemp, strFileName);

	// Save [UNLOAD5]
	strTemp.Format(L"%0.1f", tempStation.dLay5_Un_P1);
	WritePrivateProfileString(_T("UNLOAD5"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay5_Un_P1);
	WritePrivateProfileString(_T("UNLOAD5"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay5_Un_P1);
	WritePrivateProfileString(_T("UNLOAD5"), _T("LIFT3"), strTemp, strFileName);
	// Save [LOAD5]
	strTemp.Format(L"%0.1f", tempStation.dLay5_Lo_P1);
	WritePrivateProfileString(_T("LOAD5"), _T("LIFT1"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay5_Lo_P1);
	WritePrivateProfileString(_T("LOAD5"), _T("LIFT2"), strTemp, strFileName);
	strTemp.Format(L"%0.1f", tempStation.dLay5_Lo_P1);
	WritePrivateProfileString(_T("LOAD5"), _T("LIFT3"), strTemp, strFileName);

	return true;
}

