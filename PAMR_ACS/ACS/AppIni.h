#pragma once

#include "GlobalIni.h"//"Ini.h" 

class CAppIni: public CGlobalIni
{
public: 
	char mapFile[MAX_PATH];			// 지도 이미지를 저장하는 파일 이름
	double mapPixelSize;				// 지도 이미지에서 한 픽셀의 크기 (단위: m)
	int mapPixelOriginX;				// 지도 이미지에서 원점(0, 0)이 되는 X축 방향 픽셀 위치
	int mapPixelOriginY;				// 지도 이미지에서 원점(0, 0)이 되는 Y축 방향 픽셀 위치

	char pathFile[MAX_PATH];		// 경로 데이터를 저장하는 파일 이름
	char robotFile[MAX_PATH ];		// 로봇 정보를 저장하는 파일 이름
	char wallFile[MAX_PATH ];		// 벽 정보를 저장하는 파일 이름
	char magnetFile[MAX_PATH ];		// Magnet 정보를 저장하는 파일 이름

	char taskFile[MAX_PATH ];		// 작업 데이터를 저장하는 파일 이름

	char pamrssiDirectory[MAX_PATH];	// PAMR Data Directory 주소

	char stationDirectory[MAX_PATH];	// Station Data Directory 주소

	int windowSX;						// 윈도우의 X 위치 (프로그램이 새로 시작될 때 마지막 종료 위치를 기억하는데 사용)
	int windowSY;						// 윈도우의 Y 위치 (프로그램이 새로 시작될 때 마지막 종료 위치를 기억하는데 사용) 

	double mapScale;					// 프로그램이 종료될 때 지도의 확대/축소 비율
	double mapOffsetX;					// 프로그램이 종료될 때 X방향으로 지도를 스크롤 한 양
	double mapOffsetY;					// 프로그램이 종료될 때 Y방향으로 지도를 스크롤 한 양

	int serverPort;						// Server Port

	char m_strRaiServerIpAddr[MAX_PATH];
	unsigned short m_usRaiServerPort;


	RECT MainWindow;

public:
	CAppIni(const char *ini_path);
	virtual ~CAppIni();

	void LoadData();
	void SaveData();

private:
	void TransferAllData(bool bSave);
};

extern CAppIni g_ini;
