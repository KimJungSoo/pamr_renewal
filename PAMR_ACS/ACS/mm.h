#pragma once
#include "DebugLog.h"

#ifndef M_PI
#define M_PI		3.14159265358979323846	// pi 
#endif
#define M_E			2.7182818284590452354	// e 
#define M_LOG2E		1.4426950408889634074	// log_2 e 
#define M_LOG10E	0.43429448190325182765 	// log_10 e 
#define M_LN2		0.69314718055994530942 	// log_e 2 
#define M_LN10		2.30258509299404568402 	// log_e 10 
#define M_PI_2		1.57079632679489661923 	// pi/2 
#define M_PI_4		0.78539816339744830962 	// pi/4 
#define M_1_PI		0.31830988618379067154 	// 1/pi 
#define M_2_PI		0.63661977236758134308 	// 2/pi 
#define M_SQRTPI	1.77245385090551602729 	// sqrt(pi) [4.0.2] 
#define M_2_SQRTPI	1.12837916709551257390 	// 2/sqrt(pi) 
#define M_SQRT2		1.41421356237309504880 	// sqrt[2] 
#define M_SQRT3		1.73205080756887729352 	// sqrt[3] [4.0.2] 
#ifndef M_SQRT1_2
#define M_SQRT1_2	0.70710678118654752440 	// 1/sqrt[2] 
#endif
#define M_LNPI		1.14472988584940017414 	// log_e(pi) [4.0.2] 
#define M_EULER		0.57721566490153286061 	// Euler constant [4.0.2] 

#define _RAD2DEG	(180./M_PI)
#define _DEG2RAD	(M_PI/180.)

inline double RAD2DEG(double x)
{
	return x*_RAD2DEG;
}

inline double DEG2RAD(double x)
{
	return x*_DEG2RAD;
}

inline double BOUND(double x, double l, double u)
{
	if(x <= l) return l;
	if(x >= u) return u;
	return x;
}

template<typename T>
inline void Swap(T &v1, T &v2)
{
	T temp = v1;
	v1 = v2;
	v2 = temp;
}

inline double sign (double a) 
{
	return (0. <= a) ? 1. : -1.;
}

extern void Rotate(double result[2], const double src[2], double rad);

extern double GaussRand();

/*
template <typename T>
inline T min (T a, T b)
{
	return (a < b)? a : b;
}

template <typename T>
inline T max (T a, T b)
{
	return (a < b)? b : a;
}
*/

inline int INTEGER (const double a)
{
	// return (long)floor (a + 0.5);
	return (0 < a)? (int)(a + 0.5) : (int)(a - 0.5);
}

inline double Sign (const double v)
{
	return (0 < v) ? +1. : -1.;
}

inline int DeltaDeg (int ang1, int ang2)
{
	int da = ang1 - ang2;
	if(-180 < da && da < 180) return da;
	else {
		da %= 360;
		if(180 <= da) return da - 360;
		else if(da <= -180) return da + 360;
		else return da;
	}
}

inline double DeltaDeg (double ang1, double ang2)
{
	double da = ang1 - ang2;
	if(-180 < da && da < 180) return da;
	else {
		da = fmod(da, 360);
		if(180 <= da) return da - 360;
		else if(da <= -180) return da + 360;
		else return da;
	}
	return da;
}

inline double DeltaRad(double ang1, double ang2)
{
	double da = ang1 - ang2;
	if(-M_PI < da && da < M_PI) return da;
	else {
		da = fmod(da, 2*M_PI);
		if(M_PI <= da) return da - 2*M_PI;
		else if(da <= -M_PI) return da + 2*M_PI;
		else return da;
	}
	return da;
}


inline double DeltaRadHalf (double ang1, double ang2)
{
	double da = ang1 - ang2;
	if(-M_PI / 2 < da && da < M_PI / 2) return da;
	else {
		da = fmod(da, M_PI);
		if(M_PI / 2 <= da) return da - M_PI;
		else if(da <= -M_PI / 2) return da + M_PI;
		else return da;
	}
	return da;
}


inline double Length(double x, double y)
{
	return sqrt(x*x + y*y);
}

inline double Distance(double x1, double y1, double x2, double y2)
{
	double dx = x2 - x1;
	double dy = y2 - y1;

	return sqrt(dx * dx + dy * dy);
}

inline int Distance(int x1, int y1, int x2, int y2)
{
	double dx = x2 - x1;
	double dy = y2 - y1;

	return INTEGER(sqrt(dx * dx + dy * dy));
}

