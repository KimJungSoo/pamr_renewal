#include "stdafx.h"
#include "AppIni.h" 

CAppIni g_ini("PamrServer.ini"); //C:\\data\\ini\\PamrServer.ini

CAppIni::CAppIni(const char *ini_path)
: CGlobalIni(ini_path)
{
	LoadData();
}

CAppIni::~CAppIni()
{
	//SaveData();
}

void CAppIni::LoadData()
{
	TransferAllData(false);
}

void CAppIni::SaveData()
{
	TransferAllData(true);
}

void CAppIni::TransferAllData(bool bSave)
{
	Transfer (bSave, "mapFile",				mapFile,  MAX_PATH,			"C:\\data\\map\\image_map1.jpg");
	Transfer (bSave, "mapPixelSize",		mapPixelSize,				0.01);
	Transfer (bSave, "mapPixelOriginX",		mapPixelOriginX,			0);
	Transfer (bSave, "mapPixelOriginY",		mapPixelOriginY,			0);

	Transfer (bSave, "pathFile",			pathFile, MAX_PATH,			"C:\\data\\Editor\\path.txt"); //.\\data\\path.txt
	Transfer (bSave, "robotFile",			robotFile, MAX_PATH,		"C:\\data\\Editor\\robot.txt");
	Transfer (bSave, "wallFile",			wallFile, MAX_PATH,			"C:\\data\\Editor\\wall.txt");
	Transfer (bSave, "magnetFile",			magnetFile, MAX_PATH,		"C:\\data\\Editor\\magnet.txt");

	Transfer (bSave, "taskFile",			taskFile, MAX_PATH,			"C:\\data\\Editor\\task.txt");

	Transfer (bSave, "pamrssiDirectory",	pamrssiDirectory, MAX_PATH,	"C:\\data\\");

	Transfer (bSave, "stationDirectory",	stationDirectory, MAX_PATH,		"C:\\data\\station\\");

	Transfer (bSave, "windowSX",			windowSX,					0);
	Transfer (bSave, "windowSY",			windowSY,					0);

	Transfer(bSave, "MainWindowLeft",		MainWindow.left,			0);
	Transfer(bSave, "MainWindowTop",		MainWindow.top,				0);
	Transfer(bSave, "MainWindowRight",		MainWindow.right,			1024);
	Transfer(bSave, "MainWindowBottom",		MainWindow.bottom,			768);

	Transfer (bSave, "mapScale",			mapScale,					32.);
	Transfer (bSave, "mapOffsetX",			mapOffsetX,					0.);
	Transfer (bSave, "mapOffsetY",			mapOffsetY,					0.);

	Transfer (bSave, "serverPort",			serverPort,					5001);
	
	Transfer(bSave, "raiServerIpAddr",		m_strRaiServerIpAddr,	MAX_PATH, "10.211.115.30");
	Transfer(bSave, "raiServerPort",		m_usRaiServerPort,		6000);
}
