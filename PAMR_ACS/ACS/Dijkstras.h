#pragma once

// Number of vertices in the graph
#define PATH_MAX	1024
#define VERTICES_MAX	PATH_MAX


#if 0
int graph[V][V] = {
	{ 0, 4, 0, 0, 0, 0, 0, 8, 0 },
	{ 4, 0, 8, 0, 0, 0, 0, 11, 0 },
	{ 0, 8, 0, 7, 0, 4, 0, 0, 2 },
	{ 0, 0, 7, 0, 9, 14, 0, 0, 0 },
	{ 0, 0, 0, 9, 0, 10, 0, 0, 0 },
	{ 0, 0, 4, 14, 10, 0, 2, 0, 0 },
	{ 0, 0, 0, 0, 0, 2, 0, 1, 6 },
	{ 8, 11, 0, 0, 0, 0, 1, 0, 7 },
	{ 0, 0, 2, 0, 0, 0, 6, 7, 0 }
};
#endif

int minDistance(int dist[], bool sptSet[]);
void printPath(int parent[], int j, int nResultPath[]);
int printSolution(int dist[], int n, int parent[], int dest, int nResultPath[]);
void dijkstra(int graph[VERTICES_MAX][VERTICES_MAX], int src, int dest, int nResultPath[]);
