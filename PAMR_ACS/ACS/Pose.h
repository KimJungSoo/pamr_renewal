#pragma once
#include "mm.h"
#include <math.h>

class CPose {
public:
	double x, y;

public:
	CPose () : x(0), y(0) { }
	CPose (const CPose &p) : x(p.x), y(p.y) { }
	CPose (double x_, double y_) : x(x_), y(y_) { }
	// 아래 생성자에서 bool b 인자는 적용안됨. 바로 위 생성자와 구분하기 위해 사용함.
	CPose (double l_, double th_, bool b) : x(l_*cos(th_)), y(l_*sin(th_)) { }	

	void Zero()
	{
		x = y = 0;
	}
	void Rotate(double rad)
	{
		double c = cos(rad);
		double s = sin(rad);

		double x_ = x*c - y*s;
		double y_ = x*s + y*c;

		x = x_;
		y = y_;
	}
	void Translate(double x_, double y_)
	{
		x += x_;
		y += y_;
	}

public: 
	double Length() const { return sqrt(x*x + y*y); }
	double Distance(const CPose &p) const { double dx = p.x-x, dy = p.y-y; return sqrt(dx * dx + dy * dy); }

	CPose & operator += (const CPose &p) { x += p.x; y += p.y; return *this; }
	CPose & operator -= (const CPose &p) { x -= p.x; y -= p.y; return *this; }
};

inline CPose operator + (const CPose &l, const CPose &r) { return CPose (l.x+r.x, l.y+r.y); }
inline CPose operator - (const CPose &l, const CPose &r) { return CPose (l.x-r.x, l.y-r.y); }
inline CPose operator * (const CPose &l, double r)       { return CPose (l.x*r,   l.y*r  ); }
inline CPose operator / (const CPose &l, double r)       { return CPose (l.x/r,   l.y/r  ); }
