#pragma once

#include "PathContainer.h"
#include "Posture.h"
#include "ScopedLock.h"


class CAstar : public CPathContainer
{
public:
	CAstar(const char *fileName);
	CAstar(const char *fileName, const char *fileName_Magnet);

	bool FindPath(vector<vaWayPoint> &path, const CPosture &robotPos, const char *targetName);

private:
	vector<int> FindPath(int start, int goal);

	int PopOpenSet();
	bool SearchOpenSet(int id);
	bool SearchCloseSet(int id);

	vector<int> Reverse(vector<int> &path);
	vector<int> ReverseFollow(int start, int goal);

	double dist_between(int start, int goal);
	double heuristic_cost_estimate(int start, int goal);

private:
	CDeviceLock _lock;
	vector<int> _closedSet;	// The set of nodes already evaluated.
	vector<int> _openSet;	// The set of tentative nodes to be evaluated.
};
