#include "StdAfx.h"
#include "PathFollow.h"
#include "ConstParameters.h"

double CPathFollow::DetectObstacle ()
{
	double d = _obstacleDistance - ROBOT_DIM_WIDTH*2 - SAFETY_MARGIN_FRONT;
	if(d < 0)
		d = 0;

	return d;

	// 로봇이 직진할 때 전방의 장애물을 탐지 영역을 결정하고 
	// 이 영역안에 들어온 장애물과 로봇간의 최소 거리를 찾는다.

	// 로봇이 전방 장애물을 찾는 최대 거리를 결정한다.
	// 이 거리는 로봇의 최고 속도와 가속도에 의해 결정된다.
	const double frontDetect = 0.5*ROBOT_MAX_VELOCITY*ROBOT_MAX_VELOCITY/ROBOT_DECELERATION;
	// 로봇이 전방 장애물을 찾는 최소 거리를 결정한다.
	// 이 거리보다 짧은 장애물은 로봇을 긴급정시 시킨다.
	const double frontSafety = SAFETY_MARGIN_FRONT + ROBOT_DIM_FRONT;
	// 장애물을 탐지하는 로봇의 좌우 폭을 결정한다.
	const double sideSafety  = SAFETY_MARGIN_SIDE + ROBOT_DIM_WIDTH/2.;

	double minDistance = 1000.;
	int countObstacles = 0;

	return max (0., minDistance);
}
