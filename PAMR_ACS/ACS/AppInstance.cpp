
#include "stdafx.h"
#include "./util/MultimediaTimer.h"
#include "AppInstance.h"
#include "AppIni.h"


CMultimediaTimer	mt;

CGlobalMap			*g_globalMap		= nullptr;

CRobotContainer		*g_robotContainer	= NULL;

int					g_PamrNo			= 0;
//CPAMR				*g_Pamr[MAX_VEHICLE];

CAstar				*g_route			= NULL;
CWallContainer		*g_wallContainer	= NULL;
CMagnetContainer	*g_magnetContainer	= NULL;
CTaskContainer		*g_taskContainer	= NULL;
CStationContainer	*g_stationContainer	= NULL;

void InitInstance()
{
	srand((int)time(0));

	// 윈도우즈 타이머는 최소 10ms 단위로 수행되는데, 
	// 이를 멀티미디어 타이머를 구동함으로 1ms 단위로 수행되도록 한다.
	mt.Start(1);

	// g_globalMap		= new CGlobalMap(g_ini.mapFile);
	TRACE("mapfile: %s", g_ini.mapFile);
	g_globalMap			= DEBUG_NEW CGlobalMap(g_ini.mapFile, g_ini.mapPixelSize, g_ini.mapPixelOriginX, g_ini.mapPixelOriginY);

	g_robotContainer	= DEBUG_NEW CRobotContainer(g_ini.robotFile);
	g_PamrNo = 0;

	//for(int i = 0, n = g_robotContainer->_robots.size(); i < n; i++)
	//{
	//	g_Pamr[i] = new CPAMR();
	//	g_Pamr[i]->_mobility._posture.x = g_robotContainer->_robots[i].x;
	//	g_Pamr[i]->_mobility._posture.y = g_robotContainer->_robots[i].y;
	//	g_Pamr[i]->_mobility._posture.th = g_robotContainer->_robots[i].th * _DEG2RAD;

	//	g_Pamr[i]->m_nStatus = AS_DISCONNECT;

	//	g_Pamr[i]->m_ctmRcvTime = CTime::GetCurrentTime();
	//	g_Pamr[i]->m_bConnected = false;

	//	g_Pamr[i]->m_stAMRStatus.nId = g_robotContainer->_robots[i].Id;
	//	g_Pamr[i]->m_stAMRStatus.nCtrlOwnership = -1;
	//	g_Pamr[i]->m_stAMRStatus.nOrderClientId = -1;
	//	g_Pamr[i]->m_stAMRStatus.nWorkNo = -1;
	//	g_Pamr[i]->m_stAMRStatus.nWorkIndex = -1;
	//	g_Pamr[i]->m_stAMRStatus.nStatus = -1;//STATUS_READY;
	//	g_Pamr[i]->m_stAMRStatus.nStatusValue = -1;//WORK_READY;
	//	g_Pamr[i]->m_stAMRStatus.nCtrlOwnership = -1;//AMR_SELF;
	//	g_Pamr[i]->m_stAMRStatus.nFromNodeNo = -1;
	//	g_Pamr[i]->m_stAMRStatus.nToNodeNo = -1;
	//	g_Pamr[i]->m_stAMRStatus.nPos_X = static_cast<int>(g_robotContainer->_robots[i].x);
	//	g_Pamr[i]->m_stAMRStatus.nPos_Y = static_cast<int>(g_robotContainer->_robots[i].y);
	//	g_Pamr[i]->m_stAMRStatus.nPos_Th = static_cast<int>(g_robotContainer->_robots[i].th);
	//	g_Pamr[i]->m_stAMRStatus.nBattery = 0;
	//	g_Pamr[i]->m_stAMRStatus.nLoadUnload = 0;
	//	g_Pamr[i]->m_stAMRStatus.sPassNode = "";
	//	g_Pamr[i]->m_bCheckStart_SendCmd = false;
	//	g_Pamr[i]->m_nCountWorkData_SendCmd = 0;
	//	g_Pamr[i]->m_OREQData = { 0, };
	//	//g_Pamr[i]->strPath = "";

	//	g_PamrNo++;
	//}

	//g_route				= new CAstar(g_ini.pathFile);
	g_route				= DEBUG_NEW CAstar(g_ini.pathFile, g_ini.magnetFile);
	g_wallContainer		= DEBUG_NEW CWallContainer(g_ini.wallFile);
	g_magnetContainer	= DEBUG_NEW CMagnetContainer(g_ini.magnetFile);
	g_taskContainer		= DEBUG_NEW CTaskContainer(g_ini.taskFile);
	g_stationContainer	= DEBUG_NEW CStationContainer(g_ini.stationDirectory);
}

void TerminateInstance()
{
	delete g_taskContainer;
	delete g_wallContainer;
	delete g_magnetContainer;
	delete g_route;
	delete g_robotContainer;
	delete g_globalMap;
	delete g_stationContainer;

	//for(int i = 0; i < g_PamrNo; i++)
	//{
	//	delete g_Pamr[i];	
	//}

	mt.Stop();
	int a = 0;
}
