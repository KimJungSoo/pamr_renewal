#include "stdafx.h"
#include "opencv2\highgui\highgui.hpp"
#include "GlobalMap.h"

static const double epsilon = 1e-6;

// 이미지 상의 한 픽셀의 위치를 y,x로 지정하여 픽셀의 참조 포인트 값을 계산한다.
#define pPixel(img, y, x)	(img->imageData + (y) * img->widthStep + img->nChannels * img->depth / 8 * (x))

CGlobalMap::CGlobalMap(const char *imageFileName, double pixelWidth, int pixelOriginX, int pixelOriginY)
	: _imgMap(nullptr), _imgWall(nullptr), _imgAngle(nullptr)
{
	InitializeCriticalSectionAndSpinCount(&m_lock, 4000);
	_pixelWidth = pixelWidth;
	_pixelOriginX = pixelOriginX;
	_pixelOriginY = pixelOriginY;

	// 이미지 파일을 읽어들인다.
	_imgMap = cvLoadImage(imageFileName);
	assert(_imgMap);

	// 읽어들인 이미지 파일에서 벽이 존재할 확률 지도를 생성한다.
	_imgWall = cvCreateImage(cvGetSize(_imgMap), IPL_DEPTH_32F, 1);
	// 벽면의 수직 방향을 저장할 지도를 생성한다.
	_imgAngle = cvCreateImage(cvGetSize(_imgMap), IPL_DEPTH_32F, 1);

	MakeWallProb ();

	// cvNamedWindow("GlobalMap", CV_WINDOW_AUTOSIZE);
	// cvShowImage("GlobalMap", _imgWall);
}

CGlobalMap::~CGlobalMap()
{

	EnterCriticalSection(&m_lock);
	cvReleaseImage(&_imgMap);
	cvReleaseImage(&_imgWall);
	cvReleaseImage(&_imgAngle);
	_imgMap = nullptr;
	_imgWall = nullptr;
	_imgAngle = nullptr;
	LeaveCriticalSection(&m_lock);

	//cvDestroyWindow("GlobalMap");
	DeleteCriticalSection(&m_lock);
}

void CGlobalMap::ExtractWall()
{
	// 읽어들인 이미지(imgMap)에서 장애물(검정색, B,G,R = 0,0,0)이 
	// 있는 곳만 imgWall에 흰색(1.)으로 표시해 둔다.
	for(int h = 0; h < _imgMap->height; h++)
	{
		unsigned char *s = (unsigned char *)pPixel(_imgMap, h, 0);
		float *d = (float *)pPixel(_imgWall, h, 0);

		for(int w = 0; w < _imgMap->width; w++)
		{
			*d = (s[0] == s[1] && s[1] == s[2]) ? (255 - s[0]) / 255.f : 0.f;

			s += _imgMap->nChannels;
			d += _imgWall->nChannels; 
		}
	}
}

void CGlobalMap::MakeWallProb()
{
	ExtractWall ();

 	// Edge에 wxw 가우시안 커널을 컨벌루션 하여 벽의 경계를 흐릿하게 만든다.
	// 여기서 가우시안 커널 크기는 센서의 측정에 노이즈가 많이 포함되어 있다면 더 크게 한다.
	int w = 1 + 2 * (int)(1. / _pixelWidth * 0.2);
    cvSmooth(_imgWall, _imgWall, CV_GAUSSIAN, w, w);

	IplImage *diff_x = cvCreateImage(cvGetSize(_imgWall), IPL_DEPTH_32F, 1);
    IplImage *diff_y = cvCreateImage(cvGetSize(_imgWall), IPL_DEPTH_32F, 1);
	
	// Edge를 찾기위해 X축 방향과 Y축 방향으로 각각 소벨 연산을 수행한다.
	// 즉, 차분 영상을 각각 X,Y축으로 얻게된다.
    cvSobel(_imgWall, diff_x, 1, 0, 3);
    cvSobel(_imgWall, diff_y, 0, 1, 3);

	// 소벨 연산 결과를 합쳐서 크기와 방향을 계산한다. 
	cvCartToPolar(diff_x, diff_y, _imgWall, _imgAngle, 0);

	cvReleaseImage(&diff_x);
	cvReleaseImage(&diff_y);
}

bool CGlobalMap::GetPixel(double x, double y, unsigned char pixel[3])
{
	// 미터 단위로 주어진 지도상의 한 점의 픽셀 값을 읽어온다. 
	int px = M2CUx(x);
	int py = M2CUy(y);

	if((0 <= px && px < _imgWall->width) && (0 <= py && py < _imgWall->height))
	{
		// 이미지는 거꾸로 들어있다. 그래서 y축으로 뒤집는다.
		py = _imgMap->height - py - 1;

		memcpy(pixel, pPixel(_imgMap, py, px), 3);

		return true;
	}
	return false;
}

char *CGlobalMap::pGetPixel(double x, double y)
{
	// 미터 단위로 주어진 지도상의 한 점의 픽셀 값을 읽어온다.
	int px = M2CUx(x);
	int py = M2CUy(y);

	if((0 <= px && px < _imgWall->width) && (0 <= py && py < _imgWall->height))
	{
		// 이미지는 거꾸로 들어있다. 그래서 y축으로 뒤집는다.
		py = _imgMap->height - py - 1;

		return pPixel(_imgMap, py, px);
	}
	return NULL;
}

float CGlobalMap::GetWallProb(double x, double y, float *angle)
{
	// 미터 단위로 주어진 지도상의 한 점의 벽이 존재할 확률 값(0 ~ 1)을 읽어온다.
	int px = M2CUx(x);
	int py = M2CUy(y);

	if((0 <= px && px < _imgWall->width) && (0 <= py && py < _imgWall->height))
	{
		// 이미지는 거꾸로 들어있다. 그래서 y축으로 뒤집는다.
		py = _imgWall->height - py - 1;

		*angle = *(float *)pPixel(_imgAngle, py, px);

		return *(float *)pPixel(_imgWall, py, px);
	}
	return 0.f;
}

void CGlobalMap::PaintMap(HDC hDC, int sx, int sy, double scale)
{
	EnterCriticalSection(&m_lock);
	if(_imgMap != nullptr)
	{
		BITMAPINFO bi;

		int pixelByte = _imgMap->widthStep / _imgMap->width;

		bi.bmiHeader.biSize				= sizeof(BITMAPINFOHEADER);
		bi.bmiHeader.biWidth			= _imgMap->width;
		bi.bmiHeader.biHeight			= _imgMap->height;		// top-down bitmap, negative height
		bi.bmiHeader.biPlanes			= 1;
		bi.bmiHeader.biBitCount			= pixelByte * 8;
		bi.bmiHeader.biCompression		= BI_RGB;
		bi.bmiHeader.biSizeImage		= _imgMap->width * _imgMap->height * pixelByte;
		bi.bmiHeader.biXPelsPerMeter	= 100;
		bi.bmiHeader.biYPelsPerMeter	= 100;
		bi.bmiHeader.biClrUsed			= 0;
		bi.bmiHeader.biClrImportant		= 0;

		scale *= _pixelWidth;

		StretchDIBits(hDC, 
					sx - (int)(_pixelOriginX * scale), sy + (int)(_pixelOriginY * scale), 
					(int)(_imgMap->width * scale), (int)(-_imgMap->height * scale), 
					0, 0, _imgMap->width, _imgMap->height, 
					_imgMap->imageData, &bi, DIB_RGB_COLORS, SRCCOPY);
	}

	LeaveCriticalSection(&m_lock);
	// cvShowImage ("GlobalMap", _imgWall);
}

