#pragma once

#include "PINIReadWriter.h"

using namespace std;

struct sStation
{
	int Id;
	double x, y, th;
	int nRelationNode1;
	int nRelationNode2;
	int nMaxLayer;

	double dLay0_Un_P1;
	double dLay0_Un_P2;
	double dLay0_Un_P3;
	double dLay0_Lo_P1;
	double dLay0_Lo_P2;
	double dLay0_Lo_P3;

	double dLay1_Un_P1;
	double dLay1_Un_P2;
	double dLay1_Un_P3;
	double dLay1_Lo_P1;
	double dLay1_Lo_P2;
	double dLay1_Lo_P3;

	double dLay2_Un_P1;
	double dLay2_Un_P2;
	double dLay2_Un_P3;
	double dLay2_Lo_P1;
	double dLay2_Lo_P2;
	double dLay2_Lo_P3;

	double dLay3_Un_P1;
	double dLay3_Un_P2;
	double dLay3_Un_P3;
	double dLay3_Lo_P1;
	double dLay3_Lo_P2;
	double dLay3_Lo_P3;

	double dLay4_Un_P1;
	double dLay4_Un_P2;
	double dLay4_Un_P3;
	double dLay4_Lo_P1;
	double dLay4_Lo_P2;
	double dLay4_Lo_P3;

	double dLay5_Un_P1;
	double dLay5_Un_P2;
	double dLay5_Un_P3;
	double dLay5_Lo_P1;
	double dLay5_Lo_P2;
	double dLay5_Lo_P3;

	sStation(int Id_, double x_, double y_, double th_, int nRelationNode1_, int nRelationNode2_, int nMaxLayer_
			, double dLay0_Un_P1_, double dLay0_Un_P2_, double dLay0_Un_P3_, double dLay0_Lo_P1_, double dLay0_Lo_P2_, double dLay0_Lo_P3_
			, double dLay1_Un_P1_, double dLay1_Un_P2_, double dLay1_Un_P3_, double dLay1_Lo_P1_, double dLay1_Lo_P2_, double dLay1_Lo_P3_
			, double dLay2_Un_P1_, double dLay2_Un_P2_, double dLay2_Un_P3_, double dLay2_Lo_P1_, double dLay2_Lo_P2_, double dLay2_Lo_P3_
			, double dLay3_Un_P1_, double dLay3_Un_P2_, double dLay3_Un_P3_, double dLay3_Lo_P1_, double dLay3_Lo_P2_, double dLay3_Lo_P3_
			, double dLay4_Un_P1_, double dLay4_Un_P2_, double dLay4_Un_P3_, double dLay4_Lo_P1_, double dLay4_Lo_P2_, double dLay4_Lo_P3_
			, double dLay5_Un_P1_, double dLay5_Un_P2_, double dLay5_Un_P3_, double dLay5_Lo_P1_, double dLay5_Lo_P2_, double dLay5_Lo_P3_
	)
		: Id(Id_), x(x_), y(y_), th(th_), nRelationNode1(nRelationNode1_), nRelationNode2(nRelationNode2_), nMaxLayer(nMaxLayer_)
			, dLay0_Un_P1(dLay0_Un_P1_), dLay0_Un_P2(dLay0_Un_P2_), dLay0_Un_P3(dLay0_Un_P3_), dLay0_Lo_P1(dLay0_Lo_P1_), dLay0_Lo_P2(dLay0_Lo_P2_), dLay0_Lo_P3(dLay0_Lo_P3_)
			, dLay1_Un_P1(dLay1_Un_P1_), dLay1_Un_P2(dLay1_Un_P2_), dLay1_Un_P3(dLay1_Un_P3_), dLay1_Lo_P1(dLay1_Lo_P1_), dLay1_Lo_P2(dLay1_Lo_P2_), dLay1_Lo_P3(dLay1_Lo_P3_)
			, dLay2_Un_P1(dLay2_Un_P1_), dLay2_Un_P2(dLay2_Un_P2_), dLay2_Un_P3(dLay2_Un_P3_), dLay2_Lo_P1(dLay2_Lo_P1_), dLay2_Lo_P2(dLay2_Lo_P2_), dLay2_Lo_P3(dLay2_Lo_P3_)
			, dLay3_Un_P1(dLay3_Un_P1_), dLay3_Un_P2(dLay3_Un_P2_), dLay3_Un_P3(dLay3_Un_P3_), dLay3_Lo_P1(dLay3_Lo_P1_), dLay3_Lo_P2(dLay3_Lo_P2_), dLay3_Lo_P3(dLay3_Lo_P3_)		
			, dLay4_Un_P1(dLay4_Un_P1_), dLay4_Un_P2(dLay4_Un_P2_), dLay4_Un_P3(dLay4_Un_P3_), dLay4_Lo_P1(dLay4_Lo_P1_), dLay4_Lo_P2(dLay4_Lo_P2_), dLay4_Lo_P3(dLay4_Lo_P3_)
			, dLay5_Un_P1(dLay5_Un_P1_), dLay5_Un_P2(dLay5_Un_P2_), dLay5_Un_P3(dLay5_Un_P3_), dLay5_Lo_P1(dLay5_Lo_P1_), dLay5_Lo_P2(dLay5_Lo_P2_), dLay5_Lo_P3(dLay5_Lo_P3_)		
	{ }

	sStation()
		: Id(1), x(0.0), y(0.0), th(0.0), nRelationNode1(0), nRelationNode2(0), nMaxLayer(0)
			, dLay0_Un_P1(0.0), dLay0_Un_P2(0.0), dLay0_Un_P3(0.0), dLay0_Lo_P1(0.0), dLay0_Lo_P2(0.0), dLay0_Lo_P3(0.0)
			, dLay1_Un_P1(0.0), dLay1_Un_P2(0.0), dLay1_Un_P3(0.0), dLay1_Lo_P1(0.0), dLay1_Lo_P2(0.0), dLay1_Lo_P3(0.0)
			, dLay2_Un_P1(0.0), dLay2_Un_P2(0.0), dLay2_Un_P3(0.0), dLay2_Lo_P1(0.0), dLay2_Lo_P2(0.0), dLay2_Lo_P3(0.0)
			, dLay3_Un_P1(0.0), dLay3_Un_P2(0.0), dLay3_Un_P3(0.0), dLay3_Lo_P1(0.0), dLay3_Lo_P2(0.0), dLay3_Lo_P3(0.0)
			, dLay4_Un_P1(0.0), dLay4_Un_P2(0.0), dLay4_Un_P3(0.0), dLay4_Lo_P1(0.0), dLay4_Lo_P2(0.0), dLay4_Lo_P3(0.0)
			, dLay5_Un_P1(0.0), dLay5_Un_P2(0.0), dLay5_Un_P3(0.0), dLay5_Lo_P1(0.0), dLay5_Lo_P2(0.0), dLay5_Lo_P3(0.0)
	{ }
};


class CStationContainer
{
public:
	CStationContainer();

	CStationContainer(const char *fileName);
	~CStationContainer();

	void Clear();
	bool LoadStationFile(CString strFileName, int nIndex);
	bool SaveStationFile(CString strFileName, sStation tempStation);

	vector<sStation> _stations;

	PINIReadWriter* PINIReaderWriter;
};
