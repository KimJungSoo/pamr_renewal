#include "stdafx.h"
#include "MagnetContainer.h"


CMagnetContainer::CMagnetContainer(const char *fileName)
{
	if(fileName && *fileName)
	{
		LoadMagnetFile(fileName);
	}
}

CMagnetContainer::~CMagnetContainer()
{
	Clear();
}

void CMagnetContainer::Clear()
{
	_magnets.clear();
}

int CMagnetContainer::LoadMagnetFile(const char *fileName)
{
	_magnets.reserve(1000);

	FILE *fp = fopen(fileName, "rt");
	if(!fp) return -1;

	char *p;
	char text[1024 + 1] = "";

	while(fgets(text, 1024, fp))
	{
		text[1024] = '\0';

		for(p = text; (*p == ' ') || (*p == '\t') || (*p == '\r') || (*p == '\n'); ++p);

		if(*p == '\0')		continue;
		if(*p == '#')		continue;		// # 기호는 주석을 의미한다.
		if(*p == ';')						// ; 기호는 새로운 경로의 시작을 의미
		{
			_magnets.push_back(sMagnetNode(true));
			continue;
		}

		int id = -1;

		double x = -1.;
		double y = -1.;

		int pol = -1;

		char name[256] = "";

		sscanf(p, "%d %lf %lf %d %s", &id, &x, &y, &pol, name);
		_magnets.push_back(sMagnetNode(id, x, y, pol, name));
	}

	TRACE("SizeofData(_magnets): %d\n", _magnets.size());

	fclose(fp);

	return 0;
}

int CMagnetContainer::SaveMagnetFile(const char *fileName)
{
	FILE *fp = fopen(fileName, "wt");
	if(!fp) return -1;

	int nIndex = 0;

	for(unsigned int i = 0; i < _magnets.size(); ++i)
	{
		sMagnetNode &magnet = _magnets[i];

		if(magnet.close)
		{
			if(i != 0)
			{
				if(_magnets[i - 1].close == false)
				{
					if(i != (_magnets.size() - 1))
					{
						fprintf(fp, ";\n");
					}
				}
			}
		}
		else
		{
			nIndex++;

			CString strTemp = _T("");

			if(nIndex == 1)
			{
				strTemp.Format(_T("MG%d"), nIndex);
			}
			else if(i == _magnets.size() - 1)
			{
				strTemp.Format(_T("MG%d"), nIndex);
			}
			else if(_magnets[i - 1].close)
			{
				strTemp.Format(_T("MG%d"), nIndex);
			}
			else if(_magnets[i + 1].close)
			{
				strTemp.Format(_T("MG%d"), nIndex);
			}

			fprintf(fp, "%d %10.3f %10.3f %d %ws\n", nIndex, magnet.x, magnet.y, magnet.p, strTemp.GetBuffer());
		}
	}

	fclose(fp);

	return 0;
}
