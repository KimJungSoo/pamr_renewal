#pragma once

#define ROBOT_DIM_FRONT				(0.7)		// m
#define ROBOT_DIM_REAR				(0.7)		// m
#define ROBOT_DIM_WIDTH				(1.)		// m

#define ROBOT_AXLE_LENGTH			(1.)		// m

#define ROBOT_MIN_VELOCITY			(0.03)		// m/s		로봇의 최소 전진 속도
#define ROBOT_MAX_VELOCITY			(2.)		// m/s
#define ROBOT_MAX_ANGULARVEL		(3.14/2)	// rad/s
#define ROBOT_ACCELERATION			(1.0)		// m/s^2
#define ROBOT_DECELERATION			(1.0)		// 0.5// m/s^2	로봇의 전진 감속도
#define ROBOT_CENTRIPETAL_ACCEL		(0.2)
#define ROBOT_VARIANCE_LINEAR		(0.02)		//			전진 이동시 분산 값 (Kalman filter에서 사용됨)
#define ROBOT_VARIANCE_ANGULAR		(0.03)		//			회전 이동시 분산 값 (Kalman filter에서 사용됨)

#define MAX_TRACE_PATH_ERROR		(1.0)		// m		로봇이 경로를 따라 움직일 때, 이 설정값을 벗어나면 경로 복귀 모드로 들어간다.
#define SAFETY_MARGIN_FRONT			(0.5)		// m		로봇이 장애물에 대응하여 정지할 때, 전방 여유 거리 설정
#define SAFETY_MARGIN_SIDE			(0.5)		// m		로봇이 감지하는 측면 장애물의 폭 설정

#define MAX_VEHICLE					110
