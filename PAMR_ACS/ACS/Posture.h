#pragma once
#include "Pose.h"

class CPosture : public CPose {
public:
	double th;

public:
	CPosture() : CPose(0, 0), th(0) { }
	CPosture(const CPosture &p) : CPose(p.x, p.y), th(p.th) { }
	CPosture(double x_, double y_, double th_) : CPose(x_, y_), th(th_) { }

	void Zero()
	{
		CPose::Zero();
		th = 0;
	}

	void Transform(CPose &p) const
	{
		p.Rotate(th);	
		p.Translate(x, y);
	}

	void InverseTransform(CPose &p) const
	{
		p.Translate(-x, -y);
		p.Rotate(-th);	
	}

	void Transform(CPosture &p) const
	{
		p.Rotate(th);	
		p.Translate(x, y);
		p.th += th;
	}

	void InverseTransform(CPosture &p) const
	{
		p.th -= th;
		p.Translate(-x, -y);
		p.Rotate(-th);	
	}

public: 
	double Length3 () const { double mth = DeltaRad(th, 0); return sqrt(x*x + y*y + mth*mth); }
	double Distance3(const CPosture &p) const { double dx = p.x-x, dy = p.y-y, dth = DeltaRad(p.th,th); return sqrt(dx * dx + dy * dy + dth*dth); }

	CPosture & operator += (const CPosture &p) { x += p.x; y += p.y; th += p.th; return *this; }
	CPosture & operator -= (const CPosture &p) { x -= p.x; y -= p.y; th = DeltaRad(th, p.th); return *this; }
};

inline CPosture operator + (const CPosture &l, const CPosture &r) { return CPosture(l.x+r.x, l.y+r.y, l.th+r.th); }
inline CPosture operator - (const CPosture &l, const CPosture &r) { return CPosture(l.x-r.x, l.y-r.y, DeltaRad(l.th, r.th) ); }
inline CPosture operator * (const CPosture &l, double r)		  { return CPosture(l.x*r, l.y*r, l.th*r); }
inline CPosture operator / (const CPosture &l, double r)		  { return CPosture(l.x/r, l.y/r, l.th/r); }
