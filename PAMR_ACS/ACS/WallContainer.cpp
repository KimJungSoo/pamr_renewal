#include "stdafx.h"
#include "WallContainer.h"


CWallContainer::CWallContainer(const char *fileName)
{
	if(fileName && *fileName)
	{
		LoadWallFile(fileName);
	}
}

CWallContainer::~CWallContainer()
{
	Clear();
}

void CWallContainer::Clear()
{
	_walls.clear();
}

int CWallContainer::LoadWallFile(const char *fileName)
{
	_walls.reserve(1000);

	FILE *fp = fopen(fileName, "rt");
	if(!fp) return -1;

	char *p;
	char text[1024 + 1];

	while(fgets(text, 1024, fp))
	{
		text[1024] = '\0';

		for(p = text; (*p == ' ') || (*p == '\t') || (*p == '\r') || (*p == '\n'); ++p);

		if(*p == '\0')		continue;
		if(*p == '#')		continue;		// # 기호는 주석을 의미한다.
		if(*p == ';')						// ; 기호는 새로운 경로의 시작을 의미
		{
			_walls.push_back(sWallNode(true));
			continue;
		}

		double x = -1.;
		double y = -1.;
		double z = -1.;

		sscanf(p, "%lf %lf %lf", &x, &y, &z);
		_walls.push_back(sWallNode(x, y, z));
	}

	TRACE("SizeofData(_walls): %d\n", _walls.size());

	fclose(fp);

	return 0;
}

int CWallContainer::SaveWallFile(const char *fileName)
{
	FILE *fp = fopen(fileName, "wt");
	if(!fp) return -1;

	for(unsigned int i = 0; i < _walls.size(); ++i)
	{
		sWallNode &wall = _walls[i];

		if(wall.close)
		{
			if(i != 0)
			{
				if(_walls[i - 1].close == false)
				{
					if(i != (_walls.size() - 1))
					{
						fprintf(fp, ";\n");
					}
				}
			}
		}
		else
		{
			fprintf(fp, "%10.3f %10.3f %10.3f\n", wall.x, wall.y, wall.z);
		}
	}

	fclose(fp);

	return 0;
}

vector<sMapEntity> CWallContainer::MapTransform(CPosture &pose)
{
	vector<sMapEntity> transformedMap;
	transformedMap.reserve(_entities.size());

	for(unsigned int i = 0; i < _entities.size(); ++i) {
		sMapEntity &entity = _entities[i];
		sMapEntity newEntity;

		newEntity.rho   = entity.rho - (pose.x*cos(entity.alpha) + pose.y*sin(entity.alpha));
		newEntity.alpha = DeltaRad(entity.alpha, pose.th);

		transformedMap.push_back(newEntity);
	}
	return transformedMap;
}
