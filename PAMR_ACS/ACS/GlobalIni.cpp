#include "stdafx.h"
#include "GlobalIni.h"

#pragma warning(disable:4996)

CGlobalIni::CGlobalIni(const char *fileName)
{
	_appName = "Property";

	::GetCurrentDirectoryA(MAX_PATH, _fileName);
	strcat(_fileName, "\\");
	strcat(_fileName, fileName);
}

CGlobalIni::~CGlobalIni()
{
}

void CGlobalIni::LoadString(const char *keyName, char *returnedString, DWORD nSize, char *defaultValue)
{
	GetPrivateProfileStringA(_appName, keyName, defaultValue, returnedString, nSize, _fileName);	
}

int CGlobalIni::LoadInt(const char *keyName, int defaultValue)
{
	return GetPrivateProfileIntA(_appName, keyName, defaultValue, _fileName);
}

unsigned short CGlobalIni::LoadUnsignedShort(const char *keyName, unsigned short defaultValue)
{
	return (unsigned short)GetPrivateProfileIntA(_appName, keyName, (int)defaultValue, _fileName);
}

float CGlobalIni::LoadFloat(const char *keyName, float defaultValue)
{
	char defaultString[64];
	_snprintf(defaultString, 64, "%g", defaultValue);

	char returnedString[128] = "";
	GetPrivateProfileStringA(_appName, keyName, defaultString, returnedString, sizeof(returnedString), _fileName);	
	return (float)atof(returnedString);
}

void CGlobalIni::SaveString(const char *keyName, char * strssi)
{
	WritePrivateProfileStringA(_appName, keyName, strssi, _fileName);
}

void CGlobalIni::SaveInt(const char *keyName, int intData)
{
	char strssi[32+10];
	_snprintf(strssi, 32, "%d", intData);
	WritePrivateProfileStringA(_appName, keyName, strssi, _fileName);
}

void CGlobalIni::SaveFloat(const char *keyName, float floatData)
{
	char strssi[64+10];
	_snprintf(strssi, 64, "%g", floatData);
	WritePrivateProfileStringA(_appName, keyName, strssi, _fileName);
}

void CGlobalIni::Transfer(bool bSave, const char *keyName, char *strssi, DWORD nSize, char *defaultData)
{
	if(bSave)	SaveString(keyName, strssi);
	else		LoadString(keyName, strssi, nSize, defaultData);
}

void CGlobalIni::Transfer(bool bSave, const char *keyName, bool &boolData, bool defaultData)
{
	if(bSave)	SaveInt(keyName, boolData ? 1 : 0);
	else		boolData = LoadInt(keyName, defaultData) ? true : false;
}

void CGlobalIni::Transfer(bool bSave, const char *keyName, int &intData, int defaultData)
{
	if(bSave)	SaveInt(keyName, intData);
	else		intData = LoadInt(keyName, defaultData);
}

void CGlobalIni::Transfer(bool bSave, const char *keyName, long &longData, long defaultData)
{
	if(bSave)	SaveInt(keyName, (int)longData);
	else		longData = (int)LoadInt(keyName, (int)defaultData);
}

void CGlobalIni::Transfer(bool bSave, const char *keyName, DWORD &dwordData, DWORD defaultData)
{
	if(bSave)	SaveInt(keyName, (int)dwordData);
	else		dwordData = (int)LoadInt(keyName, (int)defaultData);
}

void CGlobalIni::Transfer(bool bSave, const char *keyName, float &floatData, float defaultData)
{
	if(bSave)	SaveFloat(keyName, floatData);
	else		floatData = LoadFloat(keyName, defaultData);
}

void CGlobalIni::Transfer(bool bSave, const char *keyName, double &doubleData, double defaultData)
{
	if(bSave)	SaveFloat(keyName, (float)doubleData);
	else		doubleData = (float)LoadFloat(keyName, (float)defaultData);
}

void CGlobalIni::Transfer(bool bSave, const char *keyName, unsigned short &ucData, unsigned short defaultData)
{
	//if (bSave)	SaveInt(keyName, intData);
	//else		intData = LoadInt(keyName, defaultData);
	ucData = LoadUnsignedShort(keyName, defaultData);
}