#include "stdafx.h"
#include "ConstParameters.h"
#include "MultimediaTimer.h"
#include "Mobility.h"


CMobility::CMobility()
{
	_linearVel = 0;
	_angularVel = 0;
}

CMobility::~CMobility()
{
}

double CMobility::VelLimit(double curVel, double tgtVel, double accel, double dt)
{
	if(curVel == tgtVel)
	{
	}
	else if(curVel < tgtVel)
	{
		curVel += accel*dt;
		if(curVel > tgtVel)
			curVel = tgtVel;
	}
	else
	{
		curVel -= accel*dt;
		if(curVel < tgtVel)
			curVel = tgtVel;
	}
	return curVel;
}

void CMobility::Drive (double linearVel, double angularVel, double dt)
{
//	_linearVel = VelLimit(_linearVel, linearVel, ROBOT_ACCELERATION, dt);
//	_angularVel = VelLimit(_angularVel, angularVel, ROBOT_ACCELERATION * 3, dt);
	_linearVel = linearVel;
	_angularVel = angularVel;

	_posture.x += dt * _linearVel * cos(_posture.th);
	_posture.y += dt * _linearVel * sin(_posture.th);
	_posture.th+= dt * _angularVel;
}

void CMobility::QuickStop()
{
	_linearVel = 0;
	_angularVel = 0;
}