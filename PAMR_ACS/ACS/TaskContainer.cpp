#include "stdafx.h"
#include "MapEntity.h"
#include "TaskContainer.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MAX_TODO	100

CTaskContainer::CTaskContainer(const char *fileName)
{
	_storage.reserve(1000);

	if(fileName && *fileName) {
		LoadFile(fileName);
	}
}

CTaskContainer::~CTaskContainer()
{
	Clear();
}

void CTaskContainer::Clear()
{
	_storage.clear();
}

int CTaskContainer::LoadFile(const char *fileName)
{
	FILE *fp = fopen(fileName, "rt");
	if(!fp) return -1;

	char text[1024*20+1];

	while(fgets(text, 1024*20, fp)) {
		text[1024*20] = '\0';

		char *q, *p = skip_space (text);
		if(*p == '\0') continue;
		if(*p == '#') continue;		// # 기호는 주석을 의미한다.

		sTaskInfo node;
		node.todo.reserve(10);

		node.status	= (int)strtod(p, &q);			
		if(p==q) continue;		
		p = skip_space(q); 

		q = strtok (p, " \t\r\n");
		for(int i=0; q && i<MAX_TODO; i++) {
			node.todo.push_back(q);

			q = strtok (NULL, " \t\r\n");
		}
		_storage.push_back(node);
	}	

	fclose (fp);
	return 0;
}
