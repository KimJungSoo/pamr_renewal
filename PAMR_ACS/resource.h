//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// PAMR_ACS.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PAMR_ACS_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_MY_PROGRESS          133
#define IDC_BUTTON_LIFT_UP              1000
#define IDC_MAP                         1001
#define IDC_BUTTON_LIFT_DOWN            1002
#define IDC_BUTTON_BACK_MOVE            1003
#define IDC_BUTTON_AUTONOMOUS_MOVE      1004
#define IDC_BUTTON_GUIDE_MOVE           1005
#define IDC_BUTTON_PRECISE_MOVE         1006
#define IDC_BUTTON_LOAD_MAP             1007
#define IDC_BUTTON_STATION_UNDOCKING_MOVE2 1008
#define IDC_BUTTON_STATION_UNDOCKING_MOVE 1008
#define IDC_STATIC_VOLTAGE              1009
#define IDC_BUTTON_ROBOT_INIT_0         1010
#define IDC_BUTTON_ROBOT_INIT_1         1011
#define IDC_BUTTON_ROBOT_INIT_2         1012
#define IDC_BUTTON_ROBOT_INIT_3         1013
#define IDC_VIEWCENTER                  1014
#define IDC_ROBOTCENTER                 1015
#define IDC_ROBOTVIEWCHECK              1016
#define IDC_BUMPER_RELEASE              1017
#define IDC_CHARGE                      1018
#define IDC_UNCHARGE                    1019
#define IDC_PATH_PLAN                   1020
#define IDC_PROGRESS_MY_PROGRESS        1021
#define IDC_ODO_RESET                   1021
#define IDC_STATIC_MY_PROGRESS_STATUS   1022
#define IDC_MOTOR_AUTO                  1022
#define IDC_BUTTON_MY_PROGRESS_START    1023
#define IDC_PARTICLE_X                  1023
#define IDC_BUTTON_MY_PROGRESS_STOP     1024
#define IDC_PARTICLE_Y                  1024
#define IDC_BUTTON_MY_PROGRESS_CANCLE   1025
#define IDC_PARTICLE_th                 1025
#define IDC_BUTTON_FORWARD_MOVE         1026
#define IDC_BUTTON_TURNING              1027
#define IDC_LIST1                       1028
#define IDC_LIST_EVENTMANAGER           1028
#define IDC_EDIT_TO_NODE                1029
#define IDC_BUTTON_AMRSYSTEM_INIT       1030
#define IDC_EDIT_AMRSYSTEM_STATE        1031
#define IDC_BUTTON_AMR_RIGHT            1032
#define IDC_BUTTON_AMR_LEFT             1033
#define IDC_BUTTON_AMR_UP               1034
#define IDC_BUTTON_AMR_DOWN             1035
#define IDC_EDIT_AMR_POSITION           1036
#define IDC_EDIT1                       1037
#define IDC_EDIT2                       1038
#define IDC_EDIT_AMRSYSTEM_V            1038
#define IDC_EDIT_AMRSYSTEM_W            1039
#define IDC_EDIT_GYRO                   1049
#define IDC_BUTTON_ERROR_RESET          1050
#define IDC_EDIT3                       1051
#define IDC_EDIT_PRECISEMOVE_TARGETLINE 1051
#define IDC_BUTTON1                     1052
#define IDC_BUTTON_GUIDEMOVE            1052

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1053
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
