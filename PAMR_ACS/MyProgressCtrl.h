#pragma once

#include "resource.h"

#define ID_TIMER_MY_PROGRESS_STEP_CHECK			2017010101
#define ID_TIMER_MY_PROGRESS_STATUS_CHECK		2017010102

////////////////////////////////////////////////////////////////////////////////////////////////////

// CMyProgressCtrl 대화 상자입니다.

class CMyProgressCtrl : public CDialog
{
	DECLARE_DYNAMIC(CMyProgressCtrl)

public:
	CMyProgressCtrl(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMyProgressCtrl();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_MY_PROGRESS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

////////////////////////////////////////////////////////////////////////////////////////////////////

public:
	CProgressCtrl m_ctrlProgressBar;
	int m_nStart;
	int m_nEnd;
	int m_nStep;
	int m_nPos;

	CString m_strProgressStatus;

	bool m_bTimerStart_ProgressBar;
	bool m_bTimerStart_ProgressStatus;

	bool m_bCancellable;
	bool m_bCancel;

////////////////////////////////////////////////////////////////////////////////////////////////////

	virtual BOOL OnInitDialog();

	BOOL Set(CString strDialogTitle, int nStart, int nEnd, int nStep, bool bCancellable);
	BOOL Set(CString strDialogTitle, int nStart, int nEnd, int nStep, bool bCancellable, bool bTest);

	void SetProgressPos(int nPos);
	void SetProgressStatus(CString strProgressStatus);

	bool m_bStart;
	BOOL Start();
	BOOL Stop();

	afx_msg void OnTimer(UINT_PTR nIDEvent);

////////////////////////////////////////////////////////////////////////////////////////////////////

	afx_msg void OnBnClickedButtonMyProgressStart();
	afx_msg void OnBnClickedButtonMyProgressStop();
	afx_msg void OnClose();
	afx_msg void OnBnClickedButtonMyProgressCancle();
};
