
/**
@file SSV_OpenGLViwer.h
@date 2019/08/26
@author Soon ( sgchoi@potenit.com )
@version 1.0
@brief OpenGL Windwos ClassVersion DLL
*/


#ifdef DLL_EXPORTS_SSV_OPENGLVIEWER
#define DllExport   __declspec( dllexport )
#else
#define DllExport   __declspec( dllimport )
#endif // DLL_EXPORTS_SSVLIVESTREAMING

#pragma once
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <glext.h>         // OpenGL extensions
#include <glInfo.h>        // glInfo struct
#include <WaveFunc.h>
#include <Timer.h>
#include <mutex>
#include <opencv2\opencv.hpp>
#include "../PAMR_ACS/robot/PTN_DataStruct.h"

#pragma comment(lib,"opencv_core2413d")
#pragma comment(lib,"opencv_highgui2413d")

#define START_Y 0.f

extern "C"
class DllExport SSV_OpenGLViwer
{
public:
	SSV_OpenGLViwer();
	~SSV_OpenGLViwer();

	void StartOpenGLViewer();
private:
	// constants
	const int   SCREEN_WIDTH = 1280;
	const int   SCREEN_HEIGHT = 720;
	const float CAMERA_DISTANCE = 200.0f;
	const int   TEXT_WIDTH = 8;
	const int   TEXT_HEIGHT = 15;

	// global variables
	void	*font = GLUT_BITMAP_9_BY_15;
	int		screenWidth;
	int		screenHeight;
	bool	mouseLeftDown;
	bool	mouseRightDown;
	float	mouseX, mouseY;
	float	cameraAngleX;
	float	cameraAngleY;
	float	cameraDistance;
	float	drawTime, updateTime;

private: // value
	std::mutex m_DataMutex;
	int m_iDataScale = 1;
	float m_fTX = 0.0f, m_fTY = START_Y;
	float m_fGobardMapCenterX = 0.f, m_fGobardMapCenterY = 0.f;

	//여기만 Start
private://Data Value
	ObstacleZone m_StopZone, m_WarningZone[4];
	AMRModel m_AMR_Model;
	std::vector<PTN_Points> m_vSickPoints;

public: //Set/Get
	void SetDataScale(int _iData);
	void SetGoBoardCenter(float _fX, float _fY);
	void SetStopZone(ObstacleZone _Stop);
	void SetWarningZone(ObstacleZone* _Warning);
	void SetAMR_Model(AMRModel _model);
	void SetPoints(std::vector<PTN_Points> _points);

private://funtion
	void OpenGLMain(); //opengl start

	void GradationBackGround();	//배경
	void DrawXYZ_Line();		//기준축 축 끝에 원뿔 추가
	void DrawGO_Board();		//바닥 바둑모양 그리기
	void DrawShowInfo();		//정보 출력

	void DrawAMRModel();		//AMR 모델
	void DrawZone();			//zone Draw
	void DrawSickPoints();
	/////////////////////////Obstacle ADD


	//여기만 end
private://opengl 관련함수
	void initGL();
	int  initGLUT(int argc, char **argv);
	bool initSharedMem();
	void initLights();
	void setCamera(float posX, float posY, float posZ, float targetX, float targetY, float targetZ);
	void drawString(const char *str, int x, int y, float color[4], void *font);
	void drawString3D(const char *str, float pos[3], float color[4], void *font);

	void toOrtho();
	void toPerspective();

public:	// GLUT CALLBACK functions
	static void displayCB(); //display callback
	static void reshapeCB(int w, int h);
	static void timerCB(int millisec);
	static void idleCB();
	static void keyboardCB(unsigned char key, int x, int y);
	static void mouseCB(int button, int stat, int x, int y);
	static void mouseMotionCB(int x, int y);
};
