#include "stdafx.h"

#include <time.h>
#include "SensorModule.h"

using namespace sensor;
using namespace eventManager;
using namespace std;

CSensorModule::CSensorModule(string name) :m_strSensorName(name) {
	m_bStateMachineThreadLoop = false;
	m_eState = SENSOR_STATE_INIT;
	m_nThreadPeriod = 100;
	m_nDataCheckCount = 3;
	m_nTerminateTimeout = 1000;
}
CSensorModule::~CSensorModule() {
	std::cout << "~" << m_strSensorName.c_str() << std::endl;
	m_bStateMachineThreadLoop = false;
	// Thread Terminate
	if (m_StateMachineThread.joinable())
	{
		if (m_StateMachineThread.joinable())
			m_StateMachineThread.join();
	}
	// Memory clear
	AutoCSLock cs(m_cs);

	//m_cmds = {};	
	while (!m_cmds.empty()) m_cmds.pop();
}

// State Machine Functions
int CSensorModule::ThreadFunction_StateMachine(CSensorModule *sensor) {
	int nReconnectionCount = 0;
	int nDataCheckCount = 0;
	int iSensorErrorcount = 0;
	//SG_Check 20190906
	int icheck = 0;


	std::cout << "Thread start : " << sensor->getSensorName().c_str() << " - ThreadFunction_StateMachine" << std::endl;
	while (sensor->m_bStateMachineThreadLoop) {
		//std::cout << sensor->m_strSensorName.c_str() << std::endl;
		// Clock Start
		clock_t start, runtime;
		start = clock();

		// State Machine
		if (!sensor->m_cmds.empty()) {
			eSensorCommand cmd = sensor->PopCommand();
			switch (sensor->m_eState) {
			case SENSOR_STATE_INIT:
				if (cmd == SENSOR_CMD_CONNECT) {
					sensor->m_eState = SENSOR_STATE_PROGRESSING;
					// ConnectAct가 성공하면 Run, 아니면 Error
					if (sensor->ConnectAct() == RETURN_NONE_ERROR) {
						sensor->m_eState = SENSOR_STATE_RUN;
					}
					else {
						sensor->LogError(SENSOR_CONNECT_FAILED);
						sensor->m_eState = SENSOR_STATE_ERROR;
					}
					start = clock();
				}
				break;
			case SENSOR_STATE_PROGRESSING:
				if (cmd == SENSOR_CMD_ERROR)
					sensor->m_eState = SENSOR_STATE_ERROR;
				break;
			case SENSOR_STATE_RUN:
				// 센서 Thread 생성
				if (cmd == SENSOR_CMD_DISCONNECT) {
					sensor->m_eState = SENSOR_STATE_PROGRESSING;
					if (sensor->DisconnectAct() == RETURN_NONE_ERROR) {
						sensor->m_eState = SENSOR_STATE_INIT;
					}
					else {
						sensor->LogError(SENSOR_DISCONNECT_FAILED);
						sensor->m_eState = SENSOR_STATE_ERROR;
					}
					start = clock();
				}
				else if (cmd == SENSOR_CMD_ERROR)
					sensor->m_eState = SENSOR_STATE_ERROR;
				break;
			case SENSOR_STATE_ERROR:
				if (cmd == SENSOR_CMD_RESET) {
					sensor->m_eState = SENSOR_STATE_PROGRESSING;
					// Reset이 성공하면 , 아니면 Error
					if (sensor->ResetAct() == RETURN_NONE_ERROR) {
						g_eventManager.clearError(sensor->getSensorName());
						sensor->m_eState = SENSOR_STATE_RUN;
					}
					else {
						sensor->m_eState = SENSOR_STATE_INIT;
					}
					start = clock();
				}
				else if (cmd == SENSOR_CMD_DISCONNECT) {
					sensor->m_eState = SENSOR_STATE_PROGRESSING;
					sensor->DisconnectAct();
					sensor->m_eState = SENSOR_STATE_INIT;
					start = clock();
				}
				break;
			}
		}

		// Run 상태일 때만 데이터 업데이트
		if (sensor->getStatus() == SENSOR_STATE_RUN) {
			int errorCode = sensor->UpdateData();
			// Get data 정상동작
			if (errorCode == RETURN_NONE_ERROR)
				iSensorErrorcount = nDataCheckCount = 0;
			// 데이터가 밀려들어와서 통신 프로토콜과 맞지 않는 경우
			else if (errorCode == RETURN_DATA_CHECK_FAILED)
			{
				// 버퍼 클리어???
				nDataCheckCount++;
				std::cout << nDataCheckCount << std::endl;
				if (nDataCheckCount >= sensor->m_nDataCheckCount) {
					nDataCheckCount = 0;
					sensor->LogError(SENSOR_DATA_CHECK_FAILED);
					sensor->OccurError();
					TRACE("RETURN_DATA_CHECK_FAILED :%s\n", sensor->getSensorName().c_str());
					std::cout << "RETURN_DATA_CHECK_FAILED :" << sensor->getSensorName().c_str() << std::endl;
					break;
				}
			}
			// 데이터를 Thread 주기 안에 받지 못했을 경우
			else {
				iSensorErrorcount++;
				std::cout << iSensorErrorcount << std::endl;
				if (iSensorErrorcount >= 3) {
					std::cout << "쓰레드 주기 out :" << sensor->getSensorName().c_str() << std::endl;
					TRACE("쓰레드 주기 out :%s\n", sensor->getSensorName().c_str());
					sensor->LogError(SENSOR_DATA_DELAYED);
					sensor->OccurError();
					break;
				}
			}
		}

		// Keep the Thread Period
		runtime = clock() - start;
		if (sensor->m_nThreadPeriod - runtime > 0)
			Sleep(sensor->m_nThreadPeriod - runtime);
		else {
			TRACE("runtime:%d\n", runtime);
			std::cout << "Time Out " << sensor->getSensorName().c_str() << std::endl;
			sensor->LogError(SENSOR_THREAD_DELAYED);
			sensor->OccurError();
			break;
		}
		//SG_ADD Check
		//icheck++;
		//if (icheck == 200)
		//{
		//	icheck = 0;
		//	TRACE("runtime:%d\n", runtime);
		//	sensor->LogError(SENSOR_THREAD_DELAYED);
		//	sensor->OccurError();
		//	break;
		//}
	}
	//20190906 SG_ADD

	std::cout << "Thread End : " << sensor->getSensorName().c_str() << " - ThreadFunction_StateMachine " << std::endl << "status : " << sensor->getStatus() << std::endl;
	Sleep(10);
	return RETURN_NONE_ERROR;
}
int CSensorModule::PushCommand(eSensorCommand input) {
	if (m_cmds.size() > 5)
		return RETURN_FAILED;
	m_cs.Lock();
	m_cmds.push(input);

	// thread start
	if (!m_StateMachineThread.joinable()) {
		Sleep(50);
		m_bStateMachineThreadLoop = true;
		m_StateMachineThread = std::thread(ThreadFunction_StateMachine, this);
	}
	m_cs.Unlock();
	return RETURN_NONE_ERROR;
}
eSensorCommand CSensorModule::PopCommand()
{
	m_cs.Lock();
	eSensorCommand cmd = m_cmds.front();
	m_cmds.pop();
	m_cs.Unlock();
	return cmd;
}
void CSensorModule::Connect() { PushCommand(SENSOR_CMD_CONNECT); }
void CSensorModule::Reset() { PushCommand(SENSOR_CMD_RESET); }
void CSensorModule::Disconnect() { PushCommand(SENSOR_CMD_DISCONNECT); }
void CSensorModule::OccurError() { PushCommand(SENSOR_CMD_ERROR); }
void CSensorModule::ThreadStop(){ m_bStateMachineThreadLoop = false;}

// Getter & Setter
int CSensorModule::getStatus() { return m_eState; }
int CSensorModule::getThreadPeriod() { return m_nThreadPeriod; }
int CSensorModule::getDataCheckCount() { return m_nDataCheckCount; }
std::string CSensorModule::getSensorName() { return m_strSensorName; }
int CSensorModule::setThreadPeriod(int nPeriod) {
	if (getStatus() == SENSOR_STATE_RUN)
		return RETURN_FAILED;
	if (nPeriod < 0 || nPeriod > 200)
		return RETURN_FAILED;
	m_nThreadPeriod = nPeriod;
	return RETURN_NONE_ERROR;
}
int CSensorModule::setDataCheckCount(int nCount) {
	if (getStatus() == SENSOR_STATE_RUN)
		return RETURN_FAILED;
	if (nCount < 0 || nCount > 10)
		return RETURN_FAILED;
	m_nDataCheckCount = nCount;
	return RETURN_NONE_ERROR;
}
void CSensorModule::setSensorName(std::string s) { m_strSensorName = s; }