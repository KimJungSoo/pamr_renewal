#pragma once
#include <queue>
#include <string>
#include <thread>
#include "util\CriticalSection.h"

namespace sensor {
	enum eSensorCommand {
		SENSOR_CMD_CONNECT,
		SENSOR_CMD_RESET,
		SENSOR_CMD_DISCONNECT,
		SENSOR_CMD_ERROR,
	};
	
	enum eSensorStatus {
		SENSOR_STATE_INIT,
		SENSOR_STATE_PROGRESSING,
		SENSOR_STATE_RUN,
		SENSOR_STATE_ERROR,
	};

	enum eSensorErrorCode {
		SENSOR_CONNECT_FAILED,
		SENSOR_DISCONNECT_FAILED,
		SENSOR_DATA_CHECK_FAILED,
		SENSOR_DATA_DELAYED,
		SENSOR_THREAD_DELAYED,
	};

	class CSensorModule {
	private:
		// State Machine ���� ���
		eSensorStatus m_eState;
		std::thread m_StateMachineThread;
		bool m_bStateMachineThreadLoop;
		std::queue<eSensorCommand> m_cmds;
		CriticalSection m_cs;
		static int ThreadFunction_StateMachine(CSensorModule *); // �񵿱� Thread

																 // Thread ���� ���
		int m_nThreadPeriod;
		int m_nDataCheckCount;
		int m_nTerminateTimeout;
	protected:
		std::string m_strSensorName;

		virtual int ConnectAct() = 0;
		virtual int ResetAct() = 0;
		virtual int UpdateData() = 0;
		virtual int DisconnectAct() = 0;
		virtual int LogError(int errorCode) = 0;
		eSensorCommand PopCommand();
		int PushCommand(eSensorCommand);

	public:
		CSensorModule(std::string);
		virtual ~CSensorModule() = 0;
		void OccurError();
		void Connect();
		void Reset();
		void Disconnect();
		void ThreadStop();

		// Getter & Setter
		int getStatus();
		int getThreadPeriod();
		int getDataCheckCount();
		std::string getSensorName();
		int setThreadPeriod(int p);
		void setSensorName(std::string s);
		int setDataCheckCount(int);
	};
}