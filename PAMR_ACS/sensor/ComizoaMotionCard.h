#pragma once

#include "SensorModule.h"
#include "driver/CmmsdkDef.h"

namespace sensor
{
	enum eMotorAxis
	{
		//Conveyor등 추후에 추가
		REV_LIFT_MOTOR = 0,
		FWD_LIFT_MOTOR,
		RIGHT_DRIVE_MOTOR,
		LEFT_DRIVE_MOTOR,
	};
	enum eAccelMode
	{
		eKeep = -1,
		eConstant,
		eTrapeziodal,
		eS_Curve,
	};
	enum eCounterName
	{
		eCounter_Command = 0,
		eCounter_Feed,
		eCounter_Deviation,
		eCounter_General,
		eCoutner_Remained,
	};

	class CComizoaMotionCard : public CSensorModule
	{
	private:
		int ConnectAct() override;
		int ResetAct() override;
		int UpdateData() override;
		int DisconnectAct() override;
		int LogError(int errorCode) override;

		long m_nNumAxes;

		double * m_dEncoder;
		double * m_dSpeed;
		double * m_dAccel;
		double * m_dDecel;


	public:
		CComizoaMotionCard(std::string);
		~CComizoaMotionCard();

		bool m_bConnected;
		int SetDataTimeout(int nTimeout);
		int SetConnectionTimeout(int nTimeout);
		int SetReconnectionCount(int iCount);
		int GetConnectionTimeout() { return m_iConnectionTimeout; }
		int GetReconnectionCount() { return m_iReconnectionCount; }
		int GetDataTimeout() { return m_iDataTimeout; }

		int m_iThreadPeriod;
		bool isDoneMotion(eMotorAxis eAxis);
		int MotionStop(bool bEMG, eMotorAxis eAxis, bool isWaiting, bool isBlocking);
		int SetMotionSpeed(eMotorAxis eAxis, int lSpeedMode, double dWorkSpeed, double dAccel, double dDecel);
		int SetMotionSpeed(int eAccelMode, double left, double right);

		int InitializeFromFile();

		int GetDriveMotorVelocity(double *left, double *right);
		int SetLiftMotorCount(int iCount) { return m_iLiftMotorCount = iCount; }
		int GetLiftMotorCount() { return m_iLiftMotorCount = 1; }
		int SetServoOnOff(eMotorAxis eAxis, bool bOn);
		int SetPosition(eMotorAxis eAxis, eCounterName eCounter, double dPos);
		int ResetPosition(eMotorAxis eAxis);
		int GetPosition(eMotorAxis eAxis, eCounterName eCounter, double* dCurPos);
		int InitDrivingMotorSpeed();
		int SetDriveWheelCount();
		int InitLiftMotor();
		int LiftMotorHoming();
		int LiftUpDown(bool bUpside);
		int CheckEncodeerValue(double dInputEncoder, double *dEncoderL, double *dEncoderR);
		int ConvertVWToRPM(double dVelocity, double dW, double *dResLeftRPM, double *dResRightRPM);

		//CMD 
		int Move(double dVelocity, double dW);
		int Rotate(double dVelocity, double dW);
		int AMRStop(bool bEMG);


		long m_lLeftMotorDirection, m_lRightMotorDirection;
		bool m_bDriveMotorBrake = true;  //On : True, Off : False
		bool m_bLiftMotorBrake = true; //On : True, Off : False
		double m_dLiftUpLimint;
		double m_dLiftDownLimit;
		int m_iDataTimeout;
		int m_iConnectionTimeout;
		int m_iLiftMotorCount;
		int m_iReconnectionCount;
		double m_dGyroTargetAngle;

		// Getter & Setter
		double getEncoder(long Axis);
		double getSpeed(long Axis);
		double getAccel(long Axis);
		double getDecel(long Axis);
	};
}