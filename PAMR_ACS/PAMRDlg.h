
// PAMR_ACSDlg.h : 헤더 파일
//

#pragma once

#include "resource.h"
#include "afxwin.h"
#include "robot\AMRSystem.h"
#include "sensor\SensorModule.h"
#include "opencv2\opencv.hpp"
#include <atlimage.h>

// CPAMRDlg 대화 상자
class CPAMRDlg : public CDialogEx
{
// 생성입니다.
public:
	CPAMRDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PAMR_ACS_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
	AMRSystem::CAMRSystem m_AMRSystem;
	sensor::CSensorModule ** m_sensor;
	cv::Mat m_img, m_mapImg;
	ATL::CImage m_imageMfc;

	std::vector<std::vector<eventManager::CEventNode>> m_ErrorList;
	std::vector<POINT> m_Landmarks;
	std::vector<AMRmove::PointObject> m_PointObject;

	int m_nImageRange, m_nMaxRange;
	int m_nKeyV, m_nKeyW;

	void ScannerToRobotCoor(RobotPose sensorPos, LaserScanData scanData, int index, int *X, int *Y);
	void RobotCoorToWorldCoor(RobotPose curPos, int nRobotX, int nRobotY, int *nImageX, int *nImageY);

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL DestroyWindow();
	CListBox m_listEventManager;
	CEdit m_editToNode;
	afx_msg void OnBnClickedButtonAutonomousMove();
	afx_msg void OnBnClickedButtonAmrsystemInit();
	CEdit m_editAMRSystemState;
	CEdit m_editAMRPosition;
	CEdit m_editDestIndex;
	CEdit m_editAMRSystemV;
	CEdit m_editAMRSystemW;

	void DrawImage();
	CStatic PIctureControlMap;
	BOOL PreTranslateMessage(MSG* pMsg);
	CButton m_buttonPreciseMove;
	afx_msg void OnBnClickedButtonPreciseMove();
	CEdit m_editGyro;
	afx_msg void OnBnClickedButtonErrorReset();
	CEdit m_editTargetLine;
	afx_msg void OnBnClickedButtonGuidemove();
	afx_msg void OnBnClickedButtonAmrLeft();
};
