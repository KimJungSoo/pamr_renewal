#pragma once

//#ifdef WIN32   // Windows system specific
//	#include <windows.h>
//#else          // Unix based system specific
//	#include <sys/time.h>
//#endif

using namespace std;

class PTimer
{

private:
    double startTimeInMicroSec;                 // starting time in micro-second
    double endTimeInMicroSec;                   // ending time in micro-second
    int    stopped;                             // stop flag 


    double m_dStartTimeInMicroSec;
    double m_dEndTimeInMicroSec;

#ifdef WIN32
    LARGE_INTEGER m_Frequency;                    // ticks per second
    LARGE_INTEGER m_StartCount;                   //
    LARGE_INTEGER m_EndCount;                     //
#else
    timeval m_tvStartCount;                         //
    timeval m_tvEndCount;                           //
#endif


private:
    void   start();                             // start timer
    void   stop();                              // stop the timer

public:
    PTimer();                                    // default constructor
    ~PTimer();                                   // default destructor
    void sleepMS(int nTimeMS); //sleep ms sec.
    void checkStartTime();
    double checkEndTime();

protected:
};
