#pragma once
/// Auto Lock, Unlock
template<class T>
class AutoLock
{
public:
	AutoLock(T& t) : m_t(t) { m_t.Lock(); }
	~AutoLock() { m_t.Unlock(); }
	T &m_t;
};

class CMyCriticalSection
{
public:
	CMyCriticalSection();
	~CMyCriticalSection();
	void Lock();
	void Unlock();
protected:
	CRITICAL_SECTION m_cs;
};

inline CMyCriticalSection::CMyCriticalSection() {
	InitializeCriticalSection(&m_cs);
}
inline CMyCriticalSection::~CMyCriticalSection() {
	DeleteCriticalSection(&m_cs);
}
inline void CMyCriticalSection::Lock() {
	EnterCriticalSection(&m_cs);
}
inline void CMyCriticalSection::Unlock() {
	LeaveCriticalSection(&m_cs);
}
/*
class AutoCSLock : public AutoLock<CMyCriticalSection>
{
public:
	AutoCSLock(CMyCriticalSection &cs) : AutoLock(cs) { }
};*/