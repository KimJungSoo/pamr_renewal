#include "stdafx.h"
#include "PMath.h"


PMath::PMath()
{
	srand((unsigned)time(NULL)); //랜덤 씨드를 항상 바꿔주기 위해서 설정해야한다.
}

PMath::~PMath()
{

}
double PMath::AI2MM(double dVal)
{ 
	return dVal*GIRD_RESOLUTION*CM2MM(1); 
}

double PMath::MM2AI(double dVal)
{ 
	return dVal/100; 
}

double PMath::MM2CM(double dVal)
{ 
	return dVal/10.; 
}
// double PMath::M2CM(double dVal)
// { 
// 	return dVal*100; 
// }

double PMath::CM2MM(double dVal)
{ 
	return dVal*10; 
}

double PMath::getPI()
{
	return 3.141592;
}

double PMath::getRad(double dDeg )
{
	return dDeg*3.141592/180.;
}
double PMath::getDeg(double dRad )
{
	return dRad*180./3.141592;
}
/*
RobotPose PMath::getTransformedPose(double dDist, RobotPose SensorConfiguration, RobotPose RobotPose1)
{
	RobotPose TransformedPos;

	double dGlobalX = cos(RobotPose1.getThetaRad())*(dDist*cos(SensorConfiguration.getThetaRad())+SensorConfiguration.getX())
		- sin(RobotPose1.getThetaRad())*(dDist*sin(SensorConfiguration.getThetaRad())+SensorConfiguration.getY())
		+ RobotPose1.getX();
	double dGrobalY = sin(RobotPose1.getThetaRad())*(dDist*cos(SensorConfiguration.getThetaRad())+SensorConfiguration.getX())
		+ cos(RobotPose1.getThetaRad())*(dDist*sin(SensorConfiguration.getThetaRad())+SensorConfiguration.getY())
		+ RobotPose1.getY();

	TransformedPos.setX(dGlobalX);
	TransformedPos.setY(dGrobalY);

	return TransformedPos;
}
*/
double PMath::changeAngle(double ThetaRad)
{
	if(ThetaRad>PI)			ThetaRad += -2*PI;
	else if(ThetaRad<-PI)		ThetaRad +=  2*PI;

	return ThetaRad;
}


void PMath::transformRangeDataToLocalCoordinate(int* nInputLaserssi, double dTiltDeg,RangeData* LocalCoordinateData)
{
	// 레이저스캐너에서 받은 거리데이터에 각종 offset을 고려하여 
	// 최종적으로는 로봇 중심으로부터 계산된 x,y,z를 돌려준다.
	// 이 부분에서 각종 offset이 정확히 계산되어야 한다.
	// visio 화일 참고. LaserOffset.vsd

// 	const int LASER_MAX_DIST =CSystemParameter::getInstance()->getUpperLaserMaxDist();// CSensorConfiguration::URG_04LX_LASER_MAX_DIST;
// 	const int LASER_MIN_DIST = CSystemParameter::getInstance()->getUpperLaserMinDist(); //unit mm
// 	int nScanIdx = 181;//CSensorConfiguration::URG_04LX_LASER_SCAN_IDX;
// 	double dX_LASER_OFFSET = CSystemParameter::getInstance()->getUpperLaserXOffset();//CSensorConfiguration::URG_04LX_LASER_X_LASER_OFFSET;
// 	double dZ_LASER_OFFSET = CSystemParameter::getInstance()->getUpperLaserHeight();//CSensorConfiguration::URG_04LX_LASER_Z_LASER_OFFSET;
// 	double dX_TILT_OFFSET = CSystemParameter::getInstance()->getTiltMotorXOffset();//CSensorConfiguration::X_TiltOffset;
// 	double dZ_TILT_OFFSET =CSystemParameter::getInstance()->getTiltMotorHeight();//CSensorConfiguration::Z_TiltOffset;
// 	double dTiltRad = dTiltDeg*-1*D2R;
// 
// 
// 	double dX,dY,dZ;
// 
// 	for (int i=0; i<nScanIdx; i++){   
// 		if(nInputLaserssi[i] > (double) LASER_MAX_DIST || 
// 			nInputLaserssi[i] < LASER_MIN_DIST
// 			){ // 레이저스캐너의 유효거리를 벗어나면.... 단위 파악 잘..
// 
// 				LocalCoordinateData[i].x = 0;
// 				LocalCoordinateData[i].y = 0;
// 				LocalCoordinateData[i].z = 0;
// 				continue;
// 		}
// 		dX = nInputLaserssi[i]*cos((i-90)*D2R );
// 		dY = nInputLaserssi[i]*sin((i-90)*D2R );
// 		dZ = 0;
// 
// 		// 한 방에 계산하면....
// 		LocalCoordinateData[i].x = (dX + dX_LASER_OFFSET)*cos(dTiltRad) + (dZ + dZ_LASER_OFFSET)*sin(dTiltRad) + dX_TILT_OFFSET;
// 		LocalCoordinateData[i].y = dY;
// 		LocalCoordinateData[i].z = (dX + dX_LASER_OFFSET)*sin(-dTiltRad) + (dZ + dZ_LASER_OFFSET)*cos(dTiltRad) + dZ_TILT_OFFSET;   
// 
// 	}
}

/**
 @brief Korean: 2*sigma ~ 2*sigma 범위 내에서 임의의 값을 리턴하는 함수.
 @brief English: return random value from -2*sigma to 2*sigma
 */
double PMath::calcRandomValue(double dSig)
{
	return dSig*2.0*((double)rand()/(double)RAND_MAX - 0.5);
}


/**
@brief Korean: 정규 확률밀도 함수를 구하는 함수. 
			   평균 0, sigma인 가우시안 분포를 따르는 값 출력.
@brief English: 
*/
double PMath::calcNormalProbabilityDensity(double dX, double dSig)
{
	if(dSig==0) return 0;

	return 1./sqrt(2*PI*dSig*dSig) * exp(-dX*dX/2.0/dSig/dSig);
}
/**
@brief Korean:계산시간 단축을 위해 변형된 정규 확률밀도 함수를 구하는 함수. 
			  평균 0, sigma인 가우시안 분포를 따르는 값 출력.
@brief English: 
*/
double PMath::calcSimpleNormalProbabilityDensity(double dX, double dSig)
{
	if(dSig==0) return 0;

	return exp(-dX*dX/2.0/dSig/dSig);
}

/**
@brief Korean: 입력받은 두 지점간의 거리를 계산하는 함수. 입력값 mm, 리턴값은 mm
@brief English: 
*/
double PMath::calcDistBetweenPoses(double dPosX1mm, double dPosX2mm, double dPosY1mm, double dPosY2mm)
{
	return sqrt( pow(( dPosX1mm - dPosX2mm), 2) +  pow(( dPosY1mm - dPosY2mm ), 2));
}


/**
@brief Korean: 거리센서를 통해 입력받은 데이터(polar coodinate 기준 거리(mm), 각도)를 
			   Cartesian coordiate기준(x,y)로 바꿔주는 함수. input--> 거리, 각도, 센서 offset
			   input MM단위거리, 각도(deg)
			   output: PCartesianCoordinate2D type (data class)
@brief English: 
*/
PCartesianCoordinate2D PMath::transfromPolar2CartesianMM(double dDistMM, int nThetaDeg, double dSensorOffsetMM)
{
	PCartesianCoordinate2D CartesianCoordinate;

	double dXmm = dDistMM * cos((double)nThetaDeg * D2R ) + dSensorOffsetMM;
	double dYmm = dDistMM * sin((double)nThetaDeg * D2R );

	CartesianCoordinate.setXmm( dXmm );
	CartesianCoordinate.setYmm( dYmm );
	
	return CartesianCoordinate;
}

/**
@brief Korean: 거리센서를 통해 입력받은 데이터(polar coodinate 기준 거리(m), 각도)를 
			   Cartesian coordiate기준(x,y)로 바꿔주는 함수. input--> 거리, 각도, 센서 offset
			   input M단위거리, 각도(deg)
			   output: PCartesianCoordinate2D type (data class)
@brief English: 
*/
PCartesianCoordinate2D PMath::transfromPolar2CartesianM(double dDistM, int nThetaDeg, double dSensorOffsetM)
{
	PCartesianCoordinate2D CartesianCoordinate;

	double dXm = dDistM * cos((double)nThetaDeg * D2R ) + dSensorOffsetM;
	double dYm = dDistM * sin((double)nThetaDeg * D2R );

	CartesianCoordinate.setXm( dXm );
	CartesianCoordinate.setYm( dYm );
	
	return CartesianCoordinate;
}


double PMath::getDegDiff(double dDeg1, double dDeg2)
{
	double dSub = 0.0;
	double dResult = 0.0;
	dSub = dDeg1 - dDeg2;

	if (dSub >= 180)
	{
		dResult = 360 - dSub;
	}
	else if (dSub <= -180)
	{
		dResult = 360 + dSub;
	}
	else
	{
		dResult = dSub;
	}

	dResult = fabs(dResult);

	return dResult;
}

double PMath::getDegDiffSign(double dDeg1, double dDeg2)
{
	double dSub = 0.0;
	double dResult = 0.0;

	if (dDeg1<0)
	{
		dDeg1 = 180 + fabs(180 + dDeg1);
	}
	if (dDeg2<0)
	{
		dDeg2 = 180 + fabs(180 + dDeg2);
	}

	dResult = dDeg1 - dDeg2;

	return dResult;
}