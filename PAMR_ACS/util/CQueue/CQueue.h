﻿#pragma once


// Stop bits
#define	RS_STOP1	0
#define	RS_STOP1_5	1
#define	RS_STOP2	2

// Parity bits
#define	RS_PARITY_NONE	0	// 'N'
#define	RS_PARITY_ODD	1	// 'O'
#define	RS_PARITY_EVEN	2	// 'E'
#define	RS_PARITY_MARK	3	// 'M'
#define	RS_PARITY_SPACE	4	// 'S'

//예외 처리(Exception Response-Error Code)
//드라이버에서 보내는 Error code
#define ERR_ILLEGAL_FUNCTION		1	//지원하지 않는 명령
#define ERR_ILLEGAL_DATA_ADDRESS	2	//요청한 데이터의 시작번지가 장치에서 전송할 수 있는 번지와   불일치할 경우
#define ERR_ILLEGAL_DATA_VALUE		3	//요청한 데이터의 개수가 장치에서 전송할 수 있는 개수와 불일치할  경우
#define ERR_SLAVE_DEVICE_FAILURE	4	//요청 받은 명령을 정상적으로 처리하지 못할 경우

#define ERR_CRC16		10
#define ERR_TIMEOUT		11
#define ERR_COM_READ	12
#define ERR_COM_WRITE	13
#define ERR_EXCEPTION	14
#define ERR_COM_OPEN	15

//---------------------------- 상수 정의 --------------------------//
#define	BUFF_SIZE		80000//8192
#define	WM_COMM_READ	(WM_USER+1)		
#ifndef ASCII_LF
#define	ASCII_LF		0x0a
#endif
#ifndef ASCII_CR
#define	ASCII_CR		0x0d
#endif
#define	ASCII_XON		0x11
#define	ASCII_XOFF		0x13


//---------------------------- 상수 정의 --------------------------//
//#define	BUFF_SIZE		80000//8192


// Queue 클래스 정의 //
class	CQueue
{
public:
	CQueue();
	BYTE	buff[BUFF_SIZE];
	int		m_iHead, m_iTail;
	void	Clear();
	int		GetSize();
	BOOL	PutByte(BYTE b);		// 1 byte 넣기
	BOOL	GetByte(BYTE *pb);		// 1 byte 꺼내기
};
