#pragma once

using namespace std;

template<typename T>
class PSingletone
{
private:
	static auto_ptr<T> thisInstance;


public:
	 PSingletone(){}
     virtual ~PSingletone(){}

     static T* getInstance(){
       	 if(NULL == thisInstance.get()){
       	 	auto_ptr<T> pTemp(new T);
       	 	thisInstance = pTemp;
       	 }
       	 return thisInstance.get();
        }

};

template <typename T> auto_ptr<T> PSingletone<T>::thisInstance;;    //definition and initialization of singletone instance
