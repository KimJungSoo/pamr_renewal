#include "stdafx.h"
#include "PMap.h"

PMap::PMap(int nX, int nY)
{
	cout<<"[PMap] : PMap Instace is created!!!"<<endl;
	m_nSizeX = nX;
	m_nSizeY = nY;

	//============================================
	// 메모리 공간 할당
	//============================================
	m_nMap = DBG_NEW int*[nX];

	if(m_nMap){
		for(int i = 0; i < nX; i++){
			m_nMap[i] = DBG_NEW int[nY];

		}
	}

	m_dProbabilityMap = DBG_NEW double*[nX];

	if(m_dProbabilityMap){
		for(int i=0; i <nX; i++){
			m_dProbabilityMap[i] = DBG_NEW double[nY];

		}
	}

	for(int i=0; i <m_nSizeX; i++){
		for(int j=0; j<m_nSizeY; j++){
			m_nMap[i][j] = 0;
			m_dProbabilityMap[i][j]=0;
		}
	}

	cout<<"[PMap] : PMap Instance creation is completed!!!"<<endl;	     
}

PMap::~PMap()
{

	//if(m_nMap)
	//{
	//	for(int i = 0; i < m_nSizeX; i++)
	//	{
	//		delete[] m_nMap[i];
	//		m_nMap[i] = NULL;
	//	}
	//	delete[] m_nMap;
	//}

	if(m_dProbabilityMap){
		for(int i = 0; i < m_nSizeX; i++){
			//_RPTN(_CRT_WARN, "size: %d\n", i);
			delete[] m_dProbabilityMap[i];
			m_dProbabilityMap[i] = NULL;


		}
		delete[] m_dProbabilityMap;
	}

	cout<<"[PMap] : Instance is destroyed."<<endl;
}

void PMap::setX(int nX)
{ 
	m_nSizeX = nX; 
}
void PMap::setY(int nY)
{ 
	m_nSizeY = nY; 
}
int PMap::getX()
{ 
	return m_nSizeX; 
}
int PMap::getY()
{ 
	return m_nSizeY; 
}

int** PMap::getMap()
{ 
	return m_nMap; 
}


double** PMap::getProMap() 
{ 
	return m_dProbabilityMap; 
}

void PMap::setMap(int ** nMap)
{
	m_nMap=nMap;
}