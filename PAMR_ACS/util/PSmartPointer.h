#ifndef P_SMART_POINTER_H
#define P_SMART_POINTER_H

#include <memory>
#include <iostream>
using namespace std;


template<class T>
class PSmartPointer
{
public:
	PSmartPointer();
	PSmartPointer(T *pkPoint);
	PSmartPointer( const PSmartPointer &spPoint);
	~PSmartPointer();

	operator T*() const;
	T& operator*() const;
	T* operator->() const;

	PSmartPointer<T>& operator=(const PSmartPointer &spPoint);
	PSmartPointer<T>& operator=(T* pkPoint);

	bool operator== (T* pkPoint) const;
	bool operator!= (T* pkPoint) const;
	bool operator== (const PSmartPointer &spPoint) const;
	bool operator!= (const PSmartPointer &spPoint) const;

	int GetRefCount();
	bool IsNull();

private:
	void Inc();
	void Dec();

	T* m_pkPoint;
	int* m_pkCnt;
};





//#include "PSmartPointer.h"

template<class T>
PSmartPointer<T>::PSmartPointer() : m_pkPoint(NULL), m_pkCnt(NULL)
{

}

template<class T>
PSmartPointer<T>::PSmartPointer(T *pkPoint)
	: m_pkPoint(pkPoint), m_pkCnt(NULL)
{
	if( m_pkPoint )
		Inc();
}

template<class T>
PSmartPointer<T>::PSmartPointer(const PSmartPointer &spPoint)
	: m_pkPoint(NULL), m_pkCnt(NULL)
{
	m_pkCnt = spPoint.m_pkCnt;
	m_pkPoint = spPoint.m_pkPoint;
	if( m_pkPoint )
		Inc();
}

template<class T>
PSmartPointer<T>::~PSmartPointer()
{
	Dec();
}

template<class T>
PSmartPointer<T>::operator T*() const
{
	return m_pkPoint;
}

template<class T>
T& PSmartPointer<T>::operator*() const
{
	return *m_pkPoint;
}

template<class T>
T* PSmartPointer<T>::operator->() const
{
	return m_pkPoint;
}

template<class T>
PSmartPointer<T>& PSmartPointer<T>::operator=(const PSmartPointer<T> &spPoint)
{
	if( m_pkPoint != spPoint.m_pkPoint )
	{
		if( m_pkPoint )
			Dec();
		m_pkPoint = spPoint.m_pkPoint;
		m_pkCnt = spPoint.m_pkCnt;
		if( m_pkPoint )
			Inc();
	}

	return *this;
}

template<class T>
PSmartPointer<T>& PSmartPointer<T>::operator=(T* pkPoint)
{
	if( m_pkPoint != pkPoint )
	{
		if( m_pkPoint )
			Dec();
		m_pkPoint = pkPoint;
		m_pkCnt = 0;
		if( m_pkPoint )
			Inc();
	}

	return *this;
}

template<class T>
bool PSmartPointer<T>::operator== (T* pkPoint) const
{
	return m_pkPoint == pkPoint;
}

template<class T>
bool PSmartPointer<T>::operator!= (T* pkPoint) const
{
	return m_pkPoint != pkPoint;
}

template<class T>
bool PSmartPointer<T>::operator== (const PSmartPointer &spPoint) const
{
	return m_pkPoint == spPoint.m_pkPoint;
}

template<class T>
bool PSmartPointer<T>::operator!= (const PSmartPointer &spPoint) const
{
	return m_pkPoint != spPoint.m_pkPoint;
}

template<class T>
int PSmartPointer<T>::GetRefCount()
{ 
	return m_pkCnt?*m_pkCnt:0;
}	

template<class T>
bool PSmartPointer<T>::IsNull()
{
	return m_pkPoint == NULL;
}

template<class T>
void PSmartPointer<T>::Inc()
{
	if( m_pkCnt )
		++(*m_pkCnt);
	else
		m_pkCnt = DBG_NEW int(1);
}

template<class T>
void PSmartPointer<T>::Dec()
{
	if( m_pkCnt )
	{
		if( --(*m_pkCnt) <= 0 )
		{
			//	해제 되는걸 확인하기 위해 printf추가
			//printf("해제 : %d\n", *m_pkPoint);			
			delete m_pkCnt;
			m_pkCnt = 0;
			delete m_pkPoint;
			m_pkPoint = 0;
		}
	}
}

//---------------------------------------------------------------------------
#endif /*P_SMART_POINTER_H*/