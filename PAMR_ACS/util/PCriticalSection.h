#pragma once

#ifdef WIN32
#else
#include <pthread.h>
#endif

#include "PSingletone.h"
using namespace std;

class PCriticalSection : public PSingletone <PCriticalSection>
{
private:
	//CCriticalSection m_CriticalSection;
	 HANDLE m_mutex;
	DWORD m_owner;

public:
	PCriticalSection();
	~PCriticalSection();
public:
	void Lock();
	void Unlock();	
	void init();
	void Remove();
};

#ifdef WIN32
static CRITICAL_SECTION cs; /* This is the critical section object -- once initialized,
                               it cannot be moved in memory */
                            /* If you program in OOP, declare this as a non-static member in your class */
#else
/* This is the critical section object (statically allocated). */
static pthread_mutex_t cs_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif

