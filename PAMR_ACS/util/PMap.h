﻿#pragma once


using namespace std; 
class PMap
{
public:
	static const int EMPTY_AREA = 0;
	static const int OCCUPIED_AREA = 1;
	static const int UNKNOWN_AREA = 2;
	static const int COBSTACLE_AREA = 3;
	static const int GIVEN_PATH_AREA = 4;
	static const int VIRTUAL_WALL_AREA = 5;
	static const int GRAVITATION_AREA =6;
	static const int BOTTLENECK_WARNING_AREA = 7;
	static const int BOTTLENECK_AREA = 8;
	static const int CORNER_AREA = 9;
	static const int WARNING_AREA = 10;


	static const int NOMALVELOCITY = 100;
	static const int FIRDECEL= 101;
	static const int SECDECEL = 102;
	static const int THIDECEL = 103;
	static const int FOUDECEL = 104;
	static const int FIFDECEL = 105;

	static const int FIXED_CAD_AREA = 200;
	static const int TEMP_CAD_AREA = 201;
	static const int DYNAMIC_ENVIRONMENT_AREA = 202;
	static const int LUGGAGE_AREA = 203;

private:
	int m_nSizeX;
	int m_nSizeY;   
	int **m_nMap; 
	double **m_dProbabilityMap;



public:

	void setX(int nX);
	void setY(int nY);
	int getX();
	int getY();
	int** getMap();
	double** getProMap();
	void setMap(int ** nMap);

	PMap(int nX, int nY);
	~PMap();
};
