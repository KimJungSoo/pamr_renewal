#pragma once

#ifdef WIN32 // Windows system specific
	#ifndef _AFXDLL
		#define _AFXDLL
	#endif
#else // Unix based system specific
	#include <pthread.h> //thread 
	#include <unistd.h>
#endif

#include "PTimer.h"
using namespace std;

class PThread: public PTimer
{

private:

#ifdef WIN32
 	DWORD  m_dwThreadID;
 	HANDLE m_hThreadID;	
#else
	pthread_t _thread;
	pthread_attr_t 	_thread_attr;
	pthread_cond_t m_thCond;
	pthread_mutex_t m_muCond;
#endif

	//?�레?��? ?�성?�때, ?�요??변?��? ?�수.


#ifdef WIN32
static DWORD WINAPI  _thread_function(LPVOID arg );
#else
	static void* _thread_function(void* arg );
#endif


	void (*m_funcPtr)();
	void (*m_funcPtrwithParam)(void*);
	void* m_arg;
	int m_nRate;

	//bool m_bDoThread;
	//bool m_bSuspendFlag;
// bwk 2017.04.12 - ���� : m_bDoThread, m_bSuspendFlag �� public �Ӽ����� ����-->>
public:
	bool m_bDoThread;
	bool m_bSuspendFlag;
//<<--

public:

	bool start( void (*funcPtr)(), int nRate );
	bool start(void (*funcPtr)(void*) , void* arg,  int nRate );
	bool terminate();
	bool suspend();
	bool resume();

	PThread();
	~PThread();

};
