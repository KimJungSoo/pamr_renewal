#pragma once

typedef struct
{
	double	lX;	//@field X coordinate in m 
	double	lY;	//@field Y coordinate in m 
} FPOINT_WORLD;

typedef struct
{
	double	lX[571];	//@field X coordinate in m 
	double	lY[571];	//@field Y coordinate in m 
	int		count;
} FPOINT_WORLD_COUNT;

typedef struct
{
	FPOINT_WORLD data[100];
	double xlength;
	double ylength;
	double xmean;
	double ymean;
	int count;
	double xmin, xmax;
	double ymin, ymax;
} FPOINT_WORLD_GROUP;



typedef struct
{
	double	lX;	//@field X coordinate in m 
	double	lY;	//@field Y coordinate in m 
	bool matched_flag;
} FPOINT_WORLD_MATCH;

typedef struct
{
	double xmin;
	double xmax;
	double ymin;
	double ymax;
} LRF_ROI;

