
// PAMR_ACSDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "PAMR_ACS.h"
#include "PAMRDlg.h"
#include "afxdialogex.h"

#include "SystemCheck.h"
#include "sensor\SICKLaserScanner.h"
#include "sensor\GyroSensor.h"
#include "sensor\SICKGuide.h"

using namespace cv;
using namespace sensor;
using namespace eventManager;
using namespace AMRSystem;
using namespace AMRmove;

#define MAP_CELL_SIZE 100
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CPAMRDlg 대화 상자



CPAMRDlg::CPAMRDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_PAMR_ACS_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPAMRDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LIST_EVENTMANAGER, m_listEventManager);
	DDX_Control(pDX, IDC_EDIT_AMRSYSTEM_STATE, m_editAMRSystemState);
	DDX_Control(pDX, IDC_EDIT_AMR_POSITION, m_editAMRPosition);
	DDX_Control(pDX, IDC_EDIT1, m_editDestIndex);
	DDX_Control(pDX, IDC_EDIT_AMRSYSTEM_V, m_editAMRSystemV);
	DDX_Control(pDX, IDC_EDIT_AMRSYSTEM_W, m_editAMRSystemW);
	DDX_Control(pDX, IDC_MAP, PIctureControlMap);
	DDX_Control(pDX, IDC_BUTTON_PRECISE_MOVE, m_buttonPreciseMove);
	DDX_Control(pDX, IDC_EDIT_GYRO, m_editGyro);
	DDX_Control(pDX, IDC_EDIT_PRECISEMOVE_TARGETLINE, m_editTargetLine);
}

BEGIN_MESSAGE_MAP(CPAMRDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_AUTONOMOUS_MOVE, &CPAMRDlg::OnBnClickedButtonAutonomousMove)
	ON_BN_CLICKED(IDC_BUTTON_AMRSYSTEM_INIT, &CPAMRDlg::OnBnClickedButtonAmrsystemInit)
	ON_BN_CLICKED(IDC_BUTTON_PRECISE_MOVE, &CPAMRDlg::OnBnClickedButtonPreciseMove)
	ON_BN_CLICKED(IDC_BUTTON_ERROR_RESET, &CPAMRDlg::OnBnClickedButtonErrorReset)
	ON_BN_CLICKED(IDC_BUTTON_GUIDEMOVE, &CPAMRDlg::OnBnClickedButtonGuidemove)
	ON_BN_CLICKED(IDC_BUTTON_AMR_LEFT, &CPAMRDlg::OnBnClickedButtonAmrLeft)
END_MESSAGE_MAP()

void CPAMRDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

void CPAMRDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CPAMRDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CPAMRDlg::OnDestroy()
{
	SAFE_DELETEA(m_sensor);
	CDialogEx::OnDestroy();
	//FreeConsole();
	_CrtDumpMemoryLeaks();
	g_eventManager.PushTask(MSG_INFO, INFO_PROGRAM_TERMINATED, true, false);
	//std::cin.ignore();
}

BOOL CPAMRDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	return CDialogEx::DestroyWindow();
}

// CPAMRDlg 메시지 처리기

BOOL CPAMRDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	g_eventManager.PushTask(MSG_INFO, INFO_PROGRAM_START, true, false);
	m_nKeyV = m_nKeyW = 0;

	m_sensor = DEBUG_NEW CSensorModule*[2];
	m_AMRSystem.getSensor(0, m_sensor);
	m_AMRSystem.getSensor(1, m_sensor);
	m_mapImg = imread("datafiles/"+g_strAMRVersion+"/mapImage.jpg");
	m_AMRSystem.initMCL(m_mapImg);
	//m_img = m_mapImg.clone();

	SetTimer(1, 100, NULL);
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CPAMRDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 1) {
		switch (m_AMRSystem.getStatus()) {
		case SYSTEM_STATE_NOT_INIT:
			m_editAMRSystemState.SetWindowTextW(_T("AMR needs to initialize"));
			break;
		case SYSTEM_STATE_READY:
			m_editAMRSystemState.SetWindowTextW(_T("AMR is Ready"));
			break;
		case SYSTEM_STATE_READING:
			m_editAMRSystemState.SetWindowTextW(_T("Wait for seconds"));
			break;
		case SYSTEM_STATE_RUNNING:
			m_editAMRSystemState.SetWindowTextW(_T("AMR is Running"));
			break;
		case SYSTEM_STATE_PAUSE:
			m_editAMRSystemState.SetWindowTextW(_T("Paused!!"));
			break;
		case SYSTEM_STATE_ABORTED:
			m_editAMRSystemState.SetWindowTextW(_T("Aborted!!"));
			break;
		}

		// 이벤트 매니저 에러 출력
		m_listEventManager.ResetContent();
		for (int i = 0; i < (int)m_ErrorList.size(); i++)
			m_ErrorList[i].clear();//m_ErrorList[i] = {};
		m_ErrorList.clear();//m_ErrorList = {};

		m_ErrorList = g_eventManager.getErrorArrayList();
		for (int i = 0; i < (int)m_ErrorList.size(); i++)
			for (int j = 0; j < (int)m_ErrorList[i].size(); j++)
				m_listEventManager.AddString(CA2T( ("[" + std::to_string(m_ErrorList[i][j].m_nEventCode) + "] "+ m_ErrorList[i][j].m_strErrorMSG).c_str()));

		// 현재 위치 표시
		RobotPose curPos = m_AMRSystem.getCurrentPose();
		m_editAMRPosition.SetWindowText(CA2T(("X: "+std::to_string((int)curPos.getX()) + ", Y: " + std::to_string((int)curPos.getY())+", T: " + std::to_string((int)curPos.getThetaDeg())).c_str()));
		if (m_AMRSystem.getStatus() == SYSTEM_STATE_RUNNING) {
			if (m_AMRSystem.m_kanayama.m_nNumberOfPathNode != 0) {
				int nx = m_AMRSystem.m_kanayama.m_wayPoint[m_AMRSystem.m_kanayama.m_nKanaBigWC][m_AMRSystem.m_kanayama.m_nKanaWayCount].x;
				int ny = m_AMRSystem.m_kanayama.m_wayPoint[m_AMRSystem.m_kanayama.m_nKanaBigWC][m_AMRSystem.m_kanayama.m_nKanaWayCount].y;
				m_editDestIndex.SetWindowText(CA2T((std::to_string(nx) + ", " + std::to_string(ny) + " -> " + std::to_string(m_AMRSystem.m_kanayama.m_nKanaWayCount)).c_str()));
				m_editAMRSystemV.SetWindowText(CA2T((std::to_string(m_AMRSystem.m_kanayama.getV())).c_str()));
				m_editAMRSystemW.SetWindowText(CA2T((std::to_string(m_AMRSystem.m_kanayama.getW())).c_str()));
			}
		}
		double theta = 0;
		m_AMRSystem.m_motionController.GetGyroThetaDeg(&theta);
		m_editGyro.SetWindowText(CA2T((std::to_string(theta)).c_str()));

		// 그림을 그려요
		//*
		m_img = m_mapImg.clone();

		// Node Point 그리기
		for (int i = 0; i < m_AMRSystem.m_kanayama.m_nNumberOfNodePoint; i++)
			circle(m_img, Point((int)m_AMRSystem.m_kanayama.NPFS[i].x/ MAP_CELL_SIZE, (int)m_AMRSystem.m_kanayama.NPFS[i].y/ MAP_CELL_SIZE), 1, Scalar(125, 15, 232), -1);
		circle(m_img, Point((int)m_AMRSystem.m_kanayama.getRefPosition().getX()/ MAP_CELL_SIZE, (int)m_AMRSystem.m_kanayama.getRefPosition().getY()/ MAP_CELL_SIZE), 1, Scalar(0, 255, 0), -1);

		// 반사봉 그리기
		vector<LandmarkPoint> landmark = m_AMRSystem.m_preciseMove.getLandmark();
		for (int i = 0; i < (int)landmark.size(); i++)
		{
			if(landmark[i].objectindex == -1)
				circle(m_img, Point(landmark[i].x / MAP_CELL_SIZE, landmark[i].y / MAP_CELL_SIZE), 3, Scalar(0, 0, 255), 1);
			else
				circle(m_img, Point(landmark[i].x / MAP_CELL_SIZE, landmark[i].y / MAP_CELL_SIZE), 3, Scalar(0, 0, 255), -1);
		}
		//landmark = {};
		landmark.clear();

		// 정밀주행 타깃 라인
		PointObject targetLine = m_AMRSystem.m_preciseMove.getTargetLine();
		line(m_img, Point(targetLine.nFirst_x / MAP_CELL_SIZE, targetLine.nFirst_y / MAP_CELL_SIZE), Point(targetLine.nEnd_x / MAP_CELL_SIZE, targetLine.nEnd_y / MAP_CELL_SIZE), Scalar(0, 0, 0), 2);
		double dTargetThate = atan2(targetLine.nFirst_y - targetLine.nEnd_y,targetLine.nFirst_x - targetLine.nEnd_x);
		m_editTargetLine.SetWindowText(CA2T(("deg:" + std::to_string(dTargetThate*R2D)).c_str()));

		// Laser Scanner 센서 데이터 뿌리기
		CSICKLaserScanner* lms1 = dynamic_cast<CSICKLaserScanner*>(m_sensor[0]);
		CSICKLaserScanner* lms2 = dynamic_cast<CSICKLaserScanner*>(m_sensor[1]);
		LaserScanData scan1, scan2;
		lms1->getData(&scan1, 1);
		lms2->getData(&scan2, 1);
		
		if (lms1->getStatus() == SENSOR_STATE_RUN)
		{
			RobotPose sensorPose;
			sensorPose.setX(450);
			sensorPose.setY(450);
			sensorPose.setThetaDeg(45);
			
			for (int i = 0; i < scan1.nData_len; i++) {
				if (scan1.dist[i] < 100)
					continue;
				int nX, nY, nImgX, nImgY;
				ScannerToRobotCoor(sensorPose, scan1, i, &nX, &nY);
				RobotCoorToWorldCoor(curPos, nX, nY, &nImgX, &nImgY);
				nImgX /= MAP_CELL_SIZE;
				nImgY /= MAP_CELL_SIZE;

				if (nImgX < 0 || nImgX >= m_img.cols || nImgY < 0 || nImgY >= m_img.rows)
					continue;
				m_img.at<Vec3b>(nImgY, nImgX)[0] = 100;
				m_img.at<Vec3b>(nImgY, nImgX)[1] = 255;
				m_img.at<Vec3b>(nImgY, nImgX)[2] = 100;
			}
		}
		if (lms2->getStatus() == SENSOR_STATE_RUN)
		{
			RobotPose sensorPose;
			sensorPose.setX(-450);
			sensorPose.setY(-450);
			sensorPose.setThetaDeg(-135);

			for (int i = 0; i < scan2.nData_len; i++) {
				if (scan2.dist[i] < 100)
					continue;
				int nX, nY, nImgX, nImgY;
				ScannerToRobotCoor(sensorPose, scan2, i, &nX, &nY);
				RobotCoorToWorldCoor(curPos, nX, nY, &nImgX, &nImgY);
				nImgX /= MAP_CELL_SIZE;
				nImgY /= MAP_CELL_SIZE;

				if (nImgX < 0 || nImgX >= m_img.cols || nImgY < 0 || nImgY >= m_img.rows)
					continue;
				m_img.at<Vec3b>(nImgY, nImgX)[0] = 100;
				m_img.at<Vec3b>(nImgY, nImgX)[1] = 100;
				m_img.at<Vec3b>(nImgY, nImgX)[2] = 255;
			}
		}

		// 로봇 그리기
		int nX, nY;
		RobotCoorToWorldCoor(curPos, 400, 0, &nX, &nY);
		circle(m_img, Point((int)curPos.getX()/ MAP_CELL_SIZE, (int)curPos.getY()/ MAP_CELL_SIZE), 2, Scalar(255, 0, 0), -1);
		line(m_img, Point((int)curPos.getX()/ MAP_CELL_SIZE, (int)curPos.getY()/ MAP_CELL_SIZE), Point(nX / MAP_CELL_SIZE, nY / MAP_CELL_SIZE), Scalar(0, 0, 255), 1);

		Rect rect(30, 30, m_img.cols*2/3, m_img.rows*2/3);
		m_img = m_img(rect);

		// Mat을 MFC Picture Control에 그림
		DrawImage();
		//*/
	}	

	CDialogEx::OnTimer(nIDEvent);
}


void CPAMRDlg::OnBnClickedButtonAutonomousMove()
{
	m_AMRSystem.autoDrive();
}

void CPAMRDlg::OnBnClickedButtonPreciseMove()
{
	m_AMRSystem.preciseMove();
}

void CPAMRDlg::OnBnClickedButtonAmrsystemInit()
{
	m_AMRSystem.initialize();
}

void CPAMRDlg::DrawImage() {
	// opencv Mat을 MFC 화면에 보여주기 위한 처리 Mat은 CV_8UC1~3만 사용 가능
	int bpp = 8 * m_img.elemSize();
	assert((bpp == 8 || bpp == 24 || bpp == 32));

	int padding = 0;
	//32 bit image is always DWORD aligned because each pixel requires 4 bytes
	if (bpp < 32)
		padding = 4 - (m_img.cols % 4);
	if (padding == 4)
		padding = 0;
	int border = 0;
	//32 bit image is always DWORD aligned because each pixel requires 4 bytes
	if (bpp < 32)
		border = 4 - (m_img.cols % 4);
	Mat mat_temp;
	if (border > 0 || m_img.isContinuous() == false)
		cv::copyMakeBorder(m_img, mat_temp, 0, 0, 0, border, cv::BORDER_CONSTANT, 0);
	else
		mat_temp = m_img;
	RECT r;
	PIctureControlMap.GetClientRect(&r);
	cv::Size winSize(r.right, r.bottom);
	m_imageMfc.Create(winSize.width, winSize.height, 24);
	BITMAPINFO *bitInfo = (BITMAPINFO*)malloc(sizeof(BITMAPINFO) + 256 * sizeof(RGBQUAD));
	bitInfo->bmiHeader.biBitCount = bpp;
	bitInfo->bmiHeader.biWidth = mat_temp.cols;
	bitInfo->bmiHeader.biHeight = -mat_temp.rows;
	bitInfo->bmiHeader.biPlanes = 1;
	bitInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitInfo->bmiHeader.biCompression = BI_RGB;
	bitInfo->bmiHeader.biClrImportant = 0;
	bitInfo->bmiHeader.biClrUsed = 0;
	bitInfo->bmiHeader.biSizeImage = 0;
	bitInfo->bmiHeader.biXPelsPerMeter = 0;
	bitInfo->bmiHeader.biYPelsPerMeter = 0;
	//그레이스케일 인경우 팔레트가 필요
	if (bpp == 8) {
		RGBQUAD* palette = bitInfo->bmiColors;
		for (int i = 0; i < 256; i++)
		{
			palette[i].rgbBlue = palette[i].rgbGreen = palette[i].rgbRed = (BYTE)i;
			palette[i].rgbReserved = 0;
		}
	}
	// Image is bigger or smaller than into destination rectangle
	// we use stretch in full rect
	if (mat_temp.cols == winSize.width  && mat_temp.rows == winSize.height) {
		// source and destination have same size
		// transfer memory block
		// NOTE: the padding border will be shown here. Anyway it will be max 3px width

		SetDIBitsToDevice(m_imageMfc.GetDC(),
			//destination rectangle
			0, 0, winSize.width, winSize.height,
			0, 0, 0, mat_temp.rows,
			mat_temp.data, bitInfo, DIB_RGB_COLORS);
	}
	else {
		// destination rectangle
		int destx = 0, desty = 0;
		int destw = winSize.width;
		int desth = winSize.height;

		// rectangle defined on source bitmap
		// using imgWidth instead of mat_temp.cols will ignore the padding border
		int imgx = 0, imgy = 0;
		int imgWidth = mat_temp.cols - border;
		int imgHeight = mat_temp.rows;

		StretchDIBits(m_imageMfc.GetDC(),
			destx, desty, destw, desth,
			imgx, imgy, imgWidth, imgHeight,
			mat_temp.data, bitInfo, DIB_RGB_COLORS, SRCCOPY);
	}

	HDC dc = ::GetDC(PIctureControlMap.m_hWnd);
	m_imageMfc.BitBlt(dc, 0, 0);
	::ReleaseDC(PIctureControlMap.m_hWnd, dc);
	m_imageMfc.ReleaseDC();
	m_imageMfc.Destroy();
	delete(bitInfo);
}

BOOL CPAMRDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		switch (pMsg->wParam)
		{
		case VK_UP:
			m_nKeyV += 30;
			m_AMRSystem.manualDrive();
			break;
		case VK_DOWN:
			m_nKeyV -= 30;
			m_AMRSystem.manualDrive();
			break;
		case VK_RIGHT:
			m_nKeyW -= 5;
			m_AMRSystem.manualDrive();
			break;
		case VK_LEFT:
			m_nKeyW += 5;
			m_AMRSystem.manualDrive();
			break;

			// 구동 stop
		case 'S':
			m_nKeyV = m_nKeyW = 0;
			m_AMRSystem.stop();
			break;
		case 's':
			m_nKeyV = m_nKeyW = 0;
			m_AMRSystem.stop();
			break;
		case 'W':
			m_nKeyW = 0;
			m_AMRSystem.manualDrive();
			break;
		case 'w':
			m_nKeyW = 0;
			m_AMRSystem.manualDrive();
			break;
		}
	}
	m_AMRSystem.setKeyCommand(m_nKeyV, m_nKeyW);
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CPAMRDlg::ScannerToRobotCoor(RobotPose sensorPos, LaserScanData scanData, int index, int *X, int *Y) {
	// 로봇 중심을 원점으로 하는 상대 좌표로 변환
	double deg = double(scanData.nStart_angle - sensorPos.getThetaDeg()) + (index / double(scanData.nAngleResolution));
	UINT16 dist = scanData.dist[index];
	*X = (int)(sensorPos.getX() + dist * cos(deg*D2R));
	*Y = (int)(sensorPos.getY() + dist * sin(deg*D2R));
}

void CPAMRDlg::RobotCoorToWorldCoor(RobotPose curPos, int nRobotX, int nRobotY, int *nImageX, int *nImageY)
{
	// x축 대칭 행렬 * Theta 회전 행렬 = (cos, sin; sin, -cos)
	int x = (int)(nRobotX*cos(curPos.getThetaRad()) + nRobotY*sin(curPos.getThetaRad()));
	int y = (int)(nRobotX*sin(curPos.getThetaRad()) - nRobotY*cos(curPos.getThetaRad()));
	*nImageX = (int)curPos.getX() + x;
	*nImageY = (int)curPos.getY() + y;
}


void CPAMRDlg::OnBnClickedButtonErrorReset()
{
	m_AMRSystem.errorClear();
}


void CPAMRDlg::OnBnClickedButtonGuidemove()
{
	m_AMRSystem.guideDrive();
}


void CPAMRDlg::OnBnClickedButtonAmrLeft()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
