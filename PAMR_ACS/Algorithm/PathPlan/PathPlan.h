#pragma once

#define MAX_COUNT_NODE_POINT	500
#define MAX_COUNT_PATH_NODE		1024

#define EUCLIDEAN_DISTANCE_ROBOTPOSE(A, B) sqrt(pow(A.getX() - B.getX(), 2) + pow(A.getY() - B.getY(), 2))
#define EUCLIDEAN_DISTANCE_CPOINT(A, B) sqrt(pow(A.x - B.x, 2) + pow(A.y - B.y, 2))

class CPathPlan
{
protected:
	
public:
	int m_nNumberOfNodePoint;
	int m_nNumberOfPathNode;
	int m_nUnitOfWayPoint;
	int m_nInPosition;

	int * m_distBTWwps;
	double * m_pathAngle;
	CPoint* m_pathData;
	CPoint ** m_wayPoint;
	NodePointFromServer NPFS[MAX_COUNT_NODE_POINT];
	NodePointForRobot NPFR[MAX_COUNT_NODE_POINT];
	CPathPlan();
	~CPathPlan();
	
	int LoadNodePoint(std::string);
	int LoadPathData(std::string);
	int PathPlan(CPoint);
	bool isArrived(CPoint);

};
