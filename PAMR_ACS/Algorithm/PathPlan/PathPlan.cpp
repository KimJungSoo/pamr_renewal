#include "stdafx.h"
#include <string>
#include <fstream>
#include <iostream>
#include "PathPlan.h"

using namespace eventManager;
using namespace std;

CPathPlan::CPathPlan()
{
	m_pathData = NULL;
	m_wayPoint = NULL;
	m_distBTWwps = NULL;
	m_pathAngle = NULL;

	m_nNumberOfPathNode = 0;
	m_nNumberOfNodePoint = 0;
	
	m_nUnitOfWayPoint = 100;//100; // 100mm
	m_nInPosition = 100;//50; // 5cm 반경에 오면 도착
}

CPathPlan::~CPathPlan()
{
	SAFE_DELETE(m_pathAngle);
	SAFE_DELETE(m_distBTWwps);
	for (int i = 0; i < m_nNumberOfPathNode; i++)
		SAFE_DELETE(m_wayPoint[i]);
	SAFE_DELETE(m_wayPoint);
	SAFE_DELETE(m_pathData);
}

// Map Editor에서 가져온 Nodepoint.txt 파일을 읽어야함
int CPathPlan::LoadNodePoint(string pathFile)
{
	// Node Information
	int no, x, y, th_deg, obs_flag, max_v, max_a, doorArea, reflector_flag, reflector_useSide;
	ifstream fin;
	char inputString[100];
	char* type;

	// Node File Open
	fin.open(pathFile.c_str());
	if (!fin.is_open()) {
		g_eventManager.PushTask(MSG_ERROR, ERROR_NO_NODE_FILE, true, true);
		return RETURN_FAILED;
	}

	// Get number of nodes
	fin.getline(inputString, sizeof(inputString));
	m_nNumberOfNodePoint = atoi(inputString);
	if (m_nNumberOfNodePoint <= 0 || m_nNumberOfNodePoint > MAX_COUNT_NODE_POINT) {
		g_eventManager.PushTask(MSG_ERROR, ERROR_NODE_NUMBER_RANGE, true, true);
		return RETURN_FAILED;
	}

	// Get all Node
	for (int nNodeCount = 0; nNodeCount < m_nNumberOfNodePoint; nNodeCount++)
	{
		fin.getline(inputString, 100);
		char* token = strtok_s(inputString, ",", &type);
		no = atoi(token);

		token = strtok_s(NULL, ",", &type);
		x = atoi(token);

		token = strtok_s(NULL, ",", &type);
		y = atoi(token);

		token = strtok_s(NULL, ",", &type);
		th_deg = atoi(token);

		token = strtok_s(NULL, ",", &type);
		obs_flag = atoi(token);

		token = strtok_s(NULL, ",", &type);
		max_v = atoi(token);

		token = strtok_s(NULL, ",", &type);
		max_a = atoi(token);

		token = strtok_s(NULL, ",", &type);
		doorArea = atoi(token);

		token = strtok_s(NULL, ",", &type);
		reflector_flag = atoi(token);

		token = strtok_s(NULL, ",", &type);
		reflector_useSide = atoi(token);

		NPFS[nNodeCount].no = no;
		NPFS[nNodeCount].x = x;								// mm
		NPFS[nNodeCount].y = y;								// mm
		NPFS[nNodeCount].th_deg = th_deg;						// bwk 2017.04.10 - 수정 : 시작점 방향 정보를 추가
		NPFS[nNodeCount].obs_flag = obs_flag;					// bwk 2017.04.05 - 수정 : 장애물 감지 범위 - 0 : 사용안함, 1~n : 정의된 범위
		NPFS[nNodeCount].max_v = max_v;						// bwk 2017.03.29 - 수정 : 구간별 속도 설정(0 : 기존 제한 속도, 기타 : 구간별 제한 속도)
		NPFS[nNodeCount].max_a = max_a;						// bwk 2017.03.29 - 수정 : 구간별 각속도 설정(0 : 기존 제한 각속도, 기타 : 구간별 제한 각속도)
		NPFS[nNodeCount].doorArea = doorArea;					// bwk 2017.03.29 - 수정 : Door 구간 설정(0 : 일반지역, 1 : Door Area)
		NPFS[nNodeCount].reflector_flag = reflector_flag;		// bwk 2017.06.16 - 수정 : Reflector 구간 설정(0 : 일반지역, 1~ : Reflector Mode)
		NPFS[nNodeCount].reflector_useSide = reflector_useSide;	// bwk 2017.06.16 - 수정 : Reflector 구간 사용(0 : 전체사용, 1 : 왼쪽만 사용, 2 : 오른쪽만 사용)
	}
	fin.close();
	return RETURN_NONE_ERROR;
}

// Off Line Path Data, File에서 주행데이터 가져오는 함수
int CPathPlan::LoadPathData(string pathFile)
{
	// Read File
	ifstream fin;
	char inputString[100];
	int i, j;
	fin.open(pathFile.c_str());
	if (!fin.is_open()) {
		g_eventManager.PushTask(MSG_ERROR, ERROR_NO_PATH_FILE, true, true);
		return RETURN_FAILED;
	}
	// Number of Nodes
	fin.getline(inputString, 100);
	m_nNumberOfPathNode = atoi(inputString);

	if (m_nNumberOfPathNode <= 0) {
		g_eventManager.PushTask(MSG_ERROR, ERROR_PATH_NODE_COUNT, true, true);
		return RETURN_FAILED;
	}

	// Path Node 
	SAFE_DELETE(m_pathData);
	m_pathData = DEBUG_NEW CPoint[m_nNumberOfPathNode];

	int index;
	for (i = 0; i <m_nNumberOfPathNode; i++)
	{
		// get Node
		fin.getline(inputString, 100);
		index = atoi(inputString);
		bool find_node = false;
		for (j = 0; j< m_nNumberOfNodePoint; j++)
		{
			if (index == NPFS[j].no)
			{
				find_node = true;

				NPFR[i].no = NPFS[j].no;
				NPFR[i].x = NPFS[j].x / 100;
				NPFR[i].y = NPFS[j].y / 100;
				NPFR[i].th_deg = NPFS[j].th_deg;
				NPFR[i].obs_flag = NPFS[j].obs_flag;
				NPFR[i].max_v = NPFS[j].max_v;
				NPFR[i].max_a = NPFS[j].max_a;
				NPFR[i].doorArea = NPFS[j].doorArea;
				NPFR[i].reflector_flag = NPFS[j].reflector_flag;
				NPFR[i].reflector_useSide = NPFS[j].reflector_useSide;

				m_pathData[i].x = NPFS[j].x;
				m_pathData[i].y = NPFS[j].y;

				break;
			}
		}
		
		if (!find_node)
		{
			g_eventManager.PushTask(MSG_ERROR, ERROR_CAN_NOT_FIND_NODE, true, true);
			fin.close();
			return RETURN_FAILED;
		}	
	}
	fin.close();
	return RETURN_NONE_ERROR;
}

// 현재 위치
int CPathPlan::PathPlan(CPoint curPos) // mm 단위
{
	// Validation Check
	if (m_nNumberOfPathNode <= 0) {
		g_eventManager.PushTask(MSG_ERROR, ERROR_PATH_NODE_IS_EMPTY, true, true);
		return RETURN_FAILED;
	}

	// Memory allocation
	m_wayPoint = (CPoint**)malloc(sizeof(CPoint)*m_nNumberOfPathNode);
	m_distBTWwps = (int*)malloc(sizeof(int)*m_nNumberOfPathNode);
	m_pathAngle = (double*)malloc(sizeof(double)*m_nNumberOfPathNode);

	// 현재 위치 (nX, nY)에서 처음 노드까지 Way Point를 지정함
	m_distBTWwps[0] = (int)EUCLIDEAN_DISTANCE_CPOINT(m_pathData[0], curPos);
	m_pathAngle[0] = atan2((double)(m_pathData[0].y - curPos.y), (double)(m_pathData[0].x - curPos.x));
	m_wayPoint[0] = (CPoint*)malloc(sizeof(CPoint)* (int)(m_distBTWwps[0] / m_nUnitOfWayPoint)); // nUnitOfWayPoint (mm) 단위로 Way Point를 쪼갬
	
	double cos = (double)(m_pathData[0].x - curPos.x) / m_distBTWwps[0];
	double sin = (double)(m_pathData[0].y - curPos.y) / m_distBTWwps[0];

	for (int nDistance = 1; nDistance <= m_distBTWwps[0] / m_nUnitOfWayPoint; nDistance++) // 10 cm step
	{
		double dRayOfX = (double)nDistance*cos*m_nUnitOfWayPoint;		//레이저의 X방향으로 간격만큼 증가 시킴
		double dRayOfY = (double)nDistance*sin*m_nUnitOfWayPoint;		//레이저의 Y방향으로 간격만큼 증가 시킴

		m_wayPoint[0][nDistance - 1].x = (int)(curPos.x + dRayOfX); //nX를 확률지도상의 센서데이터의 X좌표 인덱스로사용
		m_wayPoint[0][nDistance - 1].y = (int)(curPos.y + dRayOfY); //nY를 확률지도상의 센서데이터의 Y좌표 인덱스로사용
	}

	// 나머지 노드들의 Way Point를 지정함
	for (int index = 1; index < m_nNumberOfPathNode; index++){
		// 노드간 각도를 구함
		m_pathAngle[index] = atan2((double)(m_pathData[index].y - m_pathData[index - 1].y), (double)(m_pathData[index].x - m_pathData[index - 1].x));

		// 노드 사이 거리를 구하여 Way Point를 쪼갬
		m_distBTWwps[index] = (int)EUCLIDEAN_DISTANCE_CPOINT(m_pathData[index], m_pathData[index-1]);
		m_wayPoint[index] = (CPoint*)malloc(sizeof(CPoint)* (int)(m_distBTWwps[index] / m_nUnitOfWayPoint) );
		double cos = (double)(m_pathData[index].x - m_pathData[index - 1].x) / m_distBTWwps[index];
		double sin = (double)(m_pathData[index].y - m_pathData[index - 1].y) / m_distBTWwps[index];

		for (int nDistance = 1; nDistance <= m_distBTWwps[index] / m_nUnitOfWayPoint; nDistance++) // 10 cm step
		{
			double dRayOfX = (double)nDistance*cos*m_nUnitOfWayPoint;		//레이저의 X방향으로 간격만큼 증가 시킴
			double dRayOfY = (double)nDistance*sin*m_nUnitOfWayPoint;		//레이저의 Y방향으로 간격만큼 증가 시킴

			int nX = (int)(m_pathData[index - 1].x + dRayOfX);	//nX를 확률지도상의 센서데이터의 X좌표 인덱스로사용
			int nY = (int)(m_pathData[index - 1].y + dRayOfY);	//nY를 확률지도상의 센서데이터의 Y좌표 인덱스로사용
			m_wayPoint[index][nDistance - 1].x = nX;
			m_wayPoint[index][nDistance - 1].y = nY;
		}
	}
	return RETURN_NONE_ERROR;
}

bool CPathPlan::isArrived(CPoint curPos) 
{
	
	if (EUCLIDEAN_DISTANCE_CPOINT(m_pathData[m_nNumberOfPathNode - 1], curPos) < m_nInPosition)
	{
		for(int i = 0; i < m_nNumberOfPathNode; i++)
			SAFE_DELETE(m_wayPoint[i]);
		SAFE_DELETE(m_wayPoint);
		SAFE_DELETE(m_distBTWwps);
		SAFE_DELETE(m_pathAngle);
		return true;
	}
	return false;
}
