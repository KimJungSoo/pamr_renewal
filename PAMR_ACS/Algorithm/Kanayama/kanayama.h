#pragma once

#include "Algorithm\LineExtraction\line_extraction_alg.h"
#include "Algorithm\PathPlan\PathPlan.h"
#include "util\CriticalSection.h"
#include "robot\RobotPose.h"


class Ckanayama : public CPathPlan
{
private:
	RobotPose m_referencePos;
	RobotPose m_currentPos;

	float m_fKx;
	float m_fKy;
	float m_fKtheta;

	double m_dDecel;	// mm/s^2
	double m_dMaxV;	// mm/s
	double m_dMaxW;	// degree/s
	double m_dV;	// mm/s
	double m_dW;	// degree/s

	bool m_bKanayamaControlFirstInit;
	bool m_bArrived;

	// kanayama function
	void kanayama_loop_first();
	void kanayama_loop();
	void kanayama_loop_final();
	void clearJob();

public:
	int m_nKanaWayCount;
	int m_nKanaBigWC;
	Ckanayama();
	~Ckanayama(void);

	int autoFunc(RobotPose);
	int PathPlanOnFile(string, CPoint);
	
	int loadNodePoint(){ return LoadNodePoint("datafiles/" + g_strAMRVersion + "/nodepoint.txt"); }
	void userDefinedKanayama(RobotPose currentPos, RobotPose referencePos, double Kx, double Ky, double Kt, double * V, double * W);
	// Getter & Setter
	void setKanayamaGain(float Kx, float Ky, float Ktheta);
	void setPosition(RobotPose input) { m_currentPos = input; }
	RobotPose getPosition() { return m_currentPos; }
	RobotPose getRefPosition() { return m_referencePos; }
	double getV() { return m_dV; }
	double getW() { return m_dW; }
};
