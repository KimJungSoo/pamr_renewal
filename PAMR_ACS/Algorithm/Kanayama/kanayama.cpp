#include "stdafx.h"
#include "kanayama.h"

Ckanayama::Ckanayama() :CPathPlan()
{
	// input은 mm/s, degree/s
	m_dMaxV = 100;
	m_dDecel = 1000;
	m_dMaxW = 10  * D2R;
	clearJob();
}

Ckanayama::~Ckanayama(void)
{

}

void Ckanayama::clearJob()
{
	m_dV = 0;
	m_dW = 0;
	m_nKanaBigWC = 0;
	m_nKanaWayCount = 0;
	m_bKanayamaControlFirstInit = false;
	m_bArrived = false;
}

int Ckanayama::autoFunc(RobotPose curPos)
{
	// Path Node Check
	if (m_nNumberOfPathNode <= 0)
		return RETURN_FAILED;
	
	// set current Position
	m_currentPos = curPos;

	CPoint destPos;

	// way point를 지나는 중
	if (m_nKanaWayCount != int(m_distBTWwps[m_nKanaBigWC] / m_nUnitOfWayPoint))
	{
		m_referencePos.setX(m_wayPoint[m_nKanaBigWC][m_nKanaWayCount].x);
		m_referencePos.setY(m_wayPoint[m_nKanaBigWC][m_nKanaWayCount].y);
		
		if (fabs(EUCLIDEAN_DISTANCE_ROBOTPOSE(m_referencePos, m_currentPos)) < m_nUnitOfWayPoint*3)
			m_nKanaWayCount++;
	}
	// 마지막 way point를 지나 m_nKanaBigWC를 증가
	else
	{	
		// 마지막 노드 도착
		if (m_nKanaBigWC+1 == m_nNumberOfPathNode) {
			kanayama_loop_final();
			if (m_bArrived) {
				m_dV = 0;
				m_dW = 0;
				return RETURN_NONE_ERROR;
			}

			// 최대값 설정
			if (fabs(m_dV) > m_dMaxV) m_dV = m_dMaxV;
			if (fabs(m_dW) > m_dMaxW) m_dW = m_dMaxW;
			
			return RETURN_CONTINUE;
		}
		else {
			m_nKanaBigWC++;
			m_nKanaWayCount = 0;
		}
	}

	// 목적지 Theta를 구함
	double Theta_r = atan2((m_referencePos.getY() - m_currentPos.getY()), (m_referencePos.getX() - m_currentPos.getX()));
	m_referencePos.setThetaRad(Theta_r);
	// 가장 처음에는 시작 노드를 향해 방향만 튼다.
	if (!m_bKanayamaControlFirstInit) {
		kanayama_loop_first();
		if (fabs(m_dW) > m_dMaxW) m_dW = m_dMaxW;
		return RETURN_CONTINUE;
	}

	// Path에 따라 전진 시작
	if (m_nKanaBigWC != m_nNumberOfPathNode)
		kanayama_loop();
	
	// 최대값 설정
	if (fabs(m_dV) > m_dMaxV) m_dV > 0 ? m_dV = m_dMaxV : m_dV = -m_dMaxV;
	if (fabs(m_dW) > m_dMaxW) m_dW > 0 ? m_dW = m_dMaxW : m_dW = -m_dMaxW;

	return RETURN_CONTINUE;
}

// 방향만 트는 loop
void Ckanayama::kanayama_loop_first()
{
	double dErrorTh_rad = m_referencePos.getThetaRad() - m_currentPos.getThetaRad();

	if (fabs(dErrorTh_rad) > PI)
		dErrorTh_rad = dErrorTh_rad - dErrorTh_rad / fabs(dErrorTh_rad) * 2 * PI;

	// 제동거리를 호로 변환하여 각 계산
	if (fabs(dErrorTh_rad) < 5.0 * D2R)
	{
		m_bKanayamaControlFirstInit = true;
		return;
	}
	else
	{
		m_dV = 0.0;
		m_dW = m_dMaxW;
	}
}

// 주행하는 loop
void Ckanayama::kanayama_loop()
{
	double V, W;

	double Theta_c = m_currentPos.getThetaRad();

	///////////// 좌표변환 //////////////
	double X_e = (m_referencePos.getX() - m_currentPos.getX()) * cos(Theta_c) + (m_referencePos.getY() - m_currentPos.getY()) * sin(Theta_c);
	double Y_e = -(m_referencePos.getX() - m_currentPos.getX()) * sin(Theta_c) + (m_referencePos.getY() - m_currentPos.getY()) * cos(Theta_c);
	double Theta_e = m_referencePos.getThetaRad() - m_currentPos.getThetaRad();
	if (Theta_e > PI)
		Theta_e = Theta_e - 2 * PI;
	else if (Theta_e < -PI)
		Theta_e = Theta_e + 2 * PI;
	double v_ref = 5.0;

	if (fabs(Theta_e) < (10.0 * D2R))
	{
		v_ref = 5.0;
		V = v_ref * m_fKx * X_e;
		W = ((m_fKy * Y_e) + (m_fKtheta * sin(Theta_e)));
	}
	else if (fabs(Theta_e) < (20.0 * D2R))
	{
		v_ref = 4.0;
		V = v_ref * m_fKx * X_e;
		W = ((m_fKy * Y_e) + (m_fKtheta * sin(Theta_e))) / 1.0;
	}
	
	else if (fabs(Theta_e) < (50.0 * D2R))
	{
		v_ref = 3.0;
		V = v_ref * m_fKx * X_e;
		W = ((m_fKy * Y_e) + (m_fKtheta * sin(Theta_e))) / 1.0;
	}
	else if (fabs(Theta_e) < (70.0 * D2R))
	{
		v_ref = 2.5;
		V = v_ref * m_fKx * X_e;
		W = ((m_fKy * Y_e) + (m_fKtheta * sin(Theta_e))) / 1.0;
	}
	
	else
	{
		v_ref = 0.0;
		V = v_ref * m_fKx * X_e;
		W = ((m_fKy * Y_e) + (m_fKtheta * sin(Theta_e))) / 1.0;
	}
	/*
	if (fabs(X_e) >= m_nInPosition)
	{
		V = v_ref * m_fKx * X_e / fabs(X_e) * m_nInPosition;
	}
	*/
	m_dV = V;
	m_dW = W;
}

// 도착지에서는 감속하며 도착
void Ckanayama::kanayama_loop_final()
{
	double V, W;
	float K_x, K_y, K_theta;

	double Theta_r = m_referencePos.getThetaRad();
	double Theta_c = m_referencePos.getThetaRad();

	K_x = m_fKx;
	K_y = m_fKy;
	K_theta = m_fKtheta;

	///////////// 좌표변환 //////////////
	double X_e = (m_referencePos.getX() - m_currentPos.getX()) * cos(Theta_c) + (m_referencePos.getY() - m_currentPos.getY()) * sin(Theta_c);
	double Y_e = -(m_referencePos.getX() - m_currentPos.getX()) * sin(Theta_c) + (m_referencePos.getY() - m_currentPos.getY()) * cos(Theta_c);
	double Theta_e = Theta_r - Theta_c;

	if (Theta_e > PI)
		Theta_e = Theta_e - 2.0 * PI;
	else if (Theta_e < -PI)
		Theta_e = Theta_e + 2.0 * PI;

	// 제동거리
	double dBrakingDistance = 50;// (m_dV / m_dDecel)*m_dV / 2.;
	if (fabs(X_e) < dBrakingDistance) 
	{
		m_dV = 0;
		m_dW = 0;
		
		if ((fabs(Theta_e) < 1) )
		{
			m_dV = 0;
			m_dW = 0;
			m_bArrived = true;
		}
		else
		{
			V = 0.0;
			W = 1.0 * sin(Theta_e);
			m_dV = V;
			m_dW = W;
		}
	}
	else
	{
		double v_ref = 5;

		V = v_ref * m_fKx * X_e;
		W = ((m_fKy * Y_e) + (m_fKtheta * sin(Theta_e))) / 1.0;

		m_dV = V;
		m_dW = W;
		
	}
}

int Ckanayama::PathPlanOnFile(string fileName, CPoint curPos)
{
	// 파일에서 주행 노드 가져오기
	if (LoadPathData(fileName))
		return RETURN_FAILED;

	// Way Point 만들어주기
	if (PathPlan(CPoint(curPos.x, curPos.y)) != RETURN_NONE_ERROR)
		return RETURN_FAILED;

	clearJob();

	return RETURN_NONE_ERROR;
}

void Ckanayama::setKanayamaGain(float Kx, float Ky, float Ktheta)
{
	m_fKx = Kx;
	m_fKy = Ky;
	m_fKtheta = Ktheta;
}

void Ckanayama::userDefinedKanayama(RobotPose currentPos, RobotPose referencePos, double Kx, double Ky, double Kt, double * V, double * W)
{
	double Theta_c = currentPos.getThetaRad();
	double X_e = (referencePos.getX() - currentPos.getX()) * cos(Theta_c) + (referencePos.getY() - currentPos.getY()) * sin(Theta_c);
	double Y_e = -(referencePos.getX() - currentPos.getX()) * sin(Theta_c) + (referencePos.getY() - currentPos.getY()) * cos(Theta_c);
	double Theta_e = referencePos.getThetaRad() - currentPos.getThetaRad();
	if (Theta_e > PI)
		Theta_e = Theta_e - 2 * PI;
	else if (Theta_e < -PI)
		Theta_e = Theta_e + 2 * PI;
	double v_ref = 5.0;

	if (fabs(Theta_e) < (10.0 * D2R))
	{
		v_ref = 5.0;
		*V = v_ref * Kx * X_e;
		*W = ((Ky * Y_e) + (Kt * sin(Theta_e)));
	}
	else if (fabs(Theta_e) < (20.0 * D2R))
	{
		v_ref = 4.0;
		*V = v_ref * Kx * X_e;
		*W = ((Ky * Y_e) + (Kt * sin(Theta_e))) / 1.0;
	}

	else if (fabs(Theta_e) < (50.0 * D2R))
	{
		v_ref = 3.0;
		*V = v_ref * Kx * X_e;
		*W = ((Ky * Y_e) + (Kt * sin(Theta_e))) / 1.0;
	}
	else if (fabs(Theta_e) < (70.0 * D2R))
	{
		v_ref = 2.5;
		*V = v_ref * Kx * X_e;
		*W = ((Ky * Y_e) + (Kt * sin(Theta_e))) / 1.0;
	}
	else
	{
		v_ref = 0.0;
		*V = v_ref * Kx * X_e;
		*W = ((Ky * Y_e) + (Kt * sin(Theta_e))) / 1.0;
	}
}