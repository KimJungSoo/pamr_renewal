#pragma once

using namespace std;

#define PI 3.14159265358979323846
#define R2D	(180./PI)
//SG_수정 - warning 20190906
#define D2R	(PI/180.)

inline double DeltaRadLE (double ang1, double ang2)
{
	double da = ang1 - ang2;
	if(-PI < da && da < PI) return da;
	else {
		da = fmod (da, 2*PI);
		if(PI <= da) return da - 2*PI;
		else if(da <= -PI) return da + 2*PI;
		else return da;
	}
	return da;
}

struct sPointLE {
	double theta, rho, weight;
	
	double x, y;
	
	inline double Distance (sPointLE &p2)
	{
		sPointLE &p1 = *this;

		double dx = p1.x - p2.x;
		double dy = p1.y - p2.y;

		return sqrt(dx*dx + dy*dy);
	}
};

struct sLineEx 
{
	double r, alpha;		// 원점에서 선분까지 수선의 길이와 수선의 기울어진 각
	double x1, y1, x2, y2;	// 선분의 시작점과 끝점

	sLineEx () { }
	sLineEx (double r_, double alpha_, double x1_, double y1_, double x2_, double y2_) 
		: r(r_), alpha(alpha_), x1(x1_), y1(y1_), x2(x2_), y2(y2_) { }
};

extern void line_segmentation(vector<sLineEx> &lines_ret, sPointLE *points, int s, int e);

