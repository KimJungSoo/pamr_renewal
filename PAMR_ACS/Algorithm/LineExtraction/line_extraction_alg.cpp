#include "stdafx.h"
#include "line_extraction_alg.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Probabilistic line extraction
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
sPointLE find_nearest_point (double r, double alpha, double x, double y)
{
	// 점 (x, y)로부터 직선 (r, alpha) 상의 가장 가까운 점을 계산하여 리턴한다.
	double sin_alpha = sin(alpha);
	double cos_alpha = cos(alpha);
	double tan_alpha = tan(alpha);

	sPointLE p;
	p.x = r*cos_alpha + x*sin_alpha*sin_alpha - y*sin_alpha*cos_alpha;
	p.y = (x - x)*tan_alpha + y;

	return p;
}

sLineEx calc_r_alpha (sPointLE *points, int s, int e)
{
	double x_bar = 0;
	double y_bar = 0;
	double sigma_weight = 0;
	
	for(int i = s; i<e; i++) {
		x_bar += points[i].weight*points[i].x;
		y_bar += points[i].weight*points[i].y;
		sigma_weight  += points[i].weight;
	}
	x_bar /= sigma_weight;
	y_bar /= sigma_weight;

	double sigma_y_x = 0;
	double sigma_y_x_2 = 0;

	for(int i = s; i<e; i++) {
		double dy = y_bar - points[i].y;
		double dx = x_bar - points[i].x;

		sigma_y_x   += points[i].weight*dy*dx;
		sigma_y_x_2 += points[i].weight*(dy*dy - dx*dx);
	}

	sLineEx l;
	l.alpha = 0.5*atan2(-2*sigma_y_x, sigma_y_x_2);
	l.r = x_bar*cos(l.alpha) + y_bar*sin(l.alpha);

	// r값이 항상 0보다 크도록 만들어 준다.
	if(l.r < 0.) {
		l.r = -l.r;
		l.alpha = DeltaRadLE (l.alpha + PI, 0.);
	}

	return l;
}

void line_extraction(vector<sLineEx> &lines_ret, sPointLE *points, int s, int e)
{	
	sLineEx l = calc_r_alpha (points, s, e);

	sPointLE p1 = find_nearest_point (l.r, l.alpha, points[s].x, points[s].y);
	sPointLE p2 = find_nearest_point (l.r, l.alpha, points[e-1].x, points[e-1].y);

	lines_ret.push_back (sLineEx(l.r, l.alpha, p1.x, p1.y, p2.x, p2.y));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Line segmentation
///////////////////////////////////////////////////////////////////////////////////////////////////////////

double perpendicular_length (sPointLE &p1, sPointLE &p2, double vx, double vy)
{
	// 수선의 길이를 계산한다.

	double px = p2.x - p1.x;
	double py = p2.y - p1.y;

	return fabs(px*vy - py*vx);
}

pair<int, double> find_max_perpendicular_length_point (sPointLE *points, int s, int e)
{
	// 경로의 구간(s ~ e) 내에서 최대 수선의 길이를 가지는 점을 찾는다.

	int max_index = 0;
	double max_dist = 0.;

	sPointLE &p1 = points[s];
	sPointLE &p2 = points[e - 1];

	double vl = p1.Distance(p2);
	double vx = (p2.x - p1.x)/vl;
	double vy = (p2.y - p1.y)/vl;

	for (int i = s; i < e; ++i) {
		double dist = perpendicular_length (p1, points[i], vx, vy);

		if(dist > max_dist) {
			max_index = i;
			max_dist = dist;
		}
	}
#if _MSC_VER < VS2015_MSC_VER
	return make_pair<int, double> (max_index, max_dist);
#else 
	return make_pair(max_index, max_dist);
#endif
}

void line_segmentation(vector<sLineEx> &lines_ret, sPointLE *points, int s, int e)
{	
	//double threshold = 0.05;	// 수선의 길이에 대한 threshold
	//int min_no = 5;				// 선분을 이루는 최소 점의 수
	double threshold = 50.0;	// 수선의 길이에 대한 threshold
	int min_no = 5;				// 선분을 이루는 최소 점의 수
	pair<int, double> max_point = find_max_perpendicular_length_point (points, s, e);

	if(max_point.second > threshold) {
		line_segmentation (lines_ret, points, s, max_point.first + 1);
		line_segmentation (lines_ret, points, max_point.first, e);
	}
	else {
		if(e - s > min_no) {
			line_extraction (lines_ret, points, s, e);
		}
	}
}

